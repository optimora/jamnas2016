console.log('Application started at ' + new Date().toISOString());
console.log();

import Application from './lib/Application';

import routesConfig from './config/routes';
import appConfig from './config/app';

import { mapping as enumStringMap } from './config/enum-string-mapping';

new Application(appConfig, routesConfig, { enumStringMap }).start();
