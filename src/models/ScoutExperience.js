import mongoose from 'mongoose';

var Schema = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId;

export default mongoose.model('ScoutExperience', new Schema({

    scout               : { type: ObjectId, ref: 'Scout' },
    branch              : { type: ObjectId, ref: 'ScoutBranch' },
    subbranch           : { type: ObjectId, ref: 'ScoutBranch' },

    level               : { type: String, enum: ['siaga', 'penggalang', 'penegak', 'pandega', 'pembina'] },
    gudep_number        : { type: String },
    base                : { type: String },
    year                : { type: String },
    achievements        : [ {
                              sub_level         : { type: String, enum: ['mula', 'bantu', 'tata', 'ramu', 'rakit', 'terap', 'garuda'] },
                              year              : { type: String },
                              number_of_badges  : { type: String }
    } ],
    activities          : [ {
                              name  : { type: String },
                              year  : { type: String }
    } ],  
    badges              : [ {
                              category: { type: String },
                              type: { type: ObjectId, ref: 'BadgeType' }
                          } ],

    created             : Date,
    modified            : { type: Date, default: new Date()}

}, { collection: 'scout_experiences' }));
