import mongoose from 'mongoose';

var Schema = mongoose.Schema;
var ObjectId = mongoose.Schema.Types.ObjectId;

var branchTransportSchema = new Schema({

  branch              : { type: ObjectId, ref: 'ScoutBranch' },
  address                 : { type: String },
  province                : { type: ObjectId, ref: 'Location' },
  regency_city            : { type: ObjectId, ref: 'Location' },
  zipcode                 : { type: Number },
  phone                   : { type: String },
  mobile_phone            : { type: String },

  contingent           : {
    branch_leader         : {male:Number, female:Number},
    branch_staff          : {male:Number, female:Number},
    subbranch_leader      : {male:Number, female:Number},
    leader                : {male:Number, female:Number}, //pembina pendamping
    scout_penggalang      : {male:Number, female:Number},
    scout_pbk             : {male:Number, female:Number},
  },

  contingent_total : {male:Number, female:Number},

  departure               : {
    date                  : Date,
    transportation_type   : { type: String, enum: ['airplane', 'train', 'ship', 'bus'] },
    location              : String,
    time                  : String
  },

  arrival             : {
    date                  : Date,
    transportation_type   : { type: String, enum: ['airplane', 'train', 'ship', 'bus'] },
    location              : String,
    time                  : String
  },

  return             : {
    date                  : Date,
    transportation_type   : { type: String, enum: ['airplane', 'train', 'ship', 'bus'] },
    location              : String,
    time                  : String
  },

  file                : { type: ObjectId, ref: 'StoredFile'},
  created             : { type: Date }

}, { collection: 'branch_transports' });

branchTransportSchema.pre('save', function(next) {

  this.contingent_total.male = this.contingent.branch_leader.male + this.contingent.branch_staff.male + this.contingent.subbranch_leader.male + this.contingent.leader.male + this.contingent.scout_penggalang.male + this.contingent.scout_pbk.male;
  this.contingent_total.female = this.contingent.branch_leader.female + this.contingent.branch_staff.female + this.contingent.subbranch_leader.female + this.contingent.leader.female + this.contingent.scout_penggalang.female + this.contingent.scout_pbk.female;

  next();
});

export default mongoose.model('BranchTransport', branchTransportSchema);
