import mongoose from 'mongoose';

var Schema = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId;

export default mongoose.model('BranchRegistration', new Schema({

    branch                  : { type: ObjectId, ref: 'ScoutBranch'},
    address                 : { type: String },
    province                : { type: ObjectId, ref: 'Location' },
    regency_city            : { type: ObjectId, ref: 'Location' },
    zipcode                 : { type: Number },
    phone                   : { type: String },
    mobile_phone            : { type: String },

    participants    : [{
        sub_branch          : { type: ObjectId, ref: 'ScoutBranch' },
        num_male : Number,
        num_female: Number,
        num_male_leader    : Number,
        num_female_leader  : Number
    }],

    special_participants   : {
        tunanetra          : {
            num_male   : Number,
            num_female : Number,
            num_male_leader        : Number,
            num_female_leader      : Number
        },
        tunarungu          : {
            num_male   : Number,
            num_female : Number,
            num_male_leader        : Number,
            num_female_leader      : Number
        },
        tunagrahita        : {
            num_male   : Number,
            num_female : Number,
            num_male_leader        : Number,
            num_female_leader      : Number
        },
        tunadaksa          : {
            num_male   : Number,
            num_female : Number,
            num_male_leader        : Number,
            num_female_leader      : Number
        }
    },

    head_participants: {
        pinkonda                : {num_male:Number, num_female:Number},
        pinkoncab               : {num_male:Number, num_female:Number},

    },

    file                    : { type: ObjectId, ref: 'StoredFile'},

    user                    : { type: ObjectId, ref: 'User'},
    created                 : Date,
    modified                : { type: Date, default: new Date()}

}, { collection: 'branch_registration' }));
