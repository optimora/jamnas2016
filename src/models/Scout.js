import mongoose from 'mongoose';

var Schema = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId;

export default mongoose.model('Scout', new Schema({

    document_status     : { type: String, enum: ['draft','final']},
    name                : { type: String },
    user                : { type: ObjectId, ref: 'User'},
    sex                 : { type: String, enum: ['male', 'female'] },
    birthplace          : { type: String },
    birthday            : { type: Date },
    religion            : { type: String, enum: ['islam', 'kristen_protestan', 'kristen_katolik', 'budha', 'hindu', 'konghucu', 'others'] },
    blood_type          : { type: String, enum: ['A', 'B', 'AB', 'O'] },
    avatar              : { type: ObjectId, ref: 'StoredFile' },
    code                : { type: String },
    contact             : { type: ObjectId, ref: 'Contact'},
    parent_contact      : { type: ObjectId, ref: 'Contact'},
    files               : [ { type: ObjectId, ref: 'StoredFile' } ],
    emergency_contacts  : [ { type: ObjectId, ref: 'Contact'} ],
    emergency_radios    : [
                            {
                              name        : { type: String },
                              call_sign   : { type: String },
                              owner       : { type: String },
                              frequency   : { type: String },
                              local_area  : { type: String }
                            }
                          ],

    nta                 : { type: String, unique: true  }, // Nomor Tanda Anggota
    branch              : { type: ObjectId, ref: 'ScoutBranch'},
    subbranch           : { type: ObjectId, ref: 'ScoutBranch'},
    group               : { type: ObjectId, ref: 'ScoutGroup' },
    type                : { type: String, enum: ['', 'participant', 'leader-cmt', 'leader-dct', 'leader-staff', 'coach', 'committee', 'guest', 'others'] },
    subtype             : { type: String, enum: ['', 'participant-regular', 'participant-special-needs', 'participant-overseas', 'participant-embassy', 'participant-other', 'leader-head-cmt', 'leader-vice-cmt', 'leader-secretary-cmt', 'leader-staff-cmt', 'leader-medical-cmt', 'leader-cultural-cmt', 'leader-exhibition-cmt', 'leader-head-dct', 'leader-secretary-dct', 'leader-staff-dct', 'committee-organizer', 'committee-executor', 'committee-support'] },

    // Special fields for participant.
    participant_role    : { type: String, enum: ['', 'leader', 'co-leader', 'member']},

    // Special fields for Committee.
    committee_role            : { type: String },
    committee_role_detail     : { type: String },
    committee_role_extra     : { type: String },

    educations          : [
                            {
                              level           : { type: String, enum: ['SD', 'SMP', 'SMA', 'S1', 'S2', 'S3'] },
                              school_name     : { type: String },
                              city            : { type: String },
                              graduate_year   : { type: String },
                              notes           : { type: String }
                            }
                          ],

    experiences         : [ {type: ObjectId, ref: 'ScoutExperience'} ],

    courses             : [
                            {
                              name            : { type: String, enum: ['KMD', 'KML', 'KPD', 'KPL'] },
                              year            : { type: String },
                              province        : { type: ObjectId, ref: 'Location' },
                              regency_city    : { type: ObjectId, ref: 'Location' },
                              location        : { type: String },
                              notes           : { type: String }
                            }
                          ],

    external_courses    : [
                            {
                              name            : { type: String },
                              year            : { type: String },
                              province        : { type: ObjectId, ref: 'Location' },
                              regency_city    : { type: ObjectId, ref: 'Location' },
                              location        : { type: String },
                              notes           : { type: String }
                            }
                          ],

    medical_history     : [
                            {
                              disease    : { type: String },
                              medicine   : { type: String },
                              treatment  : { type: String }
                            }
                          ],

    documents           : [
                            {
                              description   : { type: String },
                              notes         : { type: String },
                              type          : { type: String },
                              stored_file   : { type: ObjectId, ref: 'StoredFile' }
                            }
                          ],

    created             : Date,
    modified            : { type: Date, default: new Date()}

}, { collection: 'scouts' }));
