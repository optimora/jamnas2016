import mongoose from 'mongoose';

var Schema = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId;

var scoutBranchSchema = new Schema({

  code			: { type: String },
  name			: { type: String },
  logo      : { type: String },

  camp_name : { type: String },
  camp_code : { type: String },
  camp_subcode  : { type: String },

  contact       : { type: ObjectId, ref: 'Contact' },
  parent        : { type: ObjectId, ref : 'ScoutBranch' },

  created       : { type: Date },
  modified      : { type: Date, default: new Date()},

  // Cache
  // TODO: Move to Redis for better performance.

  stats: {
    numberOfScouts: { type: Number, default: 0 }
  }

}, { collection: 'scout_branches' });

scoutBranchSchema.methods = {

  getSanitizedObject: function() {
    let returnedObject = this.toObject();

    delete returnedObject.__v;

    return returnedObject;
  }

};

export default mongoose.model('ScoutBranch', scoutBranchSchema);