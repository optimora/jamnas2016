import mongoose from 'mongoose';

var Schema = mongoose.Schema;
var ObjectId = mongoose.Schema.Types.ObjectId;

let contactSchema = new Schema({

  scout           : { type: Schema.ObjectId, ref: 'Scout' },
  branch          : { type: Schema.ObjectId, ref: 'ScoutBranch' },
  subbranch       : { type: Schema.ObjectId, ref: 'ScoutBranch' },

  name            : { type: String },
  occupation      : { type: String },
  address         : { type: String },
  relation        : { type: String },
  province        : { type: ObjectId, ref: 'Location' },
  regency_city    : { type: ObjectId, ref: 'Location' },
  zipcode         : { type: String },
  country         : { type: ObjectId, ref: 'Country' },
  phone           : {
    home: { type: String },
    mobile: { type: String }
  },
  type            : { type: String, enum: ['scout', 'scout-parent', 'scout-relation', 'leader', 'committee', 'others', 'branch-office'] },
  category        : { type: String, enum: ['normal-contact', 'emergency-contact'] },
  created         : { type: Date },
  modified        : { type: Date }

}, { collection: 'contacts' });

contactSchema.statics.composeNewDocument = (rawData) => {
  let rawContact = {};
  const isDefNotNullAndNotEmpty = (val) => {
    return val !== '' && val != null && val != undefined;
  };
  Object.keys(rawData).forEach((field) => {
    switch (field) {
      case '_id':
      case '__v':
        // Ignore.
        break;
      case 'type':
      case 'category':
        if (isDefNotNullAndNotEmpty(rawData[field])) {
          rawContact[field] = rawData[field];
        }
        break;
      case 'branch':
      case 'country':
      case 'province':
      case 'regency_city':
      case 'scout':
      case 'subbranch':
        if (isDefNotNullAndNotEmpty(rawData[field])) {
          rawContact[field] = mongoose.Types.ObjectId(rawData[field]);
        }
        break;
      default:
        rawContact[field] = rawData[field];
        break;
    }
  });
  return rawContact;
};

export default mongoose.model('Contact', contactSchema);
