import mongoose from 'mongoose';

var Schema = mongoose.Schema;
var ObjectId = mongoose.Schema.Types.ObjectId;

let fileModel = mongoose.model('File', new Schema({

    scout           : { type: Schema.ObjectId, ref: 'Scout' },
    branch          : { type: Schema.ObjectId, ref: 'ScoutBranch' },

    type            : { type: String, enum: ['kta', 'medical-letter', 'parent-approval', 'school-letter', 'kwarcab-willingness-letter', 'kwarcab-registration-letter', 'kwarda-willingness-letter', 'kwarda-registration-letter', 'arrival-letter', 'performance-letter', 'permit-enter-exit-area', 'other', 'insurance-letter', 'receipt'] },
    name            : { type: String },
    url             : { type: String },
    notes           : { type: String },

    created         : { type: Date },
    modified        : { type: Date}

}, { collection: 'files' }));

export default fileModel;
