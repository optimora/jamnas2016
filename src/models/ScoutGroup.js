import mongoose from 'mongoose';

var Schema = mongoose.Schema;
var ObjectId = mongoose.Schema.Types.ObjectId;

export default mongoose.model('ScoutGroup', new Schema({

    name        : { type: String },

    leader      : { type: ObjectId, ref: 'Scout' },
    co_leader   : { type: ObjectId, ref: 'Scout' },

    branch      : { type: ObjectId, ref: 'ScoutBranch' },
    subbranch      : { type: ObjectId, ref: 'ScoutBranch' },

    created     : { type: Date },
    modified    : { type: Date }

}, { collection: 'scout_groups' }));
