import mongoose from 'mongoose';

var Schema = mongoose.Schema;
var ObjectId = mongoose.Schema.Types.ObjectId;

export default mongoose.model('ScoutCamp', new Schema({

  name			: { type: String },
  code			: { type: String },
  sub_code  : { type: String },
  coordinate: {
    latitude  : Number,
    longitude : Number
  },
  color     : { type: String },

  created       : { type: Date },
  modified      : { type: Date, default: new Date()}


}, { collection: 'scout_camps' }));
