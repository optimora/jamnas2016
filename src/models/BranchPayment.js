import mongoose from 'mongoose';

var Schema = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId;

export default mongoose.model('BranchPayment', new Schema({

    branch                  : { type: ObjectId, ref: 'ScoutBranch'},
    method                  : { type: String, enum: ['atm', 'setor-tunai', 'internet-banking', 'sms-banking'] },
    amount                  : Number,
    paid                    : { type: Date, default: new Date()},

    account_name            : String,
    account_number          : String,

    file                    : { type: ObjectId, ref: 'StoredFile'},

    user                    : { type: ObjectId, ref: 'User'},
    created                 : Date,
    modified                : { type: Date, default: new Date()}

}, { collection: 'branch_payment' }));
