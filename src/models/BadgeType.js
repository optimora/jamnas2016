import mongoose from 'mongoose';

var Schema = mongoose.Schema;
var ObjectId = mongoose.Schema.Types.ObjectId;

export default mongoose.model('BadgeType', new Schema({

    name            : { type: String },
    unique_name     : { type: String },
    grade           : { type: String, enum:['siaga', 'penggalang', 'penegak', 'pandega', 'pembina'] }

}, { collection: 'badge_types' }));
