import crypto from 'crypto';
import mongoose from 'mongoose';

var Schema = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId;
var Mixed = Schema.Types.Mixed;

var userSchema = new Schema({

  provider        : { type: String, default: 'local' },

  name            : { type: String },
  email           : { type: String },
  role            : { type: String, enum: ['scout-branch-admin', 'overseas-admin', 'embassy-admin', 'committee-admin', 'admin', 'super-admin'] },
  magic_role      : { type: String },

  hashed_password : { type: String },
  salt            : { type: String },

  branch          : { type: Schema.ObjectId, ref: 'ScoutBranch' },
  scout           : { type: Schema.ObjectId, ref: 'Scout' },

  created         : Date,
  modified        : { type: Date, default: new Date()},
  last_login      : Date,

  // Cache
  // TODO: Move to Redis for better performance.

  stats: {
    numberOfUsers: { type: Number, default: 0 }
  }

}, { collection: 'users' });

userSchema
  .virtual('password')
  .set(function(password) {
    this._password = password;
    this.salt = this.makeSalt();
    this.hashed_password = this.encryptPassword(password);
  })
  .get(function() { return this._password });

userSchema.methods = {
  authenticate: function(plainText) { return this.encryptPassword(plainText) === this.hashed_password },

  makeSalt: () => Math.round((new Date().valueOf() * Math.random())) + '',

  encryptPassword: function(password) {
    if (!password) return '';
    return crypto.createHmac('sha1', this.salt).update(password).digest('hex');
  },

  validPassword: function(password) {
  	if (!password) return false;
  	return this.encryptPassword(password) == this.hashed_password;
  },

  getSanitizedObject: function() {
    let sanitizedObject = this.toObject();

    delete sanitizedObject.hashed_password;
    delete sanitizedObject.salt;

    return sanitizedObject;
  }
};

export default mongoose.model('User', userSchema);
