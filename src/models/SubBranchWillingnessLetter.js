import mongoose from 'mongoose';

var Schema = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId;

export default mongoose.model('SubBranchWillingnessLetter', new Schema({

    branch              : { type: ObjectId, ref: 'ScoutBranch'},
    subbranch           : { type: ObjectId, ref: 'ScoutBranch'},
    address             : { type: String },
    zipcode             : { type: Number },
    phone               : { type: String },
    mobile_phone        : { type: String },

    number_of           : {
      penggalang_pa     : Number,
      penggalang_pi     : Number,
      pbk_pa            : Number,
      pbk_pi            : Number,
      jumlah_peserta    : Number,
      bindamping_pa     : Number,
      bindamping_pi     : Number,
      pinkonda          : Number,
      pinkoncab         : Number,
      jumlah_pimpinan   : Number
    },

    sender_name         : { type: String },
    file                : { type: ObjectId, ref: 'StoredFile'},

    user                : { type: ObjectId, ref: 'User'},
    created             : Date,
    modified            : { type: Date, default: new Date()}

}, { collection: 'sub_branchwillingness_letters' }));
