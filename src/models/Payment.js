import mongoose from 'mongoose';

var Schema = mongoose.Schema;
var ObjectId = mongoose.Types.ObjectId;

export default mongoose.model('Payment', new Schema({

    branch              : { type: ObjectId, ref: 'ScoutBranch' },
    method              : { type: String, enum:['atm','setor-tunai','internet-banking','sms-banking'] },
    amount              : { type: String },
    payment_date        : { type: Date },
    account_name        : { type: String },
    account_number      : { type: String },
    receipt             : { type: Objectid, ref: 'File'},

    user                : { type: ObjectId, ref: 'User'},
    created             : Date,
    modified            : { type: Date, default: new Date()}

}, { collection: 'payments' }));
