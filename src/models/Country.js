import mongoose from 'mongoose';

var Schema = mongoose.Schema;
var ObjectId = mongoose.Schema.Types.ObjectId;

export default mongoose.model('Country', new Schema({

    name    : { type: String },
    code    : { type: String }

}, { collection: 'countries' }));
