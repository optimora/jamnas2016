import mongoose from 'mongoose';

var Schema = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId;
var Mixed = Schema.Types.Mixed;

export default mongoose.model('StoredFile', new Schema({

  topic         : { type: String, enum: [ 'avatar', 'scout-documents', 'branch-documents', 'subbranch-documents', 'miscellaneous' ] },
  category      : { type: String },
  extension     : { type: String },
  notes         : { type: String },

  raw_data      : { type: Mixed },

  created       : { type: Date },
  modified      : { type: Date },

  user          : { type: ObjectId, ref: 'User' },
  branch        : { type: ObjectId, ref: 'Branch' },
  subbranch     : { type: ObjectId, ref: 'Branch' },
  scout         : { type: ObjectId, ref: 'Scout' },

  // TODO: Update this field every time there's a model that refer to this file. Otherwise, eventually
  // TODO: this will be deleted by a scheduled scripts.
  active        : { type: Boolean, default: false }

}, { collection: 'stored_files' }));
