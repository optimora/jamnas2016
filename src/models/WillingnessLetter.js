import mongoose from 'mongoose';

var Schema = mongoose.Schema;
var ObjectId = mongoose.Schema.Types.ObjectId;

var willingnessLetterSchema = new Schema({

    branch              : { type: ObjectId, ref: 'ScoutBranch'},
    subbranch           : { type: ObjectId, ref: 'ScoutBranch'},
    address             : { type: String },
    zipcode             : { type: Number },
    phone               : { type: String },
    mobile_phone        : { type: String },

    number_of           : {
        penggalang_pa     : Number,
        penggalang_pi     : Number,
        pbk_pa            : Number,
        pbk_pi            : Number,
        jumlah_peserta    : Number,
        bindamping_pa     : Number,
        bindamping_pi     : Number,
        pinkonda          : Number,
        pinkoncab         : Number,
        jumlah_pimpinan   : Number
    },

    sender_name         : { type: String },
    file                : { type: ObjectId, ref: 'File'},

    user                : { type: ObjectId, ref: 'User'},
    created             : Date,
    modified            : { type: Date, default: new Date()}

}, { collection: 'willingness_letters' });

willingnessLetterSchema.pre('save', function(next) {
    this.number_of.jumlah_peserta = this.number_of.penggalang_pa + this.number_of.penggalang_pi + this.number_of.pbk_pa + this.number_of.pbk_pi;
    this.number_of.jumlah_pimpinan = this.number_of.bindamping_pa + this.number_of.bindamping_pi + this.number_of.pinkonda + this.number_of.pinkoncab;

    next();
});

export default mongoose.model('WillingnessLetter', willingnessLetterSchema);


