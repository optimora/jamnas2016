import mongoose from 'mongoose';

var Schema = mongoose.Schema;
var ObjectId = mongoose.Schema.Types.ObjectId;

export default mongoose.model('ScoutLevel', new Schema({

    name          : { type: String },
    grade         : { type: String, enum: ['siaga', 'penggalang', 'penegak', 'pandega', 'pembina'] },

    parentLevel   : { type: ObjectId, ref : 'ScoutLevel' }

}, { collection: 'scout_levels' }));
