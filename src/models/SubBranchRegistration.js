import mongoose from 'mongoose';

var Schema = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId;

export default mongoose.model('SubBranchRegistration', new Schema({

    branch                  : { type: ObjectId, ref: 'ScoutBranch'},
    subbranch               : { type: ObjectId, ref: 'ScoutBranch'},
    address                 : { type: String },
    province                : { type: ObjectId, ref: 'Location'},
    regency_city            : { type: ObjectId, ref: 'Location'},
    zipcode                 : { type: Number },
    phone                   : { type: String },
    mobile_phone            : { type: String },

    participants : [{
        leader_group        : {
            name            :String,
            sku_level       : { type: String, enum: ['ramu', 'rakit', 'terap', 'garuda'] }
        },
        vice_leader_group   : {
            name            :String,
            sku_level       : { type: String, enum: ['ramu', 'rakit', 'terap', 'garuda'] }
        },
        members : [{
            name            :String,
            sku_level       : { type: String, enum: ['ramu', 'rakit', 'terap', 'garuda'] }
        }]
    }],

    sender_name             : { type: String },
    file                    : { type: ObjectId, ref: 'StoredFile'},

    user                    : { type: ObjectId, ref: 'User'},
    created                 : Date,
    modified                : { type: Date, default: new Date()}

}, { collection: 'subbranch_registration' }));
