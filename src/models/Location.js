import mongoose from 'mongoose';

var Schema = mongoose.Schema;
var ObjectId = mongoose.Schema.Types.ObjectId;

export default mongoose.model('Location', new Schema({

    name            : { type: String },
    country         : { type: Schema.ObjectId, ref : 'Country'},
    coordinate      : {
      latitude    : Number,
      longitude   : Number
    },
    parentLocation          : { type: Schema.ObjectId, ref : 'Location' }

}, { collection: 'locations' }));
