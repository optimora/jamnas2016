export const USER_MENUS = [
  {
    url: '/users/dashboard',
    label: 'Dashboard',
    icon: 'dashboard',
    roles: []
  },
  {
    url: '/users/add',
    label: 'Pengguna',
    icon: 'user',
    roles: ['admin', 'super-admin']
  },
  {
    url: '/scout_committee',
    label: 'Daftar Panitia',
    icon: 'users',
    roles: ['admin', 'super-admin', 'committee-admin']
  },
  {
    url: '/scout_committee/add',
    label: 'Tambah Panitia',
    icon: 'users',
    roles: ['admin', 'super-admin', 'committee-admin']
  },
  {
    url: '/willingness_letters',
    label: 'Surat Kesediaan',
    icon: '',
    roles: ['admin', 'super-admin', 'committee-admin']
  },
  {
    url: '/payment_confirmations',
    label: 'Konfirmasi Pembayaran',
    icon: '',
    roles: ['admin', 'super-admin', 'committee-admin']
  },
  {
    url: '/transportation',
    label: 'Rencana Perjalanan',
    icon: '',
    roles: ['admin', 'super-admin', 'committee-admin']
  },
  {
    url: '/branch/willingness_letter',
    label: 'D.01 Kesediaan Kwarda',
    icon: '',
    roles: ['scout-branch-admin', 'overseas-admin', 'embassy-admin']
  },
  {
    url: '/branch/registration',
    label: 'D.02 Pendaftaran Kwarda',
    icon: '',
    roles: ['scout-branch-admin', 'overseas-admin', 'embassy-admin']
  },
  {
    url: '/branch/transport/add',
    label: 'D.07 Rencana Perjalanan',
    icon: '',
    roles: ['scout-branch-admin', 'overseas-admin', 'embassy-admin']
  },
  {
    url   : '/branch/payment/add',
    label : 'Konfirmasi Pembayaran',
    icon  : 'upload',
    roles : ['scout-branch-admin', 'overseas-admin', 'embassy-admin']
  },
  {
    url   : 'https://drive.google.com/file/d/0B3T3U6IcEecnUk1EaXJEOVlSRGs/view',
    label : 'Petunjuk Penggunaan',
    icon  : 'download',
    roles : []
  }
];

export const SITE_MENUS = [
  {
    url: '/',
    label: 'Beranda',
    icon: ''
  }
];
