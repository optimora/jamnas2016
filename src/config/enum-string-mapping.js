export const mapping = {
  // Scout Type
  'participant': 'Peserta',
  'leader-cmt': 'Pinkonda',
  'leader-dct': 'Pinkoncab',
  'leader-staff': 'Staf Konda',
  'coach': 'Bindamping',
  'committee': 'Panitia',
  'guest': 'Tamu',
  'others': 'Lainnya',

  // Scout Subtype
  'participant-regular': 'Reguler',
  'participant-special-needs': 'Pramuka Berkebutuhan Khusus (PBK)',
  'participant-overseas': 'Pramuka Luar Negeri',
  'participant-embassy': 'Pramuka KBRI',
  'participant-other': 'Lainnya',
  'leader-head-cmt': 'Ketua Pinkonda',
  'leader-vice-cmt': 'Wakil Pinkonda',
  'leader-secretary-cmt': 'Sekretaris Pinkonda ',
  'leader-staff-cmt': 'Anggota Pinkonda',
  'leader-medical-cmt': 'Tim Medis',
  'leader-cultural-cmt': 'Tim Kegiatan Budaya',
  'leader-exhibition-cmt': 'Tim Pameran',
  'leader-head-dct': 'Ketua Pinkoncab',
  'leader-secretary-dct': 'Sekretaris Pinkoncab',
  'leader-staff-dct': 'Anggota Pinkoncab',
  'committee-organizer': 'Panitia Pengarah',
  'committee-executor': 'Panitia Pelaksana',
  'committee-support': 'Panitia Pendukung',

  // Participant Role
  'leader': 'Pemimpin Regu',
  'co-leader': 'Wakil Pemimpin Regu',
  'member': 'Anggota Regu',

  // Committee Role General
  'committee-role-president': 'Ketua',
  'committee-role-vice-president': 'Wakil Ketua',
  'committee-role-vice-president1': 'Wakil Ketua I',
  'committee-role-vice-president2': 'Wakil Ketua II',
  'committee-role-vice-president3': 'Wakil Ketua III',
  'committee-role-secretary': 'Sekretaris',
  'committee-role-vice-secretary': 'Wakil Sekretaris',
  'committee-role-secretariat': 'Sekretariat',
  'committee-role-treasury': 'Bendahara',
  'committee-role-member': 'Anggota',

  // Committee Role Division
  'committee-role-activity-division': 'Bidang Kegiatan',
  'committee-role-logistic-division': 'Bidang Logistik',
  'committee-role-public-service-division': 'Bidang Pelayanan Umum',
  'committee-role-security-division': 'Bidang Keamanan',
  'committee-role-pr-and-oversea-division': 'Bidang Hubungan Masyarakat dan Hubungan Luar Negeri',
  'committee-role-fundraising-division': 'Tim Usaha Dana',
  'committee-role-waslitev-division': 'Tim Waslitev',
  'committee-role-risk-management-division': 'Tim Manajemen Resiko',
  'committee-role-campsite-division': 'Bidang Perkemahan',

  // Committee Role Section
  'committee-section-scout-skills-and-interest': 'Seksi Giat Keterampilan Kepramukaan dan Peminatan',
  'committee-section-water': 'Seksi Giat Air',
  'committee-section-gdv': 'Seksi Giat GDV',
  'committee-section-technology-and-cultural-art': 'Seksi Giat Teknologi dan Seni Budaya',
  'committee-section-community-service-and-tour': 'Seksi Giat Community Service dan Tour',
  'committee-section-knowledge': 'Seksi Giat Wawasan',
  'committee-section-adventure': 'Seksi Giat Petualangan',
  'committee-section-special': 'Seksi Giat Khusus',
  'committee-section-adult-member': 'Seksi Giat Anggota Dewasa',
  'committee-section-protocol-and-ceremony': 'Seksi Giat Protokol dan Upacara',
  'committee-section-facilities': 'Seksi Sarana dan Prasarana',
  'committee-section-accommodation': 'Seksi Akomodasi',
  'committee-section-consumption': 'Seksi Konsumsi',
  'committee-section-equipment': 'Seksi Perlengkapan',
  'committee-section-health': 'Seksi Kesehatan',
  'committee-section-transportation': 'Seksi Transportasi',
  'committee-section-cleanliness': 'Seksi Kebersihan ',
  'committee-section-publication': 'Seksi Publikasi',
  'committee-section-media-center': 'Seksi Media Center',
  'committee-section-documentation': 'Seksi Dokumentasi',
  'committee-section-international-relations': 'Seksi Hubungan Luar Negeri',
  'committee-section-market': 'Seksi Pasar',
  'committee-section-store': 'Seksi Kedai',
  'committee-section-sponsorship': 'Seksi Sponsorship',
  'committee-section-central-campsite': 'Perkemahan Induk - Perkemahan Bhinneka Tunggal Ika',
  'committee-section-men-campsite': 'Perkemahan Putera - Perkemahan Mashudi',
  'committee-section-women-campsite': 'Perkemahan Puteri - Perkemahan Tien Soeharto',

  // Committee Role Extras
  'committee-section-campsite-master': 'Pembina Perkemahan',
  'committee-section-campsite-vice-master': 'Wakil Pembina Perkemahan',
  'committee-section-campsite-division-activity': 'Wakil Binkemin/Bidang Giat',
  'committee-section-campsite-public-activity': 'Wakil Binkemin/Bidang Umum',
  'committee-section-campsite-sponsorship': 'Wakil Binkemin/Bidang Usaha Dana',
  'committee-section-campsite-staff': 'Staf Binkemin',
  'committee-section-campsite-master-tjilik-riwut': 'Pembina Sub Perkemahan Putera I Tjilik Riwut',
  'committee-section-campsite-master-imam-bonjol' : 'Pembina Sub Perkemahan Putera II Imam Bonjol',
  'committee-section-campsite-master-hasanudin' : 'Pembina Sub Perkemahan Putera III Hasanudin',
  'committee-section-campsite-master-p-diponegoro' : 'Pembina Sub Perkemahan Putera IV P. Diponegoro',
  'committee-section-campsite-staff-men' : 'Staf Perkemahan Putera',
  'committee-section-campsite-master-tjut-nya-dien' : 'Pembina Sub Perkemahan Putri I Tjut Nya Dien',
  'committee-section-campsite-master-ra-kartini' : 'Pembina Sub Perkemahan Putri II RA Kartini',
  'committee-section-campsite-master-kristina-m-tiahahu' : 'Pembina Sub Perkemahan Putri III Kristina M Tiahahu',
  'committee-section-campsite-master-maria-w-maramis' : 'Pembina Sub Perkemahan Putri IV Maria W. Maramis',
  'committee-section-campsite-staff-women' : 'Staf Perkemahan Puteri',


  // Payment Method
  'atm': 'ATM Transfer',
  'setor-tunai': 'Setor Tunai',
  'internet-banking': 'Internet Banking',
  'sms-banking': 'SMS Banking'

};

export function getDisplayName(key) {
  return mapping[key] && mapping[key] !== '' ? mapping[key] : ' ? ';
}
