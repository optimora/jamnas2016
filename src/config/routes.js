// Constants
import { GET, POST, PUT, DELETE } from '../lib/constants/HttpMethod';
import { JSON_API, JADE_PAGE } from '../lib/constants/ResponseType';

// Controllers
/// Page Controllers
import AdministrationController from '../controllers/pages/AdministrationController';
import ScoutsController from '../controllers/pages/ScoutsController';
import ScoutCommitteeController from '../controllers/pages/ScoutCommitteeController';
import ScoutBranchesController from '../controllers/pages/ScoutBranchesController';
import SiteController from '../controllers/pages/SiteController';
import StoredFilesController from '../controllers/pages/StoredFilesController';
import UsersController from '../controllers/pages/UsersController';

/// API Controllers
import SiteApiController from '../controllers/api-endpoints/SiteApiController';
import UsersApiController from '../controllers/api-endpoints/UsersApiController';
import ScoutsApiController from '../controllers/api-endpoints/ScoutsApiController';
import ScoutBranchesApiController from '../controllers/api-endpoints/ScoutBranchesApiController';
import ScoutGroupsApiController from '../controllers/api-endpoints/ScoutGroupsApiController';
import BranchTransportsApiController from '../controllers/api-endpoints/BranchTransportsApiController';
import LocationsApiController from '../controllers/api-endpoints/LocationsApiController';
import SubBranchWillingnessLetterApiController from '../controllers/api-endpoints/SubBranchWillingnessLetterApiController';
import SubBranchRegistrationApiController from '../controllers/api-endpoints/SubBranchRegistrationApiController';
import WillingnessLetterApiController from '../controllers/api-endpoints/WillingnessLetterApiController';
import BranchRegistrationsApiController from '../controllers/api-endpoints/BranchRegistrationsApiController';
import BranchPaymentsApiController from '../controllers/api-endpoints/BranchPaymentsApiController';
import ContactsApiController from '../controllers/api-endpoints/ContactsApiController';
import BadgeTypesApiController from '../controllers/api-endpoints/BadgeTypesApiController';
import StoredFilesApiController from '../controllers/api-endpoints/StoredFilesApiController';

// Filters
import AuthFilter from '../lib/filters/AuthFilter';
import UploadFileFilter from '../lib/filters/UploadFileFilter';
import UploadFileSingleFilter from '../lib/filters/UploadFileSingleFilter';

export default {
  // Pages
  HOME                      : { method: GET, responseType: JADE_PAGE, path: '/', controller: SiteController, handler: 'index', filters: [] },
  USERS_LOGIN               : { method: GET, responseType: JADE_PAGE, path: '/users/login', controller: UsersController, handler: 'login', filters: [] },
  USERS_LOGOUT              : { method: GET, responseType: JADE_PAGE, path: '/users/logout', controller: UsersController, handler: 'logout', filters: [] },
  USERS_DASHBOARD           : { method: GET, responseType: JADE_PAGE, path: '/users/dashboard', controller: UsersController, handler: 'dashboard', filters: [ AuthFilter ] },
  USERS_ADD                 : { method: GET, responseType: JADE_PAGE, path: '/users/add', controller: UsersController, handler: 'add', filters: [ AuthFilter ] },
  USERS_UPLOAD_FILE_DISPATCHER : { method: POST, responseType: JADE_PAGE, path: '/users/upload', controller: UsersController, handler: 'uploadFileWithTopic', filters: [ AuthFilter, UploadFileFilter ] },
  UPLOAD_TEST               : { method: GET, responseType: JADE_PAGE, path: '/upload-test', controller: UsersController, handler: 'uploadTest', filters: [ AuthFilter ] },

  // C01
  SUBBRANCH_WILLINGNESS       : { method: GET, responseType: JADE_PAGE, path: '/subbranch/:subbranch_id/willingness_letter/add', controller: UsersController, handler:'subBranchWillingnessLetter', filters: [ AuthFilter ] },
  SUBBRANCH_WILLINGNESS_VIEW  : { method: GET, responseType: JADE_PAGE, path: '/subbranch/:subbranch_id/willingness_letter', controller: UsersController, handler: 'subBranchWillingnessLetterView', filters: [ AuthFilter ] },
  SUBBRANCH_WILLINGNESS_VIEW2 : { method: GET, responseType: JADE_PAGE, path: '/subbranch/:subbranch_id/willingness_letter', controller: UsersController, handler: 'subbranchWillingnessLetterView', filters: [ AuthFilter ] },
  SUBBRANCH_WILLINGNESS_UPDATE: { method: GET, responseType: JADE_PAGE, path: '/subbranch/:subbranch_id/willingness_letter/:willlId', controller: UsersController, handler:'subBranchWillingnessLetterUpdate', filters: [ AuthFilter ] },

  // C02
  SUBBRANCH_REGISTRATION      : { method: GET, responseType: JADE_PAGE, path: '/subbranch/:subbranch_id/registration/add', controller: UsersController, handler: 'subBranchRegistration', filters: [ AuthFilter ] },
  SUBBRANCH_REGISTRATION_VIEW : { method: GET, responseType: JADE_PAGE, path: '/subbranch/:subbranch_id/registration', controller: UsersController, handler: 'subBranchRegistrationView', filters: [ AuthFilter ] },
  SUBBRANCH_REGISTRATION_VIEW2: { method: GET, responseType: JADE_PAGE, path: '/subbranch/:subbranch_id/registration', controller: UsersController, handler: 'subBranchRegistrationView', filters: [ AuthFilter ] },
  SUBBRANCH_REGISTRATION_UPDATE: { method: GET, responseType: JADE_PAGE, path: '/subbranch/:subbranch_id/registration/:registrationId', controller: UsersController, handler: 'subBranchRegistrationUpdate', filters: [ AuthFilter ] },

  // D01
  BRANCH_WILLINGNESS        : { method: GET, responseType: JADE_PAGE, path: '/branch/willingness_letter/add', controller: UsersController, handler: 'branchWillingnessLetter', filters: [ AuthFilter ] },
  BRANCH_WILLINGNESS_VIEW   : { method: GET, responseType: JADE_PAGE, path: '/branch/willingness_letter', controller: UsersController, handler: 'branchWillingnessLetterView', filters: [ AuthFilter ] },
  BRANCH_WILLINGNESS_VIEW2  : { method: GET, responseType: JADE_PAGE, path: '/branch/:branch_id/willingness_letter', controller: UsersController, handler: 'branchWillingnessLetterView', filters: [ AuthFilter ] },

  // D02
  BRANCH_REGISTRATION       : { method: GET, responseType: JADE_PAGE, path: '/branch/registration/add', controller: UsersController, handler: 'branchRegistration', filters: [] },
  BRANCH_REGISTRATION_VIEW  : { method: GET, responseType: JADE_PAGE, path: '/branch/registration', controller: UsersController, handler: 'branchRegistrationView', filters: [] },
  BRANCH_REGISTRATION_VIEW2 : { method: GET, responseType: JADE_PAGE, path: '/branch/:branch_id/registration', controller: UsersController, handler: 'branchRegistrationView', filters: [] },
  BRANCH_REGISTRATION_UPDATE  : { method: GET, responseType: JADE_PAGE, path: '/branch/registration/:registrationId', controller: UsersController, handler: 'branchRegistrationUpdate', filters: [] },

  // D07
  BRANCH_TRANSPORT          : { method: GET, responseType: JADE_PAGE, path: '/branch/transport/add', controller: UsersController, handler: 'branchTransport', filters: [] },
  BRANCH_TRANSPORT_VIEW     : { method: GET, responseType: JADE_PAGE, path: '/branch/transport/:transport_id', controller: UsersController, handler: 'branchTransportView', filters: [] },
  BRANCH_TRANSPORT_VIEW2    : { method: GET, responseType: JADE_PAGE, path: '/branch/:branch_id/transport', controller: UsersController, handler: 'branchTransportView', filters: [] },
  BRANCH_TRANSPORT_UPDATE   : { method: GET, responseType: JADE_PAGE, path: '/branch/:branch_id/transport/:transport_id', controller: UsersController, handler: 'branchTransportUpdate', filters: [] },

  // PAYMENT
  BRANCH_PAYMENT            : { method: GET, responseType: JADE_PAGE, path: '/branch/payment/add', controller: UsersController, handler: 'branchPayment', filters: [] },
  BRANCH_PAYMENT_VIEW       : { method: GET, responseType: JADE_PAGE, path: '/branch/payment/:payment_id', controller: UsersController, handler: 'branchPaymentView', filters:[] },
  BRANCH_PAYMENT_VIEW2      : { method: GET, responseType: JADE_PAGE, path: '/branch/:branch_id/payment', controller: UsersController, handler: 'branchPaymentView', filters:[] },
  BRANCH_PAYMENT_UPDATE     : { method: GET, responseType: JADE_PAGE, path: '/branch/:branch_id/payment/:payment_id', controller: UsersController, handler: 'branchPaymentUpdate', filters: [] },

  PARTICIPANTS_LIST         : { method: GET, responseType: JADE_PAGE, path: '/participants', controller: UsersController, handler: 'participantIndex', filters: [] },
  PARTICIPANTS_ADD          : { method: GET, responseType: JADE_PAGE, path: '/participants/branches-list', controller: UsersController, handler: 'addNewParticipant', filters: [] },
  PARTICIPANTS_VIEW         : { method: GET, responseType: JADE_PAGE, path: '/participants/:id/view', controller: UsersController, handler: 'participantView', filters: [] },
  BRANCH_PARTICIPANTS_ADD   : { method: GET, responseType: JADE_PAGE, path: '/participants/branch/:id/add', controller: UsersController, handler: 'addNewBranchParticipant', filters: [] },

  COMMITTEES_ADD            : { method: GET, responseType: JADE_PAGE, path: '/scout_committee/add', controller: ScoutCommitteeController, handler: 'add', filters: [] },
  COMMITTEES_LIST           : { method: GET, responseType: JADE_PAGE, path: '/scout_committee', controller: ScoutCommitteeController, handler: 'index', filters: [] },
  COMMITTEES_VIEW           : { method: GET, responseType: JADE_PAGE, path: '/scout_committee/:id', controller: ScoutCommitteeController, handler: 'view', filters: [] },

  SCOUTS_INDEX              : { method: GET, responseType: JADE_PAGE, path: '/scouts/', controller: ScoutsController, handler: 'index', filters: [ AuthFilter ] },
  SCOUTS_DETAIL             : { method: GET, responseType: JADE_PAGE, path: '/scouts/:id/', controller: ScoutsController, handler: 'detail', filters: [ AuthFilter ] },
  SCOUTS_EDIT               : { method: GET, responseType: JADE_PAGE, path: '/scouts/:id/edit', controller: ScoutsController, handler: 'edit', filters: [ AuthFilter ] },

  SCOUT_BRANCHES_INDEX      : { method: GET, responseType: JADE_PAGE, path: '/scout_branches/', controller: ScoutBranchesController, handler: 'index', filters: [ AuthFilter ] },
  SCOUT_BRANCHES_DETAIL      : { method: GET, responseType: JADE_PAGE, path: '/scout_branches/:id', controller: ScoutBranchesController, handler: 'detail', filters: [ AuthFilter ] },
  SCOUT_BRANCHES_SCOUT_INDEX : { method: GET, responseType: JADE_PAGE, path: '/scout_branches/:id/scouts', controller: ScoutBranchesController, handler: 'getScoutsByBranchId', filters: [ AuthFilter ] },
  SCOUT_BRANCHES_ADD_SCOUT  : { method: GET, responseType: JADE_PAGE, path: '/scout_branches/:id/scouts/add', controller: ScoutBranchesController, handler: 'addScout', filters: [ AuthFilter ] },

  STORED_FILES_DOWNLOAD     : { method: GET, responseType: JADE_PAGE, path: '/stored_files/:id', controller: StoredFilesController, handler: 'getFileContentById', filters: [ AuthFilter ] },

  // Admin Pages
  ADMIN_PAYMENT_CONFIRMATIONS : { method: GET, responseType: JADE_PAGE, path: '/payment_confirmations/', controller: AdministrationController, handler: 'getPaymentConfirmations', filters: [ AuthFilter ] },
  ADMIN_TRANSPORTATION        : { method: GET, responseType: JADE_PAGE, path: '/transportation/', controller: AdministrationController, handler: 'getTransportation', filters: [ AuthFilter ] },
  ADMIN_WILLINGNESS_LETTERS   : { method: GET, responseType: JADE_PAGE, path: '/willingness_letters/', controller: AdministrationController, handler: 'getWillingnessLetters', filters: [ AuthFilter ] },

  // Mockups
  MOCKUP_ADMIN_BRANCH_WILLINGNESS        : { method: GET, responseType: JADE_PAGE, path: '/admin/branch/willingness_letters/', controller: UsersController, handler: 'adminBranchWillingnessLetters', filters: [ ] },
  MOCKUP_ADMIN_SUBBRANCH_WILLINGNESS     : { method: GET, responseType: JADE_PAGE, path: '/admin/subbranch/willingness_letters/', controller: UsersController, handler: 'adminSubbranchWillingnessLetters', filters: [ ] },
  MOCKUP_ADMIN_BRANCH_PAYMENT            : { method: GET, responseType: JADE_PAGE, path: '/admin/branch/payments/', controller: UsersController, handler: 'adminBranchPayments', filters: [ ] },
  MOCKUP_ADMIN_BRANCH_TRANSPORT          : { method: GET, responseType: JADE_PAGE, path: '/admin/branch/transports/', controller: UsersController, handler: 'adminBranchTransports', filters: [ ] },
  MOCKUP_ADMIN_COMMITTEE                 : { method: GET, responseType: JADE_PAGE, path: '/admin/committees/', controller: UsersController, handler: 'adminCommittees', filters: [ ] },
  MOCKUP_ADMIN_PARTICIPANT               : { method: GET, responseType: JADE_PAGE, path: '/admin/participants/', controller: UsersController, handler: 'adminParticipants', filters: [ ] },
  MOCKUP_ADMIN_PARTICIPANT_BY_BRANCH     : { method: GET, responseType: JADE_PAGE, path: '/admin/branch/participants/', controller: UsersController, handler: 'adminBranchParticipants', filters: [ ] },
  MOCKUP_ADMIN_PARTICIPANT_BY_SUBBRANCH  : { method: GET, responseType: JADE_PAGE, path: '/admin/branch/:brId/participants/', controller: UsersController, handler: 'adminSubBranchParticipants', filters: [ ] },

  // API Endpoints
  API_HOME                    : { method: GET, responseType: JSON_API, path: '/api', controller: SiteApiController, handler: 'index', filters: [] },

  API_CONTACT_INDEX           : { method: GET, responseType: JSON_API, path: '/api/contacts', controller: ContactsApiController, handler: 'index', filters: [] },
  API_CONTACT_CREATE          : { method: POST, responseType: JSON_API, path: '/api/contacts', controller: ContactsApiController, handler: 'addNewContact', filters: [] },
  API_CONTACT_UPDATE          : { method: PUT, responseType: JSON_API, path: '/api/contacts/:id', controller: ContactsApiController, handler: 'updateById', filters: [] },
  API_CONTACT_VIEW            : { method: GET, responseType: JSON_API, path: '/api/contacts/:id', controller: ContactsApiController, handler: 'getById', filters: [] },

  API_USERS_INDEX             : { method: GET, responseType: JSON_API, path: '/api/users', controller: UsersApiController, handler: 'index', filters: [] },
  API_USERS_CREATE            : { method: POST, responseType: JSON_API, path: '/api/users', controller: UsersApiController, handler: 'add', filters: [] },
  API_USERS_AUTHENTICATE      : { method: POST, responseType: JSON_API, path: '/api/users/actions/authenticate', controller: UsersApiController, handler: 'authenticate', filters: [] },
  API_USERS_DATA              : { method: GET, responseType: JSON_API, path: '/api/users/actions/get_current_user', controller: UsersApiController, handler: 'authenticate', filters: [ AuthFilter ] },

  API_SCOUTS                  : { method: GET, responseType: JSON_API, path: '/api/scouts', controller: ScoutsApiController, handler: 'index', filters: [ AuthFilter ] },
  API_SCOUTS_BY_NTA           : { method: GET, responseType: JSON_API, path: '/api/scouts/nta/:nta', controller: ScoutsApiController, handler: 'filter_nta', filters: [ AuthFilter ] },
  API_SCOUTS_BY_TYPE          : { method: GET, responseType: JSON_API, path: '/api/scouts/type/:type', controller: ScoutsApiController, handler: 'filter_type', filters: [ ] },
  API_SCOUTS_BRANCH_STATS     : { method: GET, responseType: JSON_API, path: '/api/scouts/branches/stats', controller: ScoutsApiController, handler: 'branch_stats', filters: [ ] },
  API_SCOUTS_BY_BRANCH        : { method: GET, responseType: JSON_API, path: '/api/scouts/branches/:branch', controller: ScoutsApiController, handler: 'filter_branch', filters: [ AuthFilter ] },
  API_SCOUTS_BY_BRANCH_GROUP  : { method: GET, responseType: JSON_API, path: '/api/scouts/branches/:branch/:group', controller: ScoutsApiController, handler: 'filter_branch_group', filters: [ AuthFilter ] },
  API_SCOUTS_BY_GROUP         : { method: GET, responseType: JSON_API, path: '/api/scouts/groups/:group', controller: ScoutsApiController, handler: 'filter_group', filters: [ AuthFilter ] },
  API_SCOUTS_CREATE           : { method: POST, responseType: JSON_API, path: '/api/scouts', controller: ScoutsApiController, handler: 'create', filters: [ AuthFilter ] },
  API_SCOUTS_READ             : { method: GET, responseType: JSON_API, path: '/api/scouts/:scoutId', controller: ScoutsApiController, handler: 'read', filters: [ AuthFilter ] },
  API_SCOUTS_UPDATE           : { method: PUT, responseType: JSON_API, path: '/api/scouts/:id', controller: ScoutsApiController, handler: 'update', filters: [ AuthFilter ] },
  API_SCOUTS_DELETE           : { method: DELETE, responseType: JSON_API, path: '/api/scouts/:id', controller: ScoutsApiController, handler: 'deleteById', filters: [ AuthFilter ] },
  API_SCOUTS_GENERATE_IDCARD  : { method: GET, responseType: JSON_API, path: '/api/scouts/:scoutId/card', controller: ScoutsApiController, handler: 'generateIdCard', filters: [ ] },
  API_SCOUTS_GENERATE_IDCARDS : { method: GET, responseType: JSON_API, path: '/api/scouts/cards/:skip/:limit', controller: ScoutsApiController, handler: 'generateIdCards', filters: [ ] },


  API_BADGE_TYPES_INDEX       : { method: GET, responseType: JSON_API, path: '/api/badge_types', controller: BadgeTypesApiController, handler: 'index', filters: [ AuthFilter ] },

  API_BRANCHES                : { method: GET, responseType: JSON_API, path: '/api/scout_branches', controller: ScoutBranchesApiController, handler: 'index', filters: [ AuthFilter ] },
  API_BRANCHES_CHILDREN       : { method: GET, responseType: JSON_API, path: '/api/scout_branches/:id/scout_branches', controller: ScoutBranchesApiController, handler: 'getChildren', filters: [ AuthFilter ] },
  API_BRANCHES_CREATE         : { method: POST, responseType: JSON_API, path: '/api/scout_branches', controller: ScoutBranchesApiController, handler: 'create', filters: [ AuthFilter ] },
  API_BRANCHES_READ           : { method: GET, responseType: JSON_API, path: '/api/scout_branches/:branchId', controller: ScoutBranchesApiController, handler: 'read', filters: [ AuthFilter ] },
  API_BRANCHES_UPDATE         : { method: PUT, responseType: JSON_API, path: '/api/scout_branches/:branchId', controller: ScoutBranchesApiController, handler: 'update', filters: [ AuthFilter ] },
  API_BRANCHES_SCOUTS         : { method: GET, responseType: JSON_API, path: '/api/scout_branches/:id/scouts', controller: ScoutBranchesApiController, handler: 'getScoutsByBranchId', filters: [ AuthFilter ] },
  API_BRANCHES_USERS          : { method: GET, responseType: JSON_API, path: '/api/scout_branches/:id/users', controller: ScoutBranchesApiController, handler: 'getUsersByBranchId', filters: [ AuthFilter ] },
  API_BRANCHES_GENERATE_IDCARDS : { method: GET, responseType: JSON_API, path: '/api/scouts_branches/:id/cards/:skip/:limit', controller: ScoutBranchesApiController, handler: 'generateIdCards', filters: [ ] },


  API_GROUPS                  : { method: GET, responseType: JSON_API, path: '/api/scout_groups', controller: ScoutGroupsApiController, handler: 'index', filters: [ AuthFilter ] },
  API_GROUPS_CREATE           : { method: POST, responseType: JSON_API, path: '/api/scout_groups', controller: ScoutGroupsApiController, handler: 'create', filters: [ AuthFilter ] },
  API_GROUPS_READ             : { method: GET, responseType: JSON_API, path: '/api/scout_groups/:groupId', controller: ScoutGroupsApiController, handler: 'read', filters: [ AuthFilter ] },
  API_GROUPS_UPDATE           : { method: PUT, responseType: JSON_API, path: '/api/scout_groups/:groupId', controller: ScoutGroupsApiController, handler: 'update', filters: [ AuthFilter ] },

  API_LOCATIONS               : { method: GET, responseType: JSON_API, path: '/api/locations', controller: LocationsApiController, handler: 'index', filters: [ AuthFilter ] },
  API_LOCATIONS_BY_PARENT     : { method: GET, responseType: JSON_API, path: '/api/locations/:locationId/locations', controller: LocationsApiController, handler: 'filter_parent', filters: [ AuthFilter ] },
  API_LOCATIONS_CREATE        : { method: POST, responseType: JSON_API, path: '/api/locations', controller: LocationsApiController, handler: 'create', filters: [ AuthFilter ] },
  API_LOCATIONS_READ          : { method: GET, responseType: JSON_API, path: '/api/locations/:locationId', controller: LocationsApiController, handler: 'read', filters: [ AuthFilter ] },
  API_LOCATIONS_UPDATE        : { method: PUT, responseType: JSON_API, path: '/api/locations/:locationId', controller: LocationsApiController, handler: 'update', filters: [ AuthFilter ] },

  // C01
  API_SUBBRANCH_WILLINGNESS_LETTER    : { method: GET, responseType: JSON_API, path: '/api/subbranch_willingness_letter', controller: SubBranchWillingnessLetterApiController, handler: 'index', filters: [ AuthFilter ] },
  API_SUBBRANCH_WILLINGNESS_CREATE    : { method: PUT, responseType: JSON_API, path: '/api/subbranch_willingness_letter', controller: SubBranchWillingnessLetterApiController, handler: 'create', filters: [ AuthFilter, UploadFileSingleFilter ] },
  API_SUBBRANCH_WILLINGNESS_BY_BRANCH : { method: GET, responseType: JSON_API, path: '/api/subbranch_willingness_letter/branch/:branchId', controller: SubBranchWillingnessLetterApiController, handler: 'filter_branch', filters: [ AuthFilter ] },
  API_SUBBRANCH_WILLINGNESS_BY_SUBBRANCH : { method: GET, responseType: JSON_API, path: '/api/subbranch_willingness_letter/subbranch/:subBranchId', controller: SubBranchWillingnessLetterApiController, handler: 'filter_sub_branch', filters: [ AuthFilter ] },
  API_SUBBRANCH_WILLINGNESS_READ      : { method: GET, responseType: JSON_API, path: '/api/subbranch_willingness_letter/:sbwlId', controller: SubBranchWillingnessLetterApiController, handler: 'read', filters: [ AuthFilter ] },
  API_SUBBRANCH_WILLINGNESS_UPDATE    : { method: POST, responseType: JSON_API, path: '/api/subbranch_willingness_letter/:sbwlId', controller: SubBranchWillingnessLetterApiController, handler: 'update', filters: [ AuthFilter ] },

  // C02
  API_SUBBRANCH_REGISTRATION          : { method: GET, responseType: JSON_API, path: '/api/subbranch_registration', controller: SubBranchRegistrationApiController, handler: 'index', filters: [ AuthFilter ] },
  API_SUBBRANCH_REGISTRATION_CREATE   : { method: PUT, responseType: JSON_API, path: '/api/subbranch_registration', controller: SubBranchRegistrationApiController, handler: 'create', filters: [ AuthFilter, UploadFileSingleFilter ] },
  API_SUBBRANCH_REGISTRATION_BY_BRANCH: { method: GET, responseType: JSON_API, path: '/api/subbranch_registration/branch/:branchId', controller: SubBranchRegistrationApiController, handler: 'filter_branch', filters: [ AuthFilter ] },
  API_SUBBRANCH_REGISTRATION_BY_SUBBRANCH : { method: GET, responseType: JSON_API, path: '/api/subbranch_registration/subbranch/:subBranchId', controller: SubBranchRegistrationApiController, handler: 'filter_sub_branch', filters: [ AuthFilter ] },
  API_SUBBRANCH_REGISTRATION_READ     : { method: GET, responseType: JSON_API, path: '/api/subbranch_registration/:sbrId', controller: SubBranchRegistrationApiController, handler: 'read', filters: [ AuthFilter ] },
  API_SUBBRANCH_REGISTRATION_UPDATE   : { method: POST, responseType: JSON_API, path: '/api/subbranch_registration/:sbrId', controller: SubBranchRegistrationApiController, handler: 'update', filters: [ AuthFilter ] },

  // D01
  API_BRANCH_WILLINGNESS_LETTER           : { method: GET, responseType: JSON_API, path: '/api/willingness_letter', controller: WillingnessLetterApiController, handler: 'index', filters: [] },
  API_BRANCH_WILLINGNESS_LETTER_CREATE    : { method: PUT, responseType: JSON_API, path: '/api/willingness_letter', controller: WillingnessLetterApiController, handler: 'create', filters: [] },
  API_BRANCH_WILLINGNESS_LETTER_BY_BRANCH : { method: GET, responseType: JSON_API, path: '/api/willingness_letter/branch/:branchId', controller: WillingnessLetterApiController, handler: 'filter_branch', filters: [] },
  API_BRANCH_WILLINGNESS_LETTER_READ      : { method: GET, responseType: JSON_API, path: '/api/willingness_letter/:wlId', controller: WillingnessLetterApiController, handler: 'read', filters: [] },
  API_BRANCH_WILLINGNESS_LETTER_UPDATE    : { method: POST, responseType: JSON_API, path: '/api/willingness_letter/:wlId', controller: WillingnessLetterApiController, handler: 'update', filters: [] },

  // D02
  API_BRANCH_REGISTRATION           : { method: GET, responseType: JSON_API, path: '/api/branch_registration', controller: BranchRegistrationsApiController, handler: 'index', filters: [] },
  API_BRANCH_REGISTRATION_CREATE    : { method: PUT, responseType: JSON_API, path: '/api/branch_registration', controller: BranchRegistrationsApiController, handler: 'create', filters: [ UploadFileSingleFilter ] },
  API_BRANCH_REGISTRATION_BY_BRANCH : { method: GET, responseType: JSON_API, path: '/api/branch_registration/branch/:branchId', controller: BranchRegistrationsApiController, handler: 'filter_branch', filters: [] },
  API_BRANCH_REGISTRATION_READ   : { method: GET, responseType: JSON_API, path: '/api/branch_registration/:brId', controller: BranchRegistrationsApiController, handler: 'read', filters: [] },
  API_BRANCH_REGISTRATION_UPDATE : { method: POST, responseType: JSON_API, path: '/api/branch_registration/:brId', controller: BranchRegistrationsApiController, handler: 'update', filters: [] },

  // D07
  API_BRANCH_TRANSPORTS              : { method: GET, responseType: JSON_API, path: '/api/branch_transport', controller: BranchTransportsApiController, handler: 'index', filters: [] },
  API_BRANCH_TRANSPORTS_BY_BRANCH    : { method: GET, responseType: JSON_API, path: '/api/branch_transport/branches/:branchId', controller: BranchTransportsApiController, handler: 'filter_branch', filters: [] },
  API_BRANCH_TRANSPORTS_CREATE       : { method: PUT, responseType: JSON_API, path: '/api/branch_transport', controller: BranchTransportsApiController, handler: 'create', filters: [ UploadFileSingleFilter ] },
  API_BRANCH_TRANSPORTS_READ         : { method: GET, responseType: JSON_API, path: '/api/branch_transport/:transportId', controller: BranchTransportsApiController, handler: 'read', filters: [] },
  API_BRANCH_TRANSPORTS_UPDATE       : { method: POST, responseType: JSON_API, path: '/api/branch_transport/:transportId', controller: BranchTransportsApiController, handler: 'update', filters: [] },

  // D07
  API_BRANCH_PAYMENTS              : { method: GET, responseType: JSON_API, path: '/api/branch_payment', controller: BranchPaymentsApiController, handler: 'index', filters: [] },
  API_BRANCH_PAYMENTS_BY_BRANCH    : { method: GET, responseType: JSON_API, path: '/api/branch_payment/branches/:branchId', controller: BranchPaymentsApiController, handler: 'filter_branch', filters: [] },
  API_BRANCH_PAYMENTS_CREATE       : { method: PUT, responseType: JSON_API, path: '/api/branch_payment', controller: BranchPaymentsApiController, handler: 'create', filters: [ UploadFileSingleFilter ] },
  API_BRANCH_PAYMENTS_READ         : { method: GET, responseType: JSON_API, path: '/api/branch_payment/:paymentId', controller: BranchPaymentsApiController, handler: 'read', filters: [] },
  API_BRANCH_PAYMENTS_UPDATE       : { method: POST, responseType: JSON_API, path: '/api/branch_payment/:paymentId', controller: BranchPaymentsApiController, handler: 'update', filters: [] },

  API_STORED_FILES_DETAIL            : { method: GET, responseType: JSON_API, path: '/api/stored_files/:id', controller: StoredFilesApiController, handler: 'getById', filters: [ AuthFilter ] },
  API_STORED_FILES_UPDATE            : { method: PUT, responseType: JSON_API, path: '/api/stored_files/:id', controller: StoredFilesApiController, handler: 'updateById', filters: [ AuthFilter ] }
};
