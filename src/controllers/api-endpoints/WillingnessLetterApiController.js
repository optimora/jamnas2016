import { OK, UNAUTHORIZED, INTERNAL_SERVER_ERROR } from '../../lib/constants/HttpStatus';

import BaseController from '../../lib/BaseController';
import JsonApiResponse from '../../lib/JsonApiResponse';

import WillingnessLetter from '../../models/WillingnessLetter';

export default class WillingnessLetterApiController extends BaseController {
  index(req, res) {
    WillingnessLetter.find(function(error, docs){
      if (error) return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengambil data kesediaan kwartir.', {}, error);
      else return new JsonApiResponse(res, OK, 'Data kesediaan kwartir.', docs).serve();
    });
  }

  create(req, res) {
    if(typeof req.body.file !== 'undefined' && typeof req.body.file.name === 'undefined' ) delete req.body.file;

    let wl = new WillingnessLetter(req.body);
    wl.branch = req.user.branch._id;
    wl.created = new Date().toISOString();

    wl.save((saveError, doc) => {
      if (saveError) {
        //this._logger.error('Error on saving WillingnessLetter data.', saveError);
        return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal melakukan penyimpanan data kesediaan kwartir.', doc, saveError).serve();
      }
      else {
        return new JsonApiResponse(res, OK, 'Sukses.', doc).serve();
      }
    });

  }

  read(req, res) {
    WillingnessLetter.findById(req.params.wlId, function(error, doc) {
      if (error) {
        //this._logger.error('Error while finding WillingnessLetter data.', error);
        return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengambil data kesediaan kwartir ' + req.params.wlId + '.', {}, error);
      }
      else {
        return new JsonApiResponse(res, OK, 'Data kesediaan kwartir ' + req.params.wlId + '.', doc).serve();
      }
    });
  }

  filter_branch(req, res) {
    WillingnessLetter.findOne({branch: req.params.branchId}, function(error, doc) {
      if (error) {
        //this._logger.error('Error while finding WillingnessLetter data.', error);
        return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengambil data kesediaan kwartir ' + req.params.branchId + '.', {}, error);
      }
      else {
        return new JsonApiResponse(res, OK, 'Data kesediaan kwartir ' + req.params.branchId + '.', doc).serve();
      }
    });

  }

  update(req, res) {
    WillingnessLetter.update({ _id: req.params.wlId }, req.body, function(error, num) {
      if (error) return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengubah data kesediaan kwartir '+req.params.wlId+'.', {}, error);
      else return new JsonApiResponse(res, OK, 'Data kesediaan kwartir '+req.params.wlId+' berhasil diperbaharui.', req.body).serve();
    });
  }
};
