import { OK, UNAUTHORIZED, INTERNAL_SERVER_ERROR } from '../../lib/constants/HttpStatus';

import BaseController from '../../lib/BaseController';
import JsonApiResponse from '../../lib/JsonApiResponse';

import SubBranchWillingnessLetter from '../../models/SubBranchWillingnessLetter';
import StoredFile from '../../models/StoredFile';

const getFileExtension = (file) => {
  const fileNameSegments = file.originalname.split('.');
  return fileNameSegments[fileNameSegments.length - 1];
};

export default class SubBranchWillingnessLetterApiController extends BaseController {
  index(req, res) {
    SubBranchWillingnessLetter.find(function(error, docs){
      if (error) return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengambil data kesediaan kwartir.', {}, error);
      else return new JsonApiResponse(res, OK, 'Data kesediaan kwartir.', docs).serve();
    });
  }

  create(req, res) {
    console.log(req.body);
    console.log(req.file);
    if(!req.file) {
      // TODO: Kalau gak ada file nya?
      return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengunggah konfirmasi pembayaran kwartir.', {}, {}).serve();
    }
    else {
      let file = new StoredFile();

      file.extension = getFileExtension(req.file);
      file.category = 'willingness';
      file.topic = 'subbranch-documents';

      file.raw_data = req.file;

      file.user = req.user._id;
      file.branch = req.user.branch._id;

      file.markModified('raw_data');

      file.save((saveError) => {
        if (saveError) {
          // Simpan file gagal
          return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal melakukan penyimpanan konfirmasi pembayaran kwartir.', {}, saveError).serve();
        }
        else {
          let sbwl = new SubBranchWillingnessLetter(req.body);
          sbwl.branch = req.user.branch._id;
          sbwl.created = new Date().toISOString();
          sbwl.file = file;

          sbwl.save((saveError, doc) => {
            if (saveError) {
              //this._logger.error('Error on saving WillingnessLetter data.', saveError);
              return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal melakukan penyimpanan konfirmasi pembayaran kwartir.', {}, saveError).serve();
            }
            else {
              return new JsonApiResponse(res, OK, 'Sukses.', doc).serve();
            }
          });
        }
      });
    }
  }

  read(req, res) {
    SubBranchWillingnessLetter.findById(req.params.sbwlId, function(error, doc) {
      if (error) {
        //this._logger.error('Error while finding WillingnessLetter data.', error);
        return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengambil data kesediaan kwartir ' + req.params.sbwlId + '.', {}, error);
      }
      else {
        return new JsonApiResponse(res, OK, 'Data kesediaan kwartir ' + req.params.sbwlId + '.', doc).serve();
      }
    });
  }

  filter_branch(req, res) {
    SubBranchWillingnessLetter.findOne({branch: req.params.branchId}, function(error, doc) {
      if (error) {
        //this._logger.error('Error while finding WillingnessLetter data.', error);
        return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengambil data kesediaan kwartir ' + req.params.branchId + '.', {}, error);
      }
      else {
        return new JsonApiResponse(res, OK, 'Data kesediaan kwartir ' + req.params.branchId + '.', doc).serve();
      }
    });

  }

  filter_sub_branch(req, res) {
    SubBranchWillingnessLetter.findOne({subbranch: req.params.subBranchId}, function(error, doc) {
      if (error) {
        //this._logger.error('Error while finding WillingnessLetter data.', error);
        return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengambil data kesediaan kwartir ' + req.params.subBranchId + '.', {}, error);
      }
      else {
        return new JsonApiResponse(res, OK, 'Data kesediaan kwartir ' + req.params.subBranchId + '.', doc).serve();
      }
    });

  }

  update(req, res) {
    if(typeof req.body.file !== 'undefined' && typeof req.body.file.name === 'undefined' ) delete req.body.file;

    SubBranchWillingnessLetter.update({ _id: req.params.sbwlId }, req.body, function(error, num) {
      if (error) return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengubah data kesediaan kwartir '+req.params.sbwlId+'.', {}, error);
      else return new JsonApiResponse(res, OK, 'Data kesediaan kwartir '+req.params.sbwlId+' berhasil diperbaharui.', req.body).serve();
    });
  }
};
