import mongoose from 'mongoose';

import { OK, UNAUTHORIZED, INTERNAL_SERVER_ERROR } from '../../lib/constants/HttpStatus';

import BaseController from '../../lib/BaseController';
import JsonApiResponse from '../../lib/JsonApiResponse';

import ScoutGroup from '../../models/ScoutGroup';

var ObjectId = mongoose.Schema.Types.ObjectId;

export default class ScoutGroupsApiController extends BaseController {
  _findScoutGroupExistenceOrError(req, res, callback) {
    ScoutGroup
      .find({ _id: ObjectId(req.params.id) })
      .exec((findError, scoutGroup) => {
        if (findError) {
          return new JsonApiResponse();
        }
        if (!scoutGroup) {
          return new JsonApiResponse();
        }
        callback(scoutGroup);
      });
  }

  index(req, res) {
    let conditions = {};
    let $andConditions = [];
    let $orConditions = [];
    Object.keys(req.query).forEach((field) => {
      switch (field) {
        case 'branch':
          conditions = Object.assign({}, conditions, {
            [field]: req.query[field]
          });
          break;
        case '__since':
          $andConditions.push({
            created: {
              $gt: new Date(req.query[field])
            }
          });
          break;
        default:
          $orConditions.push({
            [field]: req.query[field]
          });
          break;
      }
    });

    if ($andConditions.length > 0) {
      conditions['$and'] = $andConditions;
    }

    if ($orConditions.length > 0) {
      conditions['$or'] = $orConditions;
    }

    ScoutGroup
      .find(conditions)
      .exec((error, docs) => {
        if (error) {
          this._logger.error('Error while finding ScoutGroup data.', error);
          return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengambil data regu.', null, error);
        }
        else {
          return new JsonApiResponse(res, OK, 'Data regu.', docs).serve();
        }
      })

  }

  create(req, res) {
    let group = new ScoutGroup();

    Object.keys(req.body).forEach((field) => {
      group[field] = req.body[field];
    });

    group.created = new Date();

    group.save(function(error) {
      if (error) {
        this._logger.error('Error while saving ScoutGroup data.', error);
        return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal melakukan penyimpanan data kelompok.', {}, error).serve();
      }
      else {
        return new JsonApiResponse(res, OK, 'Data kelompok berhasil disimpan.', group).serve();
      }
    });
  }

  read(req, res) {
    ScoutGroup.findById(req.params.groupId, function(error, doc){
      if (error) {
        this._logger.error('', error);
        return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengambil data kelompok ' + req.params.groupId + '.', {}, error).serve();
      }
      else {
        return new JsonApiResponse(res, OK, 'Data kelompok '+ req.params.groupId +'.', doc).serve();
      }
    });
  }

  update(req, res) {
    ScoutGroup.update({ _id: req.params.groupId }, req.body.group, function(error, num) {
      if (error) return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengubah data kelompok '+req.params.groupId+'.', {}, error);
      else return new JsonApiResponse(res, OK, 'Data kelompok ' + req.params.groupId + ' berhasil diperbaharui.', req.body.group).serve();
    });
  }
}
