import { OK, UNAUTHORIZED, INTERNAL_SERVER_ERROR } from '../../lib/constants/HttpStatus';

import BaseController from '../../lib/BaseController';
import JsonApiResponse from '../../lib/JsonApiResponse';

import BranchRegistration from '../../models/BranchRegistration';
import StoredFile from '../../models/StoredFile';

const getFileExtension = (file) => {
  const fileNameSegments = file.originalname.split('.');
  return fileNameSegments[fileNameSegments.length - 1];
};

export default class BranchRegistrationsApiController extends BaseController {
  index(req, res) {
    BranchRegistration.find(function(error, docs){
      if (error) return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengambil data registrasi kwartir.', {}, error);
      else return new JsonApiResponse(res, OK, 'Data registrasi kwartir.', docs).serve();
    });
  }

  create(req, res) {
    console.log(req.body);
    console.log(req.file);

    if(!req.file) {
      // TODO: Kalau gak ada file nya?
      return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengunggah konfirmasi pembayaran kwartir.', {}, {}).serve();
    }
    else {
      let file = new StoredFile();

      file.extension = getFileExtension(req.file);
      file.category = 'registration';
      file.topic = 'branch-documents';

      file.raw_data = req.file;

      file.user = req.user._id;
      file.branch = req.user.branch._id;

      file.markModified('raw_data');

      file.save((saveError) => {
        if (saveError) {
          // Simpan file gagal
          return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal melakukan penyimpanan konfirmasi pembayaran kwartir.', {}, saveError).serve();
        }
        else {
          let br = new BranchRegistration(req.body);
          br.branch = req.user.branch._id;
          br.created = new Date().toISOString();
          br.file = file;

          br.save((saveError, doc) => {
            if (saveError) {
              //this._logger.error('Error on saving WillingnessLetter data.', saveError);
              return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal melakukan penyimpanan konfirmasi pembayaran kwartir.', {}, saveError).serve();
            }
            else {
              return new JsonApiResponse(res, OK, 'Sukses.', doc).serve();
            }
          });
        }
      });
    }
  }

  read(req, res) {
    BranchRegistration.findById(req.params.brId, function(error, doc) {
      if (error) {
        //this._logger.error('Error while finding WillingnessLetter data.', error);
        return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengambil data registrasi kwartir ' + req.params.brId + '.', {}, error);
      }
      else {
        return new JsonApiResponse(res, OK, 'Data registrasi kwartir ' + req.params.brId + '.', doc).serve();
      }
    });
  }

  filter_branch(req, res) {
    BranchRegistration
        .findOne({branch: req.params.branchId})
        .populate('branch')
        //.populate('participants.sub_branch')
        .exec(function(error, doc) {
          if (error) {
            //this._logger.error('Error while finding WillingnessLetter data.', error);
            return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengambil data registrasi kwartir ' + req.params.branchId + '.', {}, error);
          }
          else {
            return new JsonApiResponse(res, OK, 'Data kesediaan kwartir ' + req.params.branchId + '.', doc).serve();
          }
    });

  }

  update(req, res) {
    if(typeof req.body.file !== 'undefined' && typeof req.body.file.name === 'undefined' ) delete req.body.file;

    BranchRegistration.update({ _id: req.params.brId }, req.body, function(error, num) {
      if (error) return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengubah data registrasi kwartir '+req.params.brId+'.', {}, error);
      else return new JsonApiResponse(res, OK, 'Data registrasi kwartir '+req.params.brId+' berhasil diperbaharui.', req.body).serve();
    });
  }
};
