import async from 'mongoose';
import mongoose from 'mongoose';

var ObjectId = mongoose.Schema.Types.ObjectId;

import { OK, UNAUTHORIZED, INTERNAL_SERVER_ERROR, INVALID } from '../../lib/constants/HttpStatus';

import BaseController from '../../lib/BaseController';
import JsonApiResponse from '../../lib/JsonApiResponse';

import Contact from '../../models/Contact';

export default class ContactApiController extends BaseController {
  _findContactExistenceOrError(req, res, callback) {
    Contact
      .find({ _id: ObjectId(req.params.id) })
      .exec((findError, contact) => {
        if (findError) {
          this._logger.error('Error while finding Contact data.', findError);
          return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengambil data kontak anggota pramuka.', {}, error).serve();
        }
        if (!contact) {
          return new JsonApiResponse(res, INVALID, 'Data kontak tidak ditemukan.', {}, error).serve();
        }
        callback(contact);
      });
  }

  _sanitizeSubmittedData(req) {
    Object.keys(req.body).forEach((field) => {
      switch (field) {
        case 'branch':
        case 'province':
        case 'regency_city':
        case 'country':
        case 'type':
        case 'category':
        case 'scout':
          if (req.body[field] === '') {
            delete req.body[field];
          }
          break;
        default:
          break;
      }
    });
  }

  index() {
    Contact
      .find({})
      .exec((findError, contacts) => {
        if (findError) {
          this._logger.error('Error while finding contact data.', findError);
          return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengambil data kontak anggota pramuka.', {}, error).serve();
        }
        else {
          return new JsonApiResponse(res, OK, 'Sukses.', contacts, error)
        }
      });
  }

  getById(req, res) {
    this._findContactExistenceOrError(req, res, (contact) => {
      return new JsonApiResponse(res, OK, 'Sukses.', contact, error).serve();
    });
  }

  updateById(req, res) {
    this._findContactExistenceOrError(req, res, (contact) => {
      this._sanitizeSubmittedData(req);
      Object.keys(req.body).forEach((field) => {
        switch (field) {
          default:
            contact[field] = req.body[field];
        }
      });

      contact.save((saveError, contact) => {
        if (saveError) {
          this._logger.error('Error while saving contact data.', saveError);
          return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal menyimpan data anggota pramuka.', {}, error).serve();
        }
        else {
          return new JsonApiResponse(res, OK, 'Sukses.', contact).serve();
        }
      });
    });
  }

  addNewContact(req, res) {
    let contact = new Contact();

    this._sanitizeSubmittedData(req);

    Object.keys(req.body).forEach((field) => {
      switch (field) {
        case 'branch':
        case 'country':
        case 'province':
        case 'scout':
        case 'regency_city':
          contact[field] = ObjectId(req.body[field]);
          break;
        default:
          contact[field] = req.body[field];
          break;
      }
    });

    contact.save((saveError, contact) => {
      if (saveError) {
        this._logger.error('Error while saving contact data.', saveError);
        return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengambil data kontak anggota pramuka.', {}, saveError).serve();
      }
      return new JsonApiResponse(res, OK, 'Sukses.', contact);
    });
  }
}
