import { OK, UNAUTHORIZED, INTERNAL_SERVER_ERROR } from '../../lib/constants/HttpStatus';

import BaseController from '../../lib/BaseController';
import JsonApiResponse from '../../lib/JsonApiResponse';

import Location from '../../models/Location';

export default class LocationsApiController extends BaseController {
  index(req, res) {

    Location.find((error, docs) => {
      if (error) {
        this._logger.error('Error while obtaining Location data.', error);
        return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengambil data lokasi.', {}, error).serve();
      }
      else return new JsonApiResponse(res, OK, 'Data lokasi.', docs).serve();
    });

  }

  filter_parent(req, res) {
    Location.find({ parentLocation:req.params.locationId }, (error, docs) => {
      if (error) {
        this._logger.error('Error while obtaining Location children data.', error);
        return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengambil data lokasi.', null, error);
      }
      else return new JsonApiResponse(res, OK, 'Data lokasi.', docs).serve();
    });
  }

  create(req, res) {
    let Location = new Location(req.body.location);

    Location.save((error) => {
      if (error) return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal melakukan penyimpanan data lokasi.', {}, error);
      return new JsonApiResponse(res, OK, 'Data lokasi berhasil disimpan.', Location).serve();
    });
  }

  read(req, res) {
    Location.findById(req.params.locationId, (error, doc) => {
      if (error) return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengambil data lokasi '+req.params.locationId+'.', {}, error);
      else return new JsonApiResponse(res, OK, 'Data lokasi '+req.params.locationId+'.', doc).serve();
    });
  }

  update(req, res) {
    Location.update({ _id: req.params.locationId }, req.body.location, (error, num) => {
      if (error) return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengubah data lokasi '+req.params.locationId+'.', {}, error);
      else return new JsonApiResponse(res, OK, 'Data lokasi '+req.params.locationId+' berhasil diperbaharui.', req.body.location).serve();
    });
  }
}
