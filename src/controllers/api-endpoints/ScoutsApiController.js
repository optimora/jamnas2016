import async from 'async';
import mongoose from 'mongoose';
import _ from 'underscore';
import moment from 'moment';
import path from 'path';
import mime from 'mime';

var ObjectId = mongoose.Types.ObjectId;

import { OK, UNAUTHORIZED, INTERNAL_SERVER_ERROR, INVALID } from '../../lib/constants/HttpStatus';

import BaseController from '../../lib/BaseController';
import JsonApiResponse from '../../lib/JsonApiResponse';

import Contact from '../../models/Contact';
import Scout from '../../models/Scout';
import ScoutBranch from '../../models/ScoutBranch';
import ScoutExperience from '../../models/ScoutExperience';
import ScoutGroup from '../../models/ScoutGroup';
import StoredFile from '../../models/StoredFile';
import User from '../../models/User';

export default class ScoutsApiController extends BaseController {
  _findScoutExistenceOrError(req, res, callback, options = { populatedFields: [] }) {
    Scout
      .findOne({ _id: req.params.id })
      .exec((findError, scout) => {
        if (findError) {
          return new JsonApiResponse();
        }
        if (!scout) {
          return new JsonApiResponse();
        }
        callback(scout);
      });
  }

  _prepareSavedData(req, scout, callback) {
    console.log(req.body);

    let pendingActions = [];

    Object.keys(req.body).forEach((field) => {
      switch (field) {
        case 'avatar':
          if (req.body[field] !== '') {
            scout[field] = ObjectId(req.body[field]);

            StoredFile
              .findOne({ _id: req.body[field] })
              .exec((findError, storedFile) => {
                if (findError) {
                  this._logger.error('Error while checking StoredFile data for avatar image.', findError);
                }
                else {
                  if (!storedFile) {
                    this._logger.error('Can\'t find StoredFile data for avatar.');
                  }
                  else {
                    storedFile.scout = scout._id;
                    storedFile.subbranch = scout.subbranch;
                    storedFile.active = true;
                    storedFile.modified = new Date();
                  }
                }
              });
          }
          if (req.body[field] === '' && scout[field] != undefined && scout[field] != null) {
            scout[field] = undefined;
          }
          break;
        case 'contact':
        case 'parent_contact':
          if (typeof req.body[field] === 'string') {
            scout[field] = ObjectId(req.body[field]);
          }
          else {
            if (scout[field]) {
              pendingActions.push((callback) => {
                Contact.remove({ _id: scout[field]}, (removeError) => {
                  if (removeError) {
                    this._logger.error('Error while removing old Scout contact data.', removeError);
                  }
                  this._logger.warning('Contact with id ' + scout[field] + ' should have been removed.');
                  callback(removeError, null)
                });
              });
            }

            pendingActions.push((callback) => {
              let contact = new Contact();
              let composedContact = Contact.composeNewDocument(req.body[field]);

              Object.keys(composedContact).forEach((contactField) => {
                contact[contactField] = composedContact[contactField];
              });

              switch (field) {
                case 'contact':
                  contact.type = 'scout';
                  break;
                case 'parent_contact':
                  contact.type = 'scout-parent';
                  break;
                default:
                  break;
              }

              contact.category = 'normal-contact';

              contact.scout = scout._id;
              contact.branch = scout.branch;
              contact.subbranch = scout.subbranch;

              contact.save((saveError) => {
                if (saveError) {
                  this._logger.error('Error while saving contact data.', saveError);
                }
                else {
                  scout[field] = contact._id;
                }
                callback(saveError, contact);
              });
            });
          }
          break;
        case 'emergency_contacts':
          if (scout[field] && scout[field].length > 0) {
            scout[field].forEach((emergencyContactId) => {
              pendingActions.push((callback) => {
                Contact.remove({ _id: emergencyContactId }, (removeError) => {
                  if (removeError) {
                    this._logger.error('Error while removing old Scout emergency contact data.', removeError);
                  }
                  this._logger.warning('Contact (emergency) with id ' + scout[field] + ' should have been removed.');
                  callback(removeError, null)
                });
              });
            });
          }
          req.body[field].forEach((rawEmergencyContact) => {
            if (typeof rawEmergencyContact === 'string' && rawEmergencyContact !== '') {
              scout[field] = ObjectId(req.body[field]);
            }
            else if (rawEmergencyContact && typeof rawEmergencyContact === 'object') {
              pendingActions.push((callback) => {
                let emergencyContact = new Contact();
                let composedEmergencyContact = Contact.composeNewDocument(rawEmergencyContact);

                Object.keys(composedEmergencyContact).forEach((emergencyContactField) => {
                  emergencyContact[emergencyContactField] = composedEmergencyContact[emergencyContactField];
                });

                emergencyContact.category = 'emergency-contact';

                emergencyContact.scout = scout._id;
                emergencyContact.branch = scout.branch;
                emergencyContact.subbranch = scout.subbranch;

                emergencyContact.save((saveError) => {
                  if (saveError) {
                    this._logger.error('Error while saving Scout emergency contact data.', saveError);
                  }
                  else {
                    scout[field] = emergencyContact._id;
                  }
                  callback(saveError, emergencyContact);
                });
              });
            }
            else {
              this._logger.error('Error, unexpected data submitted.', null);
            }
          });
          break;
        case 'experiences':
          let bulkSaveScoutExperienceActions = [];
          req.body[field].forEach((experience) => {
            if (typeof experience === 'string' && experience !== '') {
              scout[field] = ObjectId(experience);
            }
            else {
              if (experience._id && experience._id !== '') {
                ScoutExperience
                  .findOne({ _id: experience._id })
                  .exec((findError, scoutExperience) => {
                    if (findError) {
                      this._logger.error('Can\'t find ScoutExperience data.', findError);
                    }
                    else {
                      Object.keys(experience).forEach((experienceField) => {
                        switch (experienceField) {
                          case '_id':
                          case '__v':
                          case 'branch':
                          case 'subbranch':
                            break;
                          default:
                            scoutExperience[experienceField] = experience[experienceField];
                        }
                      });

                      scoutExperience.modified = new Date();

                      scoutExperience.save((saveError) => {
                        if (saveError) {
                          this._logger.error('Error while updating scout experience data.', saveError);
                        }
                      });
                    }
                  })
              }
              else {
                if (scout[field] && scout[field].length > 0) {
                  scout[field].forEach((scoutExperienceId) => {
                    pendingActions.push((callback) => {
                      ScoutExperience.remove({ _id: scoutExperienceId }, (removeError) => {
                        if (removeError) {
                          this._logger.error('Error while removing old ScoutExperience data.');
                        }
                        this._logger.warning('ScoutExperience with id ' + scoutExperienceId + ' should have been removed.');
                        callback(removeError, null);
                      });
                    });
                  });
                }
                bulkSaveScoutExperienceActions.push((callback) => {
                  let scoutExperience = new ScoutExperience();

                  Object.keys(experience).forEach((experienceField) => {
                    scoutExperience[experienceField] = experience[experienceField];
                  });

                  scoutExperience.branch = scout.branch;
                  scoutExperience.subbranch = scout.subbranch;
                  scoutExperience.scout = scout._id;

                  scoutExperience.created = new Date();

                  scoutExperience.save((saveError) => {
                    if (saveError) {
                      this._logger.error('Error while creating new ScoutExperience data.', saveError);
                    }
                    callback(saveError, scoutExperience);
                  });
                });
              }
            }
          });

          if (bulkSaveScoutExperienceActions.length > 0) {
            pendingActions.push((callback) => {
              async.parallel(
                bulkSaveScoutExperienceActions,
                (asyncError, results) => {
                  scout.experiences = [];
                  results.forEach((result) => {
                    scout.experiences.push(result._id);
                  });
                  callback(asyncError, results);
                }
              );
            });
          }
          break;
        case 'branch':
        case 'subbranch':
        case 'group':
          if (req.body[field] !== '') {
            scout[field] = ObjectId(req.body[field]);
          }
          break;
        case 'documents':
          let documents = [];
          req.body[field].forEach((scoutDocumentData) => {
            let data = Object.assign({}, scoutDocumentData);
            if (data.stored_file && data.stored_file !== '') {
              data.stored_file = ObjectId(scoutDocumentData.stored_file);

              StoredFile
                .findOne({ _id: scoutDocumentData.stored_file })
                .exec((findError, storedFile) => {
                  if (findError) {
                    this._logger.error('Error while finding StoredFile data.', findError);
                  }
                  else {
                    storedFile.scout = scout._id;
                    storedFile.subbranch = scout.subbranch;
                    storedFile.active = true;
                    storedFile.modified = new Date();

                    storedFile.save((saveError) => {
                      if (saveError) {
                        this._logger.error('Error while updating StoredFile data.', saveError);
                      }
                    });
                  }
                });
            }
            else {
              data.stored_file = undefined;
            }
            documents.push(data);
          });
          scout[field] = documents;
          break;
        default:
          scout[field] = req.body[field];
      }
    });

    if (pendingActions.length > 0) {
      async.parallel(
        pendingActions,
        (asyncError, results) => {
          if (asyncError) {
            this._logger.error('Something happened while executing parallel actions to save Scout data.', asyncError);
          }
          else {
            callback(asyncError, results);
          }
        }
      );
    }
    else {
      callback(null, null);
    }
  }

  _sanitizeSubmittedData(req) {
    Object.keys(req.body).forEach((field) => {
      switch (field) {
        case '_id':
        case '__v':
          delete req.body[field];
          break;
        case 'contact':
        case 'parent_contact':
        case 'type':
        case 'subtype':
        case 'participant_role':
        case 'group':
          if (req.body[field] === '') {
            delete req.body[field];
          }
          break;
      }
    });
  }

  index(req, res) {
    let conditions = {};
    let $andConditions = [];
    let $orConditions = [];

    Object.keys(req.query).forEach((field) => {
      switch (field) {
        case 'name':
          $orConditions.push({
            [field]: new RegExp(req.query[field], 'i')
          });
          break;
        case 'branch':
        case 'subbranch':
          conditions[field] = ObjectId(req.query[field]);
          break;
        case 'nta':
          conditions[field] = field;
          break;
        case 'type':
        case 'sub_type':
        case 'participant_role':
        case 'committee_role':
        case 'committee_role_detail':
        case 'committee_role_extra':
          const splitParams = req.query[field].split(',');
          if (splitParams.length === 1) {
            conditions[field] = req.query[field];
          }
          else {
            splitParams.forEach((param) => {
              $orConditions.push({
                [field]: param
              });
            });
          }
          break;
        default:
          break;
      }
    });

    if ($andConditions.length > 0) {
      conditions['$and'] = $andConditions;
    }

    if ($orConditions.length > 0) {
      conditions['$or'] = $orConditions;
    }

    let query = Scout.find(conditions);

    if (req.query['_populate']) {
      query.populate('branch subbranch contact emergency_contacts stored_files');
    }

    if (req.query['_limit']) {
      query.limit(parseInt(req.query['_limit']));
    }

    query.sort({
      created: -1
    });

    query.exec((error, docs) => {
      this._logger.info('Incoming query.', conditions);
      if (error) {
        this._logger.error('Error while finding data.', error);
        return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengambil data anggota pramuka.', {}, error);
      }
      else {
        console.log(docs);
        const additionalData = {
          next: docs.length > 0 ? '/api/scouts?' + Object.keys(req.query).map(key => key + '=' + req.query[key]).join('&') + '&_since=' + docs[docs.length - 1].created : ''
        };
        return new JsonApiResponse(res, OK, 'Data anggota pramuka.', docs).setAdditionalData(additionalData).serve();
      }
    });
  }

  filter_nta(req, res) {
    Scout.find({nta: new RegExp('^' + req.params.nta, 'i' )},function(error, docs){
      if (error) return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengambil data anggota pramuka.', {}, error);
      else return new JsonApiResponse(res, OK, 'Data anggota pramuka.', docs).serve();
    });
  }

  filter_type(req, res) {
    Scout.find({type: req.params.type},function(error, docs){
      if (error) return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengambil data anggota pramuka.', {}, error);
      else return new JsonApiResponse(res, OK, 'Data anggota pramuka.', docs).serve();
    });
  }

  filter_branch(req, res) {
    Scout.find({branch:req.params.branch},function(error, docs){
      if (error) return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengambil data anggota pramuka.', {}, error);
      else return new JsonApiResponse(res, OK, 'Data anggota pramuka.', docs).serve();
    });
  }

  filter_branch_group(req, res) {
    Scout.find({branch: req.params.branch, group: req.params.group},function(error, docs){
      if (error) return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengambil data anggota pramuka.', {}, error);
      else return new JsonApiResponse(res, OK, 'Data anggota pramuka.', docs).serve();
    });
  }

  filter_group(req, res) {
    Scout.find({group:req.params.group},function(error, docs){
      if (error) return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengambil data anggota pramuka.', {}, error).serve();
      else return new JsonApiResponse(res, OK, 'Data anggota pramuka.', docs).serve();
    });
  }

  create(req, res) {
    Scout
      .findOne({ nta: req.body.nta || '' })
      .exec((findError, existingScout) => {
        if (findError) {
          this._logger.error('Error while saving scout data.', error);
          return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal melakukan penyimpanan data anggota pramuka.', {}, error).serve();
        }
        else {
          console.log(existingScout);
          if (existingScout) {
            return new JsonApiResponse(res, INVALID, 'Anggota dengan NTA ' + existingScout.nta + ' sebelumnya telah disimpan di dalam sistem. Mohon cek kembali data Anda.', {}).serve();
          }
          else {
            let scout = new Scout();

            this._sanitizeSubmittedData(req);
            this._prepareSavedData(req, scout, () => {
              if (!scout.branch) {
                scout.branch = req.user.branch;
              }

              scout.created = new Date();

              scout.save((error) => {
                if (error) {
                  this._logger.error('Error while saving scout data.', error);
                  return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal melakukan penyimpanan data anggota pramuka.', {}, error).serve();
                }
                else {
                  ScoutBranch
                    .findOne({ _id: scout.branch })
                    .exec((findError, branch) => {
                      if (findError) {
                        this._logger.error('Error while finding branch stats data.', findError);
                      }
                      else {
                        if (branch.stats) {
                          branch.stats.numberOfScouts++;
                          branch.save();
                        }
                      }
                    });

                  ScoutBranch
                    .findOne({ _id: scout.subbranch })
                    .exec((findError, subbranch) => {
                      if (findError) {
                        this._logger.error('Error while finding subbranch stats data.', findError);
                      }
                      else {
                        if (!subbranch) {
                          this._logger.error('Can\'t find subbranch data. This is not expected.', subbranch)
                        }
                        else if (!subbranch.stats) {
                          subbranch.stats = {
                            numberOfScouts: 1
                          };
                          subbranch.save();
                        }
                        else {
                          subbranch.stats.numberOfScouts++;
                          subbranch.save();
                        }
                      }
                    });

                  scout.documents.forEach((file) => {
                    StoredFile
                      .findOne({ _id: file._id })
                      .exec((findError, storedFile) => {
                        if (findError) {
                          this._logger.error('Error while finding file data for branch data update.', findError);
                        }
                        else {
                          storedFile.branch = scout.branch;
                          storedFile.subbranch = scout.subbranch;
                          storedFile.scout = scout._id;
                        }
                      });
                  });

                  return new JsonApiResponse(res, OK, 'Data kepramukaan berhasil disimpan.', scout).serve();
                }
              });
            });
          }
        }
      });
  }

  read(req, res) {
    Scout.findById(req.params.scoutId, function(error, doc){
      if (error) return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengambil data anggota pramuka '+req.params.scoutId+'.', {}, error);
      else return new JsonApiResponse(res, OK, 'Data anggota pramuka '+req.params.scoutId+'.', doc).serve();
    });
  }

  update(req, res) {
    this._sanitizeSubmittedData(req);
    Scout
      .findOne({ _id: req.params.id })
      .exec((findError, scout) => {
        if (findError) {
          this._logger.error('Error while finding Scout data.', findError);
          return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengubah data anggota pramuka ' + req.params.id + '.', {}, findError).serve();
        }
        else {
          this._prepareSavedData(req, scout, (error) => {
            if (error) {
              this._logger.error('Something happened after preparing saved data.', error);
              return new JsonApiResponse(res, INVALID, 'Gagal mengubah data anggota pramuka '+ req.params.id + '.', {}, error).serve();
            }
            else {
              scout.modified = new Date();

              scout.save((saveError) => {
                if (saveError) {
                  this._logger.error('Error while saving scout data..', saveError);
                  return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengubah data anggota pramuka.', {}, error).serve();
                }
                else {
                  return new JsonApiResponse(res, OK, 'Berhasil.', scout).serve();
                }
              });
            }
          });
        }
      });
  }

  deleteById(req, res) {
    this._findScoutExistenceOrError(req, res, (scout) => {
      if (['admin', 'super-admin'].indexOf(req.user.role) === -1 && scout.branch.toString() !== req.user.branch._id) {
        return new JsonApiResponse(res, UNAUTHORIZED, 'Anda tidak diizinkan untuk menghapus data ini.', scout).serve();
      }
      else {
        Scout.remove({ _id: scout._id }, (removeError) => {
          if (removeError) {
            this._logger.error('Error while deleting Scout data.', error);
            return new JsonApiResponse(res, INVALID, 'Gagal menghapus data anggota pramuka. Cobalah beberapa saat lagi.', {}, error).serve();
          }
          else {
            Contact.remove({ scout: scout._id }, (removeError) => {
              if (removeError) {
                this._logger.error('Error while deleting unused Contact data.', removeError);
              }
            });

            ScoutExperience.remove({ scout: scout._id }, (removeError) => {
              if (removeError) {
                this._logger.error('Error while deleting unused ScoutExperience data.', removeError);
              }
            });

            ScoutBranch
              .findOne({ _id: scout.branch })
              .exec((findError, scoutBranch) => {
                if (findError) {
                  this._logger.error('Error while finding ScoutBranch (branch) after deleting Scout.', findError);
                }
                else {
                  if (!scoutBranch) {
                    this._logger.error('Can\'t find ScoutBranch (branch) data. No stats updated.')
                  }
                  else {
                    scoutBranch.stats.numberOfScouts--;
                    scoutBranch.save((saveError) => {
                      if (saveError) {
                        this._logger.error('Error while updating ScoutBranch (branch) stats after deleting Scout.', saveError);
                      }
                    });
                  }
                }
              });

            ScoutBranch
              .findOne({ _id: scout.subbranch })
              .exec((findError, scoutBranch) => {
                if (findError) {
                  this._logger.error('Error while finding ScoutBranch (subbranch) after deleting Scout.', findError);
                }
                else {
                  if (!scoutBranch) {
                    this._logger.error('Can\'t find ScoutBranch (subbranch) data. No stats updated.');
                  }
                  else {
                    scoutBranch.stats.numberOfScouts--;
                    scoutBranch.save((saveError) => {
                      if (saveError) {
                        this._logger.error('Error while updating ScoutBranch (subbranch) stats after deleting Scout.', saveError);
                      }
                    });
                  }
                }
              });

            return new JsonApiResponse(res, OK, 'Berhasil.', scout).serve();
          }
        });
      }
    });
  }

  branch_stats(req, res) {
    Scout
        .find()
        .populate('branch')
        .sort({ 'branch.name': 1 })
        .exec(function(err,scouts) {
          var scoutsByBranch = _.groupBy(scouts, function(num){ return num.branch._id; });
          var scoutsStatByBranch = _.map(scoutsByBranch, function(branchScouts){
            var stats = _.countBy(branchScouts, function(num) {
              return num.type
            });

            stats.branch = branchScouts[0].branch;
            return stats;
          });

          if (err) return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengambil data anggota pramuka.', {}, err);
          else return new JsonApiResponse(res, OK, 'Data anggota pramuka.', scoutsStatByBranch).serve();
        });
  }

  generateIdCards(req, res) {
    Scout
        .find({})
        .skip(parseInt(req.params.skip))
        .limit(parseInt(req.params.limit))
        .populate('avatar branch subbranch contact contact.province contact.regency_city')
        .exec(function(error, docs){
      if (error) return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengambil data anggota pramuka '+req.params.scoutId+'.', {}, error);
      else {

        // Silahkan disesuaikan sesuai konfigurasi masing2
        var rootFolder = '/Users/arkkadhiratara/Workspaces/jamnas2016/';
        var fileFolder = '/Users/arkkadhiratara/Downloads/files/';

        // var rootFolder = '/home/emanuelcahyono/tools/node-app/jamnas2016/';
        // var fileFolder = '/home/emanuelcahyono/Documents/files/';

        var Canvas = require('canvas')
            , Image = Canvas.Image
            , fs = require('fs')
            , qrcode = require('qrcode');

        _.each(docs,function(doc,index,list){
          console.log(doc._id);
          // Draw Canvas
          var fileName = "";
          var type = "";
          var canvas = new Canvas(464,669);
          var ctx = canvas.getContext('2d')

          // 'participant', 'leader-cmt', 'leader-dct', 'leader-staff', 'coach', 'committee', 'guest', 'others'

          // Assign camp color
          // Use indexOf to avoid typo
          if(doc.subbranch.camp_name.indexOf('TJILIK') >= 0) {
            ctx.fillStyle = '#54a5f7'; // biru laut
            fileName = doc.branch.code + "_" + doc.subbranch.code + "_tjilik_" +  doc.name.replace(/ /g,"_").replace(/\./g,"").replace(/,/g,"") + "_" + doc._id;
          } else if (doc.subbranch.camp_name.indexOf('IMAM') >= 0) {
            ctx.fillStyle = '#63CCC5'; // hijau tosca
            fileName = doc.branch.code + "_" + doc.subbranch.code + "_imam_" +  doc.name.replace(/ /g,"_").replace(/\./g,"").replace(/,/g,"") + "_" + doc._id;
          } else if (doc.subbranch.camp_name.indexOf('HASANUDIN') >= 0){
            ctx.fillStyle = '#E31A1A'; // merah
            fileName = doc.branch.code + "_" + doc.subbranch.code + "_hasanudin_" +  doc.name.replace(/ /g,"_").replace(/\./g,"").replace(/,/g,"") + "_" + doc._id;
          } else if (doc.subbranch.camp_name.indexOf('DIPONEGORO') >= 0){
            ctx.fillStyle = '#CF8B59'; // coklat
            fileName = doc.branch.code + "_" + doc.subbranch.code + "_diponegoro_" +  doc.name.replace(/ /g,"_").replace(/\./g,"").replace(/,/g,"") + "_" + doc._id;
          } else {
            ctx.fillStyle = '#FFFFFF';
            fileName = doc.branch.code + "_" + doc.subbranch.code + "_other_" +  doc.name.replace(/ /g,"_").replace(/\./g,"").replace(/,/g,"") + "_" + doc._id;
          }

          switch(doc.type) {

            case 'coach':
              type = "BINDAMPING ";
              fileName = "peserta_bindamping_" + fileName;
              break;

            case 'participant':
              type = "   PESERTA   ";
              fileName = "peserta_" + fileName;
              break;

            case 'leader-cmt':
              ctx.fillStyle = '#ffef0a'; //Pinkonda - kuning
              type = "  PINKONDA   ";
              fileName = "pinkonda_" + fileName;
              break;

            case 'leader-staff':
              ctx.fillStyle = '#ffef0a';
              type = "STAF KONDA  ";
              fileName = "stafkonda_" + fileName;
              break;

            case 'leader-dct':
              ctx.fillStyle = '#0820B0'; // Pinkoncab - biru gelap
              type = " PINKONCAB  ";
              fileName = "pinkoncab_" + fileName;
              break;

            case 'committee':
              ctx.fillStyle = '#cea7ce'; // Panitia
              type = "   PANITIA   ";

              // TODO: Division
              fileName = "panitia_committee_" +  doc.name.replace(/ /g,"_").replace(/\./g,"").replace(/,/g,"") + "_" + doc._id;
              break;

            case 'guest':
              ctx.fillStyle = '#d9dadb'; // Tamu
              type = "TAMU UNDANGAN";
              fileName = "tamu_" +  doc.name.replace(/ /g,"_").replace(/\./g,"").replace(/,/g,"") + "_" + doc._id;
              break;

            default:
              ctx.fillStyle = '#FFF';
              fileName = "other_" +  doc.name.replace(/ /g,"_").replace(/\./g,"").replace(/,/g,"") + "_" + doc._id;
              break;
          }


          ctx.fillRect(0,0,464,669);   // Draw a rectangle with default settings
          ctx.save();

          // Draw card
          var img = new Image()
          img.src = fs.readFileSync(rootFolder + 'public/assets/images/card.png')
          ctx.drawImage(img, 0, 0, img.width, img.height)
          //ctx.drawImage(img, 30, 16, img.width * 0.225, img.height * 0.225);
          //ctx.drawImage(img, 30, 16, img.width, img.height);

          // Draw QR Code
          //var qrdata = { _id: doc._id, nta: doc.nta, name: doc.name, sex: doc.sex, type: doc.type};
          //var qrtext = JSON.stringify(qrdata);

          qrcode.draw(doc.id, function(err,canvas){
            var img = new Image;
            img.src = canvas.toBuffer();
            ctx.drawImage(img, 275, 50, 150, 150);
          });

          if(typeof doc.code !== 'undefined') {
            ctx.fillStyle = '#000';
            ctx.font = 'normal normal 10px Courier';

            var te = ctx.measureText(doc.code);
            ctx.fillText(doc.code, 350 - te.width/2, 195);
          }

          // Draw Photo
          ctx.fillStyle = '#fff';
          ctx.fillRect(148,218,164,204);   // Draw a rectangle with default settings

          if(typeof doc.avatar !== 'undefined') {
            var img = new Image();

            try {
              img.src = fs.readFileSync(fileFolder + doc.avatar.raw_data.filename);
              ctx.drawImage(img, 150, 220, 160, 200);
            } catch (err) {
              console.log(err);
            }

          }

          ctx.fillStyle = '#000';
          // Draw type text
          ctx.font = 'bold 40px Helvetica';
          ctx.fillText(type, 100, 475);

          // Draw Name - #1 Row
          ctx.font = 'normal normal 20px Helvetica';
          ctx.fillText(doc.name, 45, 530);
          //ctx.fillText(doc.branch.name + " / " + doc.subbranch.name, 45, 575)


          // Draw #2 Row
          if(doc.type == 'leader-cmt' || doc.type == 'leader-staff') {
            ctx.fillText(doc.branch.name.replace("Kwarda ",""), 45, 575);
          } else if(doc.type == 'committee') {
            ctx.fillText(doc.committee_role, 45, 575);
          } else if(doc.type == 'guest') {
            ctx.fillText(doc.contact.province.name + " / " + doc.contact.regency_city.name  , 45, 575);
          } else {
            ctx.fillText(doc.branch.name.replace("Kwarda ","") + " / " + doc.subbranch.name.replace("Kwarcab ",""), 45, 575);
          }

          //ctx.fillText(doc.subbranch.camp_name + "[ " + doc.subbranch.camp_code + '/' + doc.subbranch.camp_subcode + " ]", 45, 620)


          // Draw CAMP NAME - #3 Row
          if(doc.type == 'participant' || doc.type == 'coach' || doc.type == 'others') {
            var camp_name = doc.subbranch.camp_name.split(' & ');
            if(doc.sex == 'male') camp_name = camp_name[0];
            else camp_name = camp_name[1];
            ctx.fillText(camp_name, 45, 620);
          } else if (doc.type == 'committee') {
            ctx.fillText(doc.committee_role_detail, 45, 620);
          } else if(doc.type == 'guest') {
            ctx.fillText(moment(doc.created).format('DD/MM/YYYY'), 45, 620);
          }



          // SAVE EVERYTHING
          var out = fs.createWriteStream(rootFolder + 'idcards/' + fileName + '.png')
              , stream = canvas.createPNGStream();

          stream.on('data', function(chunk){
            out.write(chunk);
          });
        });

        return new JsonApiResponse(res, OK, 'Generate ID Card pramuka sukses', docs.length).serve();
      }
    });


  }
  
  

  generateIdCard(req, res) {
    Scout
        .findById(req.params.scoutId)
        .populate('avatar branch subbranch contact contact.province contact.regency_city')
        .exec(function(error, doc){
          if (error) return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengambil data anggota pramuka '+req.params.scoutId+'.', {}, error);
          else {

            // Silahkan disesuaikan sesuai konfigurasi masing2
            var rootFolder = '/Users/arkkadhiratara/Workspaces/jamnas2016/';
            var fileFolder = '/Users/arkkadhiratara/Downloads/files/';

            var Canvas = require('canvas')
                , Image = Canvas.Image
                , fs = require('fs')
                , qrcode = require('qrcode');


            console.log(doc._id);
            // Draw Canvas
            var fileName = "";
            var type = "";
            var canvas = new Canvas(464,669);
            var ctx = canvas.getContext('2d')

            // 'participant', 'leader-cmt', 'leader-dct', 'leader-staff', 'coach', 'committee', 'guest', 'others'

            // Assign camp color
            // Use indexOf to avoid typo
            if(doc.subbranch.camp_name.indexOf('TJILIK') >= 0) {
              ctx.fillStyle = '#54a5f7'; // biru laut
              fileName = doc.branch.code + "_" + doc.subbranch.code + "_tjilik_" +  doc.name.replace(/ /g,"_").replace(/\./g,"").replace(/,/g,"") + "_" + doc._id;
            } else if (doc.subbranch.camp_name.indexOf('IMAM') >= 0) {
              ctx.fillStyle = '#63CCC5'; // hijau tosca
              fileName = doc.branch.code + "_" + doc.subbranch.code + "_imam_" +  doc.name.replace(/ /g,"_").replace(/\./g,"").replace(/,/g,"") + "_" + doc._id;
            } else if (doc.subbranch.camp_name.indexOf('HASANUDIN') >= 0){
              ctx.fillStyle = '#FF0000'; // merah
              fileName = doc.branch.code + "_" + doc.subbranch.code + "_hasanudin_" +  doc.name.replace(/ /g,"_").replace(/\./g,"").replace(/,/g,"") + "_" + doc._id;
            } else if (doc.subbranch.camp_name.indexOf('DIPONEGORO') >= 0){
              ctx.fillStyle = '#CF8B59'; // coklat
              fileName = doc.branch.code + "_" + doc.subbranch.code + "_diponegoro_" +  doc.name.replace(/ /g,"_").replace(/\./g,"").replace(/,/g,"") + "_" + doc._id;
            } else {
              ctx.fillStyle = '#FFFFFF';
              fileName = doc.branch.code + "_" + doc.subbranch.code + "_other_" +  doc.name.replace(/ /g,"_").replace(/\./g,"").replace(/,/g,"") + "_" + doc._id;
            }

            switch(doc.type) {

              case 'coach':
                type = " BINDAMPING ";
                fileName = "peserta_bindamping_" + fileName;
                break;

              case 'participant':
                type = "   PESERTA   ";
                fileName = "peserta_" + fileName;
                break;

              case 'leader-cmt':
                ctx.fillStyle = '#ffef0a'; //Pinkonda - kuning
                type = "  PINKONDA   ";
                fileName = "pinkonda_" + fileName;
                break;

              case 'leader-dct':
                ctx.fillStyle = '#0068c9'; // Pinkoncab - biru gelap
                type = "  PINKONCAB  ";
                fileName = "pinkoncab_" + fileName;
                break;

              case 'committee':
                ctx.fillStyle = '#cea7ce'; // Panitia
                type = "   PANITIA   ";

                // TODO: Division
                fileName = "panitia_committee_" +  doc.name.replace(/ /g,"_").replace(/\./g,"").replace(/,/g,"") + "_" + doc._id;
                break;

              case 'guest':
                ctx.fillStyle = '#d9dadb'; // Tamu
                type = "TAMU UNDANGAN";
                fileName = "tamu_" +  doc.name.replace(/ /g,"_").replace(/\./g,"").replace(/,/g,"") + "_" + doc._id;
                break;

              default:
                ctx.fillStyle = '#FFF';
                fileName = "other_" +  doc.name.replace(/ /g,"_").replace(/\./g,"").replace(/,/g,"") + "_" + doc._id;
                break;
            }


            ctx.fillRect(0,0,464,669);   // Draw a rectangle with default settings
            ctx.save();

            // Draw card
            var img = new Image()
            img.src = fs.readFileSync(rootFolder + 'public/assets/images/card.png')
            ctx.drawImage(img, 0, 0, img.width, img.height)
            //ctx.drawImage(img, 30, 16, img.width * 0.225, img.height * 0.225);
            //ctx.drawImage(img, 30, 16, img.width, img.height);

            // Draw QR Code
            //var qrdata = { _id: doc._id, nta: doc.nta, name: doc.name, sex: doc.sex, type: doc.type};
            //var qrtext = JSON.stringify(qrdata);

            qrcode.draw(doc.id, function(err,canvas){
              var img = new Image;
              img.src = canvas.toBuffer();
              ctx.drawImage(img, 275, 50, 150, 150);
            });

            if(typeof doc.code !== 'undefined') {
              ctx.fillStyle = '#000';
              ctx.font = 'normal normal 10px Courier';

              var te = ctx.measureText(doc.code);
              ctx.fillText(doc.code, 350 - te.width/2, 195);
            }

            // Draw Photo
            ctx.fillStyle = '#fff';
            ctx.fillRect(148,218,164,204);   // Draw a rectangle with default settings

            if(typeof doc.avatar !== 'undefined') {
              var img = new Image();

              try {
                img.src = fs.readFileSync(fileFolder + doc.avatar.raw_data.filename);
                ctx.drawImage(img, 150, 220, 160, 200);
              } catch (err) {
                console.log(err);
              }

            }

            ctx.fillStyle = '#000';
            // Draw type text
            ctx.font = 'bold 40px Helvetica';
            ctx.fillText(type, 100, 475);

            // Draw Name - #1 Row
            ctx.font = 'normal normal 20px Helvetica';
            ctx.fillText(doc.name, 45, 530);
            //ctx.fillText(doc.branch.name + " / " + doc.subbranch.name, 45, 575)


            // Draw #2 Row
            if(doc.type == 'leader-cmt' || doc.type == 'leader-staff') {
              ctx.fillText(doc.branch.name.replace("Kwarda ",""), 45, 575);
            } else if(doc.type == 'committee') {
              ctx.fillText(doc.committee_role, 45, 575);
            } else if(doc.type == 'guest') {
              ctx.fillText(doc.contact.province.name + " / " + doc.contact.regency_city.name  , 45, 575);
            } else {
              ctx.fillText(doc.branch.name.replace("Kwarda ","") + " / " + doc.subbranch.name.replace("Kwarcab ",""), 45, 575);
            }

            //ctx.fillText(doc.subbranch.camp_name + "[ " + doc.subbranch.camp_code + '/' + doc.subbranch.camp_subcode + " ]", 45, 620)


            // Draw CAMP NAME - #3 Row
            if(doc.type == 'participant' || doc.type == 'coach' || doc.type == 'others') {
              var camp_name = doc.subbranch.camp_name.split(' & ');
              if(doc.sex == 'male') camp_name = camp_name[0];
              else camp_name = camp_name[1];
              ctx.fillText(camp_name, 45, 620);
            } else if (doc.type == 'committee') {
              ctx.fillText(doc.committee_role_detail, 45, 620);
            } else if(doc.type == 'guest') {
              ctx.fillText(moment(doc.created).format('DD/MM/YYYY'), 45, 620);
            }



            // SAVE EVERYTHING
            var out = fs.createWriteStream(rootFolder + 'idcards/' + fileName + '.png')
                , stream = canvas.createPNGStream();

            stream.on('data', function(chunk){
              out.write(chunk);
            });

            var file = rootFolder + 'idcards/' + fileName + '.png';

            var filename = path.basename(file);
            var mimetype = mime.lookup(file);

            res.setHeader('Content-disposition', 'attachment; filename=' + filename);
            res.setHeader('Content-type', mimetype);

            var filestream = fs.createReadStream(file);
            filestream.pipe(res);

            //return new JsonApiResponse(res, OK, 'Generate ID Card pramuka sukses', doc._id).serve();
          }
        });


  }
}
