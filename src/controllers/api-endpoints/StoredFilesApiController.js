import { INTERNAL_SERVER_ERROR, INVALID, OK } from '../../lib/constants/HttpStatus';

import BaseController from '../../lib/BaseController';
import JsonApiResponse from '../../lib/JsonApiResponse';

import StoredFile from '../../models/StoredFile';

export default class StoredFilesApiController extends BaseController {
  index(req, res) {
    return new JsonApiResponse(res, OK, message, data).serve();
  }

  getById(req, res) {
    StoredFile
      .findOne({ _id: req.params.id })
      .exec((findError, storedFile) => {
        if (findError) {
          this._logger.error('Error while finding StoredFile data.', findError);

        }
        else {
          if (!storedFile) {

          }
          else {

          }
        }
      });
  }

  updateById(req, res) {
    StoredFile
      .findOne({ _id: req.params.id })
      .exec((findError, storedFile) => {
        if (findError) {
          this._logger.error('Error while updating StoredFile data.', findError);
        }
        else {
          if (!storedFile) {

          }
          else {
            Object.keys(req.body).forEach((field) => {
              switch (field) {
                case 'category':
                case 'notes':
                  storedFile[field] = req.body[field];
                  break;
                default:
                  break;
              }
            });
          }
        }
      });
  }

}
