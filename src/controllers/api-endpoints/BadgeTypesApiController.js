import { INTERNAL_SERVER_ERROR, INVALID, OK } from '../../lib/constants/HttpStatus';

import BaseController from '../../lib/BaseController';
import JsonApiResponse from '../../lib/JsonApiResponse';

import BadgeType from '../../models/BadgeType';

export default class BadgeTypesApiController extends BaseController {
  index(req, res) {
    let conditions = {};

    Object.keys(req.query).forEach((field) => {
      switch (field) {
        case 'name':
        case 'unique_name':
          conditions[field] = new RegExp(req.query[field], 'i');
          break;
        case 'grade':
          conditions[field] = req.query[field];
          break;
        default:
          break;
      }
    });

    BadgeType
      .find(conditions)
      .limit(10)
      .exec((findError, badgeTypes) => {
        if (findError) {
          this._logger.error('Error while finding BadgeType data.', findError);
          return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengambil data tanda kecapakapan khusus.', null, findError).serve();
        }
        else {
          return new JsonApiResponse(res, OK, 'Sukses.', badgeTypes).serve();
        }
      });
  }
}
