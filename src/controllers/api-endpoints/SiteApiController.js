import { OK } from '../../lib/constants/HttpStatus';

import BaseController from '../../lib/BaseController';
import JsonApiResponse from '../../lib/JsonApiResponse';

export default class SiteApiController extends BaseController {
  index(req, res) {
    let data = {
      information: 'Situs Resmi JAMNAS 2016.'
    };
    let message = 'Hello world!';
    return new JsonApiResponse(res, OK, message, data).serve();
  }
}
