import { OK, UNAUTHORIZED, INTERNAL_SERVER_ERROR } from '../../lib/constants/HttpStatus';

import BaseController from '../../lib/BaseController';
import JsonApiResponse from '../../lib/JsonApiResponse';

import BranchTransport from '../../models/BranchTransport';
import StoredFile from '../../models/StoredFile';

const getFileExtension = (file) => {
  const fileNameSegments = file.originalname.split('.');
  return fileNameSegments[fileNameSegments.length - 1];
};

export default class BranchTransportsApiController extends BaseController {
  index(req, res) {

    let query = BranchTransport.find({});

    if (req.query['_populate'] + '' === 'true') {
      query
        .populate('branch')
        .populate('province')
        .populate('regency_city');
    }

    query.exec((error, docs) => {
      if (error) {
        return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengambil data transportasi.', null, error).serve();
      }
      else {
        return new JsonApiResponse(res, OK, 'Data transportasi.', docs).serve();
      }
    });

  }

  filter_branch(req, res) {
    BranchTransport.find({branch:req.params.branch},function(error, docs){
      if (error) return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengambil data transportasi.', {}, error);
      else return new JsonApiResponse(res, OK, 'Data transportasi.', docs).serve();
    });
  }

  create(req, res) {
    console.log(req.body);
    console.log(req.file);
    if(!req.file) {
      // TODO: Kalau gak ada file nya?
      return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengunggah konfirmasi pembayaran kwartir.', {}, {}).serve();
    }
    else {
      let file = new StoredFile();

      file.extension = getFileExtension(req.file);
      file.category = 'transport';
      file.topic = 'branch-documents';

      file.raw_data = req.file;

      file.user = req.user._id;
      file.branch = req.user.branch._id;

      file.markModified('raw_data');

      file.save((saveError) => {
        if (saveError) {
          // Simpan file gagal
          return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal melakukan penyimpanan konfirmasi pembayaran kwartir.', {}, saveError).serve();
        }
        else {
          // Berhasil
          let transport = new BranchTransport(req.body);

          transport.branch = req.user.branch._id;
          transport.created = new Date().toISOString();
          transport.file = file;

          transport.save((saveError, doc) => {
            if (saveError) {
              //this._logger.error('Error on saving WillingnessLetter data.', saveError);
              return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal melakukan penyimpanan konfirmasi pembayaran kwartir.', {}, saveError).serve();
            }
            else {
              return new JsonApiResponse(res, OK, 'Sukses.', doc).serve();
            }
          });

        }
      });

    }
  }

  read(req, res) {
    BranchTransport.findById(req.params.transportId, function(error, doc){
      if (error) return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengambil data transportasi '+req.params.transportId+'.', {}, error);
      else return new JsonApiResponse(res, OK, 'Data transportasi '+req.params.transportId+'.', doc).serve();
    });

  }

  update(req, res) {
    if(typeof req.body.file !== 'undefined' && typeof req.body.file.name === 'undefined' ) delete req.body.file;
    BranchTransport.update({ _id: req.params.transportId }, req.body, function(error, num) {
      if (error) return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengubah data transportasi '+req.params.transportId+'.', {}, error);
      else return new JsonApiResponse(res, OK, 'Data transportasi '+req.params.transportId+' berhasil diperbaharui.', req.params.transportId).serve();
    });
  }
};
