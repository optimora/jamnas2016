import _ from 'underscore';
import moment from 'moment';

import { OK, UNAUTHORIZED, INTERNAL_SERVER_ERROR } from '../../lib/constants/HttpStatus';

import BaseController from '../../lib/BaseController';
import JsonApiResponse from '../../lib/JsonApiResponse';

import Scout from '../../models/Scout';
import ScoutBranch from '../../models/ScoutBranch';

export default class ScoutBranchesApiController extends BaseController {
  index(req, res) {
    ScoutBranch
      .find({ parent: null })
      .exec((error, docs) => {
        if (error) {
          this._logger.error('Error on finding ScoutBranch data.', error);
          return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengambil data kwratir.', {}, error).serve();
        }
        else {
          let results = [];
          docs.forEach((doc) => {
            if (req.user.magic_role && req.user.magic_role !== '' && doc.name.indexOf('(pura-puranya)') !== -1) {
              results.push(doc.getSanitizedObject());
            }
            else {
              if (doc.name.indexOf('(pura-puranya)') === -1) {
                results.push(doc.getSanitizedObject());
              }
            }
          });
          return new JsonApiResponse(res, OK, 'Data Kwartir Cabang berhasil diambil.', results).serve();
        }
    });
  }

  getChildren(req, res) {
    ScoutBranch
      .find({ parent: req.params.id })
      .exec((error, branches) => {
        if (error) {
          this._logger.error('Error on finding ScoutBranch children data.', error);
          return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengambil data kwratir.', {}, error).serve();
        }
        else {
          let results = [];
          branches.forEach((branch) => {
            if (req.user.magic_role && req.user.magic_role !== '' && branch.name.indexOf('(pura-puranya)') !== -1) {
              results.push(branch.getSanitizedObject());
            }
            else {
              if (branch.name.indexOf('(pura-puranya)') === -1) {
                results.push(branch.getSanitizedObject());
              }
            }
          });
          return new JsonApiResponse(res, OK, 'Data Kwartir Cabang berhasil diambil.', results).serve();
        }
      });
  }

  create(req, res) {
    let branch = new ScoutBranch(req.body);

    branch.save(function(error) {
      if (error) {
        this._logger.error('Error on saving ScoutBranch data.', error);
        return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal melakukan penyimpanan data kwratir.', branch, error);
      }
      else {
        return new JsonApiResponse(res, OK, 'Data kwartir berhasil disimpan.', branch).serve();
      }
    });
  }

  read(req, res) {
    ScoutBranch.findById(req.params.branchId, function(error, doc) {
      if (error) {
        this._logger.error('Error while finding ScoutBranch data.', error);
        return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengambil data anggota kwartir ' + req.params.branchId + '.', {}, error);
      }
      else {
        return new JsonApiResponse(res, OK, 'Data kwartir ' + req.params.branchId + '.', doc.getSanitizedObject()).serve();
      }
    });

  }

  update(req, res) {
    ScoutBranch.update({ _id: req.params.branchId }, req.body.branch, function(error, num) {
      if (error) {
        this._logger.error('Error while updating ScoutBranch data.', error);
        return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengubah data kwartir '+req.params.branchId+'.', {}, error);
      }
      else {
        return new JsonApiResponse(res, OK, 'Data kwartir '+req.params.branchId+' berhasil diperbaharui.', req.body.branch).serve();
      }
    });
  }

  getScoutsByBranchId(req, res) {
    Scout
      .find({ branch: req.params.id })
      .exec((findError, scouts) => {
        if (findError) {
          this._logger.error('Error while finding Scout data.', findError);
          return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengambil data peserta '+req.params.id+'.', {}, error).serve();
        }
        else {
          return new JsonApiResponse(res, OK, 'Berhasil.', scouts).serve();
        }
      });
  }

  getUsersByBranchId(req, res) {

  }

  generateIdCards(req, res) {
    Scout
        .find({ branch: req.params.id })
        .skip(parseInt(req.params.skip))
        .limit(parseInt(req.params.limit))
        .populate('avatar branch subbranch contact contact.province contact.regency_city')
        .exec(function(error, docs){
          if (error) return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengambil data anggota pramuka '+req.params.scoutId+'.', {}, error);
          else {

            // Silahkan disesuaikan sesuai konfigurasi masing2
            var rootFolder = '/Users/arkkadhiratara/Workspaces/jamnas2016/';
            var fileFolder = '/Users/arkkadhiratara/Downloads/files/';

            // var rootFolder = '/home/emanuelcahyono/tools/node-app/jamnas2016/';
            // var fileFolder = '/home/emanuelcahyono/Documents/files/';

            var Canvas = require('canvas')
                , Image = Canvas.Image
                , fs = require('fs')
                , qrcode = require('qrcode');

            _.each(docs,function(doc,index,list){
              console.log(doc._id);
              // Draw Canvas
              var fileName = "";
              var type = "";
              var canvas = new Canvas(464,669);
              var ctx = canvas.getContext('2d')

              // 'participant', 'leader-cmt', 'leader-dct', 'leader-staff', 'coach', 'committee', 'guest', 'others'

              // Assign camp color
              // Use indexOf to avoid typo
              if(doc.subbranch.camp_name.indexOf('TJILIK') >= 0) {
                ctx.fillStyle = '#54a5f7'; // biru laut
                fileName = doc.branch.code + "_" + doc.subbranch.code + "_tjilik_" +  doc.name.replace(/ /g,"_").replace(/\./g,"").replace(/,/g,"") + "_" + doc._id;
              } else if (doc.subbranch.camp_name.indexOf('IMAM') >= 0) {
                ctx.fillStyle = '#63CCC5'; // hijau tosca
                fileName = doc.branch.code + "_" + doc.subbranch.code + "_imam_" +  doc.name.replace(/ /g,"_").replace(/\./g,"").replace(/,/g,"") + "_" + doc._id;
              } else if (doc.subbranch.camp_name.indexOf('HASANUDIN') >= 0){
                ctx.fillStyle = '#E31A1A'; // merah
                fileName = doc.branch.code + "_" + doc.subbranch.code + "_hasanudin_" +  doc.name.replace(/ /g,"_").replace(/\./g,"").replace(/,/g,"") + "_" + doc._id;
              } else if (doc.subbranch.camp_name.indexOf('DIPONEGORO') >= 0){
                ctx.fillStyle = '#CF8B59'; // coklat
                fileName = doc.branch.code + "_" + doc.subbranch.code + "_diponegoro_" +  doc.name.replace(/ /g,"_").replace(/\./g,"").replace(/,/g,"") + "_" + doc._id;
              } else {
                ctx.fillStyle = '#FFFFFF';
                fileName = doc.branch.code + "_" + doc.subbranch.code + "_other_" +  doc.name.replace(/ /g,"_").replace(/\./g,"").replace(/,/g,"") + "_" + doc._id;
              }

              switch(doc.type) {

                case 'coach':
                  type = "BINDAMPING ";
                  fileName = "peserta_bindamping_" + fileName;
                  break;

                case 'participant':
                  type = "   PESERTA   ";
                  fileName = "peserta_" + fileName;
                  break;

                case 'leader-cmt':
                  ctx.fillStyle = '#ffef0a'; //Pinkonda - kuning
                  type = "  PINKONDA   ";
                  fileName = "pinkonda_" + fileName;
                  break;

                case 'leader-staff':
                  ctx.fillStyle = '#ffef0a';
                  type = "STAF KONDA  ";
                  fileName = "stafkonda_" + fileName;
                  break;

                case 'leader-dct':
                  ctx.fillStyle = '#0820B0'; // Pinkoncab - biru gelap
                  type = " PINKONCAB  ";
                  fileName = "pinkoncab_" + fileName;
                  break;

                case 'committee':
                  ctx.fillStyle = '#cea7ce'; // Panitia
                  type = "   PANITIA   ";

                  // TODO: Division
                  fileName = "panitia_committee_" +  doc.name.replace(/ /g,"_").replace(/\./g,"").replace(/,/g,"") + "_" + doc._id;
                  break;

                case 'guest':
                  ctx.fillStyle = '#d9dadb'; // Tamu
                  type = "TAMU UNDANGAN";
                  fileName = "tamu_" +  doc.name.replace(/ /g,"_").replace(/\./g,"").replace(/,/g,"") + "_" + doc._id;
                  break;

                default:
                  ctx.fillStyle = '#FFF';
                  fileName = "other_" +  doc.name.replace(/ /g,"_").replace(/\./g,"").replace(/,/g,"") + "_" + doc._id;
                  break;
              }


              ctx.fillRect(0,0,464,669);   // Draw a rectangle with default settings
              ctx.save();

              // Draw card
              var img = new Image()
              img.src = fs.readFileSync(rootFolder + 'public/assets/images/card.png')
              ctx.drawImage(img, 0, 0, img.width, img.height)
              //ctx.drawImage(img, 30, 16, img.width * 0.225, img.height * 0.225);
              //ctx.drawImage(img, 30, 16, img.width, img.height);

              // Draw QR Code
              //var qrdata = { _id: doc._id, nta: doc.nta, name: doc.name, sex: doc.sex, type: doc.type};
              //var qrtext = JSON.stringify(qrdata);

              qrcode.draw(doc.id, function(err,canvas){
                var img = new Image;
                img.src = canvas.toBuffer();
                ctx.drawImage(img, 275, 50, 150, 150);
              });

              if(typeof doc.code !== 'undefined') {
                ctx.fillStyle = '#000';
                ctx.font = 'normal normal 10px Courier';

                var te = ctx.measureText(doc.code);
                ctx.fillText(doc.code, 350 - te.width/2, 195);
              }

              // Draw Photo
              ctx.fillStyle = '#fff';
              ctx.fillRect(148,218,164,204);   // Draw a rectangle with default settings

              if(typeof doc.avatar !== 'undefined') {
                var img = new Image();

                try {
                  img.src = fs.readFileSync(fileFolder + doc.avatar.raw_data.filename);
                  ctx.drawImage(img, 150, 220, 160, 200);
                } catch (err) {
                  console.log(err);
                }

              }

              ctx.fillStyle = '#000';
              // Draw type text
              ctx.font = 'bold 40px Helvetica';
              ctx.fillText(type, 100, 475);

              // Draw Name - #1 Row
              ctx.font = 'normal normal 20px Helvetica';
              ctx.fillText(doc.name, 45, 530);
              //ctx.fillText(doc.branch.name + " / " + doc.subbranch.name, 45, 575)


              // Draw #2 Row
              if(doc.type == 'leader-cmt' || doc.type == 'leader-staff') {
                ctx.fillText(doc.branch.name.replace("Kwarda ",""), 45, 575);
              } else if(doc.type == 'committee') {
                ctx.fillText(doc.committee_role, 45, 575);
              } else if(doc.type == 'guest') {
                ctx.fillText(doc.contact.province.name + " / " + doc.contact.regency_city.name  , 45, 575);
              } else {
                ctx.fillText(doc.branch.name.replace("Kwarda ","") + " / " + doc.subbranch.name.replace("Kwarcab ",""), 45, 575);
              }

              //ctx.fillText(doc.subbranch.camp_name + "[ " + doc.subbranch.camp_code + '/' + doc.subbranch.camp_subcode + " ]", 45, 620)


              // Draw CAMP NAME - #3 Row
              if(doc.type == 'participant' || doc.type == 'coach' || doc.type == 'others') {
                var camp_name = doc.subbranch.camp_name.split(' & ');
                if(doc.sex == 'male') camp_name = camp_name[0];
                else camp_name = camp_name[1];
                ctx.fillText(camp_name, 45, 620);
              } else if (doc.type == 'committee') {
                ctx.fillText(doc.committee_role_detail, 45, 620);
              } else if(doc.type == 'guest') {
                ctx.fillText(moment(doc.created).format('DD/MM/YYYY'), 45, 620);
              }



              // SAVE EVERYTHING
              var out = fs.createWriteStream(rootFolder + 'idcards/' + fileName + '.png')
                  , stream = canvas.createPNGStream();

              stream.on('data', function(chunk){
                out.write(chunk);
              });
            });

            return new JsonApiResponse(res, OK, 'Generate ID Card pramuka sukses', docs.length).serve();
          }
        });


  }
}
