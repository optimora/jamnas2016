import { OK, UNAUTHORIZED, INTERNAL_SERVER_ERROR } from '../../lib/constants/HttpStatus';

import BaseController from '../../lib/BaseController';
import JsonApiResponse from '../../lib/JsonApiResponse';

import SubBranchRegistration from '../../models/SubBranchRegistration';
import StoredFile from '../../models/StoredFile';

const getFileExtension = (file) => {
  const fileNameSegments = file.originalname.split('.');
  return fileNameSegments[fileNameSegments.length - 1];
};

export default class SubBranchRegistrationApiController extends BaseController {
  index(req, res) {
    SubBranchRegistration.find(function(error, docs){
      if (error) return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengambil data registrasi kwartir cabang.', {}, error);
      else return new JsonApiResponse(res, OK, 'Data registrasi kwartir.', docs).serve();
    });
  }

  create(req, res) {
    console.log(req.body);
    console.log(req.file);
    if(!req.file) {
      // TODO: Kalau gak ada file nya?
      return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengunggah konfirmasi pembayaran kwartir.', {}, {}).serve();
    }
    else {
      let file = new StoredFile();

      file.extension = getFileExtension(req.file);
      file.category = 'registration';
      file.topic = 'subbranch-documents';

      file.raw_data = req.file;

      file.user = req.user._id;
      file.branch = req.user.branch._id;

      file.markModified('raw_data');

      file.save((saveError) => {
        if (saveError) {
          // Simpan file gagal
          return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal melakukan penyimpanan konfirmasi pembayaran kwartir.', {}, saveError).serve();
        }
        else {
          // Berhasil
          let sbr = new SubBranchRegistration(req.body);
          sbr.branch = req.user.branch._id;
          sbr.created = new Date().toISOString();
          sbr.file = file;

          sbr.save((saveError, doc) => {
            if (saveError) {
              //this._logger.error('Error on saving WillingnessLetter data.', saveError);
              return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal melakukan penyimpanan konfirmasi pembayaran kwartir.', {}, saveError).serve();
            }
            else {
              return new JsonApiResponse(res, OK, 'Sukses.', doc).serve();
            }
          });
        }
      });

    }
  }

  read(req, res) {
    SubBranchRegistration.findById(req.params.sbrId, function(error, doc) {
      if (error) {
        //this._logger.error('Error while finding WillingnessLetter data.', error);
        return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengambil data registrasi kwartir cabang ' + req.params.sbrId + '.', {}, error);
      }
      else {
        return new JsonApiResponse(res, OK, 'Data registrasi kwartir cabang' + req.params.brId + '.', doc).serve();
      }
    });
  }

  filter_branch(req, res) {
    SubBranchRegistration
        .findOne({branch: req.params.branchId})
        .populate('branch')
        //.populate('participants.sub_branch')
        .exec(function(error, doc) {
          if (error) {
            //this._logger.error('Error while finding WillingnessLetter data.', error);
            return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengambil data registrasi kwartir cabang' + req.params.branchId + '.', {}, error);
          }
          else {
            return new JsonApiResponse(res, OK, 'Data kesediaan kwartir cabang' + req.params.branchId + '.', doc).serve();
          }
    });

  }

  filter_sub_branch(req, res) {
    SubBranchRegistration.findOne({subbranch: req.params.subBranchId}, function(error, doc) {
      if (error) {
        //this._logger.error('Error while finding WillingnessLetter data.', error);
        return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengambil data registrasi kwartir cabang' + req.params.subBranchId + '.', {}, error);
      }
      else {
        return new JsonApiResponse(res, OK, 'Data kesediaan kwartir ' + req.params.subBranchId + '.', doc).serve();
      }
    });

  }

  update(req, res) {
    if(typeof req.body.file !== 'undefined' && typeof req.body.file.name === 'undefined' ) delete req.body.file;

    SubBranchRegistration.update({ _id: req.params.sbrId }, req.body, function(error, num) {
      if (error) return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengubah data registrasi kwartir cabang '+req.params.sbrId+'.', {}, error);
      else return new JsonApiResponse(res, OK, 'Data registrasi kwartir cabang '+req.params.brId+' berhasil diperbaharui.', req.body).serve();
    });
  }
};
