import mongoose from 'mongoose';
import passport from 'passport';

import { OK, UNAUTHORIZED, INVALID, INTERNAL_SERVER_ERROR } from '../../lib/constants/HttpStatus';

import BaseController from '../../lib/BaseController';
import JsonApiResponse from '../../lib/JsonApiResponse';

import ScoutBranch from '../../models/ScoutBranch';
import User from '../../models/User';

var ObjectId = mongoose.Types.ObjectId;

export default class UsersApiController extends BaseController {

  _findUserExistenceOrError(req, res, callback) {
    User
      .find({ _id: ObjectId(req.params.id) })
      .exec((findError, user) => {
        if (findError) {
          return new JsonApiResponse();
        }
        if (!user) {
          return new JsonApiResponse();
        }
        callback(user);
      });
  }

  index(req, res) {
    User
      .find({ })
      .exec((findError, users) => {
        if (findError) {
          return new JsonApiResponse();
        }
        else {
          return new JsonApiResponse();
        }
      });
  }

  add(req, res) {
    User
      .findOne({ email: req.body.email })
      .exec((findError, existingUser) => {
        if (findError) {
          this._logger.error('Error on saving user data.', saveError);
          return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal menyimpan data user.', user, saveError).serve();
        }
        else {
          if (existingUser) {
            return new JsonApiResponse(res, INVALID, 'User dengan email yang dikirim telah terdaftar.', null, {}).serve();
          }
          else {
            let user = new User();

            ['name', 'email', 'password', 'role', 'branch'].forEach((field) => {
              if (req.body[field]) {
                switch (field) {
                  case 'branch':
                    user[field] = ObjectId(req.body[field]);
                    break;
                  default:
                    user[field] = req.body[field];
                    break;
                }
              }
            });

            user.created = new Date();

            var saveAndServe = () => {
              user.save((saveError, user) => {
                if (saveError) {
                  this._logger.error('Error on saving user data.', saveError);
                  return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal menyimpan data user.', user, saveError).serve();
                }
                else {
                  return new JsonApiResponse(res, OK, 'Sukses.', user.getSanitizedObject()).serve();
                }
              });
            };

            if ([].indexOf(user.role) != -1) {
              ScoutBranch
                .find({ parent: user.branch })
                .exec((findBranchError, branches) => {
                  if (findBranchError) {
                    this._logger.error('Error while finding ScoutBranch data to update user initial stats.', findBranchError);
                  }
                  else {
                    let dashboardStats = [];
                    branches.forEach((branch) => {
                      dashboardStats.push({
                        branch: branch.toObject(),
                        count: 0
                      });
                    });
                    user.stats = {
                      dashboard: dashboardStats
                    };
                  }
                  saveAndServe();
                })
            }
            else {
              saveAndServe();
            }
          }
        }
      });
  }

  authenticate(req, res) {
    passport.authenticate('local', (error, user, info) => {
      let data = {
        info
      };
      if (error) {
        this._logger.error('Error on executing authentication process.', error);
        return new JsonApiResponse(res, UNAUTHORIZED, 'Terjadi kesalahan di server.', data, error).serve()
      }
      if (!user) {
        return new JsonApiResponse(res, UNAUTHORIZED, 'Gagal melakukan autentikasi. Cek lagi email dan password Anda.', data, null).serve();
      }
      req.logIn(user, (loginError) => {
        if (loginError) {
          this._logger.error('Error on logging in.', loginError);
          return new JsonApiResponse(res, UNAUTHORIZED, 'Gagal melakukan proses login.', data, loginError).serve();
        }
        User
          .findOne({ _id: user._id })
          .exec((findError, currentUserData) => {
            if (findError) {
              this._logger.error('Error while finding last login data for user ' + user.email + '.', findError);
            }
            else {
              currentUserData.last_login = new Date();
              currentUserData.save((saveError) => {
                if (saveError) {
                  this._logger.error('Error while saving last login data for user ' + user.email + '.', saveError);
                }
              })
            }
          });
        return new JsonApiResponse(res, OK, 'Berhasil.', user, null).serve();
      });
    })(req, res);
  }

  getCurrentUser(req, res) {
    return new JsonApiResponse(res, OK, '', req.user || null).serve();
  }

  updateUser(req, res) {
    this._findUserExistenceOrError(req, res, (user) => {

    });
  }
};
