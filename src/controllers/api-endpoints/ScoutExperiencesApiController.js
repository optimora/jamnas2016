import { FORBIDDEN, INTERNAL_SERVER_ERROR, INVALID, OK, UNAUTHORIZED } from '../../lib/constants/HttpStatus';

import BaseController from '../../lib/BaseController';
import JsonApiResponse from '../../lib/JsonApiResponse';

import ScoutExperience from '../../models/ScoutExperience';

export default class ScoutExperiencesApiController extends BaseController {

  _prepareSavedData(scoutExperience, req) {

  }

  _sanitizeSubmittedDate(req) {

  }

  index(req, res) {

  }

  create(req, res) {
    this._sanitizeSubmittedDate(req);

    let scoutExperience = new ScoutExperience();

    this._prepareSavedData(scoutExperience, req);

    scoutExperience.created = new Date();

    scoutExperience.save((saveError) => {
      if (saveError) {
        this._logger.error('Error while saving ScoutExperience data.', saveError);
        return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal melakukan penyimpanan data pengalaman anggota pramuka.', {}, error).serve();
      }
      else {
        return new JsonApiResponse(res, OK, 'Sukses.', scoutExperience).serve();
      }
    });
  }

  edit(req, res) {

  }

  remove(req, res) {

  }
}
