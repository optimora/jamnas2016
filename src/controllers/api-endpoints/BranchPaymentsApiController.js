import { OK, UNAUTHORIZED, INTERNAL_SERVER_ERROR } from '../../lib/constants/HttpStatus';

import BaseController from '../../lib/BaseController';
import JsonApiResponse from '../../lib/JsonApiResponse';

import BranchPayment from '../../models/BranchPayment';
import StoredFile from '../../models/StoredFile';

const getFileExtension = (file) => {
  const fileNameSegments = file.originalname.split('.');
  return fileNameSegments[fileNameSegments.length - 1];
};


export default class BranchPaymentsApiController extends BaseController {
  index(req, res) {
    let query = BranchPayment.find({});

    if (req.query['_populate'] + '' === 'true') {
      query.populate('branch');
    }

    query.exec((error, docs) => {
      if (error) {
        return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengambil data konfirmasi pembayaran kwartir.', {}, error).serve();
      }
      else {
        return new JsonApiResponse(res, OK, 'Data registrasi kwartir.', docs).serve();
      }
    });
  }

  create(req, res) {
    if(!req.file) {
      // TODO: Kalau gak ada file nya?
      return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengunggah konfirmasi pembayaran kwartir.', {}, {}).serve();
    }
    else {
      let file = new StoredFile();

      file.extension = getFileExtension(req.file);
      file.category = 'payment';
      file.topic = 'branch-documents';

      file.raw_data = req.file;

      file.user = req.user._id;
      file.branch = req.user.branch._id;

      file.markModified('raw_data');

      file.save((saveError) => {
        if (saveError) {
          // Simpan file gagal
          return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal melakukan penyimpanan konfirmasi pembayaran kwartir.', {}, saveError).serve();
        }
        else {
          // Berhasil
          let bp = new BranchPayment(req.body);

          bp.branch = req.user.branch._id;
          bp.created = new Date().toISOString();
          bp.file = file;

          bp.save((saveError, doc) => {
            if (saveError) {
              //this._logger.error('Error on saving WillingnessLetter data.', saveError);
              return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal melakukan penyimpanan konfirmasi pembayaran kwartir.', {}, saveError).serve();
            }
            else {
              return new JsonApiResponse(res, OK, 'Sukses.', doc).serve();
            }
          });
          
        }
      });
    }


    
  }

  read(req, res) {
    BranchPayment.findById(req.params.paymentId, function(error, doc) {
      if (error) {
        //this._logger.error('Error while finding WillingnessLetter data.', error);
        return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengambil konfirmasi pembayaran kwartir ' + req.params.paymentId + '.', {}, error);
      }
      else {
        return new JsonApiResponse(res, OK, 'Data konfirmasi pembayaran ' + req.params.paymentId + '.', doc).serve();
      }
    });
  }

  filter_branch(req, res) {
    BranchPayment
        .findOne({branch: req.params.branchId})
        .populate('branch')
        //.populate('participants.sub_branch')
        .exec(function(error, doc) {
          if (error) {
            //this._logger.error('Error while finding WillingnessLetter data.', error);
            return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengambil konfirmasi pembayaran kwartir ' + req.params.branchId + '.', {}, error);
          }
          else {
            return new JsonApiResponse(res, OK, 'Data kesediaan kwartir ' + req.params.branchId + '.', doc).serve();
          }
    });

  }

  update(req, res) {
    if(typeof req.body.file !== 'undefined' && typeof req.body.file.name === 'undefined' ) delete req.body.file;

    BranchPayment.update({ _id: req.params.paymentId }, req.body, function(error, num) {
      if (error) return new JsonApiResponse(res, INTERNAL_SERVER_ERROR, 'Gagal mengubah konfirmasi pembayaran kwartir '+req.params.paymentId+'.', {}, error);
      else return new JsonApiResponse(res, OK, 'Data konfirmasi pembayaran '+req.params.paymentId+' berhasil diperbaharui.', req.params.paymentId).serve();
    });
  }
};
