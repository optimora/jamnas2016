import { OK } from '../../lib/constants/HttpStatus';

import BaseController from '../../lib/BaseController';
import JadePageResponse from '../../lib/JadePageResponse';

export default class SiteController extends BaseController {
  index(req, res) {
    return res.redirect('/users/login');
  }
}
