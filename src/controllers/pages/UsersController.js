import async from 'async';
import moment from 'moment';
import _ from 'underscore';

import { OK } from '../../lib/constants/HttpStatus';

import BaseController from '../../lib/BaseController';
import JadePageResponse from '../../lib/JadePageResponse';

import ScoutBranch from '../../models/ScoutBranch';
import StoredFile from '../../models/StoredFile';
import User from '../../models/User';

/**
 * LEGACY
 */
import BranchRegistration from '../../models/BranchRegistration';
import BranchTransport from '../../models/BranchTransport';
import BranchPayment from '../../models/BranchPayment';
import SubBranchRegistration from '../../models/SubBranchRegistration';
import SubBranchWillingnessLetter from '../../models/SubBranchWillingnessLetter';
import Location from '../../models/Location';
import Scout from '../../models/Scout';

export default class UsersController extends BaseController {
  login(req, res) {
    return new JadePageResponse(req, res, OK, 'pages/users/login', {
      title: 'Login'
    }).serve();
  }

  logout(req, res) {
    req.logout();
    res.redirect('/');
  };

  // User Administration Pages

  dashboard(req, res) {
    async.parallel(
      [
        (callback) => {
          ScoutBranch
            .findOne({ _id: req.user.branch })
            .exec(callback);
        }
      ],
      (asyncError, results) => {
        if (asyncError) {
          // TODO: Use appropriate page.
          return res.status(500).send('Oops!');
        }
        else {
          if (['admin', 'super-admin', 'committee-admin'].indexOf(req.user.role) !== -1) {
            return res.redirect('/scout_branches');
          }
          else {
            return new JadePageResponse(req, res, OK, 'pages/users/dashboard', {
              title: 'Dashboard',
              user: req.user,
              branch: results[0],
              version: this.getPayload().appVersion
            }).serve();
          }
        }
      }
    );
  }

  add(req, res) {
    return new JadePageResponse(req, res, OK, 'pages/users/add', {
      title: 'Tambah Pengguna Baru'
    }).serve();
  }

  index(req, res) {
    return new JadePageResponse(req, res, OK, 'pages/users/index', {
      title: 'Daftar User'
    }).serve();
  }

  addNewParticipant(req, res) {
    return new JadePageResponse(req, res, OK, 'pages/users/add_new_participant', {
      title: 'Data Peserta'
    }).serve();
  }

  addNewBranchParticipant(req, res) {
    return new JadePageResponse(req, res, OK, 'pages/users/add_branch_participants', {
      title: 'Tambah Peserta Kwartir Cabang'
    }).serve();
  }

  addNewCommittee(req, res) {
    return new JadePageResponse(req, res, OK, 'pages/users/add_new_committee', {
      title: 'Tambah Panitia Penyelenggara'
    }).serve();
  }

  committeeIndex(req, res) {
    return new JadePageResponse(req, res, OK, 'pages/users/committee_index', {
      title: 'Daftar Panitia Penyelenggara'
    }).serve();
  }

  participantIndex(req, res) {
    return new JadePageResponse(req, res, OK, 'pages/users/participant_index', {
      title: 'Daftar Peserta'
    }).serve();
  }

  committeeView(req, res) {
    return new JadePageResponse(req, res, OK, 'pages/users/committee_view', {
      title: 'Data Panitia Penyelenggara'
    }).serve();
  }

  participantView(req, res) {
    return new JadePageResponse(req, res, OK, 'pages/users/participant_view', {
      title: 'Data Peserta'
    }).serve();
  }

  branchWillingnessLetter(req, res) {
    return new JadePageResponse(req, res, OK, 'pages/users/branch_willingness_letter', {
      title: 'Form Kesediaan Kwartir Daerah',
      user: req.user,
    }).serve();
  }

  branchWillingnessLetterView(req, res) {
    return new JadePageResponse(req, res, OK, 'pages/users/branch_willingness_letter_view', {
      title: 'Kesediaan Kwartir Daerah Mengikuti Jambore Nasional X 2016',
      user: req.user,
    }).serve();
  }

  /**
   * LEGACY
   */

  // C01
  subBranchWillingnessLetter(req, res) {
    SubBranchWillingnessLetter
      .findOne({subbranch: req.params.subbranch_id})
      .exec(function(error, doc) {
        if(doc) res.redirect('/subbranch/'+req.params.subbranch_id+'/willingness_letter')
        else {
          ScoutBranch
            .findById(req.params.subbranch_id)
            .exec(function(error, doc){
              if (error) {
                // TODO: Use appropriate page
                return res.status(500).send('Oops!');
              }
              else {
                return new JadePageResponse(req, res, OK, 'legacy/subbranch_willingness_letter_form', {
                  title: 'Form Kesediaan Kwartir Cabang',
                  subbranch: doc,
                  user: req.user,
                }).serve();
              }
            });
        }

      });


  }

  subBranchWillingnessLetterUpdate(req, res) {
    SubBranchWillingnessLetter
      .findOne({subbranch: req.params.subbranch_id})
      .populate('branch subbranch')
      .exec(function (error, doc) {

            if (error) {
              // TODO: Use appropriate page
              return res.status(500).send('Oops!');
            }
            else {

              return new JadePageResponse(req, res, OK, 'legacy/subbranch_willingness_letter_update', {
                title: 'Form Kesediaan Kwartir Cabang',
                subbranch_willingness_letter: doc,
                user: req.user,
              }).serve();

            }

      });
  }

  subBranchWillingnessLetterView(req, res) {
    SubBranchWillingnessLetter
      .findOne({subbranch: req.params.subbranch_id})
      .populate('branch subbranch')
      .exec(function(error, doc) {
        if (error || !doc) {
          res.redirect('/subbranch/'+req.params.subbranch_id+'/willingness_letter/add');
        }
        else {
          return new JadePageResponse(req, res, OK, 'legacy/subbranch_willingness_letter_view', {
            title: 'Kesediaan Kwartir Cabang Mengikuti Jambore Nasional X 2016',
            user: req.user,
            subbranch_willingness_letter: doc
          }).serve();
        }

      });
  }

  // C02
  subBranchRegistration(req, res) {
    async.parallel(
      [
        (callback) => {
          ScoutBranch
            .findById(req.params.subbranch_id)
            .exec(callback);
        },

        (callback) => {
          Location
            .find({parentLocation: null })
            .sort('name')
            .exec(callback);
        }
      ],
      (asyncError, results) => {
        if (asyncError) {
          // TODO: Use appropriate page
          return res.status(500).send('Oops!');
        }
        else {
          return new JadePageResponse(req, res, OK, 'legacy/subbranch_registration_form', {
            title: 'Form Registrasi Kwartir Cabang',
            user: req.user,
            subbranch: results[0],
            provinces: results[1]
          }).serve();
        }
      }
    )




  }

  subBranchRegistrationUpdate(req, res) {
    async.parallel(
      [
        (callback) => {
          SubBranchRegistration
            .findById(req.params.registrationId)
            .populate('province regency_city subbranch branch')
            .exec(callback);
        },
        (callback) => {
          ScoutBranch
            .findById(req.params.subbranch_id)
            .exec(callback);
        },

        (callback) => {
          Location
            .find({parentLocation: null })
            .sort('name')
            .exec(callback);
        }
      ],
      (asyncError, results) => {
        if (asyncError) {
          // TODO: Use appropriate page
          return res.status(500).send('Oops!');
        }
        else {
          return new JadePageResponse(req, res, OK, 'legacy/subbranch_registration_update', {
            title: 'Form Registrasi Kwartir Cabang',
            user: req.user,
            subbranch_registration: results[0],
            subbranch: results[1],
            provinces: results[2]
          }).serve();
        }
      }
    )




  }

  subBranchRegistrationView(req, res) {
    SubBranchRegistration
      .findOne({subbranch: req.params.subbranch_id})
      .populate('branch subbranch province regency_city')
      .exec(function(error, doc) {
        if (error || !doc) {
          res.redirect('/subbranch/'+req.params.subbranch_id+'/registration/add');
        }
        else {
          return new JadePageResponse(req, res, OK, 'legacy/subbranch_registration_view', {
            title: 'Data Registrasi Kwartir Cabang',
            user: req.user,
            subbranch_registration: doc
          }).serve();
        }

      });
  }

  // D02
  branchRegistration(req, res) {
    async.parallel(
      [
        (callback) => {
          ScoutBranch
            .find({ parent: req.user.branch._id })
            .sort('name')
            .exec(callback);
        },

        (callback) => {
          Location
            .find({parentLocation: null })
            .sort('name')
            .exec(callback);
        }
      ],
      (asyncError, results) => {
        if (asyncError) {
          // TODO: Use appropriate page
          return res.status(500).send('Oops!');
        }
        else {

          return new JadePageResponse(req, res, OK, 'legacy/branch_registration_form', {
            title : 'Form Registrasi Kwartir Daerah',
            user  : req.user,
            sub_branches: results[0],
            provinces: results[1]
          }).serve();
        }
      }
    )
  }

  branchRegistrationUpdate(req, res) {
    async.parallel(
      [
        (callback) => {
          BranchRegistration
            .findById(req.params.registrationId)
            .populate('province regency_city participants.sub_branch')
            .exec(callback);
        },

        (callback) => {
          ScoutBranch
            .find({ parent: req.user.branch._id })
            .sort('name')
            .exec(callback);
        },

        (callback) => {
          Location
            .find({parentLocation: null })
            .sort('name')
            .exec(callback);
        }
      ],
      (asyncError, results) => {
        if (asyncError) {
          // TODO: Use appropriate page
          return res.status(500).send('Oops!');
        }
        else {

          return new JadePageResponse(req, res, OK, 'legacy/branch_registration_update', {
            title : 'Form Registrasi Kwartir Daerah',
            user  : req.user,
            branch_registration :  results[0],
            sub_branches: results[1],
            provinces: results[2]
          }).serve();
        }
      }
    )
  }


  branchRegistrationView(req, res) {
    BranchRegistration
      .findOne({branch: req.user.branch._id})
      .populate('branch province regency_city participants.sub_branch')
      .exec(function(error, doc) {
        if (error || !doc) {
          res.redirect('/branch/registration/add');
        }
        else {
          return new JadePageResponse(req, res, OK, 'legacy/branch_registration_view', {
            title: 'Data Registrasi Kwartir Daerah',
            user: req.user,
            branch_registration: doc
          }).serve();
        }

      });


  }

  // TRANSPORT
  /*branchTransport(req, res) {
    Location
      .find({parentLocation: null })
      .sort('name')
      .exec(function(error,docs) {
        if (error) {
          // TODO: Use appropriate page
          return res.status(500).send('Oops!');
        }
        else {

          return new JadePageResponse(req, res, OK, 'legacy/branch_transport_form', {
            title: 'Rencana Keberangkatan dan Kepulangan Kontingen',
            user: req.user,
            provinces: docs
          }).serve();
        }
      });
  }*/

/*  branchTransportView(req, res) {
    BranchTransport
      .findOne({branch: req.user.branch._id})
      .populate('branch province regency_city')
      .exec(function(error, doc) {
        if (error || !doc) {
          //res.redirect('/branch/transport/add');
          return res.status(500).send('Oops!');
        }
        else {
          return new JadePageResponse(req, res, OK, 'legacy/branch_transport_view', {
            title: 'Data Rencana Keberangkatan dan Kepulangan Kontingen',
            user: req.user,
            moment: moment,
            branch_transport: doc
          }).serve();

        }

      });
  }*/

  branchTransportView(req, res) {
    async.parallel(
      [
        (callback) => {
          BranchTransport
            .findOne({branch: req.user.branch._id})
            .populate('branch province regency_city')
            .exec(callback);
        },
        (callback) => {
          BranchTransport
            .find({ branch: req.user.branch })
            .populate('branch')
            .exec(callback);
        }
      ],
      (asyncError, results) => {
        if (asyncError) {
          // TODO: Use appropriate page
          return res.status(500).send('Oops!');
        }
        else {
          return new JadePageResponse(req, res, OK, 'legacy/branch_transport_view', {
            title : 'Data Rencana Keberangkatan dan Kepulangan Kontingen',
            user  : req.user,
            branch_transport: results[0],
            branch_transports: results[1],
            moment: moment,
          }).serve();
        }
      }
    )
  }

  branchTransport(req, res) {
    async.parallel(
      [
        (callback) => {
          BranchTransport
            .find({ branch: req.user.branch })
            .populate('branch')
            .exec(callback);
        },
        (callback) => {
          Location
            .find({parentLocation: null })
            .sort('name')
            .exec(callback);
        }
      ],
      (asyncError, results)=> {
        if(asyncError) {
          return res.status(500).send('Oops!');
        }
        else {
          return new JadePageResponse(req, res, OK, 'legacy/branch_transport_form', {
            title: 'Rencana Keberangkatan dan Kepulangan Kontingen',
            user: req.user,
            branch_transports: results[0],
            provinces: results[1],
            moment: moment,
          }).serve();
        }
      }
    )
  }

  branchTransportUpdate(req, res) {
    async.parallel(
      [
        (callback) => {
          BranchTransport
            .findOne({_id: req.params.transport_id})
            .populate('branch province regency_city')
            .exec(callback);
        },
        (callback) => {
          BranchTransport
            .find({ branch: req.user.branch })
            .populate('branch')
            .exec(callback);
        },
        (callback) => {
          Location
            .find({parentLocation: null })
            .sort('name')
            .exec(callback);
        }
      ],
      (asyncError, results)=> {
        if(asyncError) {
          return res.status(500).send('Oops!');
        }
        else {
          return new JadePageResponse(req, res, OK, 'legacy/branch_transport_update', {
            title: 'Rencana Keberangkatan dan Kepulangan Kontingen',
            user: req.user,
            branch_transport: results[0],
            branch_transports: results[1],
            provinces: results[2],
            moment: moment,
          }).serve();
        }
      }
    )
  }

  // PAYMENT
  branchPayment(req, res) {
    async.parallel(
      [
        (callback) => {
          BranchPayment
            .find({ branch: req.user.branch })
            .exec(callback);
        }

      ],
      (asyncError, results) => {
        if (asyncError) {
          // TODO: Use appropriate page
          return res.status(500).send('Oops!');
        }
        else {
          return new JadePageResponse(req, res, OK, 'legacy/branch_payment_form', {
            title : 'Konfirmasi Pembayaran',
            user  : req.user,
            branch_payments: results[0],
            moment: moment,
          }).serve();
        }
      }
    )

  }

  branchPaymentUpdate(req, res) {
    async.parallel(
      [
        (callback) => {
          BranchPayment
            .findOne({ _id: req.params.payment_id })
            .populate('branch')
            .exec(callback);
        },
        (callback) => {
          BranchPayment
            .find({ branch: req.user.branch })
            .exec(callback);
        }

      ],
      (asyncError, results) => {
        if (asyncError) {
          // TODO: Use appropriate page
          return res.status(500).send('Oops!');
        }
        else {
          return new JadePageResponse(req, res, OK, 'legacy/branch_payment_update', {
            title : 'Konfirmasi Pembayaran',
            user  : req.user,
            branch_payment: results[0],
            branch_payments: results[1],
            moment: moment,
          }).serve();
        }
      }
    )

  }

  branchPaymentView(req, res) {
    async.parallel(
      [
        (callback) => {
          BranchPayment
            .findOne({ _id: req.params.payment_id })
            .populate('branch')
            .exec(callback);
        },
        (callback) => {
          BranchPayment
            .find({ branch: req.user.branch })
            .exec(callback);
        }
      ],
      (asyncError, results) => {
        if (asyncError) {
          // TODO: Use appropriate page
          return res.status(500).send('Oops!');
        }
        else {
          return new JadePageResponse(req, res, OK, 'legacy/branch_payment_view', {
            title : 'Konfirmasi Pembayaran Peserta Jambore Nasional X 2016',
            user  : req.user,
            branch_payment: results[0],
            branch_payments: results[1],
            moment: moment,
          }).serve();
        }
      }
    )
  }

  uploadFileWithTopic(req, res, next) {
    this._logger.info('File uploaded.', req.file);

    if (!req.file) {
      this._logger.warning('Can\'t get file data.');
      return new JadePageResponse(req, res, OK, 'pages/users/upload_file_with_topic', {
        title: '',
        queryString: req.query,
        data: {
          success: false,
          uploadedFile: null,
          storedFile: null,
          error: null
        }
      }).serve();
    }
    else {
      const getFileExtension = (file) => {
        const fileNameSegments = file.originalname.split('.');
        return fileNameSegments[fileNameSegments.length - 1];
      };

      let file = new StoredFile();

      file.extension = getFileExtension(req.file);
      file.category = req.query.category && req.query.category != '' ? req.query.category : 'unknown';
      file.topic = req.query.topic && req.query.topic != '' ? req.query.topic : 'miscellaneous';

      file.raw_data = req.file;

      file.user = req.user._id;
      file.branch = req.user.branch._id;

      file.markModified('raw_data');

      file.save((saveError) => {
        if (saveError) {
          this._logger.error('Upload error.', saveError);
          return new JadePageResponse(req, res, OK, 'pages/users/upload_file_with_topic', {
            title: '',
            queryString: req.query,
            data: {
              success: false,
              uploadedFile: null,
              storedFile: null,
              error: saveError
            }
          }).serve();
        }
        else {
          return new JadePageResponse(req, res, OK, 'pages/users/upload_file_with_topic', {
            title: '',
            queryString: req.query,
            data: {
              success: true,
              uploadedFile: req.file,
              storedFile: file,
              error: null
            }
          }).serve();
        }
      });
    }
  }

  uploadTest(req, res) {
    return new JadePageResponse(req, res, OK, 'pages/users/upload_test', {
      title: 'Test Upload'
    }).serve();
  }

  adminBranchWillingnessLetters(req, res) {
    return new JadePageResponse(req, res, OK, 'pages/users/branch_willingness_letters', {
      title: 'Daftar Surat Kesediaan Kwartir Daerah'
    }).serve();
  }

  adminSubbranchWillingnessLetters(req, res) {
    return new JadePageResponse(req, res, OK, 'pages/users/subbranch_willingness_letters', {
      title: 'Daftar Surat Kesediaan Kwartir Cabang'
    }).serve();
  }

  adminBranchPayments(req, res) {
    return new JadePageResponse(req, res, OK, 'pages/users/payment_confirmations', {
      title: 'Daftar Data Konfirmasi Pembayaran'
    }).serve();
  }

  adminBranchTransports(req, res) {
    return new JadePageResponse(req, res, OK, 'pages/users/branch_transports', {
      title: 'Daftar Data Keberangkatan Kedatangan dan Kepulangan'
    }).serve();
  }

  adminCommittees(req, res) {
    return new JadePageResponse(req, res, OK, 'pages/users/committees_list', {
      title: 'Daftar Data Panitia'
    }).serve();
  }

  adminParticipants(req, res) {
    return new JadePageResponse(req, res, OK, 'pages/users/participants_list', {
      title: 'Daftar Data Peserta'
    }).serve();
  }

  adminBranchParticipants(req, res) {
    return new JadePageResponse(req, res, OK, 'pages/users/branch_participants', {
      title: 'Daftar Data Peserta Berdasarkan Kwartir Daerah',
      scoutsStatByBranch: scoutsStatByBranch
    }).serve();
  }

  adminSubBranchParticipants(req, res) {
    return new JadePageResponse(req, res, OK, 'pages/users/subbranch_participants', {
      title: 'Daftar Data Peserta Berdasarkan Kwartir Cabang'
    }).serve();
  }

}
