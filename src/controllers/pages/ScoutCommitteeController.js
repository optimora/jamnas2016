import async from 'async';
import mongoose from 'mongoose';

var ObjectId = mongoose.Schema.Types.ObjectId;

import { OK } from '../../lib/constants/HttpStatus';

import BaseController from '../../lib/BaseController';
import JadePageResponse from '../../lib/JadePageResponse';

import BadgeType from '../../models/BadgeType';
import Location from '../../models/Location';
import Scout from '../../models/Scout';
import ScoutBranch from '../../models/ScoutBranch';

export default class ScoutCommitteeController extends BaseController {
  add(req, res) {
    async.parallel(
      [
        (callback) => {
          ScoutBranch
            .findOne({ _id: req.user.branch })
            .exec(callback);
        },
        (callback) => {
          ScoutBranch
            .find({ parent: null })
            .exec((findError, branches) => {
              if (findError) {
                callback(findError, branches);
              }
              else {
                let results = [];
                branches.forEach((branch) => {
                  if (req.user.magic_role && req.user.magic_role !== '' && branch.name.indexOf('(pura-puranya)') !== -1) {
                    results.push(branch);
                  }
                  else {
                    if (branch.name.indexOf('(pura-puranya)') === -1) {
                      results.push(branch);
                    }
                  }
                });
                callback(findError, results);
              }
            });
        }
      ],
      (asyncError, results) => {
        if (asyncError) {
          this._logger.error('Error while async.', asyncError);
          // TODO: Show appropriate page?
          res.status(500).send('Oops!');
        }
        else {
          let branch = results[0];
          let branches= results[1];
          const enumStringMap = this.getPayload().rawData.enumStringMap;
          const getCommitteeRole = () => Object.keys(enumStringMap).filter(role => role.indexOf('committee-role') !== -1).map(role => ({ value: role, name: enumStringMap[role] }));
          return new JadePageResponse(req, res, OK, 'pages/scout_committee/add', {
            title: 'Tambah Data Panitia',
            branch,
            branches,
            enumStringMap,
            committeeRoles: getCommitteeRole(),
            version: this.getPayload().appVersion
          }).serve();
        }
      }
    );
  }

  index(req, res) {
    return new JadePageResponse(req, res, OK, 'pages/scout_committee/index', {
      title: 'Daftar Panitia',
      enumStringMap: this.getPayload().rawData.enumStringMap,
      version: this.getPayload().appVersion
    }).serve();
  }
}