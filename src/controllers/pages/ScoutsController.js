import async from 'async';
import mongoose from 'mongoose';

var ObjectId = mongoose.Schema.Types.ObjectId;

import { OK } from '../../lib/constants/HttpStatus';

import BaseController from '../../lib/BaseController';
import JadePageResponse from '../../lib/JadePageResponse';

import BadgeType from '../../models/BadgeType';
import Location from '../../models/Location';
import Scout from '../../models/Scout';
import ScoutBranch from '../../models/ScoutBranch';

export default class ScoutsController extends BaseController {
  _findScoutExistenceOrError(req, res, callback, options = { populatedFields: null }) {
    let query = Scout
      .findOne({ _id: req.params.id });

    if (options.populatedFields) {
      query.populate(options.populatedFields.join(' '));
    }

    query.exec((findError, scout) => {
      if (findError) {
        this._logger.error('Error while finding scout data.', findError);
        // TODO: Replace with appropriate page?
        return res.status(500).send('Oops.');
      }
      if (!scout) {
        // TODO: Replace with appropriate page?
        return res.status(404).send('Nothing here.');
      }
      callback(scout);
    });
  }

  index(req, res) {
    return new JadePageResponse(req, res, OK, 'pages/scouts/index', {
      title: 'Index',
      version: this.getPayload().appVersion
    }).serve();
  }

  edit(req, res) {
    this._findScoutExistenceOrError(req, res, (scout) => {
      async.parallel(
        [
          (callback) => {
            ScoutBranch
              .findOne({ _id: req.user.branch })
              .exec(callback);
          },
          (callback) => {
            ScoutBranch
              .find({ parent: null })
              .exec(callback);
          },
          (callback) => {
            ScoutBranch
              .findOne({ _id: scout.subbranch })
              .exec(callback);
          },
          (callback) => {
            Location
              .find({ parentLocation: null })
              .exec(callback);
          },
          (callback) => {
            BadgeType
              .find({})
              .exec(callback)
          }
        ],
        (asyncError, results) => {
          if (asyncError) {
            this._logger.error('Error while executing parallel actions', asyncError);
            // TODO: Replace with appropriate page?
            return res.status(500).send('Oops!');
          }
          else {
            let branch = results[0];
            let branches = results[1];
            let subbranch = results[2];
            let locations = results[3];
            let badgeTypes = results[4];

            const enumStringMap = this.getPayload().rawData.enumStringMap;
            const getCommitteeRole = () => Object.keys(enumStringMap).filter(role => role.indexOf('committee-role') !== -1).map(role => ({ value: role, name: enumStringMap[role] }));

            return new JadePageResponse(req, res, OK, 'pages/scouts/edit', {
              title: (subbranch || { name: '' }).name + ' - Ubah Data Keanggotaan',
              branch,
              branches,
              committeeRoles: getCommitteeRole(),
              enumStringMap,
              subbranch,
              locations,
              badgeTypes,
              scout,
              version: this.getPayload().appVersion
            }).serve();
          }
        }
      );
    }, { populatedFields: ['contact', 'parent_contact', 'emergency_contacts', 'experiences'] });
  }

  detail(req, res) {
    let query = Scout.findOne({ _id: req.params.id });

    ['contact', 'parent_contact', 'emergency_contacts'].forEach((field) => {
      query.populate({
        path: field,
        populate: {
          path: 'regency_city'
        }
      });
      query.populate({
        path: field,
        populate: {
          path: 'province'
        }
      });
    });

    query
      .populate(['documents', 'branch', 'subbranch', 'group'].join(' '))
      .exec((findError, scout) => {
        if (findError) {
          this._logger.error('Error while finding Scout data.', findError);
          // TODO: Display appropriate page.
          return res.status(500).send('Oops!');
        }
        else {
          if (!scout) {
            // TODO: Display appropriate page?
            return res.status(404).send('Data not found.');
          }
          else {
            async.parallel(
              [
                (callback) => {
                  ScoutBranch
                    .findOne({ _id: scout.branch })
                    .exec(callback);
                },
                (callback) => {
                  ScoutBranch
                    .findOne({ _id: scout.subbranch })
                    .exec(callback);
                }
              ],
              (asyncError, results) => {
                if (asyncError) {
                  this._logger.error('Error while executing async actions to find Scout branch and subbranch data.', asyncError);
                 // TODO: Display appropriate page.
                  return res.status(500).send('Oops!');
                }
                else {
                  const branch = results[0];
                  const subbranch = results[1];
                  return new JadePageResponse(req, res, OK, 'pages/scouts/detail', {
                    title: 'Detail Peserta No. ' + scout.nta,
                    branch,
                    scout,
                    subbranch
                  }).serve();
                }
              }
            );
          }
        }
      });
  }
}
