import { OK } from '../../lib/constants/HttpStatus';

import BaseController from '../../lib/BaseController';
import JadePageResponse from '../../lib/JadePageResponse';

export default class AdministrationController extends BaseController {
  getPaymentConfirmations(req, res) {
    return new JadePageResponse(req, res, OK, 'pages/administration/payment_confirmations', {
      title: 'Data Pembayaran',
      version: this.getPayload().appVersion
    }).serve();
  }

  getTransportation(req, res) {
    return new JadePageResponse(req, res, OK, 'pages/administration/transportation', {
      title: 'Data Rencana Perjalanan Peserta',
      version: this.getPayload().appVersion
    }).serve();
  }

  getWillingnessLetters(req, res) {
    return new JadePageResponse(req, res, OK, 'pages/administration/willingness_letters', {
      title: '',
      version: this.getPayload().appVersion
    }).serve();
  }
}
