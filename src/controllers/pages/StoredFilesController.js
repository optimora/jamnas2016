import fs from 'fs';

import { OK } from '../../lib/constants/HttpStatus';

import BaseController from '../../lib/BaseController';
import JadePageResponse from '../../lib/JadePageResponse';

import StoredFile from '../../models/StoredFile';

export default class StoredFilesController extends BaseController {
  getFileContentById(req, res) {
    StoredFile
      .findOne({ _id: req.params.id })
      .exec((findError, storedFile) => {
        if (findError) {
          this._logger.error('Error while finding StoredFile data.', findError);
          return res.status(500).send('Oops!');
        }
        else {
          if (!storedFile) {
            return res.status(404).send('File not found.')
          }
          else {
            try {
              let storedFileOnDiskData = fs.statSync(storedFile.raw_data.path);
              let contentType = '';
              switch (storedFile.extension) {
                case 'jpg':
                case 'jpeg':
                case 'png':
                  contentType = 'image/' + storedFile.extension;
                  break;
                case 'pdf':
                  contentType = 'application/pdf';
                  break;
                default:
                  break;
              }

              const attachmentFileName = 'jamnas2016_' + storedFile._id + '.' + storedFile.extension;

              res.writeHead(OK, {
                'Content-Type': contentType,
                'Content-Length': storedFileOnDiskData.size
              });

              fs.createReadStream(storedFile.raw_data.path).pipe(res);
            }
            catch (whateverError) {
              this._logger.error('Error.', whateverError);
              return res.status(500).send('Oops!');
            }
          }
        }
      });
  }
}
