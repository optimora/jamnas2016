import async from 'async';

import { OK } from '../../lib/constants/HttpStatus';

import BaseController from '../../lib/BaseController';
import JadePageResponse from '../../lib/JadePageResponse';

import Location from '../../models/Location';
import ScoutBranch from '../../models/ScoutBranch';

export default class ScoutBranchesController extends BaseController {
  index(req, res) {
    if (['admin', 'super-admin', 'committee-admin'].indexOf(req.user.role) !== -1) {
      return new JadePageResponse(req, res, OK, 'pages/scout_branches/index', {
        title: 'Dashboard',
        user: req.user,
        version: this.getPayload().appVersion
      }).serve();
    }
  }

  detail(req, res) {
    async.parallel(
      [
        (callback) => {
          ScoutBranch
            .findOne({ _id: req.params.id})
            .exec(callback);
        }
      ],
      (asyncError, results) => {
        if (asyncError) {
          // TODO: Use appropriate page.
          return res.status(500).send('Oops!');
        }
        else {
          if (['admin', 'super-admin', 'committee-admin'].indexOf(req.user.role) !== -1) {
            return new JadePageResponse(req, res, OK, 'pages/users/dashboard', {
              title: 'Dashboard',
              user: req.user,
              branch: results[0],
              version: this.getPayload().appVersion
            }).serve();
          }
          else {
            return res.status(403).send('Nope!');
          }
        }
      }
    );
  }

  addScout(req, res) {
    async.parallel(
      [
        (callback) => {
          ScoutBranch
            .findOne({ _id: req.user.branch })
            .exec(callback);
        },
        (callback) => {
          ScoutBranch
            .findOne({ _id: req.params.id })
            .populate('parent')
            .exec(callback);
        },
        (callback) => {
          Location
            .find({ parent: null })
            .exec(callback);
        }
      ],
      (asyncError, results) => {
        if (asyncError) {
          this._logger.error('');
        }
        else {
          let branch = results[0];
          let subbranch = results[1];
          let locations = results[2];
          return new JadePageResponse(req, res, OK, 'pages/scout_branches/add_scout', {
            title: subbranch.name + ' - Tambah Data Keanggotaan',
            branch,
            subbranch,
            locations,
            version: this.getPayload().appVersion
          }).serve();
        }
      }
    );
  }

  getScoutsByBranchId(req, res) {
    async.parallel(
      [
        (callback) => {
          ScoutBranch
            .findOne({ _id: req.user.branch })
            .exec(callback);
        },
        (callback) => {
          ScoutBranch
            .findOne({ _id: req.params.id })
            .populate('parent')
            .exec(callback);
        }
      ],
      (asyncError, results) => {
        if (asyncError) {
          // TODO: Use appropriate page.
          return res.status(500).send('Oops!');
        }
        else {
          if (results[1]) {
            return new JadePageResponse(req, res, OK, 'pages/scout_branches/scouts_index', {
              title: 'Daftar Peserta ' + results[1].name,
              user: req.user,
              branch: results[1].parent || results[0],
              subbranch: results[1],
              userBranch: results[0],
              rawEnumData: this.getPayload().rawData.enumStringMap,
              version: this.getPayload().appVersion
            }).serve();
          }
          else {
            return res.status(404).send('There is nothing here.');
          }
        }
      }
    );
  }
}
