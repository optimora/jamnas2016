// List of supported response. Please update this enum every time new Response Type added.

export const JADE_PAGE = 'JADE_PAGE';
export const JSON_API = 'JSON_API';
