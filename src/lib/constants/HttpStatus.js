export const OK = 200;

export const INVALID = 400;
export const UNAUTHORIZED = 401;
export const FORBIDDEN = 403;
export const NOT_FOUND = 404;

export const INTERNAL_SERVER_ERROR = 500;
