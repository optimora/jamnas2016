import onFinished from 'on-finished';

export default class HttpRequestLogger {
  constructor(options) {
    this._options = options || {};
  }

  _printTextMessage(req, res) {
    onFinished(res, (err, res) => {
      let strTime = new Date().toISOString();
      let method = req.method;
      let url = req.originalUrl || req.url;
      console.log(method + ' ' + url + ' ' + res.statusCode);
    });
  }

  createMiddleware() {
    return (req, res, next) => {
      this._printTextMessage(req, res);
      next();
    };
  }
}
