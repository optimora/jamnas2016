export default class Logger {
  constructor() {

  }

  _printTextMessage(type, message) {
    let strTime = new Date().toISOString();
    console.log('[' + strTime + ' ' + type + '] ' + message);
  }

  _printObjectMessage(type, obj) {
    let strTime = new Date().toISOString();
    let strObject = JSON.stringify(obj);
    console.log('[' + strTime + ' ' + type + '] Printing object: ' + strObject);
  }

  warning(message, obj) {
    this._printTextMessage('WARNING', message);
    if (obj) {
      this._printObjectMessage('WARNING', obj);
    }
  }

  info(message, obj) {
    this._printTextMessage('INFO', message);
    if (obj) {
      this._printObjectMessage('INFO', obj);
    }
  }

  error(message, errorObject) {
    this._printTextMessage('ERROR', message);
    if (errorObject) {
      this._printObjectMessage('ERROR', errorObject);
    }
  }
}
