import { USER_MENUS, SITE_MENUS } from '../../config/menus';

export default class ViewHelper {
  constructor(data) {
    this._data = data;
  }

  _getRenderedUserMenuData(routeUrl, userRole) {
    let renderedSiteMenuData = SITE_MENUS.slice(0);
    let renderedUserMenuData = USER_MENUS.slice(0);

    let menuRoleFilter = (menu) => {
      if (menu.roles && menu.roles.length > 0) {
        return menu.roles.indexOf(userRole) != -1;
      }
      return true;
    };

    renderedUserMenuData = renderedUserMenuData.filter(menuRoleFilter);

    let menuFilter = (menu) => {
      let menuData = menu;
      if (routeUrl.indexOf(menu.url) != -1) {
        menuData.active = true;
      }
      else {
        menuData.active = false;
      }
    };

    renderedUserMenuData.forEach(menuFilter);
    renderedSiteMenuData.forEach(menuFilter);

    return {
      site: renderedSiteMenuData,
      user: renderedUserMenuData
    };
  }

  getDefaultMenuViewData(req, res) {
    let user = req.user || {};
    return this._getRenderedUserMenuData(req.originalUrl, user.role);
  }
};
