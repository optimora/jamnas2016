export default class DataHelper {
  constructor() {

  }

  static applyExpectedFields(expectedFields, source, destination) {
    expectedFields.forEach((field) => {
      destination[field] = source[field];
    });
  }
};
