import nodemailer from 'nodemailer';
import sgTransport from 'nodemailer-sendgrid-transport';

var mailer = nodemailer.createTransport(sgTransport(config.mail.sendgrid.auth.key));

export default class MailHelper {
  constructor() {
    
  }
};
