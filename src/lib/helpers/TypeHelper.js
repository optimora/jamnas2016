export default class TypeHelper {
  static isArray(obj) {
    return obj && obj.constructor === Array;
  }
};
