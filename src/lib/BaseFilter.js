import Logger from './logger/Logger'

export default class BaseFilter {
  constructor(routeConfig) {
    this._routeConfig = routeConfig;

    this._logger = new Logger();
  }

  getRouteResponseType() {
    return this._routeConfig.responseType;
  }

  check(req, res, next) {
    // TODO: Override this method in child class.
  }

  // Low level handlers.

  __createHandler() {
    return (req, res, next) => {
      this.check(req, res, next);
    };
  }
};
