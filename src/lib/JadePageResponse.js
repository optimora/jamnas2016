import { JADE_PAGE } from './constants/ResponseType';

import BaseResponse from './BaseResponse';
import ViewHelper from './helpers/ViewHelper';

export default class JadePageResponse extends BaseResponse {
  constructor(req, res, status, template, data) {
    super(res, status);

    this._req = req;
    this._template = template;
    this._data = data || {};

    this._additionalDefaultViewData = {};
    this._viewHelper = new ViewHelper();
  }

  getId() {
    return JADE_PAGE;
  }

  injectDefaultData(obj) {
    this._additionalDefaultViewData = Object.assign({}, this._additionalViewData, obj);
    return this;
  }

  setData(k, v) {
    this._data[k] = v;
  }

  _initDefaultData() {
    this._data.__defaultViewData = Object.assign({
      menus: this._viewHelper.getDefaultMenuViewData(this._req),
      user: this._req.user || null
    }, this._additionalDefaultViewData);
  }

  _createResponse() {
    this._initDefaultData();
    this._res.set({
      'Cache-Control': 'no-cache, no-store, must-revalidate',
      'Pragma': 'no-cache',
      'Expires': '0'
    });
    return this._res.status(this._status).render(this._template, this._data);
  }
}
