import Logger from './logger/Logger';
import Route from './Route';

export default class RouteConfigParser {
  constructor(routeConfigMap, controllerPayload) {
    this._routeMap = routeConfigMap;
    this._controllerPayload = controllerPayload;

    this._controllerInstanceMap = {};

    this._logger = new Logger();

    this._initialize();
  }

  _initialize() {
    this._logger.info('Initializing Route Handlers.');

    Object.keys(this._routeMap).forEach((id) => {
      this._logger.info('- Parsing route data for id ' + id + '.');
      let routeConfig = this._routeMap[id];
      let ControllerClass = routeConfig.controller;
      if (typeof ControllerClass !== 'function') {
        this._logger.error('Invalid controller class.', {
          invalidClass: ControllerClass
        });
        throw new Error('Invalid controller class. Have you passed correct class?');
      }
      if (!this._controllerInstanceMap[routeConfig.controller.name]) {
        this._controllerInstanceMap[routeConfig.controller.name] = new ControllerClass(this._controllerPayload);
      }
    });

    this._logger.info('Registering route data finished.')
  }

  _createMiddlewares(routeConfig) {
    let middlewares = [];

    routeConfig.filters.forEach((FilterClass) => {
      let filterClassInstance = new FilterClass(routeConfig);
      middlewares.push(filterClassInstance.__createHandler());
    });

    return middlewares;
  }

  _createRouteInstance(id) {
    let routeConfig = this._routeMap[id];
    let controllerInstance = this._controllerInstanceMap[routeConfig.controller.name];

    let method = routeConfig.method;
    let responseType = routeConfig.responseType;
    let path = routeConfig.path;
    let handler = controllerInstance.__createHandler(routeConfig);
    let middlewares = this._createMiddlewares(routeConfig);

    return new Route(id, method, responseType, path, handler, middlewares);
  }

  createRoutes() {
    let routeInstances = [];

    Object.keys(this._routeMap).forEach((id) => {
      routeInstances.push(this._createRouteInstance(id));
    });

    this._routeInstances = routeInstances;

    return this._routeInstances;
  }
};
