export default class BaseResponse {
  constructor(res, status) {
    this._res = res;
    this._status = status;
  }

  setHeader(field, value) {
    this._res.set(field, value);
  }

  setHeaders(headers) {
    Object.keys(headers).forEach((header) => {
      this._res.set(header.field, header.value);
    });
  }

  setStatus(status) {
    this._status = status;
  }

  // Call this every time you want to return a response.
  serve() {
    this._createResponse();
  }

  _createResponse() {
    // TODO: Override this in the child class.
  }

  // Express Utility

  __createResponse() {
    this._createResponse();
  }
};
