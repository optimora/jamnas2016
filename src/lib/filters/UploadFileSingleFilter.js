import multer from 'multer';

import { OK, UNAUTHORIZED, FORBIDDEN, INTERNAL_SERVER_ERROR } from '../constants/HttpStatus';
import { JADE_PAGE, JSON_API } from '../constants/ResponseType';

import config from '../../config/app';

import BaseFilter from '../BaseFilter';
import JadePageResponse from '../JadePageResponse';
import JsonApiResponse from '../JsonApiResponse';

var uploadHandler = multer({
  dest: config.app.storedFilesLocation,
  fileFilter: (req, file, callback) => {
    const getFileExtension = (file) => {
      const fileNameSegments = file.originalname.split('.');
      return fileNameSegments[fileNameSegments.length - 1];
    };

    callback(null, ['jpg', 'jpeg', 'png', 'pdf'].indexOf(getFileExtension(file).toLowerCase()) != -1);
  },
  limits: {
    fieldSize: 1024
  }
});

export default class UploadSingleFileFilter extends BaseFilter {
  check (req, res, next) {
    uploadHandler.single('file')(req, res, (error) => {
      if (error) {
        this._logger.error('Error while processing file in filter.', error);
        next(error)
      }
      else {
        next();
      }
    });
  }
}
