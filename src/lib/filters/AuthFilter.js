import { OK, UNAUTHORIZED, FORBIDDEN, INTERNAL_SERVER_ERROR } from '../constants/HttpStatus';
import { JADE_PAGE, JSON_API } from '../constants/ResponseType';

import BaseFilter from '../BaseFilter';
import JadePageResponse from '../JadePageResponse';
import JsonApiResponse from '../JsonApiResponse';

export default class AuthFilter extends BaseFilter {
  check(req, res, next) {
    if (!req.user) {
      switch (this.getRouteResponseType()) {
        case JADE_PAGE:
          // TODO: Return appropriate page?
          return res.redirect('/users/login');
        case JSON_API:
          return new JsonApiResponse(res, FORBIDDEN, 'No!');
      }
    }
    next();
  }
}
