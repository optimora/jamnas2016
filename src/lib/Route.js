export default class Route {
  constructor(id, method, responseType, path, handler, middlewares) {
    this._id = id;
    this._method = method;
    this._responseType = responseType;
    this._path = path;
    this._handler = handler;
    this._middlewares = middlewares;
  }

  getResponseType() {
    return this._responseType;
  }

  getPath() {
    return this._path;
  }

  __applyHandler(expressAppInstance) {
    const sillyMiddleware = (req, res, next) => {
      if (req.hostname.indexOf('jamnasx2016.com') != -1) {
        res.redirect('http://daftar.jamborenasional.com');
      }
      else {
        next();
      }
    };

    let args = [
      this._path,
      sillyMiddleware,
      ...this._middlewares,
      (req, res, next) => {
        this._handler(req, res, next);
      }
    ];

    expressAppInstance[this._method].apply(expressAppInstance, args);
  }
}
