import { OK, UNAUTHORIZED, INTERNAL_SERVER_ERROR } from './constants/HttpStatus';
import { JSON_API, JADE_PAGE } from './constants/ResponseType';

import Logger from './logger/Logger';
import JadePageResponse from './JadePageResponse';
import JsonApiResponse from './JsonApiResponse';

export default class BaseController {
  constructor(payload) {
    this._payload = payload;

    this._logger = new Logger();
  }

  authorize(req, res, next, authorizeFilterFunctions) {
    for (var i = 0; i < authorizeFilterFunctions.length; i++) {
      let fn = authorizeFilterFunctions[i];
      if (!fn()) {
        return new JsonApiResponse();
      }
    }
  }

  getPayload() {
    return this._payload;
  }

  _forbidAccess(req, res, routeData) {
    if (routeData) {
      switch (routeData.type) {
        case JSON_API:

        case JADE_PAGE:

      }
    }
  }

  // Low level handlers.

  __createHandler(route) {
    return (req, res, next) => {
      this[route.handler](req, res, next, route);
    }
  }
}
