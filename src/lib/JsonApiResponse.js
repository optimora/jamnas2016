import { JSON_API } from './constants/ResponseType';

import BaseResponse from './BaseResponse';
import TypeHelper from './helpers/TypeHelper';

export default class JsonApiResponse extends BaseResponse {
  constructor(res, status, message, data, error) {
    super(res, status);

    this._message = message;
    this._data = data;
    this._error = error;

    this._additionalData = {};
  }

  getId() {
    return JSON_API;
  }

  setAdditionalData(additionalData) {
    if (Object.keys(additionalData).indexOf('result') !== -1 || Object.keys(additionalData).indexOf('results') !== -1) {
      throw new Error('Error while set additional data. Field name \'results\' or \'result\' is not allowed.');
    }
    this._additionalData = Object.assign({}, this._additionalData, additionalData);

    return this;
  }

  _createResponse() {
    let responseObject = this._createResponseObject();
    return this._res.status(this._status).json(responseObject);
  }

  _createResponseObject() {
    let responseObject = {
      error: this._error || null,
      message: this._message || null,
      success: this._status === 200
    };

    if (TypeHelper.isArray(this._data)) {
      responseObject.results = this._data;
    }
    else {
      responseObject.result = this._data;
    }

    return Object.assign({}, responseObject, this._additionalData);
  }
}
