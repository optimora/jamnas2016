import childProcess from 'child_process';
import connectRedis from 'connect-redis';
import express from 'express';
import expressSession from 'express-session';
import bodyParser from 'body-parser';
import jade from 'jade';
import mongoose from 'mongoose';
import passport from 'passport';
import passportLocal from 'passport-local';
import passportHttpBearer from 'passport-http-bearer';
import redis from 'redis';
import webpack from 'webpack';
import webpackDevMiddleware from 'webpack-dev-middleware';

import HttpRequestLogger from './logger/HttpRequestLogger';
import Logger from './logger/Logger';
import RouteConfigParser from './RouteConfigParser';

import User from '../models/User';
import ScoutBranch from '../models/ScoutBranch';
import Contact from '../models/Contact';

import webpackConfig from '../../webpack.config';

var app = express();
var LocalStrategy = passportLocal.Strategy;
var RedisStore = connectRedis(expressSession);

export default class Application {
  constructor(appConfig, routesConfig, rawData) {
    this._config = {
      application: appConfig,
      routes: routesConfig,
      rawData
    };

    this._logger = new Logger();

    this._appHash = new Date().getTime() + '';

    childProcess.exec('git rev-parse HEAD', (error, output) => {
      if (error) {
        this._logger.error('Error while getting last commit hash in repository.', error);
      }
      else {
        this._appHash = output;
      }
      this._initialize();
    });
  }

  _initialize() {
    this._initializeCommonSettings();
    this._initializeWebpackMiddleware();
    this._initializeDatabaseConnection();
    this._initializeSessionStore();
    this._initializeAuthProvider();
    this._initializeRoutes();
  }

  _initializeCommonSettings() {
    app.set('views', process.cwd() + '/src/views');
    app.set('view engine', 'jade');
    app.use('/public', express.static('public'));
    app.use(bodyParser());

    // Use logger.
    let requestLogger = new HttpRequestLogger();
    app.use(requestLogger.createMiddleware());
  }

  _initializeRoutes() {
    const controllerPayload = {
      appVersion: this._appHash,
      rawData: {
        enumStringMap: this._config.rawData.enumStringMap
      }
    };
    let routeConfigParser = new RouteConfigParser(this._config.routes, controllerPayload);
    let routeInstances = routeConfigParser.createRoutes();

    routeInstances.forEach((routeInstance) => {
      routeInstance.__applyHandler(app);
    });

    // Handle error and not found.
    app.use((err, req, res, next) => {
      // TODO: Display appropriate page.
      console.error(err.stack);
      res.status(500).send('Oops!');
    });

    app.use((req, res, next) => {
      // TODO: Display appropriate page.
      res.status(404).send('Nothing here.');
    });
  }

  _initializeAuthProvider() {
    // TODO: Move to different class?
    passport.use(new LocalStrategy(
      {
        usernameField: 'email',
        passwordField: 'password'
      },
      (email, password, done) => {
        User
          .findOne({ email: email })
          .populate({
             path: 'branch',
             populate: {
               path: 'contact',
               model: 'Contact'
             }
          })
          .exec((findError, user) => {
            if (findError) {
              return done(findError);
            }
            if (!user || !user.validPassword(password)) {
              return done({ reason: 'Invalid credential data.' }, false, { message: 'Email atau password salah.' });
            }

            return done(null, user.getSanitizedObject());
          });
      }
    ));
    passport.serializeUser(function(user, done) {
      done(null, user);
    });
    passport.deserializeUser(function(id, done) {
      done(null, id);
    });

    app.use(passport.initialize());
    app.use(passport.session());

    this._logger.info('Authentication provider initialized.');
  }

  _initializeSessionStore() {
    // TODO: Move to different class?
    this._logger.info('Trying to connect to session store.');
    const redisConfig = this._config.application.redis;
    const redisStoreConfig = {
      pass: redisConfig.password
    };
    let redisClient = redis.createClient();
    redisClient.auth(redisConfig.password)
    redisClient.on('error', (err) => {
      this._logger.error('Can\'t connect to session store.', err);
    });
    redisClient.on('ready', () => {
      this._logger.info('Successfully connected to session store.')
    });
    let securityConfig = this._config.application.security;

    app.use(expressSession({
      store: new RedisStore(redisStoreConfig),
      secret: securityConfig.secret,
      resave: false,
      saveUninitialized: false
    }));
  }

  _initializeDatabaseConnection() {
    let mongodbConfig = this._config.application.mongodb;
    let uri = 'mongodb://' + mongodbConfig.host + ':' + mongodbConfig.port + '/' + mongodbConfig.collection;
    this._logger.info('Trying to connect to database.')
    mongoose.connect(uri, (err) => {
      if (err) {
        throw new Error('Can\'t connect to database.');
      }
      this._logger.info('Successfully connected to the database.')
    });
  }

  _initializeWebpackMiddleware() {
    if (process.env.JAMNAS2016_ENV === 'production') {
      this._logger.info('Production Mode detected. Running app as production instance.');
    }
    else if (process.env.JAMNAS2016_ENV === 'dev-api') {
      this._logger.info('API Development Mode detected. Running app without activating webpack dev middleware.')
    }
    else {
      this._logger.info('Development environtment detected, activating webpack dev middleware.');
      let compiler = webpack(webpackConfig);
      app.use(webpackDevMiddleware(compiler, {
        noInfo: false,
        publicPath: webpackConfig.output.publicPath
      }));
    }
  }

  start() {
    let port = this._config.application.app.port;
    app.listen(port, () => {
      this._logger.info(this._config.application.app.name + ' Server is listening on port: ' + port);
    });
  }
}
