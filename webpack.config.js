var fs = require('fs');
var webpack = require('webpack');

var webpackConfig = {
  entry: {}, // Will be automatically filled below.
  output: {
    filename: '[name].public.js',
    path: __dirname + '/public/assets/js',
    publicPath: '/public/assets/js/'
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        loader: 'babel',
        query: {
          presets: [ 'react', 'es2015' ]
        }
      }
    ]
  }
};

var initScriptFiles = fs.readdirSync(__dirname + '/public/src/init-scripts/');
initScriptFiles.forEach(function(fileName) {
  webpackConfig.entry[fileName.replace('.js', '')] = [ './public/src/init-scripts/' + fileName ];
});

if (process.env.JAMNAS2016_ENV === 'production') {
  webpackConfig.plugins = [
    new webpack.optimize.UglifyJsPlugin({
      mangle: false
    }),
    new webpack.DefinePlugin({
      "process.env": {
        NODE_ENV: JSON.stringify('production')
      }
    })
  ]
}

module.exports = webpackConfig;
