var appConfig = require('../../build/config/app.js')

var should = require('should');
var assert = require('assert');
var supertest = require('supertest');
var mongoose = require('mongoose');
var winston = require('winston');
var crypto = require("crypto");

var request = supertest("http://127.0.0.1:8080");

describe('Scout API', function() {
    describe('GET /api/scouts', function() {
        it('should return scouts', function (done) {
            request.get('/api/scouts')
                .set('Accept', 'application/json')
                .expect(200)
                .end(function(err, res) {
                    if (err) throw err;
                    assert.equal(res.body.success, true);
                    done();
                })
        });
    });

    describe('PUT /api/scouts', function() {
        it('should create new scout', function (done) {
            var data = {
                scout: {
                    nta         : crypto.randomBytes(10).toString('hex'),
                    gender      : 'male',
                    birthplace  : 'Jakarta',
                    birthday    : new Date('2000-01-22T14:56:59.301Z'),
                    religion    : 'islam',
                    blood_type  : 'A'
                }
            };

            request.put('/api/scouts')
                .set('Accept', 'application/json')
                .send(data)
                .expect('Content-Type', /json/)
                .expect(200)
                .end(function(err, res) {
                    if (err) throw err;
                    assert.equal(res.body.success, true);
                    done();
                })
        });
    });

    describe('GET /api/scouts/:scoutId', function() {
        var scoutId;

        // pick first scout for test
        before(function(done){
            request.get('/api/scouts')
                .set('Accept', 'application/json')
                .expect(200)
                .end(function(err, res) {
                    if (err) throw err;
                    assert.equal(res.body.success, true);
                    scoutId = res.body.results[0]._id;
                    done();
                })
        });

        it('should return scouts', function (done) {
            request.get('/api/scouts/'+scoutId)
                .set('Accept', 'application/json')
                .expect(200)
                .end(function(err, res) {
                    if (err) throw err;
                    assert.equal(res.body.success, true);
                    done();
                })
        });
    });

    describe('POST /api/scouts/:scoutId', function() {
        var scoutId;

        // pick first scout for test
        before(function(done){
            request.get('/api/scouts')
                .set('Accept', 'application/json')
                .expect(200)
                .end(function(err, res) {
                    if (err) throw err;
                    assert.equal(res.body.success, true);
                    scoutId = res.body.results[0]._id;
                    done();
                })
        });

        it('should update existing scout', function (done) {
            var data = {
                scout: {
                    nta         : crypto.randomBytes(10).toString('hex'),
                    gender      : 'female',
                    birthplace  : 'Aceh',
                    birthday    : new Date('1998-01-22T14:56:59.301Z'),
                    religion    : 'kristen',
                    blood_type  : 'C'
                }
            };
            
            request.post('/api/scouts/'+scoutId)
                .set('Accept', 'application/json')
                .send(data)
                .expect('Content-Type', /json/)
                .expect(200)
                .end(function(err, res) {
                    if (err) throw err;
                    assert.equal(res.body.success, true);
                    done();
                })
        });
    });
});



describe('ScoutBranch API', function() {
    describe('GET /api/branches', function() {
        it('should return brances', function (done) {
            request.get('/api/branches')
                .set('Accept', 'application/json')
                .expect(200)
                .end(function(err, res) {
                    if (err) throw err;
                    assert.equal(res.body.success, true);
                    done();
                })
        });
    });

    describe('PUT /api/branches', function() {
        it('should create new branch', function (done) {
            var data = {
                branch: {
                    id         : crypto.randomBytes(10).toString('hex'),
                    alias      : 'Lorem Ipsum',
                }
            };

            request.put('/api/branches')
                .set('Accept', 'application/json')
                .send(data)
                .expect('Content-Type', /json/)
                .expect(200)
                .end(function(err, res) {
                    if (err) throw err;
                    assert.equal(res.body.success, true);
                    done();
                })
        });
    });

    describe('GET /api/branches/:branchId', function() {
        var branchId;

        // pick first scout for test
        before(function(done){
            request.get('/api/branches')
                .set('Accept', 'application/json')
                .expect(200)
                .end(function(err, res) {
                    if (err) throw err;
                    assert.equal(res.body.success, true);
                    branchId = res.body.results[0]._id;
                    done();
                })
        });

        it('should return branches', function (done) {
            request.get('/api/branches/'+branchId)
                .set('Accept', 'application/json')
                .expect(200)
                .end(function(err, res) {
                    if (err) throw err;
                    assert.equal(res.body.success, true);
                    done();
                })
        });
    });

    describe('POST /api/branches/:scoutId', function() {
        var branchId;

        // pick first scout for test
        before(function(done){
            request.get('/api/branches')
                .set('Accept', 'application/json')
                .expect(200)
                .end(function(err, res) {
                    if (err) throw err;
                    assert.equal(res.body.success, true);
                    branchId = res.body.results[0]._id;
                    done();
                })
        });

        it('should update existing branch', function (done) {
            var data = {
                branch: {
                    id         : crypto.randomBytes(10).toString('hex'),
                    alias      : 'Dolor Ismet',
                }
            };

            request.post('/api/branches/'+branchId)
                .set('Accept', 'application/json')
                .send(data)
                .expect('Content-Type', /json/)
                .expect(200)
                .end(function(err, res) {
                    if (err) throw err;
                    assert.equal(res.body.success, true);
                    done();
                })
        });
    });

    describe('PUT /api/locations', function() {
        it('should create new location', function (done) {
            var data = {
                location: {
                    name      : 'Jakarta',
                }
            };

            request.put('/api/locations')
                .set('Accept', 'application/json')
                .send(data)
                .expect('Content-Type', /json/)
                .expect(200)
                .end(function(err, res) {
                    if (err) throw err;
                    assert.equal(res.body.success, true);
                    done();
                })
        });
    });
});
