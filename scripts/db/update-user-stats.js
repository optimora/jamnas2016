/**
 * CAUTION: THIS OPERATION IS VERY HEAVY! DON'T DO THIS IN PEAK HOURS.
 */

var mongoose = require('mongoose');

var Scout = require('../../build/models/Scout').default;
var ScoutBranch = require('../../build/models/ScoutBranch').default;
var User = require('../../build/models/User').default;

var config = require('../../build/config/app').default;

// Connect to database first.
var mongodbConfig = config.mongodb;
var uri = 'mongodb://' + mongodbConfig.host + ':' + mongodbConfig.port + '/' + mongodbConfig.collection;

mongoose.connect(uri, (err) => {
  if (err) {
    throw new Error('Can\'t connect to database.');
  }
  console.log('Connected to the database.');
});

User
  .find({})
  .exec(function(findError, users) {
    if (findError) {
      console.log('Error!');
      console.log(findError);
      mongoose.connection.close();
    }
    else {
      users.forEach(function(user) {
        if (user.role === 'scout-branch-admin') {
          ScoutBranch
            .find({ parent: user.branch })
            .exec(function(findError, branches) {
              var functions = [];

              branches.forEach(function(branch) {
                functions.push(function(callback) {
                  Scout
                    .find({ subbranch: branch._id })
                    .exec(function(findError, scouts) {
                      if (findError) {
                        console.log('Error while counting scouts data.');
                        console.log(findError);
                        callback(null, 0);
                      }
                      else {
                        callback(null, {
                          branch: branch.toObject(),
                          count: scouts.length
                        });
                      }
                    });
                });

                async.parallel(
                  functions,
                  function(asyncError, results) {
                    user.stats.dashboard = results;
                    user.markModified('stats');
                    user.save(function(saveError) {
                      if (saveError) {
                        console.log('Error saving stats data for user ' + user.email + '.');
                        console.log(saveError);
                      }
                      else {
                        
                      }
                    });
                  }
                );
              });
            });
        }
      });
    }
  });