var mongoose = require('mongoose');

var User = require('../../build/models/User').default;

var config = require('../../build/config/app').default;

// Connect to database first.
var mongodbConfig = config.mongodb;
var uri = 'mongodb://' + mongodbConfig.host + ':' + mongodbConfig.port + '/' + mongodbConfig.collection;

mongoose.connect(uri, (err) => {
  if (err) {
    throw new Error('Can\'t connect to database.');
  }
  console.log('Connected to the database.');
});

// Create Super Admin.

var user = new User();

user.name = 'Seorang Super Admin';
user.role = 'admin';
user.email = 'super.admin@jamnasx2016.com';
user.password = '1234567890';

user.save(function(saveError, user) {
  if (saveError) {
    console.log('Failed.');
    console.log(saveError);
  }
  else {
    console.log('Success.');
  }

  mongoose.connection.close();
});
