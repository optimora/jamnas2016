var async     = require('async');
var fs       = require('fs');
var mongoose   = require('mongoose');

var BadgeType = require('../../build/models/BadgeType').default;
var ScoutBranch = require('../../build/models/ScoutBranch').default;

var config = require('../../build/config/app').default;

// Connect to database first.
var mongodbConfig = config.mongodb;
var uri = 'mongodb://' + mongodbConfig.host + ':' + mongodbConfig.port + '/' + mongodbConfig.collection;

mongoose.connect(uri, (err) => {
  if (err) {
    throw new Error('Can\'t connect to database.');
  }
  console.log('Connected to the database.');
});

// Read file.
var branches = JSON.parse(fs.readFileSync('branches.json'));
// Sanitize
branches.forEach(function(branch) {
  Object.keys(branch).forEach(function(key) {
    if (branch[key] === 'NULL') {
      branch[key] = null;
    }
  });
});

var functions = [];

var orphan = function(branch) {
  return branch.parent == null;
};
var child = function(branch) {
  return branch.parent != null;
};

var numerize = function(n) {
  if (n < 10) {
    return '00' + n;
  }
  if (n < 100) {
    return '0' + n;
  }
  return n + '';
};

branches.filter(orphan).forEach(function(rawBranchData) {
  functions.push(function(callback) {
    var branch = new ScoutBranch();

    Object.keys(rawBranchData).forEach(function(key) {
      if (key === 'code') {
        branch[key] = numerize(rawBranchData[key]);
      }
      else {
        branch[key] = rawBranchData[key];
      }
    });

    branch.save(callback);
  });
});

branches.filter(child).forEach(function(rawBranchData) {
  functions.push(function(callback) {
    ScoutBranch
      .findOne({code: numerize(rawBranchData.parent)})
      .exec(function(findError, parentBranch) {
        if (findError) {
          callback(findError, null);
        }
        else {
          var branch = new ScoutBranch();

          Object.keys(rawBranchData).forEach(function(key) {
            if (key === 'code' || key === 'camp_subcode') {
              branch[key] = numerize(rawBranchData[key]);
            }
            else if (key === 'parent') {
              branch[key] = mongoose.Types.ObjectId(parentBranch.id);
            }
            else {
              branch[key] = rawBranchData[key];
            }
          });

          branch.save(callback);
        }
      });
  });
});

async.series(
  functions,
  function(asyncError, results) {
    if (asyncError) {
      console.log(asyncError);
    }
    mongoose.connection.close();
    console.log('Done, ' + results.length + ' data has been successfully inserted.');
  }
);
