var mongoose = require('mongoose');

var ScoutBranch = require('../../build/models/ScoutBranch').default;
var User = require('../../build/models/User').default;

var config = require('../../build/config/app').default;

// Connect to database first.
var mongodbConfig = config.mongodb;
var uri = 'mongodb://' + mongodbConfig.host + ':' + mongodbConfig.port + '/' + mongodbConfig.collection;

mongoose.connect(uri, (err) => {
  if (err) {
    throw new Error('Can\'t connect to database.');
  }
  console.log('Connected to the database.');
});

// Create Dummy Branches

var branch = {
  "camp_subcode" : null,
  "camp_code" : null,
  "camp_name" : null,
  "parent" : null,
  "logo" : "1.png",
  "name" : "(pura-Puranya) Kwarda Nanggroe Aceh Darussalam",
  "code" : "999",
  "stats" : {
    "numberOfScouts" : 0
  },
  "created": new Date()
};

var subbranches = [
  {
    "camp_subcode" : "1044",
    "camp_code" : "1",
    "camp_name" : "TJILIK RIWUT & TJUT NYA DIEN",
    "created" : new Date(),
    "logo" : null,
    "name" : "(pura-puranya) Kwarcab Kota Dumai",
    "code" : "1000",
    "stats" : {
      "numberOfScouts" : 0
    }
  },
  {
    "camp_subcode" : "1021",
    "camp_code" : "1",
    "camp_name" : "TJILIK RIWUT & TJUT NYA DIEN",
    "created" : new Date(),
    "logo" : null,
    "name" : "(pura-puranya) Kwarcab Indragiri Hulu",
    "code" : "1001",
    "stats" : {
      "numberOfScouts": 0
    }
  }
];

var parentScoutBranch = new ScoutBranch(branch);

parentScoutBranch.save(function(saveError) {
  if (saveError) {
    console.log(saveError);
  }
  else {
    subbranches.forEach(function(rawData) {
      var scoutBranchChild = new ScoutBranch(rawData);
      scoutBranchChild.parent = parentScoutBranch._id;

      scoutBranchChild.save(function(saveError) {
        if (saveError) {
          console.log(saveError);
        }
      });
    });

    var user = new User();

    user.name = 'Admin Tester';
    user.role = 'scout-branch-admin';
    user.email = 'admin.tester@jamnasx2016.com';
    user.password = '1234567890';

    user.save(function(saveError, user) {
      if (saveError) {
        console.log('Failed.');
        console.log(saveError);
      }
      else {
        console.log('Success.');
      }

      mongoose.connection.close();
    });

  }
});
