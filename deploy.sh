echo 'Stopping running app instance.'
pm2 stop jamnas2016-web
echo 'Deploying app.'
echo 'Pulling latest changes from repository server.'
git pull origin master
echo 'Run build for server side files.'
npm run build
echo '- Completed.'
echo 'Run build for client side files.'
npm run build-static
echo '- Completed.'
echo 'Trying to start process manager.'
pm2 start build/index.js --name jamnas2016-web
echo 'App deployed.'
