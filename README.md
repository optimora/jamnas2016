# JAMNAS 2016 Projects

## Prerequisites
This document assumes that you've install all supporting software to run this. Ensure that all the following software listed below has been installed:

* Node.js >= v4.0.0
* Redis >= 2.x
* MongoDB >= 2.x

## Setup Instructions
Clone this repo, and then follow the following instructions. Don't forget to open your terminal.

### Installing Node.js Dependencies
If this is the first time for you to install, then execute this command:

```
npm install
```

### Running The Server
We use ES2015 JavaScript for both client and server side for this project. Each time any of the source files edited, we need to compile them. For development mode, do each of these instructions below every time you want to start development process.


#### Add Config File
To run this server, you need to provide all credentials needed to connect instance with all data provider. Rename `src/config/app.js.default` to `src/config/app.js`, and you should good to go.

Sample config file:
```
#!javascript
export default {
  app: {
    name: 'JAMNAS2016',
    port: 8080
  },
  mail: {
    sendgrid: {
      auth: {
        user: 'SENDGRID_USER',
        key: 'SENDGRID_KEY'
      }
    }
  },
  mongodb: {
    host: '127.0.0.1',
    port: 27017,
    username: null,
    password: null,
    collection: 'jamnas2016'
  },
  redis: {
    host: '127.0.0.1',
	  port: 6379
  },
  security: {
    salt: 'FwbDbzTRHRBWWq2hZQjSxjD6rPv6bth6p03KpPwn',
    secret: 'XdG8hqJxy4cHh3rY7yjsSFRahI4J6Az0iI2ZWwvG'
  }
};
```

#### Build and Watch Your Server Source Code
Run the following command, and **don't close the window**.

```
npm run build-and-watch
```

#### Start Server
Run the following command, and also **don't close the window**.

```
nodemon build/index.js
```

## Developer Notes

### ES2015
We use ES2015 or ES6 syntax both in server side and client side ``*.js` files. This allow us to use several features of new language like described here: http://es6-features.org/

### Route Config
All routes for this projects are stored in `src/config/routes.js`. Every route consists of the following structure:
```
{
  ...,
  <string>: {
    method: <HttpMethod>,
    responseType: <HttpResponseType>,
    path: <string>,
    controller: <ControllerClass>,
    handler: <string>,
    filters: [ ..., FilterClass ]
  }
}
```



### Controller
Every route will be handled by 1 function/method inside a controller file. The structure of the controller described below:

```
#!javascript
// Some import statements here.

export default class SomethingController extends BaseController {
  functionOne(req, res) {

  }

  functionTwo(req, res) {

  }
}
```

Every function that you use as a route handler should have 2 parameters (`req` and `res`) and 1 response return statement.

### Client Side Scripts
For sake of simplicity, please ensure that **each route should only have 1 page handler *.js file**. And please note that all page handler file should be saved in `init-scripts` directory. It's the only entry point directory that will be compiled by webpack.

### Testing
Testing is available using `mocha` command on project root.
