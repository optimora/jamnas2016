import React from 'react';

export default class FileInputField extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      fileName: '',
      formId: 'upload_form_' + new Date().getTime(),
      iframeId: 'upload_target_' + this._generateId()
    };

    // Internal global callback function.
    window.__jamnas2016FileInputFieldNotify = (data) => {
      this._uploadFileResultsDispatcher(data);
    };
  }

  static get propTypes() {
    return {
      name: React.PropTypes.string.isRequired,
      topic: React.PropTypes.string.isRequired,
      label: React.PropTypes.string.isRequired,

      onSuccess: React.PropTypes.func.isRequired,
      onFail: React.PropTypes.func.isRequired
    }
  }

  _generateId() {
    return (new Date()).getTime();
  }

  _handleInputFileChange(event) {
    this.setState({ fileName: event.target.value });

    this._startUpload();
  }

  _startUpload() {
    $('#' + this.state.formId).submit();
  }

  _uploadFileResultsDispatcher(data) {
    if (data.success) {
      this.props.onSuccess(data);
    }
    else {
      this.props.onFail(data);
    }
  }

  render() {
    let iframeStyle = { width: '1px', height: '1px', border: 0 };
    return (
      <div>
        <form encType='multipart/form-data' target={this.state.iframeId} id={this.state.formId} method='post' action={'/users/upload?fieldHint=' + this.props.name + '&topic=' + this.props.topic} >
          <input id={this.props.name} name={this.props.name} type='file' value={this.state.fileName} onChange={this._handleInputFileChange.bind(this)}
            className='inputFile' />
          <label htmlFor={this.props.name}>{this.props.label}</label>
        </form>
        <iframe name={this.state.iframeId} id={this.state.iframeId} src='about:blank' style={iframeStyle}></iframe>
      </div>
    );
  }
}