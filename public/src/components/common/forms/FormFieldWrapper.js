import React from 'react';

export default class FormFieldWrapper extends React.Component {
  constructor(...args) {
    super(...args);

    this.state = {
      isPristine: true,
      value: ''
    };
  }

  static get propTypes() {
    return {
      name: React.PropTypes.string.isRequired,
      onChange: React.PropTypes.func.isRequired,
      fieldValue: React.PropTypes.string,

      // Optional Runtime Props.
      className: React.PropTypes.string,
      isValid: React.PropTypes.bool,
      validationMessage: React.PropTypes.string
    }
  }

  _generateWrapperClassName() {
    let result = 'form-group';
    if (!this._isValid()) {
      result += ' has-error';
    }
    return result + ' ' + (this.props.className || '');
  }

  _handleFieldValueChange(value) {
    this.setState(
      {
        value,
        isPristine: false
      },
      () => {
        this.props.onChange(this.props.name, value);
      }
    );
  }

  _isValid() {
    if (this.props.isValid != undefined && this.props.isValid != null) {
      return this.props.isValid;
    }
    return true;
  }

  componentWillMount() {
    this.setState({ value: this.props.fieldValue });
  }

  componentWillReceiveProps(props) {
    this.setState({ value: props.fieldValue });
  }

  render() {
    let mark = <span></span>;
    if (!this._isValid() && this.props.validationMessage && this.props.validationMessage != '') {
      mark = <mark className='register'>{this.props.validationMessage || ''}</mark>
    }

    return (
      <div className={this._generateWrapperClassName()}>
        {React.Children.map(this.props.children, (child) => {
          if (child) {
            if (child.type === 'input' || child.type === 'select' || child.type === 'textarea') {
              return React.cloneElement(child, {
                key: this.props.name,
                value: this.state.value,
                onChange: (event) => {
                  this._handleFieldValueChange(event.target.value);
                }
              });
            }
            return child;
          }
          return null;
        })}
        {mark}
      </div>
    );
  }
}