import React from 'react';

export default class SimpleSelectField extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      fieldValue: ''
    };
  }

  static get propTypes() {
    return {
      label: React.PropTypes.string,
      name: React.PropTypes.string,
      placeholder: React.PropTypes.string,
      options: React.PropTypes.array,

      // Internal handlers.
      onChange: React.PropTypes.func
    }
  }

  _handleChange(event) {
    this.setState({
      fieldValue: event.target.value
    });

    if (this.props.onChangeListener) {
      this.props.onChangeListener(this.props.name, event.target.value);
    }
  }

  render() {
    return (
      <div className='form-group'>
        <label htmlFor={this.props.name}>{this.props.label}</label>
        <select className='form-control' value={this.state.fieldValue} onChange={this._handleChange.bind(this)}>
          <option value=''>{this.props.placeholder}</option>
          {this.props.options.map(option => {
            return <option key={option.key} value={option.key}>{option.value}</option>
          })}
        </select>
      </div>
    );
  }
};
