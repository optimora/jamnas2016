import React from 'react';

export default class TextField extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      fieldValue: ''
    };
  }

  static get propTypes() {
    return {
      name: React.PropTypes.string.isRequired,
      label: React.PropTypes.string,
      placeholder: React.PropTypes.string,
      rows: React.PropTypes.number,

      onChangeListener: React.PropTypes.func
    }
  }

  _handleChange(event) {
    this.setState({
      fieldValue: event.target.value
    });

    if (this.props.onChangeListener) {
      this.props.onChangeListener(this.props.name, event.target.value);
    }
  }

  render() {
    return (
      <div className='form-group'>
        { this.props.label ? <label htmlFor={this.props.name}>{this.props.label}</label> : null }
        <textarea rows={this.props.rows} className='form-control' name={this.props.name} onChange={this._handleChange.bind(this)}>
          {this.state.fieldValue}
        </textarea>
      </div>
    );
  }
}
