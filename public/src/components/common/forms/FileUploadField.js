import React from 'react';

import DialogHelper from '../../../lib/helpers/DialogHelper';

export default class FileUploadField extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      fieldValue: this.props.fieldValue,

      uploadedFileData: null,
      status: 'idle'
    };


    this.formId = 'upload_form_' + this.props.name + new Date().getTime();
    this.iframeId = 'upload_target_' + this.props.name + new Date().getTime();
    this.callbackFunctionName = this.props.name + '-fileFieldCallbackFunction';

    // Internal global callback function.
    window[this.callbackFunctionName] = (data) => {
      this._uploadFileResultsDispatcher(data);
    };

  }

  static get propTypes() {
    return {
      name: React.PropTypes.string.isRequired,
      topic: React.PropTypes.string.isRequired,
      label: React.PropTypes.string.isRequired,
      fieldValue: React.PropTypes.string.isRequired,

      onSuccess: React.PropTypes.func.isRequired,
      onFail: React.PropTypes.func.isRequired
    }
  }

  _emitFieldValueChange() {
    if (this.state.uploadedFileData && this.state.uploadedFileData.storedFile) {
      this.props.onChange(this.state.uploadedFileData.storedFile._id);
    }
    this.props.onSuccess('');
  }

  _handleInputFileChange() {
    this.setState({
      status: 'uploading'
    });

    this._startUpload();
  }

  _handleRemoveFile(event) {
    event.preventDefault();
    DialogHelper.showConfirmationMessage(
      'Konfirmasi', 'Apakah Anda yakin? File yang dihapus tidak dapat dikembalikan.',
      () => {
        DialogHelper.showWarningMessage('Berhasil', 'Penghapusan telah dilakukan.');
        this.setState(
          {
            uploadedFileData: null,
            fieldValue: ''
          },
          () => {
            this._emitFieldValueChange();
          }
        );
      },
      () => {
        DialogHelper.showSuccessMessage('Dibatalkan', 'Aksi penghapusan data telah dibatalkan.')
      }
    );
  }

  _startUpload() {
    $('#' + this.formId).submit();
  }

  _uploadFileResultsDispatcher(data) {
    console.log('External upload file results loaded.', data);

    this.setState(
      {
        status: 'idle',
        uploadedFileData: data,
        fieldValue: (data.storedFile || {})._id || null
      },
      () => {
        if (data.success) {
          this.props.onSuccess(data);
        }
        else {
          this.props.onFail(data);
        }
      }
    );
  }

  render() {
    const iframeStyle = { width: '1px', height: '1px', border: 0 };

    let fileUploadControl;
    let deleteLink = <div></div>;

    if (this.state.status === 'idle') {
      fileUploadControl = <label htmlFor={this.props.name}>{this.props.label}</label>
    }
    else {
      fileUploadControl = <div>Harap tunggu..</div>
    }

    const displayDeleteOrViewControl = () => {
      deleteLink = <div>
        <a className='btn btn-danger btn-xs' href='#' onClick={this._handleRemoveFile.bind(this)}>Hapus</a>{' '}
        <a className='btn btn-success btn-xs' href={'/stored_files/' + this.state.fieldValue} target='_blank'>Lihat</a>
      </div>;
      fileUploadControl = <div></div>;
    };
    if (this.state.status === 'idle' && this.state.uploadedFileData != null && this.state.uploadedFileData.storedFile != null) {
      displayDeleteOrViewControl();
    }
    if (this.state.status === 'idle' && this.state.fieldValue !== '' && this.state.fieldValue != null && this.state.fieldValue != undefined) {
      displayDeleteOrViewControl();
    }

    return (
      <div>
        {deleteLink}
        <form encType='multipart/form-data' target={this.iframeId} id={this.formId} method='post' action={'/users/upload?fieldHint=' + this.props.name + '&topic=' + this.props.topic + '&callbackFunctionName=' + this.callbackFunctionName} >
          <input id={this.props.name} name={this.props.name} type='file' value={this.state.fileName} onChange={this._handleInputFileChange.bind(this)}
            className='inputFile' />
          {fileUploadControl}
        </form>
        <iframe name={this.iframeId} id={this.iframeId} src='about:blank' style={iframeStyle}></iframe>
      </div>
    )
  }
}