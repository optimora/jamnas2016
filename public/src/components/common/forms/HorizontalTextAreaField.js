import React from 'react';

export default class HorizontalTextAreaField extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      fieldValue: ''
    };
  }

  static get propTypes() {
    return {
      label: React.PropTypes.string,
      name: React.PropTypes.string,
      placeholder: React.PropTypes.string,

      onChangeListener: React.PropTypes.func
    }
  }

  _handleChange(event) {
    this.setState({
      fieldValue: event.target.value
    });

    if (this.props.onChangeListener) {
      this.props.onChangeListener(this.props.name, event.target.value);
    }
  }

  render() {
    return (
      <div className="row padding-bottom-xs">
        <div className="form-group form-horizontal">
            <label className="control-label col-sm-2" for={this.props.name}>{this.props.label}</label>
            <div className="col-sm-10">
              <textarea className={"form-control " + this.props.className} onChange={this._handleChange.bind(this)}>
                {this.state.fieldValue}
              </textarea>
            </div>
        </div>
      </div>
    );
  }
};
