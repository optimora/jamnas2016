import React from 'react';

export default class SubmitButton extends React.Component {
  static get propTypes() {
    return {
      formId: React.PropTypes.string,
      label: React.PropTypes.string,
      submitHandler: React.PropTypes.func
    };
  }

  render() {
    // TODO: Provide icon and better label?
    return (
      <button onClick={this.props.submitHandler} className={this.props.className} type='submit' form={this.props.formId}>
        {this.props.icon ? <i className={"fa "+ this.props.icon + " margin-right-xs"}></i> : null }
        {this.props.label}
      </button>
    );
  }
};
