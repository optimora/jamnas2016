import React from 'react';

export default class PasswordField extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      fieldValue: ''
    };
  }

  static get propTypes() {
    return {
      label: React.PropTypes.string,
      name: React.PropTypes.string,
      placeholder: React.PropTypes.string,

      onChangeListener: React.PropTypes.func
    }
  }

  _handleChange(event) {
    this.setState({
      fieldValue: event.target.value
    });

    if (this.props.onChangeListener) {
      this.props.onChangeListener(this.props.name, event.target.value);
    }
  }

  render() {
    return (
      <div className='form-group'>
        <label htmlFor={this.props.name}>{this.props.label}</label>
        <input type="password" className='form-control' name={this.props.name} value={this.state.fieldValue} onChange={this._handleChange.bind(this)} />
      </div>
    );
  }
};
