import React from 'react';

export default class SimpleDatePickerField extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      day: '' + this.props.fieldValue.getDate(),
      month: '' + (this.props.fieldValue.getMonth() + 1),
      year: '' + this.props.fieldValue.getFullYear()
    };
  }

  static get propTypes() {
    return {
      minYear: React.PropTypes.number.isRequired,
      fieldValue: React.PropTypes.object.isRequired,

      isValid: React.PropTypes.bool,
      validationMessage: React.PropTypes.string,

      onChange: React.PropTypes.func.isRequired
    };
  }

  _handleFieldValueChange(fieldName, value) {
    let updatedData = {};
    updatedData[fieldName] = value;
    this.setState(
      {
        [fieldName]: value
      },
      () => {
        let day = this.state.day === '' ? 1 : parseInt(this.state.day);
        let month = this.state.month === '' ? 1 : parseInt(this.state.month) - 1;
        let year = this.state.year === '' ? 1 : parseInt(this.state.year);
        this.props.onChange(new Date(year, month, day, 0, 0, 0));
      }
    );
  }

  render() {
    const generateDays = () => {
      let results = [];
      for (var i = 1; i <= 31; i++) {
        results.push(i);
      }
      return results;
    };
    const generateYears = () => {
      let results = [];
      for (var i = this.props.minYear; i <= new Date().getFullYear(); i++) {
        results.push(i);
      }
      return results;
    };
    return (
      <div className="form-inline">
        <div className="form-group">
          <select className="form-control margin-right-xs" value={this.state.day} onChange={(event) => this._handleFieldValueChange('day', event.target.value)}>
            {generateDays().map((day) => {
              return <option key={day} value={day}>{day}</option>;
            })}
          </select>
          <select className="form-control margin-right-xs" value={this.state.month} onChange={(event) => this._handleFieldValueChange('month', event.target.value)}>
            <option key="1" value="1">Januari</option>
            <option key="2" value="2">Februari</option>
            <option key="3" value="3">Maret</option>
            <option key="4" value="4">April</option>
            <option key="5" value="5">Mei</option>
            <option key="6" value="6">Juni</option>
            <option key="7" value="7">Juli</option>
            <option key="8" value="8">Agustus</option>
            <option key="9" value="9">September</option>
            <option key="10" value="10">Oktober</option>
            <option key="11" value="11">November</option>
            <option key="12" value="12">Desember</option>
          </select>
          <select className="form-control margin-right-xs" value={this.state.year} onChange={(event) => this._handleFieldValueChange('year', event.target.value)}>
            {generateYears().map((year) => {
              return <option key={year} value={year}>{year}</option>;
            })}
          </select>
        </div>
      </div>
    );
  }
}