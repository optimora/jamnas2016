import React from 'react';

export default class TextField extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      fieldValue: '',
      isPristine: true,
      isValid: this.props.isValid || false
    };
  }

  static get propTypes() {
    return {
      name: React.PropTypes.string.isRequired,
      disabled: React.PropTypes.bool,
      isValid: React.PropTypes.bool,
      label: React.PropTypes.string,
      placeholder: React.PropTypes.string,
      validationRules: React.PropTypes.array,

      onChangeListener: React.PropTypes.func
    }
  }

  _handleChange(event) {
    this.setState(
      {
        fieldValue: event.target.value,
        isPristine: false
      },
      () => {
        if (this.props.onChangeListener) {
          this.props.onChangeListener(this.props.name, this.state.fieldValue);
        }
      }
    );
  }

  _generateErrorClassNameIfAny() {
    if (!this.state.isPristine && !this.props.isValid) {
      return ' has-error';
    }
    return '';
  }

  render() {
    return (
      <div className={'form-group' + this._generateErrorClassNameIfAny()}>
        { this.props.label ? <label htmlFor={this.props.name}>{this.props.label}</label> : null }
        <input disabled={this.props.disabled} type="text" className='form-control' name={this.props.name} value={this.state.fieldValue} placeholder={this.props.placeholder} onChange={this._handleChange.bind(this)} />
      </div>
    );
  }
}
