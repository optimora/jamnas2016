import React from 'react';

export default class HeaderContent extends React.Component {
  static get propTypes() {
    return {
      subtitle: React.PropTypes.string.isRequired,
      title: React.PropTypes.string.isRequired
    };
  }

  render() {
    return(
      <div>
        <div className = 'col-md-12'>
          <div className = 'col-md-10 margin-top-lg'>
            <p className = ''> {this.props.subtitle}</p>
            <h1 className = 'bold-700 margin-top-minus-xs'>{this.props.title}</h1>
          </div>
          <div className = 'col-md-2 margin-top-xs'>
            <img src={this.props.logo} className='float-right' style={{ 'width': '60%' }}/>
          </div>
        </div>
      </div>
    );
  }
}
