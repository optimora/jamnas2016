import React from 'react';

class ScoutDataTableRow extends React.Component {
  static get propTypes() {
    return {
      no: React.PropTypes.number.isRequired,
      name: React.PropTypes.string.isRequired,
      branchName: React.PropTypes.string.isRequired,
      subbranchName: React.PropTypes.string.isRequired,
      role: React.PropTypes.string.isRequired,
      created: React.PropTypes.string.isRequired,
      actions: React.PropTypes.element.isRequired,

      roleDictionary: React.PropTypes.object
    };
  }

  render() {
    const getRole = (roleEnum) => this.props.roleDictionary ? this.props.roleDictionary[roleEnum] : roleEnum;
    return (
      <tr>
        <td>{this.props.no}</td>
        <td>{this.props.name}</td>
        <td>{this.props.branchName}</td>
        <td>{this.props.subbranchName}</td>
        <td>{getRole(this.props.role)}</td>
        <td>{this.props.created}</td>
        <td className="align-center">
          {this.props.actions}
        </td>
      </tr>
    );
  }
}

export default class ScoutDataTable extends React.Component {
  constructor(...args) {
    super(...args);

    this.state = {
      searchQuery: ''
    };
  }

  static get propTypes() {
    return {
      title: React.PropTypes.string.isRequired,
      inputDataUrl: React.PropTypes.string.isRequired,
      rows: React.PropTypes.array.isRequired,

      disableInput: React.PropTypes.bool,
      roleDictionary: React.PropTypes.object,
      onSearchButtonClicked: React.PropTypes.func
    };
  }

  _handleSearchButtonClicked() {
    this.props.onSearchButtonClicked(this.state.searchQuery);
  }

  _handleSearchFieldChange(event) {
    this.setState({
      searchQuery: event.target.value
    });
  }

  render() {
    let inputControl;
    if (typeof this.props.disableInput === 'boolean' && !this.props.disableInput) {
      inputControl = <div className="col-lg-2 pull-left">
        <a href={this.props.inputDataUrl} className="btn btn-sm btn-primary">
          <i className="fa fa-plus"></i> Input Data
        </a>
      </div>
    }
    else {
      inputControl = <div className="col-lg-9 pull-left">
        <a href='#' className="btn btn-sm bg-dark-gray">
          <i className="fa fa-plus"></i> Input Data
        </a>
        <p className='fg-red'>Penginputan data sudah tidak dapat dilakukan! Untuk informasi lebih lanjut, harap hubungi Panitia Jambore Nasional 2016</p>
      </div>;
    }

    let counter = 0;
    return(
      <div>
        <div className="ibox float-e-margins">
          <div className="ibox-title">
            <h5>{this.props.title}</h5>
          </div>
          <div className="ibox-content">
            <div className="row">
              {inputControl}
              <div className="col-lg-3 pull-right">
                <div className="input-group">
                  <input type="text" placeholder="Search" className="input-sm form-control" value={this.state.searchQuery} onChange={this._handleSearchFieldChange.bind(this)} />
                  <span className="input-group-btn">
                    <button type="button" className="btn btn-sm btn-primary" onClick={() => this._handleSearchButtonClicked()}> Go!</button>
                  </span>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-4">
                <p className="padding-top-bottom-xs no-margin"></p>
              </div>
            </div>
            <div className="table-responsive">
              <table className="table table-striped table-bordered table-hover dataTables-example" >
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>Nama</th>
                    <th>Kwartir Daerah</th>
                    <th>Kwartir Cabang</th>
                    <th>Keikutsertaan</th>
                    <th>Tanggal Daftar</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  {this.props.rows.map((row) => {
                    counter++;
                    let actions = <a href={'#'} className="btn btn-success btn-xs no-margin">
                      Lihat
                    </a>;

                    let props = {
                      no: counter,
                      name: row.name,
                      branchName: row.branchName,
                      subbranchName: row.subbranchName,
                      role: row.role,
                      created: row.created,
                      actions: row.actions || actions,
                      roleDictionary: this.props.roleDictionary
                    };
                    return (
                      <ScoutDataTableRow key={props.no} {...props} />
                    );
                  })}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
