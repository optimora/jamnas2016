import React from 'react';

export default class ScoutSubbranchFormHeader extends React.Component {
  static get propTypes() {
    return {
      branch: React.PropTypes.object.isRequired,
      subbranch: React.PropTypes.object.isRequired
    };
  }

  render() {
    let getImageUrl = () => {
      // TODO: Repair data.
      if (this.props.branch.logo.length === 5) {
        return '/public/assets/images/branch_logo/0' + this.props.branch.logo;
      }
      else {
        return '/public/assets/images/branch_logo/' + this.props.branch.logo;
      }
    };
    return (
      <div>
        <div className='col-md-12'>
          <div className='col-md-10 margin-top-lg'>
            <p> Peserta Jambore Nasional X 2016 - <strong>{this.props.branch.name}</strong></p>
            <h1 className='fg-black margin-top-minus-xs'>{this.props.subbranch.name}</h1>
          </div>
          <div className='col-md-2 margin-top-xs'>
            <img src={getImageUrl()} className='float-right' style={{ 'width': '60%' }}/>
          </div>
        </div>
      </div>
    );
  }
}
