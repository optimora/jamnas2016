import React from 'react';

import ScoutBranchDataGridItem from './ScoutBranchDataGridItem';

export default class ScoutBranchDataGrid extends React.Component {
  static get propTypes() {
    return {
      name: React.PropTypes.string.isRequired,
      items: React.PropTypes.array.isRequired
    };
  }

  render() {
    return(
      <div className="ibox float-e-margins">
        <div className="ibox-title">
          <h4>{ this.props.name }</h4>
        </div>
        <div className="ibox-content padding-bottom-lg">
          <div className="row">
            {this.props.items.map((item) => <ScoutBranchDataGridItem key={item.id} {...item} />)}
          </div>
        </div>
      </div>
    );
  }
}
