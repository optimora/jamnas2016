import React from 'react';

export default class ScoutBranchDataGridItem extends React.Component {
  static get propTypes() {
    return {
      id: React.PropTypes.string.isRequired,
      label: React.PropTypes.string.isRequired,
      name: React.PropTypes.string.isRequired,
      count: React.PropTypes.oneOfType([React.PropTypes.string, React.PropTypes.number]).isRequired,

      color: React.PropTypes.string,
      detailUrl: React.PropTypes.string,
      extraParams: React.PropTypes.object
    };
  }

  render() {
    const getQueryString = (obj) => {
      return Object.keys(obj).map(param => param + '=' + obj[param]).join('&');
    };
    const getDetailUrl = () => {
      if (typeof this.props.detailUrl === 'string' && this.props.detailUrl !== '') {
        return this.props.detailUrl + '/' + this.props.id;
      }
      return '/scout_branches/' + this.props.id + '/scouts' + (this.props.extraParams ? '?' + getQueryString(this.props.extraParams) : '');
    };
    let style = {};
    if (this.props.color) {
      style = {
        backgroundColor: this.props.color + ' !important'
      }
    }
    return(
      <div className='col-md-3 margin-bottom-md'>
        <center className='bg-jamnas-red scout-branch-grid' style={style}>
          <p>{this.props.label}</p>
          <h3 className='bold-700 margin-top-minus-xs height-40'>{this.props.name}</h3>
          <h1 className='font-40'>{this.props.count}</h1>
          <h3 className='margin-top-minus-xs'><i className='fa fa-users'></i> Peserta</h3>
          <a href={getDetailUrl()} target='_self' className='btn fg-red margin-top-md bg-gray'>
            <i className='fa fa-list'></i> Selengkapnya
          </a>
        </center>
      </div>
    );
  }
}
