import Service from './Service';

export default class AdministrationService extends Service {
  getPaymentConfirmationList(data, successCallback, errorCallback) {
    this._callService('get', '/api/branch_payment', data, successCallback, errorCallback);
  }

  getTransportationList(data, successCallback, errorCallback) {
    this._callService('get', '/api/branch_transport', data, successCallback, errorCallback);
  }

  getWillingnessLetterList(mode, id, successCallback, errorCallback) {
    switch (mode) {
      case 'branch':
        this._callService('get', '/api/branch_payment', {}, successCallback, errorCallback);
        break;
      case 'subbranch':
        this._callService('get', '/api/branch_payment', {}, successCallback, errorCallback);
        break;
      default:
        errorCallback({
          responseJSON: {
            error: 'Invalid mode.'
          }
        });
    }
  }
}
