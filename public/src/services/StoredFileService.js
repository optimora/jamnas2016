import Service from './Service';

export default class StoredFileService extends Service {
  getById(id, successCallback, errorCallback) {
    this._callService('get', '/api/stored_files/' + id, {}, successCallback, errorCallback);
  }

  updateById(id, data, successCallback, errorCallback) {
    this._callService('put', '/api/stored_files/' + id, data, successCallback, errorCallback);
  }
}
