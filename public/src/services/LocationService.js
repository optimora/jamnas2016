import Service from './Service';

export default class LocationService extends Service {
  getAll(data, successCallback, errorCallback) {
    this._callService('get', '/api/locations', data, successCallback, errorCallback);
  }

  getChildren(id, successCallback, errorCallback) {
    this._callService('get', '/api/locations/' + id + '/locations', {}, successCallback, errorCallback);
  }
}
