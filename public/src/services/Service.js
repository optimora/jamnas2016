export default class Service {
  constructor() {

  }

  /**
  * Call external API Server. For GET request, it will automatically convert
  * object to query string.
  */
  _callService(method, url, data, successCallback, errorCallback) {
    url = window.location.origin + url;
    if (method === 'get') {
      url = url + this._convertToQueryString(data);
      $.ajax({
        method,
        url
      })
      .done(successCallback)
      .fail(errorCallback);
    }
    else {
      $.ajax({
        method,
        url,
        data: JSON.stringify(data),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json'
      })
      .done(successCallback)
      .fail(errorCallback);
    }
  }

  _convertToQueryString(obj) {
    let keys = Object.keys(obj);
    for (var i = 0; i < keys.length; i++) {
      keys[i] = keys[i] + '=' + obj[keys[i]];
    }
    return (keys.length > 0 ? '?' : '') + keys.join('&');
  }
}
