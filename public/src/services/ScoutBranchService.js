import Service from './Service';

export default class ScoutBranchService extends Service {
  getAll(data, successCallback, errorCallback) {
    this._callService('get', '/api/scout_branches', data, successCallback, errorCallback);
  }

  getChildren(id, successCallback, errorCallback) {
    this._callService('get', '/api/scout_branches/' + id + '/scout_branches', {}, successCallback, errorCallback);
  }

  getUsersByScoutBranchId(id, data, successCallback, errorCallback) {
    this._callService('get', '/api/scout_branches/' + id + '/users', data, successCallback, errorCallback);
  }

  getScoutsByBranchId(id, data, successCallback, errorCallback) {
    this._callService('get', '/api/scout_branches' + id + '/scouts', data, successCallback, errorCallback);
  }
}
