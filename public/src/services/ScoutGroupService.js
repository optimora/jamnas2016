import Service from './Service';

export default class ScoutGroupService extends Service {
  create(data, successCallback, errorCallback) {
    this._callService('post', '/api/scout_groups', data, successCallback, errorCallback);
  }

  getAll(data, successCallback, errorCallback) {
    this._callService('get', '/api/scout_groups', data, successCallback, errorCallback);
  }
}
