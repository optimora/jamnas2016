import Service from './Service';

export default class BranchWillingnessLetterService extends Service {
  create(data, successCallback, errorCallback) {
    this._callService('put', '/api/branch_registration', data, successCallback, errorCallback);
  }

  read(data, successCallback, errorCallback) {
    this._callService('get', '/api/branch_registration/' + data.id, data, successCallback, errorCallback);
  }

  update(data, successCallback, errorCallback) {
    this._callService('post', '/api/branch_registration/'+ data.id, data, successCallback, errorCallback)
  }

  delete(data, successCallback, errorCallback) {
    this._callService('delete', '/api/branch_registration/' + data.id, data, successCallback, errorCallback);
  }

  list(successCallback, errorCallback) {
    this._callService('get', '/api/branch_registration', {}, successCallback, errorCallback);
  }

  filter_branch(id, successCallback, errorCallback) {
    this._callService('get', '/api/branch_registration/branch/' + id, {}, successCallback, errorCallback);
  }


};
