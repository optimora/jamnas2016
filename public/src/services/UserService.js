import Service from './Service';

export default class UserService extends Service {
  authenticate(data, successCallback, errorCallback) {
    this._callService('post', '/api/users/actions/authenticate', data, successCallback, errorCallback);
  }

  addUser(data, successCallback, errorCallback) {
    this._callService('post', '/api/users', data, successCallback, errorCallback)
  }

  getCurrentUser(successCallback, errorCallback) {
    this._callService('get', '/api/users/actions/get_current_user', {}, successCallback, errorCallback);
  }

  getUser(id, successCallback, errorCallback) {
    this._callService('get', '/api/users/' + id, {}, successCallback, errorCallback);
  }

  getNextResults(url, successCallback, errorCallback) {
    this._callService('get', url, {}, successCallback, errorCallback);
  }

  getDashboardStats(successCallback, errorCallback) {
    // What?
  }
}
