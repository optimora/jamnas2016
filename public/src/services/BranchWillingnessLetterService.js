import Service from './Service';

export default class BranchWillingnessLetterService extends Service {
  create(data, successCallback, errorCallback) {
    this._callService('put', '/api/willingness_letter', data, successCallback, errorCallback);
  }

  read(data, successCallback, errorCallback) {
    this._callService('get', '/api/willingness_letter/' + data.id, data, successCallback, errorCallback);
  }

  update(data, successCallback, errorCallback) {
    this._callService('post', '/api/willingness_letter/'+ data.id, data, successCallback, errorCallback)
  }

  delete(data, successCallback, errorCallback) {
    this._callService('delete', '/api/willingness_letter/' + data.id, data, successCallback, errorCallback);
  }

  list(successCallback, errorCallback) {
    this._callService('get', '/api/willingness_letter', {}, successCallback, errorCallback);
  }

  filter_branch(id, successCallback, errorCallback) {
    this._callService('get', '/api/willingness_letter/branch/' + id, {}, successCallback, errorCallback);
  }


};
