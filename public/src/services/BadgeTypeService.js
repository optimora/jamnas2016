import Service from './Service';

export default class BadgeTypeService extends Service {
  getAll(data, successCallback, errorCallback) {
    this._callService('get', '/api/badge_types', data, successCallback, errorCallback);
  }
}
