import Service from './Service';

export default class ScoutService extends Service {
  getAll(data, successCallback, errorCallback) {
    this._callService('get', '/api/scouts', data, successCallback, errorCallback);
  }

  getById(id, successCallback, errorCallback) {
    this._callService('get', '/api/scouts/' + id, {}, successCallback, errorCallback);
  }

  updateById(id, data, successCallback, errorCallback) {
    this._callService('put', '/api/scouts/' + id, data, successCallback, errorCallback);
  }

  deleteById(id, successCallback, errorCallback) {
    this._callService('delete', '/api/scouts/' + id, {}, successCallback, errorCallback);
  }

  create(data, successCallback, errorCallback) {
    this._callService('post', '/api/scouts', data, successCallback, errorCallback);
  }
}
