export default class ValidationRule {
  constructor(field, handlers) {
    this._field = field;
    this._handlers = handlers;
  }

  passed() {
    let result = true;
    this._handlers.forEach((handler) => {
      result = result && handler.
    });
  }
}