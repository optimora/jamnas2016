import sweetAlert from 'sweetalert';

export default class DialogHelper {



  static showConfirmationMessage(title, text, confirmCallback, cancelCallback) {
    const alertOptions = {
      title,
      text,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#dd6b55',
      confirmButtonText: 'Ya',
      cancelButtonText: 'Batalkan',
      closeOnConfirm: false,
      closeOnCancel: false,
      html: true
    };
    sweetAlert(alertOptions, (isConfirmed) => {
      if (isConfirmed) {
        if (confirmCallback && typeof confirmCallback === 'function') {
          confirmCallback();
        }
      }
      else {
        if (cancelCallback && typeof cancelCallback === 'function') {
          cancelCallback();
        }
      }
    })
  }

  static showErrorMessage(title, text) {
    sweetAlert(title, text, 'error');
  }

  static showSuccessMessage(title, text) {
    sweetAlert(title, text, 'success');
  }

  static showWarningMessage(title, text) {
    sweetAlert(title, text, 'warning');
  }

}