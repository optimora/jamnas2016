import * as ActionType from './actionTypes';

import AdministrationService from '../../../services/AdministrationService';

var administrationService = new AdministrationService();

export const updatePaymentConfirmationList = (paymentConfirmations) => ({ type: ActionType.UPDATE_PAYMENT_CONFIRMATION_LIST, paymentConfirmations });
export const updateTransportationList = (transportation) => ({ type: ActionType.UPDATE_TRANSPORTATION_LIST, transportation });
export const updateWillingnessLetters = (willingnessLetters) => ({ type: ActionType.UPDATE_WILLINGNESS_LETTERS, willingnessLetters });

export const fetchTransportationList = () => (dispatch) => {
  administrationService.getTransportationList(
    { _populate: true },
    (data) => {
      dispatch(updateTransportationList(data.results));
    },
    (error) => {
      // TODO: What?
    }
  );
};

export const fetchPaymentConfirmationList = () => dispatch => {
  administrationService.getPaymentConfirmationList(
    { _populate: true },
    (data) => {
      dispatch(updatePaymentConfirmationList(data.results));
    },
    (error) => {

    }
  );
};

