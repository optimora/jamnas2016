import { Provider } from 'react-redux';
import { applyMiddleware, combineReducers, createStore } from 'redux';

import React from 'react';
import ReactDom from 'react-dom';
import thunk from 'redux-thunk';

import * as CommonAction from '../../actions/common/actions';
import * as Action from './actions/actions';
import administrationPaymentConfirmationPageReducers from './reducers/administrationPaymentConfirmationPageReducers';

import Page from '../Page';

import PaymentConfirmationPageContainer from './containers/PaymentConfirmationPageContainer';
import HeaderContent from '../../components/common/ui/HeaderContent';

export default class AddPage extends Page {
  _createReducers() {
    return combineReducers({
      PaymentConfirmationPage: administrationPaymentConfirmationPageReducers
    });
  }

  _createStore() {
    let reducers = this._createReducers();
    return createStore(reducers, applyMiddleware(thunk));
  }

  _initialize() {
    this._store.dispatch(Action.fetchPaymentConfirmationList());
  }

  setup() {
    this._store = this._createStore();
    this.render(
      <div>
        <HeaderContent subtitle='Daftar Data Konfirmasi Pembayaran' title="Kwartir Daerah" />
      </div>,
      $(this._selectors.header)[0]
    );
    this.render(
      <Provider store={this._store}>
        <PaymentConfirmationPageContainer />
      </Provider>,
      $(this._selectors.main)[0]
    );
    this._initialize();
  }
}