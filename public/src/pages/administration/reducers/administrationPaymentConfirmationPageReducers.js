import * as ActionType from '../actions/actionTypes';

const initialState = {
  paymentConfirmations: []
};

export default function administrationPaymentConfirmationPageReducers(state = initialState, action = null) {
  console.log(action);
  if (action) {
    switch (action.type) {
      case ActionType.UPDATE_PAYMENT_CONFIRMATION_LIST:
        return Object.assign({}, initialState, {
          paymentConfirmations: action.paymentConfirmations
        });
    }
  }
  return state;
}