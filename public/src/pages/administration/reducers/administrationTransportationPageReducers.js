import * as ActionType from '../actions/actionTypes';

const initialState = {
  transportation: []
};

export default function administrationTransportationPageReducers(state = initialState, action = null) {
  console.log(action);
  if (action) {
    switch (action.type) {
      case ActionType.UPDATE_TRANSPORTATION_LIST:
        return Object.assign({}, initialState, {
          transportation: action.transportation
        });
    }
  }
  return state;
}