import moment from 'moment';
import React from 'react';

export default class TransportationTable extends React.Component {
  static get propTypes() {
    return {
      transportation: React.PropTypes.array.isRequired
    };
  }
  
  render() {
    const dateFormat = 'D MMM YYYY';
    return(
      <div className='row'>
        <div className='col-lg-12'>
          <div className='ibox float-e-margins'>
            <div className='ibox-title'>
              <h5>Data Keberangkatan, Kedatangan, dan Kepulangan</h5>
            </div>
            <div className='ibox-content'>
              <div className='row margin-bottom-xs'>
                <div className='col-lg-4 pull-left'>
                  <p className='padding-top-bottom-xs no-margin'>Menunjukkan {this.props.transportation.length} hasil.</p>
                </div>
                { /*
                <div className='col-lg-3 pull-right'>
                  <div className='input-group'>
                    <input type='text' placeholder='Search' className='input-sm form-control' />
                    <span className='input-group-btn'>
                      <button type='button' className='btn btn-sm btn-primary'> Go!</button>
                    </span>
                  </div>
                </div>
                */ }
              </div>
              <div className='table-responsive'>
                <table className='table table-striped table-bordered table-hover dataTables-example' >
                  <thead>
                    <tr>
                      <th className='align-center'>#</th>
                      <th className='align-center'>Kwartir Daerah</th>
                      <th className='align-center'>Jumlah Kontingen</th>
                      <th className='align-center'>Tanggal Keberangkatan</th>
                      <th className='align-center'>Tanggal Kedatangan</th>
                      <th className='align-center'>Tanggal Kepulangan</th>
                      <th className='align-center'>Detail</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.props.transportation.map((transportation, index) =>
                      <tr key={index}>
                        <td>{index + 1}</td>
                        <td>{transportation.province.name}</td>
                        <td className='align-center'>{transportation.contingent_total.female + transportation.contingent_total.male}</td>
                        <td className='align-center'>{moment(transportation.departure.date).format(dateFormat)}</td>
                        <td className='align-center'>{moment(transportation.arrival.date).format(dateFormat)}</td>
                        <td className='align-center'>{moment(transportation.return.date).format(dateFormat)}</td>
                        <td className='align-center'>
                          <a href='#' className='btn btn-success btn-xs no-margin margin-right-xs'>Lihat Detail Data</a>
                        </td>
                      </tr>
                    )}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>

    );
  }
};
