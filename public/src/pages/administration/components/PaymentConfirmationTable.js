import accounting from 'accounting';
import moment from 'moment';
import React from 'react';

export default class PaymentConfirmationTable extends React.Component {
  static get propTypes() {
    return {
      paymentConfirmations: React.PropTypes.array.isRequired
    };
  }
  
  render() {
    const generateDownloadFormControl = (paymentConfirmation) => {
      if (paymentConfirmation.file && paymentConfirmation.file !== '') {
        return <a href={'/stored_files/' + paymentConfirmation.file} className='btn btn-success btn-xs no-margin margin-right-xs' target='_blank'>
          Lihat Bukti Transfer
        </a>;
      }
      else {
        return <div> - </div>;
      }
    };

    return(
      <div className='row'>
        <div className='col-lg-12'>
          <div className='ibox float-e-margins'>
            <div className='ibox-title'>
              <h5>Data Konfirmasi Pembayaran</h5>
            </div>
            <div className='ibox-content'>
              <div className='row margin-bottom-xs'>
                <div className='col-lg-4 pull-left'>
                  <p className='padding-top-bottom-xs no-margin'>Menunjukkan {this.props.paymentConfirmations.length} hasil.</p>
                </div>
                <div className='col-lg-3 pull-right'>
                  <div className='input-group'>
                    <input type='text' placeholder='Search' className='input-sm form-control' />
                    <span className='input-group-btn'>
                      <button type='button' className='btn btn-sm btn-primary'> Go!</button>
                    </span>
                  </div>
                </div>
              </div>
              <div className='table-responsive'>
                <table className='table table-striped table-bordered table-hover dataTables-example' >
                  <thead>
                    <tr>
                      <th className='align-center'>#</th>
                      <th className='align-center'>Tanggal</th>
                      <th className='align-center'>Kwartir Daerah</th>
                      <th className='align-center'>Nominal</th>
                      <th className='align-center'>Bukti Transfer</th>
                      <th className='align-center'>Keterangan</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.props.paymentConfirmations.map((paymentConfirmation, index) =>
                      <tr>
                        <td>{index + 1}</td>
                        <td className='align-center'>{moment(paymentConfirmation.paid).format('DD MMM YYYY')}</td>
                        <td className='align-center'>{paymentConfirmation.branch.name}</td>
                        <td className='align-center'>
                          <table style={{ width: '100%' }}>
                            <tr>
                              <td align='left'>Rp.</td>
                              <td style={{ textAlign: 'right' }}>{accounting.formatMoney(paymentConfirmation.amount, '', 0, '.', ',')}</td>
                            </tr>
                          </table>
                        </td>
                        <td className='align-center'>
                          {generateDownloadFormControl(paymentConfirmation)}
                        </td>
                        <td className='align-center'>{paymentConfirmation.method || ' - '}</td>
                      </tr>
                    )}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>

    );
  }
}
