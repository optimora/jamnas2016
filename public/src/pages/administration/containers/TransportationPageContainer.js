import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import React from 'react';

import * as commonActions from '../../../actions/common/actions';
import * as actions from '../actions/actions';

import DialogHelper from '../../../lib/helpers/DialogHelper';

import TransportationTable from '../components/TransportationTable';

class IndexPageContainer extends React.Component {
  static get propTypes() {
    return {
      TransportationPage: React.PropTypes.object.isRequired,
      actions: React.PropTypes.object.isRequired
    }
  }

  render() {
    return <div>
      <TransportationTable transportation={this.props.TransportationPage.transportation} />
    </div>;
  }
}

const actionCreators = Object.assign({}, commonActions, actions);

const mapStateToProps = (state) => {
  return state;
};

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(actionCreators, dispatch)
  };
};

let connector = connect(
  mapStateToProps,
  mapDispatchToProps
);

export default connector(IndexPageContainer);
