import { Provider } from 'react-redux';
import { applyMiddleware, combineReducers, createStore } from 'redux';

import React from 'react';
import ReactDom from 'react-dom';
import thunk from 'redux-thunk';

import * as CommonAction from '../../actions/common/actions';
import * as Action from './actions/actions';
import administrationTransportationPageReducers from './reducers/administrationTransportationPageReducers';

import Page from '../Page';

import TransportationPageContainer from './containers/TransportationPageContainer';
import HeaderContent from '../../components/common/ui/HeaderContent';

export default class AddPage extends Page {
  _createReducers() {
    return combineReducers({
      TransportationPage: administrationTransportationPageReducers
    });
  }

  _createStore() {
    let reducers = this._createReducers();
    return createStore(reducers, applyMiddleware(thunk));
  }

  _initialize() {
    this._store.dispatch(Action.fetchTransportationList());
  }

  setup() {
    this._store = this._createStore();
    this.render(
      <div>
        <HeaderContent subtitle='Daftar Data Keberangkatan Kedatangan dan Kepulangan' title="Kwartir Daerah" />
      </div>,
      $(this._selectors.header)[0]
    );
    this.render(
      <Provider store={this._store}>
        <TransportationPageContainer />
      </Provider>,
      $(this._selectors.branch_transport_list)[0]
    );
    this._initialize();
  }
}