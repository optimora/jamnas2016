import React from 'react';
import ReactDom from 'react-dom';

export default class IndexPage {
  constructor(selectors, data) {
    this._selectors = selectors;
    this._data = data;
  }

  setup() {
    ReactDom.render(
        <div>
          <section id="home">
            <div className="container-fluid landing-background">
              <div className="row">
                <div className="col-lg-6">
                  <div className="m-t-lg padding-170 welcome">

                  </div>
                </div>
                <div className="col-lg-6">
                  <div className="landing-login">
                    <h1><small>Sistem Pendaftaran Peserta</small><br />Jambore Nasional X 2016</h1>
                    <hr />

                    <a href="/users/login" className="btn btn-success btn-xxl btn-block">
                      <i className="fa fa-sign-in"></i>
                      &nbsp; Login
                    </a>

                    <hr />
                    <div className="row">
                      <div className="col-lg-6">
                        <p className="text-color">
                          Apabila Anda belum memiliki akun atau mengalami kendala teknis, harap segera kontak tim teknis IT.
                        </p>
                      </div>
                      <div className="col-lg-6">
                        <address>
                          <strong><span className="navy">Kontak Teknis IT (Ardya Armaji)</span></strong><br />
                          <abbr title="Email">E:</abbr> ardya@optimora.com<br />
                          <abbr title="Phone">P:</abbr> +62 812 8811 1084
                        </address>
                      </div>
                    </div>



                  </div>
                </div>
              </div>
            </div>
          </section>

        </div>,
        $(this._selectors.index)[0]
    );
  }
};

/**
 * Created by saras on 5/15/16.
 */
