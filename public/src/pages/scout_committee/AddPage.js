import { Provider } from 'react-redux';
import { applyMiddleware, combineReducers, createStore } from 'redux';

import React from 'react';
import thunk from 'redux-thunk';

import * as Action from './actions/scoutCommitteeAddPageActions';

import Page from '../Page';
import scoutCommitteeAddPageReducers from './reducers/scoutCommitteeAddPageReducers';

import AddPageContainer from './containers/AddPageContainer';

export default class AddPage extends Page {
  constructor(selectors) {
    super(selectors);

    this.branch = window.currentPageData.branch;
    this.branches = window.currentPageData.branches;
    this.committeeRoles = window.currentPageData.committeeRoles;
    this.enumStringMap = window.currentPageData.enumStringMap;
  }

  _createReducers() {
    return combineReducers({
      AddPage: scoutCommitteeAddPageReducers
    });
  }

  _createStore() {
    let reducers = this._createReducers();
    return createStore(reducers, applyMiddleware(thunk));
  }

  _initialize() {
    this._store = this._createStore();
  }

  setup() {
    const enumStringMap = this.enumStringMap;

    this._initialize();
    this.render(
      <div className="row wrapper border-bottom white-bg page-heading padding-bottom-xs">
        <div className="col-lg-12">
          <h1>Pendaftaran Panitia Jambore Nasional</h1>
        </div>
      </div>,
      $(this._selectors.header)[0]
    );
    this.render(
      <Provider store={this._store}>
        <AddPageContainer enumStringMap={enumStringMap} />
      </Provider>,
      $(this._selectors.main)[0]
    );
    this._store.dispatch(Action.updateBranches(this.branches));
    this._store.dispatch(Action.updateCommitteeRoles(this.committeeRoles))
  }
}
