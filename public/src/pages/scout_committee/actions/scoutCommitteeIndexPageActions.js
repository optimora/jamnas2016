import * as ActionType from './scoutCommitteeIndexPageActionTypes';

import ScoutService from '../../../services/ScoutService';

var scoutService = new ScoutService();

export const notifyFailed = (message) => ({ type: ActionType.NOTIFY_FAILED, message });
export const notifyDeleted = (message) => ({ type: ActionType.NOTIFY_DELETED, message });
export const updateScoutList = (scouts) => ({ type: ActionType.UPDATE_SCOUT_LIST, scouts });

// Async Actions

export const fetchScouts = (query = {}) => {
  return (dispatch) => {
    scoutService.getAll(
      Object.assign({ _populate: true, type: 'committee', _limit: 20 }, query),
      (data) => {
        dispatch(updateScoutList(data.results));
      },
      (data) => {
        dispatch(notifyFailed('Gagal mengambil data kepramukaan dari server. Silakan coba beberapa saat lagi.'))
      }
    );
  };
};

export const deleteScout = (id, successCallback, errorCallback) => {
  return (dispatch) => {
    scoutService.deleteById(
      id,
      (data) => {
        if (successCallback && typeof successCallback === 'function') {
          successCallback(data.responseJSON);
        }
        dispatch(fetchScouts())
      },
      (data) => {
        if (errorCallback && typeof errorCallback === 'function') {
          errorCallback(data.responseJSON);
        }
      }
    );
  };
};