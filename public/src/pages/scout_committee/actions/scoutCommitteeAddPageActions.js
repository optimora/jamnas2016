import * as ActionType from './scoutCommitteeAddPageActionTypes';
import * as CommonAction from '../../../actions/common/actions';
import * as CommonActionType from '../../../actions/common/actionTypes';

import ScoutService from '../../../services/ScoutService';
import ScoutBranchService from '../../../services/ScoutBranchService';

var scoutService = new ScoutService();
var scoutBranchService = new ScoutBranchService();

// Sync Actions

export const updateCommitteeRoles = (committeeRoles) => ({ type: ActionType.UPDATE_COMMITTEE_ROLES, committeeRoles });
export const updateCommitteeRoleDetails = (committeeRoleDetails) => ({ type: ActionType.UPDATE_COMMITTEE_ROLE_DETAILS, committeeRoleDetails });
export const updateCommitteeRoleExtras = (committeeRoleExtras) => ({ type: ActionType.UPDATE_COMMITTEE_ROLE_EXTRAS, committeeRoleExtras });
export const updateBranches = (branches) => ({ type: ActionType.UPDATE_BRANCHES, branches });
export const updateSubbranches = (subbranches) => ({ type: ActionType.UPDATE_SUBBRANCHES, subbranches });
export const notifySaved = (message) => ({ type: ActionType.NOTIFY_SAVED, message });
export const notifyFailed = (message) => ({ type: ActionType.NOTIFY_FAILED, message });

// Async Actions

export const addScoutCommittee = () => {
  return (dispatch, getState) => {
    const state = getState();
    let scout = state.AddPage.scout;

    dispatch(validateSync(
      () => {
        if (Object.keys(state.AddPage.errors).length > 0) {
          dispatch(notifyFailed('Gagal menyimpan data, periksa kembali data Anda.'));
        }
        else {
          scoutService.create(
            scout,
            (data) => {
              dispatch(CommonAction.redirect('/scouts/' + data.result._id + '/edit'));
            },
            (data) => {
              let message = data.responseJSON['message'];
              if (!message || message === '') {
                message = 'Gagal menyimpan data, periksa kembali data Anda.'
              }
              dispatch(notifyFailed(message));
            }
          );
        }
      },
      (data) => {
        dispatch(notifyFailed('Gagal menyimpan data, mohon periksa kembali data Anda.'));
      }
    ));
  };
};

export const validateSync = (successCallback, errorCallback) => {
  return (dispatch, getState) => {
    const state = getState();
    let hasError = false;
    let scout = Object.assign({}, state.AddPage.scout);
    Object.keys(scout).forEach((field) => {
      let value = scout[field];
      switch (field) {
        case 'type':
        case 'sex':
        case 'religion':
        case 'blood_type':
        case 'name':
        case 'nta':
        case 'birthplace':
        case 'branch':
        case 'subbranch':
          if (value === '') {
            hasError = true;
            dispatch(CommonAction.formValidationError(field, 'Kolom ini harus diisi.'));
          }
          break;
        case 'group':
          if (scout.type === 'participant' && value === '') {
            hasError = true;
            dispatch(CommonAction.formValidationError(field, 'Kolom ini harus diisi.'));
          }
          break;
        default:
          break;
      }
    });
    if (hasError) {
      errorCallback();
    }
    else {
      successCallback();
    }
  };
};