import * as CommonActionType from '../../../actions/common/actionTypes';
import * as ActionType from '../actions/scoutCommitteeAddPageActionTypes';

import DialogHelper from '../../../lib/helpers/DialogHelper';

const defaultState = {
  branches: [],
  subbranches: [],
  committeeRoles: [],
  committeeRoleDetails: [],
  committeeRoleExtras: [],
  scout: {
    name: '',
    document_status: 'draft',
    sex: '',
    birthplace: '',
    birthday: new Date(1900, 0, 1, 0, 0, 0),
    religion: '',
    blood_type: '',
    avatar: '',
    nta: '',
    type: 'committee',
    subtype: '',
    participant_role: '',
    branch: '',
    subbranch: '',
    group: '',
    committee_role: '',
    committee_role_detail: '',
    committee_role_extra: ''
  },
  errors: {}
};

export default function scoutCommitteeAddPageReducers(state = defaultState, action = null) {
  console.log(action);
  switch (action.type) {
    case CommonActionType.FORM_UPDATE_VALUE:
      let errors = Object.assign({}, state.errors);
      delete errors[action.field];
      return Object.assign({}, state, {
        scout: Object.assign({}, state.scout, {
          [action.field]: action.value
        }),
        errors: errors
      });
    case CommonActionType.FORM_VALIDATION_ERROR:
      return Object.assign({}, state, {
        errors: Object.assign({}, state.errors, {
          [action.field]: action.message
        })
      });
    case CommonActionType.REDIRECT:
      window.location.assign(window.location.origin + action.url);
      return state;
    case ActionType.UPDATE_COMMITTEE_ROLES:
      return Object.assign({}, state, {
        committeeRoles: action.committeeRoles
      });
    case ActionType.UPDATE_COMMITTEE_ROLE_DETAILS:
      return Object.assign({}, state, {
        committeeRoleDetails: action.committeeRoleDetails
      });
    case ActionType.UPDATE_COMMITTEE_ROLE_EXTRAS:
      return Object.assign({}, state, {
        committeeRoleExtras: action.committeeRoleExtras
      });
    case ActionType.UPDATE_BRANCHES:
      return Object.assign({}, state, {
        branches: action.branches
      });
    case ActionType.UPDATE_SUBBRANCHES:
      return Object.assign({}, state, {
        subbranches: action.subbranches
      });
    case ActionType.NOTIFY_FAILED:
      DialogHelper.showErrorMessage('Gagal', action.message);
      return state;
    default:
      return state;
  }
}
