import * as ActionType from '../actions/scoutCommitteeIndexPageActionTypes';

const defaultState = {
  scouts: []
};

export default function scoutCommitteeIndexPageReducers(state = defaultState, action = null) {
  switch (action.type) {
    case ActionType.UPDATE_SCOUT_LIST:
      return Object.assign({}, state, {
        scouts: action.scouts
      });
    default:
      return state;
  }
};