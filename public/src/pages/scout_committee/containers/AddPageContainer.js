import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import React from 'react';

import * as commonActions from '../../../actions/common/actions';
import * as commonScoutActions from '../../../actions/common/scoutCommonActions';
import * as scoutCommitteeAddPageActions from '../actions/scoutCommitteeAddPageActions';

import BasicForm from '../../scouts/components/BasicForm';

class AddPageContainer extends React.Component {
  static get propTypes() {
    return {
      AddPage: React.PropTypes.object.isRequired,
      actions: React.PropTypes.object.isRequired,
      enumStringMap: React.PropTypes.object.isRequired
    }
  }

  _handleFieldValueChange(name, value) {
    this.props.actions.formUpdateValue(name, value);
    switch (name) {
      case 'branch':
        this.props.actions.fetchSubbranches(value);
        this.props.actions.formUpdateValue('subbranch', '');
        break;
      case 'committee_role':
        this.props.actions.formUpdateValue('committee_role_detail', '');
        this.props.actions.formUpdateValue('committee_role_extra', '');
        this.props.actions.classifyAndUpdateCommitteeRoleDetails(value, this.props.enumStringMap || {});
        break;
      case 'committee_role_detail':
        this.props.actions.formUpdateValue('committee_role_extra', '');
        this.props.actions.classifyAndUpdateCommitteeRoleExtras(value, this.props.enumStringMap || {});
        break;
      default:
        break;
    }
  }

  _handleSubmit() {
    this.props.actions.addScoutCommittee();
  }

  render() {
    let alert = <div></div>;
    if (this.props.AddPage.messageType) {
      alert = <div className={'alert alert-' + this.props.AddPage.messageType}>
        {this.props.AddPage.message}
      </div>
    }

    const data = Object.assign({}, this.props.AddPage.scout);

    return (
      <div id='scoutsAddPageComponents' className='wrapper wrapper-content'>
        <div className='col-lg-12 margin-bottom-xlg'>
          {alert}
          <BasicForm
            data={data}
            branches={this.props.AddPage.branches}
            subbranches={this.props.AddPage.subbranches}
            groups={[]}
            committeeRoles={this.props.AddPage.committeeRoles}
            committeeRoleDetails={this.props.AddPage.committeeRoleDetails}
            committeeRoleExtras={this.props.AddPage.committeeRoleExtras}
            errors={this.props.AddPage.errors}
            onChange={(name, value) => this._handleFieldValueChange(name, value)}
            onSave={() => {this._handleSubmit()}}
            onAddNewGroup={() => { console.log('Disabled.') }} />
        </div>
      </div>
    );
  }
}

const actionCreators = Object.assign({}, commonActions, commonScoutActions, scoutCommitteeAddPageActions);

const mapStateToProps = (state) => {
  return state;
};

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(actionCreators, dispatch)
  };
};

let connector = connect(
  mapStateToProps,
  mapDispatchToProps
);

export default connector(AddPageContainer);
