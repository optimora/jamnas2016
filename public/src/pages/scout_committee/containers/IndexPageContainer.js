import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import React from 'react';

import * as commonActions from '../../../actions/common/actions';
import * as commonScoutActions from '../../../actions/common/scoutCommonActions';
import * as scoutCommitteeIndexPageActions from '../actions/scoutCommitteeIndexPageActions';

import DialogHelper from '../../../lib/helpers/DialogHelper';

import BasicForm from '../../scouts/components/BasicForm';
import ScoutDataTable from '../../../components/common/ui/ScoutDataTable';

class IndexPageContainer extends React.Component {
  static get propTypes() {
    return {
      IndexPage: React.PropTypes.object.isRequired,
      actions: React.PropTypes.object.isRequired,
      enumStringMap: React.PropTypes.object.isRequired
    }
  }

  _deleteScout(id) {
    this.props.actions.deleteScout(
      id,
      () => {
        DialogHelper.showWarningMessage('Berhasil', 'Data telah dihapus.');
      },
      () => {
        DialogHelper.showErrorMessage('Gagal', 'Terjadi kesalahan saat melakukan penghapusan data.')
      }
    );
  }

  _generateScoutViewEntries() {
    let entries = this.props.IndexPage.scouts.slice(0);
    let results = [];
    let counter = 0;
    entries.forEach((entry) => {
      counter++;
      results.push({
        no: counter,
        name: entry.name,
        branchName: entry.branch.name,
        subbranchName: entry.subbranch.name,
        role: entry.type,
        created: entry.created,
        actions: <div>
          <a href={'/scouts/' + entry._id} className='btn btn-success btn-xs no-margin margin-right-xs'>Lihat</a>
          <a href={'/scouts/' + entry._id + '/edit'} className='btn btn-info btn-xs no-margin margin-right-xs'>Ubah</a>
          <a href='#' className='btn btn-danger btn-xs no-margin' onClick={() => this._initiateDeleteScoutAction(entry)}>Hapus</a>
        </div>
      });
    });

    return results;
  }

  _initiateDeleteScoutAction(scout) {
    DialogHelper.showConfirmationMessage(
      'Apakah Anda yakin akan menghapus data milik ' + scout.name + '?',
      'Seluruh data kepramukaan dan kontak yang terkait dengan data tersebut di atas akan dihilangkan. <strong>Aksi ini tidak dapat dibatalkan</strong>.',
      () => {
        this._deleteScout(scout._id);
      },
      () => {
        DialogHelper.showSuccessMessage('Aksi Dibatalkan', 'Aksi penghapusan dibatalkan.')
      }
    );
  }

  _searchScoutByName(name) {
    this.props.actions.fetchScouts(
      { name }
    );
  }

  render() {
    return <div className="wrapper wrapper-content">
      <div className="row">
        <div className="col-lg-12">
          <ScoutDataTable
            disableInput={false}
            rows={this._generateScoutViewEntries()}
            title={'Data Panitia Jamnas X 2016'}
            inputDataUrl={'/scout_committee/add'}
            onSearchButtonClicked={this._searchScoutByName.bind(this)} />
        </div>
      </div>
    </div>;
  }
}

const actionCreators = Object.assign({}, commonActions, commonScoutActions, scoutCommitteeIndexPageActions);

const mapStateToProps = (state) => {
  return state;
};

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(actionCreators, dispatch)
  };
};

let connector = connect(
  mapStateToProps,
  mapDispatchToProps
);

export default connector(IndexPageContainer);
