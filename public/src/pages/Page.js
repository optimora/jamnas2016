import React from 'react';
import ReactDom from 'react-dom';

export default class Page {
  constructor(selectors) {
    this._selectors = selectors;
    this._renderedComponents = {};

    let selectorKeys = Object.keys(this._renderedComponents);
    for (var i = 0; i < selectorKeys.length; i++) {
      this._renderedComponents[selectorKeys[i]] = null;
    }

    window.currentPageInstance = this;

    // TODO: This is actually not what I want. -_-
    this._renderedComponentCounter = 0;
  }

  getRenderedComponent(selector) {
    return this._renderedComponents[selector];
  }

  render(component, element) {
    this._renderedComponents['component' + this._renderedComponentCounter] = ReactDom.render(component, element);
    this._renderedComponentCounter++;
  }
}
