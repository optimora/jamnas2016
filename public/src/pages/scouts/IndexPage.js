import { Provider } from 'react-redux';
import { applyMiddleware, combineReducers, createStore } from 'redux';

import React from 'react';
import ReactDom from 'react-dom';
import thunk from 'redux-thunk';

import * as Action from './actions/scoutsEditPageActions';
import * as CommonAction from '../../actions/common/actions';
import * as CommonScoutAction from '../../actions/common/scoutCommonActions';

import scoutsEditPageReducers from './reducers/scoutsEditPageReducers';

import Page from '../Page';

import EditPageContainer from './containers/EditPageContainer';
import ScoutSubbranchFormHeader from '../../components/common/ui/ScoutSubbranchFormHeader';

export default class IndexPage extends Page {
  constructor(...args) {
    super(...args);
  }

  setup() {

  }
}
