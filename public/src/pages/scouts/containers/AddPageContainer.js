import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import React from 'react';

import * as commonActions from '../../../actions/common/actions';
import * as scoutsAddPageActions from '../actions/scoutsAddPageActions';

import BasicForm from '../components/BasicForm';

class AddPageContainer extends React.Component {
  constructor(...args) {
    super(...args);

    this.branch = window.currentPageData.branch;
    this.subbranch = window.currentPageData.subbranch;
  }

  static get propTypes() {
    return {
      AddPage: React.PropTypes.object.isRequired,
      actions: React.PropTypes.object.isRequired
    }
  }

  _handleAddNewGroup(groupName) {
    this.props.actions.addGroup(this.branch._id, this.subbranch._id, groupName);
  }

  _handleFieldValueChange(name, value) {
    this.props.actions.formUpdateValue(name, value);
  }

  _handleSubmit() {
    this.props.actions.addScout(this.props.AddPage.scout);
  }

  render() {
    let alert = <div></div>;
    if (this.props.AddPage.messageType) {
      alert = <div className={'alert alert-' + this.props.AddPage.messageType}>
        {this.props.AddPage.message}
      </div>
    }

    const data = Object.assign({}, this.props.AddPage.scout);

    return (
      <div id='scoutsAddPageComponents' className='wrapper wrapper-content'>
        <div className='col-lg-12 margin-bottom-xlg'>
          {alert}
          <BasicForm data={data} groups={this.props.AddPage.groups} locations={[]} subLocations={[]} branch={this.branch} subbranch={this.subbranch} errors={this.props.AddPage.errors}
            onChange={(name, value) => this._handleFieldValueChange(name, value)}
            onSave={() => {this._handleSubmit()}}
            onAddNewGroup={(groupName) => { this._handleAddNewGroup(groupName) }} />
        </div>
      </div>
    );
  }
}

const actionCreators = Object.assign({}, commonActions, scoutsAddPageActions);

const mapStateToProps = (state) => {
  return state;
};

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(actionCreators, dispatch)
  };
};

let connector = connect(
  mapStateToProps,
  mapDispatchToProps
);

export default connector(AddPageContainer);
