import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import React from 'react';

import * as commonActions from '../../../actions/common/actions';
import * as commonScoutActions from '../../../actions/common/scoutCommonActions';
import * as scoutsEditPageActions from '../actions/scoutsEditPageActions';

import BasicForm from '../components/BasicForm';
import ContactForm from '../components/ContactForm';
import CourseForm from '../components/CourseForm';
import DocumentForm from '../components/DocumentForm';
import EducationForm from '../components/EducationForm';
import ExperienceForm from '../components/ExperienceForm';
import MedicalHistoryForm from '../components/MedicalHistoryForm';

class EditPageContainer extends React.Component {
  constructor(...args) {
    super(...args);

    this.scout = window.currentPageData.scout;
    this.badgeTypes = window.currentPageData.badgeTypes;
    this.branch = window.currentPageData.branch;
    this.locations = window.currentPageData.locations;
    this.subbranch = window.currentPageData.subbranch;
  }

  static get propTypes() {
    return {
      EditPage: React.PropTypes.object.isRequired,
      actions: React.PropTypes.object.isRequired,
      enumStringMap: React.PropTypes.object.isRequired
    }
  }

  _fetchLocationChildren(id, fieldId) {
    this.props.actions.fetchLocationChildren(id, fieldId);
    this.props.actions.formUpdateValue('regency_city', '');
  }

  _handleFieldValueChange(name, value) {
    console.log(name, value);
    this.props.actions.formUpdateValue(name, value);

    switch (name) {
      case 'branch':
        this.props.actions.fetchSubbranches(value);
        this.props.actions.formUpdateValue('subbranch', '');
        break;
      case 'committee_role':
        this.props.actions.formUpdateValue('committee_role_detail', '');
        this.props.actions.formUpdateValue('committee_role_extra', '');
        this.props.actions.classifyAndUpdateCommitteeRoleDetails(value, this.props.enumStringMap || {});
        break;
      case 'committee_role_detail':
        this.props.actions.formUpdateValue('committee_role_extra', '');
        this.props.actions.classifyAndUpdateCommitteeRoleExtras(value, this.props.enumStringMap || {});
        break;
      default:
        break;
    }
  }

  _handleAddNewGroup(groupName) {
    this.props.actions.addGroup(this.branch._id, this.subbranch._id, groupName);
  }

  _handleSubmit() {
    console.log(this.props.EditPage);
    this.props.actions.updateScoutData(this.scout._id);
  }

  render() {
    const basicEducations = [
      {
        level: 'SD',
        school_name: '',
        city: '',
        graduate_year: null,
        notes: ''
      },
      {
        level: 'SMP',
        school_name: '',
        city: '',
        graduate_year: null,
        notes: ''
      }
    ];

    const advancedEducations = [
      {
        level: 'SMA',
        school_name: '',
        city: '',
        graduate_year: null,
        notes: ''
      },
      {
        level: 'S1',
        school_name: '',
        city: '',
        graduate_year: null,
        notes: ''
      },
      {
        level: 'S2',
        school_name: '',
        city: '',
        graduate_year: null,
        notes: ''
      },
      {
        level: 'S3',
        school_name: '',
        city: '',
        graduate_year: null,
        notes: ''
      }
    ];

    let educations;
    if (this.props.EditPage.scout.type === 'participant' || this.props.EditPage.scout.type === '') {
      educations = [...basicEducations];
    }
    else {
      educations = [...basicEducations, ...advancedEducations]
    }

    if (this.props.EditPage.scout.educations.length > 0) {
      this.props.EditPage.scout.educations.forEach((education, index) => {
        if (educations[index]) {
          educations[index] = education;
        }
      })
    }

    let emptyCourseData = [];
    ['KMD', 'KML', 'KPD', 'KPL'].forEach((name) => {
      emptyCourseData.push({
        name,
        year: '',
        location: '',
        notes: ''
      });
    });

    let courses = emptyCourseData.slice(0);
    if (this.props.EditPage.scout.courses.length > 0) {
      let counter = 0;
      this.props.EditPage.scout.courses.forEach((course) => {
        courses[counter] = course;
        counter++;
      });
    }

    const emptyScoutData = {
      name: '',
      document_status: 'draft',
      sex: '',
      birthplace: '',
      birthday: new Date(1900, 0, 1, 0, 0, 0),
      religion: '',
      blood_type: '',
      avatar: '',
      nta: '',
      type: '',
      subtype: '',
      participant_role: '',
      subbranch: '',
      group: '',
      committee_role: '',
      committee_role_detail: '',
      committee_role_extra: ''
    };

    const emptyContactData = {
      name         : '',
      occupation   : '',
      address      : '',
      relation     : '',
      province     : '',
      regency_city : '',
      zipcode      : '',
      country      : '',
      phone        : {
        home: '',
        mobile: ''
      },
      type         : '',
      category     : ''
    };

    let contactForms = [
      <ContactForm
        key='contact'
        caption='ALAMAT & KONTAK'
        branch={this.branch}
        subbranch={this.subbranch}
        data={Object.assign({}, emptyContactData, this.props.EditPage.scout.contact)}
        locations={this.props.EditPage.locations}
        subLocations={this.props.EditPage.contactSubLocations}
        subLocationsFieldId='contactSubLocations'
        onChange={(values) => {this._handleFieldValueChange('contact', values)}}
        onProvinceChange={(id, fieldId) => {this._fetchLocationChildren(id, fieldId)}}
        onSave={this._handleSubmit.bind(this)}/>
    ];
    if (this.props.EditPage.scout.type === 'participant') {
      contactForms.push(
        <ContactForm
          key='parentContact'
          caption='ORANG TUA'
          branch={this.branch}
          subbranch={this.subbranch}
          data={Object.assign({}, emptyContactData, this.props.EditPage.scout.parent_contact)}
          mode='extended'
          locations={this.props.EditPage.locations}
          subLocations={this.props.EditPage.parentContactSubLocations}
          subLocationsFieldId='parentContactSubLocations'
          onChange={(values) => {this._handleFieldValueChange('parent_contact', values)}}
          onProvinceChange={(id, fieldId) => {this._fetchLocationChildren(id, fieldId)}}
          onSave={this._handleSubmit.bind(this)}/>
      );
    }

    let courseForm = null;
    if (this.props.EditPage.scout.type !== 'participant') {
      courseForm = <CourseForm
        courses={courses}
        externalCourses={this.props.EditPage.scout.external_courses}
        onChange={(fieldSegment, values) => this._handleFieldValueChange(fieldSegment, values)}
        onSave={this._handleSubmit.bind(this)} />
    }

    let documents = [
      {
        type: 'scout-member-card-document',
        description: 'KTA Gerakan Pramuka',
        stored_file: '',
        notes: ''
      },
      {
        type: 'health-document',
        description: 'Surat Keterangan Sehat dari Dokter',
        stored_file: '',
        notes: ''
      },
      {
        type: 'parent-document',
        description: 'Surat Keterangan dari Orang Tua',
        stored_file: '',
        notes: ''
      },
      {
        type: 'school-document',
        description: 'Surat Keterangan dari Sekolah',
        stored_file: '',
        notes: ''
      },
      {
        type: 'insurance',
        description: 'Asuransi',
        stored_file: '',
        notes: ''
      }
    ];

    this.props.EditPage.scout.documents.forEach((storedDocument, index) => {
      documents[index] = Object.assign({}, documents[index], storedDocument);
    });

    return (
      <div id='scoutsEditPageComponents' className='wrapper wrapper-content'>
        <div className='col-lg-12 margin-bottom-xlg'>
          <BasicForm
            data={Object.assign({}, emptyScoutData, this.props.EditPage.scout)}
            branches={this.props.EditPage.branches}
            subbranches={this.props.EditPage.subbranches}
            groups={this.props.EditPage.groups}
            branch={this.branch}
            subbranch={this.subbranch}
            committeeRoles={this.props.EditPage.committeeRoles}
            committeeRoleDetails={this.props.EditPage.committeeRoleDetails}
            committeeRoleExtras={this.props.EditPage.committeeRoleExtras}
            onChange={(name, value) => this._handleFieldValueChange(name, value)}
            onSave={this._handleSubmit.bind(this)}
            onAddNewGroup={this._handleAddNewGroup.bind(this)}
            lockScoutTypeChanges={true} />
          {contactForms.map((contactForm) => contactForm)}
          <EducationForm
            data={educations}
            onChange={(values) => this._handleFieldValueChange('educations', values)}
            onSave={this._handleSubmit.bind(this)} />
          <ExperienceForm
            experiences={this.props.EditPage.scout.experiences}
            badgeTypes={this.props.EditPage.badgeTypes}
            type={this.props.EditPage.scout.type}
            onChange={(values) => this._handleFieldValueChange('experiences', values)}
            onSave={this._handleSubmit.bind(this)} />
          {courseForm}
          <MedicalHistoryForm
            medicalHistory={this.props.EditPage.scout.medical_history}
            emergencyContacts={this.props.EditPage.scout.emergency_contacts}
            emergencyRadios={this.props.EditPage.scout.emergency_radios}
            onChange={(fieldSegment, values) => this._handleFieldValueChange(fieldSegment, values)}
            onSave={this._handleSubmit.bind(this)} />
          <DocumentForm
            documents={documents}
            onChange={(values) => this._handleFieldValueChange('documents', values)}
            onSave={this._handleSubmit.bind(this)} />
        </div>
      </div>
    );
  }
}

const actionCreators = Object.assign({}, commonActions, commonScoutActions, scoutsEditPageActions);

const mapStateToProps = (state) => {
  return state;
};

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(actionCreators, dispatch)
  };
};

let connector = connect(
  mapStateToProps,
  mapDispatchToProps
);

export default connector(EditPageContainer);
