import React from 'react';

import BasicFormView from '../components/BasicFormView';
import ContactFormView from '../components/ContactFormView';
import CourseFormView from '../components/CourseFormView';
import EducationFormView from '../components/EducationFormView';
import MedicalHistoryFormView from '../components/MedicalHistoryFormView';

export default class DetailPageContainer extends React.Component {
  constructor(...args) {
    super(...args);
  }

  static get propTypes() {
    return {
      branch: React.PropTypes.object.isRequired,
      scout: React.PropTypes.object.isRequired,
      subbranch: React.PropTypes.object.isRequired
    };
  }

  render() {
    return (
      <div className='wrapper wrapper-content'>
        <div className='col-lg-12 margin-bottom-xlg'>
          {/*
            <p style={{ textAlign: 'right' }}>
              <a href={'/scouts/' + this.props.scout._id + '/edit'} className='btn btn-w-m btn-warning'>Ubah Data</a>
            </p>
          */}
          <BasicFormView {...this.props} />
          <ContactFormView contact={this.props.scout.contact || null} />
          <ContactFormView contact={this.props.scout.parent_contact || null} />
          <CourseFormView courses={this.props.scout.courses} />
          <EducationFormView educations={this.props.scout.educations} />
          <MedicalHistoryFormView medicalHistory={this.props.scout.medical_history} emergencyContacts={this.props.scout.emergency_contacts} emergencyRadios={this.props.scout.emergency_radios} />
        </div>
      </div>
    );
  }
}