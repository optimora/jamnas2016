import * as CommonActionType from '../../../actions/common/actionTypes';
import * as CommonScoutActionType from '../../../actions/common/scoutCommonActionTypes';
import * as ActionType from '../actions/scoutsEditPageActionTypes';

import DialogHelper from '../../../lib/helpers/DialogHelper';

const initialState = {
  badgeTypes: [],
  branches: [],
  committeeRoles: [],
  committeeRoleDetails: [],
  committeeRoleExtras: [],
  groups: [],
  locations: [],
  contactSubLocations: [],
  parentContactSubLocations: [],
  subbranches: [],

  errors: {},

  scout: {
    name: '',
    document_status: 'draft',
    sex: '',
    birthplace: '',
    birthday: new Date(1900, 0, 1, 0, 0, 0),
    religion: '',
    blood_type: '',
    avatar: '',
    nta: '',
    type: '',
    subtype: '',
    participant_role: '',
    group: '',
    committee_role: '',
    committee_role_detail: '',
    committee_role_extra: '',

    contact: {},
    parent_contact: {},

    emergency_contacts: [],
    emergency_radios: [],
    files: [],
    courses: [],
    external_courses: [],
    educations: [],
    experiences: [],
    medical_history: [],
    documents: []
  }
};

export default function scoutsEditPageReducers(state = initialState, action) {
  console.log(action, state.scout.committee_role_detail);
  switch (action.type) {
    case CommonActionType.SHOW_MESSAGE:
      return state;
    case CommonActionType.HIDE_MESSAGE:
      return state;
    case CommonActionType.FORM_UPDATE_VALUE:
      let updatedData = {
        [action.field]: action.value
      };
      return Object.assign({}, state, {
        scout: Object.assign({}, state.scout, updatedData)
      });
    case CommonActionType.LOCATION_CHILDREN_UPDATE:
      return Object.assign({}, state, {
        [action.fieldId]: action.locations
      });
    case CommonScoutActionType.UPDATE_BRANCHES:
      return Object.assign({}, state, {
        branches: action.branches
      });
    case CommonScoutActionType.UPDATE_COMMITTEE_ROLES:
      return Object.assign({}, state, {
        committeeRoles: action.committeeRoles
      });
    case CommonScoutActionType.UPDATE_COMMITTEE_ROLE_DETAILS:
      return Object.assign({}, state, {
        committeeRoleDetails: action.committeeRoleDetails
      });
    case CommonScoutActionType.UPDATE_COMMITTEE_ROLE_EXTRAS:
      return Object.assign({}, state, {
        committeeRoleExtras: action.committeeRoleExtras
      });
    case CommonScoutActionType.UPDATE_SUB_BRANCHES:
      return Object.assign({}, state, {
        subbranches: action.subbranches
      });
    case ActionType.INITIALIZE:
      return Object.assign({}, state, {
        scout: Object.assign({}, state.scout, action.scout),
        badgeTypes: action.badgeTypes,
        locations: action.locations
      });
    case ActionType.NOTIFY_FAILED:
      DialogHelper.showErrorMessage('Gagal', action.message);
      return state;
    case ActionType.NOTIFY_SAVED:
      DialogHelper.showSuccessMessage('Berhasil', action.message);
      return state;
    case ActionType.UPDATE_BADGE_TYPE_DATA:
      return Object.assign({}, state, {
        badgeTypes: action.badgeTypes
      });
    case ActionType.UPDATE_GROUP_DATA:
      $('#modal-form').modal('hide');
      return Object.assign({}, state, {
        groups: action.groups
      });
    default:
      return state;
  }
};
