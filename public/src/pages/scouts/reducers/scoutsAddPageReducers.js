import * as CommonActionType from '../../../actions/common/actionTypes';
import * as ActionType from '../actions/scoutsAddPageActionTypes';

import DialogHelper from '../../../lib/helpers/DialogHelper';

const initialState = {
  errors: {},
  message: '',
  messageType: null,

  groups: [],

  scout: {
    name: '',
    document_status: 'draft',
    sex: '',
    birthplace: '',
    birthday: new Date(1900, 0, 1, 0, 0, 0),
    religion: '',
    blood_type: '',
    avatar: '',
    nta: '',
    type: '',
    subtype: '',
    participant_role: '',
    subbranch: '',
    group: ''
  }
};

export default function scoutsAddPageReducers(state = initialState, action) {
  console.log(action);
  switch (action.type) {
    case CommonActionType.SHOW_MESSAGE:
      return Object.assign({}, state, {
        message: action.message,
        messageType: action.messageType
      });
    case CommonActionType.HIDE_MESSAGE:
      return Object.assign({}, state, {
        message: '',
        messageType: null
      });
    case CommonActionType.FORM_UPDATE_VALUE:
      let errors = Object.assign({}, state.errors);
      delete errors[action.field];
      return Object.assign({}, state, {
        scout: Object.assign({}, state.scout, {
          [action.field]: action.value
        }),
        errors: errors
      });
    case CommonActionType.FORM_VALIDATION_ERROR:
      return Object.assign({}, state, {
        errors: Object.assign({}, state.errors, {
          [action.field]: action.message
        })
      });
    case CommonActionType.REDIRECT:
      window.location.assign(window.location.origin + action.url);
      return state;
    case ActionType.UPDATE_GROUP_DATA:
      // Mutation? Need to check later.
      $('#modal-form').modal('hide');
      return Object.assign({}, state, {
        groups: action.groups
      });
    case ActionType.NOTIFY_FAILED:
      DialogHelper.showErrorMessage('Gagal', action.message);
      return state;
    default:
      return state;
  }
};
