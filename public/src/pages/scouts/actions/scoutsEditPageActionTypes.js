export const INITIALIZE = 'INITIALIZE';
export const UPDATE_BADGE_TYPE_DATA = 'UPDATE_BADGE_TYPE_DATA';
export const UPDATE_GROUP_DATA = 'UPDATE_GROUP_DATA';
export const UPDATE_SCOUT_DATA = 'UPDATE_SCOUT_DATA';
export const NOTIFY_SAVED = 'NOTIFY_SAVED';
export const NOTIFY_FAILED = 'NOTIFY_FAILED';