import * as ActionType from './scoutsEditPageActionTypes.js';

import BadgeTypeService from '../../../services/BadgeTypeService';
import ScoutService from '../../../services/ScoutService';
import ScoutGroupService from '../../../services/ScoutGroupService';

var badgeTypeService = new BadgeTypeService();
var scoutService = new ScoutService();
var scoutGroupService = new ScoutGroupService();

export const initialize = (scout, locations, badgeTypes) => {
  return {
    type: ActionType.INITIALIZE,
    scout,
    locations,
    badgeTypes
  };
};

export const notifySaved = (message) => {
  return {
    type: ActionType.NOTIFY_SAVED,
    message
  };
};

export const notifyFailed = (message) => {
  return {
    type: ActionType.NOTIFY_FAILED,
    message
  };
};

export const updateScoutGroups = (groups) => {
  return {
    type: ActionType.UPDATE_GROUP_DATA,
    groups
  };
};

export const updateBadgeTypes = (badgeTypes) => {
  return {
    type: ActionType.UPDATE_BADGE_TYPE_DATA,
    badgeTypes
  };
};

// Async Actions

export const fetchGroups = (branchId, subbranchId) => {
  return (dispatch) => {
    scoutGroupService.getAll(
      {
        branch: branchId,
        subbranch: subbranchId
      },
      (data) => {
        dispatch(updateScoutGroups(data.results));
      },
      (data) => {
        dispatch(notifyFailed('Gagal mengambil ulang data regu.'));
      }
    )
  };
};

export const updateScoutData = (id) => {
  return (dispatch, getState) => {
    let state = getState();
    scoutService.updateById(
      id,
      state.EditPage.scout,
      () => {
        dispatch(notifySaved('Sukses.'));
      },
      (data) => {
        dispatch(notifyFailed('Gagal menyimpan data kepesertaan.'));
      }
    );
  };
};

export const addGroup = (branchId, subbranchId, name) => {
  return (dispatch) => {
    scoutGroupService.create(
      {
        branch: branchId,
        subbranch: subbranchId,
        name: name
      },
      () => {
        dispatch(fetchGroups(branchId, subbranchId));
      },
      (data) => {
        dispatch(notifyFailed('Gagal menambahkan data regu.'));
      }
    );
  };
};

export const getBadgeTypes = (badgeTypeName) => {
  return (dispatch) => {
    badgeTypeService.getAll(
      {
        name: badgeTypeName
      },
      (data) => {
        dispatch(updateBadgeTypes(data.results));
      },
      (data) => {
        dispatch(notifyFailed('Gagal mengambil data kecakapan khusus.'))
      }
    );  
  };
};