export const UPDATE_GROUP_DATA = 'UPDATE_GROUP_DATA';
export const NOTIFY_SAVED = 'NOTIFY_SAVED';
export const NOTIFY_FAILED = 'NOTIFY_FAILED';