import * as CommonAction from '../../../actions/common/actions';
import * as ActionType from './scoutsAddPageActionTypes.js';

import ScoutService from '../../../services/ScoutService.js';
import ScoutGroupService from '../../../services/ScoutGroupService.js';

var scoutService = new ScoutService();
var scoutGroupService = new ScoutGroupService();

const validationFieldRules = {
  'name': ['REQUIRED']
};

export const notifySaved = (message) => {
  return {
    type: ActionType.NOTIFY_SAVED,
    message
  };
};

export const notifyFailed = (message) => {
  return {
    type: ActionType.NOTIFY_FAILED,
    message
  };
};

export const updateScoutGroups = (groups) => {
  return {
    type: ActionType.UPDATE_GROUP_DATA,
    groups
  };
};

export const validateField = (fieldName) => {
  return (dispatch) => {
    switch (fieldName) {
      case 'name':

        break;
      case 'nta':

        break;
    }
  };
};

// Async Actions

export const addGroup = (branchId, subbranchId, name) => {
  return (dispatch) => {
    scoutGroupService.create(
      {
        branch: branchId,
        subbranch: subbranchId,
        name: name
      },
      () => {
        dispatch(fetchGroups(branchId, subbranchId));
      },
      (data) => {
        dispatch(notifyFailed('Gagal menambahkan data regu.'));
      }
    );
  };
};

export const fetchGroups = (branchId, subbranchId) => {
  return (dispatch) => {
    scoutGroupService.getAll(
      {
        branch: branchId,
        subbranch: subbranchId
      },
      (data) => {
        dispatch(updateScoutGroups(data.results));
      },
      (data) => {
        dispatch(notifyFailed('Gagal mengambil ulang data regu.'));
      }
    )
  };
};

export const addScout = (scout) => {
  return (dispatch, getState) => {
    const state = getState();

    dispatch(validateSync(
      () => {
        if (Object.keys(state.AddPage.errors).length > 0) {
          dispatch(notifyFailed('Gagal menyimpan data, periksa kembali data Anda.'));
        }
        else {
          scoutService.create(
            scout,
            (data) => {
              dispatch(CommonAction.redirect('/scouts/' + data.result._id + '/edit'));
            },
            (data) => {
              let message = data.responseJSON['message'];
              if (!message || message === '') {
                message = 'Gagal menyimpan data, periksa kembali data Anda.'
              }
              dispatch(notifyFailed(message));
            }
          );
        }
      },
      (data) => {
        dispatch(notifyFailed('Gagal menyimpan data, mohon periksa kembali data Anda.'));
      }
    ));
  };
};

export const validateSync = (successCallback, errorCallback) => {
  return (dispatch, getState) => {
    const state = getState();
    let hasError = false;
    let scout = Object.assign({}, state.AddPage.scout);
    Object.keys(scout).forEach((field) => {
      let value = scout[field];
      switch (field) {
        case 'type':
        case 'sex':
        case 'religion':
        case 'blood_type':
        case 'name':
        case 'nta':
        case 'birthplace':
          if (value === '') {
            hasError = true;
            dispatch(CommonAction.formValidationError(field, 'Kolom ini harus diisi.'));
          }
          break;
        case 'group':
          if (scout.type === 'participant' && value === '') {
            hasError = true;
            dispatch(CommonAction.formValidationError(field, 'Kolom ini harus diisi.'));
          }
          break;
        default:
          break;
      }
    });
    if (hasError) {
      errorCallback();
    }
    else {
      successCallback();
    }
  };
};

export const validateMemberId = (id) => {

};