import { Provider } from 'react-redux';
import { applyMiddleware, combineReducers, createStore } from 'redux';

import React from 'react';
import ReactDom from 'react-dom';
import thunk from 'redux-thunk';

import * as Action from './actions/scoutsEditPageActions';
import * as CommonAction from '../../actions/common/actions';
import * as CommonScoutAction from '../../actions/common/scoutCommonActions';

import scoutsEditPageReducers from './reducers/scoutsEditPageReducers';

import Page from '../Page';

import EditPageContainer from './containers/EditPageContainer';
import ScoutSubbranchFormHeader from '../../components/common/ui/ScoutSubbranchFormHeader';

export default class EditPage extends Page {
  constructor(selectors) {
    super(selectors);

    this.branch = window.currentPageData['branch'];
    this.branches = window.currentPageData['branches'];
    this.badgeTypes = window.currentPageData['badgeTypes'];
    this.committeeRoles = window.currentPageData['committeeRoles'];
    this.enumStringMap = window.currentPageData['enumStringMap'];
    this.locations = window.currentPageData['locations'];
    this.scout = window.currentPageData['scout'];
    this.subbranch = window.currentPageData['subbranch'];
  }

  _createReducers() {
    return combineReducers({
      EditPage: scoutsEditPageReducers
    });
  }

  _createStore() {
    let reducers = this._createReducers();
    return createStore(reducers, applyMiddleware(thunk));
  }

  _initialize() {
    this._store.dispatch(Action.initialize(
      this.scout, this.locations, this.badgeTypes
    ));
    this._store.dispatch(Action.fetchGroups(
      this.branch._id, this.subbranch._id
    ));
    if (this.scout.contact && this.scout.contact.province && this.scout.contact.province != '') {
      this._store.dispatch(CommonAction.fetchLocationChildren(this.scout.contact.province, 'contactSubLocations'));
    }
    if (this.scout.parent_contact && this.scout.parent_contact.province && this.scout.parent_contact.province != '') {
      this._store.dispatch(CommonAction.fetchLocationChildren(this.scout.contact.province, 'parentContactSubLocations'));
    }
    this._store.dispatch(CommonScoutAction.updateBranches(this.branches));
    this._store.dispatch(CommonScoutAction.updateCommitteeRoles(this.committeeRoles))
  }

  setup() {
    const enumStringMap = this.enumStringMap;

    this._store = this._createStore();
    this._initialize();
    this.render(
      <div className="row wrapper border-bottom white-bg page-heading padding-bottom-xs">
        <div className="col-lg-12">
          <ScoutSubbranchFormHeader branch={this.branch} subbranch={this.subbranch} />
        </div>
      </div>,
      $(this._selectors.header)[0]
    );
    this.render(
      <Provider store={this._store}>
        <EditPageContainer enumStringMap={enumStringMap} />
      </Provider>,
      $(this._selectors.main)[0]
    );
    this._store.dispatch(CommonScoutAction.classifyAndUpdateCommitteeRoleDetails(this.scout.committee_role, this.enumStringMap || {}));
    this._store.dispatch(CommonScoutAction.classifyAndUpdateCommitteeRoleExtras(this.scout.committee_role_detail, this.enumStringMap || {}));
  }
}
