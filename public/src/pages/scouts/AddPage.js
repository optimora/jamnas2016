import { Provider } from 'react-redux';
import { applyMiddleware, combineReducers, createStore } from 'redux';

import React from 'react';
import ReactDom from 'react-dom';
import thunk from 'redux-thunk';

import * as CommonAction from '../../actions/common/actions';
import * as Action from './actions/scoutsAddPageActions';
import scoutsAddPageReducers from './reducers/scoutsAddPageReducers';

import Page from '../Page';

import AddPageContainer from './containers/AddPageContainer';
import ScoutSubbranchFormHeader from '../../components/common/ui/ScoutSubbranchFormHeader';

export default class AddPage extends Page {
  constructor(selectors) {
    super(selectors);

    this.branch = window.currentPageData.branch;
    this.subbranch = window.currentPageData.subbranch;
  }

  _createReducers() {
    return combineReducers({
      AddPage: scoutsAddPageReducers
    });
  }

  _createStore() {
    let reducers = this._createReducers();
    return createStore(reducers, applyMiddleware(thunk));
  }

  _initialize() {
    this._store.dispatch(Action.fetchGroups(this.branch._id, this.subbranch._id));
    this._store.dispatch(CommonAction.formUpdateValue('subbranch', this.subbranch._id));
  }

  setup() {
    this._store = this._createStore();
    this.render(
      <div className="row wrapper border-bottom white-bg page-heading padding-bottom-xs">
        <div className="col-lg-12">
          <ScoutSubbranchFormHeader branch={this.branch} subbranch={this.subbranch} />
        </div>
      </div>,
      $(this._selectors.header)[0]
    );
    this.render(
      <Provider store={this._store}>
        <AddPageContainer />
      </Provider>,
      $(this._selectors.main)[0]
    );
    this._initialize();
  }
}
