import React from 'react';

import FormFieldWrapper from '../../../components/common/forms/FormFieldWrapper';

export default class JuniorExperienceForm extends React.Component {
  constructor(...args) {
    super(...args);

    this._initialize();
  }

  static get propTypes() {
    return {
      experiences: React.PropTypes.array.isRequired,
      badgeTypes: React.PropTypes.array.isRequired,
      badgeTypeDictionary: React.PropTypes.object.isRequired,

      onChange: React.PropTypes.func.isRequired
    }
  }

  _handleAddNewBadgeEntry(event, value) {
    event.preventDefault();
    if (this.state.experiences[1].badges.filter((entry) => entry.type === value.type && entry.category === value.category).length > 0) {
      alert('Anda telah menambahkan TKK "' + value.value + '" ini ke kategori ' + value.category + ' sebelumnya.');
    }
    else {
      let experiences = this.state.experiences.slice(0);
      experiences[1].badges.push(value);
      this.setState(
        {
          experiences
        },
        () => {
          this.props.onChange(this.state.experiences);
        }
      );
    }
  }

  _handleRemoveBadgeEntry(event, id) {
    event.preventDefault();
    let experiences = this.state.experiences.slice(0);
    experiences[1].badges = experiences[1].badges.filter((badge) => badge._id !== id);
    this.setState(
      {
        experiences
      },
      () => {
        this.props.onChange(this.state.experiences);
      }
    );
  }

  _handleFieldValueChange(index, field, value) {
    let experiences = this.state.experiences.slice(0);
    experiences[index][field] = value;
    this.setState(
      {
        experiences
      },
      () => {
        this.props.onChange(this.state.experiences);
      }
    );
  }

  _handleSubFieldValueChange(parentIndex, index, field, value) {
    let experiences = this.state.experiences.slice(0);
    experiences[parentIndex].achievements[index][field] = value;
    this.setState(
      {
        experiences
      },
      () => {
        this.props.onChange(this.state.experiences);
      }
    );
  }

  _handleInternalFieldValueChange(field, value) {
    let fieldSegments = field.split('.');
    if (fieldSegments.length > 1) {
      this.setState({
        [fieldSegments[0]]: Object.assign({}, this.state[fieldSegments[0]], {
          [fieldSegments[1]]: value
        })
      }, () => {console.log(this.state)});
    }
    else {
      this.setState({
        [field]: value
      });
    }
  }

  _handleAddNewActivityEntry(index, fieldSegment) {
    let experiences = this.state.experiences.slice(0);
    experiences[index].activities.push(this.state[fieldSegment]);
    this.setState(
      {
        experiences
      },
      () => {
        this.props.onChange(this.state.experiences)
      }
    );
    this.setState({
      [fieldSegment]: {
        year: '',
        name: ''
      }
    })
  }

  _handleRemoveActivityEntry(index, removalIndex) {
    let experiences = this.state.experiences.slice(0);
    experiences[index].activities = [
      ...experiences[index].activities.slice(0, removalIndex),
      ...experiences[index].activities.slice(removalIndex + 1)
    ];
    this.setState(
      {
        experiences
      },
      () => {
        this.props.onChange(this.state.experiences)
      }
    );
  }

  _initialize() {
    this.state = {
      experiences: this.props.experiences.slice(0),

      errors: {},

      _firstActivityItem: {
        name: '',
        year: ''
      },
      _secondActivityItem: {
        name: '',
        year: ''
      },
      _badgeTypeName: '',
      _category: 'purwa'
    };
  }

  _printCaptionByEnum(raw) {
    return raw[0].toUpperCase() + raw.split('').slice(1).join('');
  }

  componentWillReceiveProps(props) {
    this.setState({
      experiences: props.experiences
    });
  }

  componentWillReceiveProps(props) {
    this.setState({
      experiences: props.experiences
    });
  }

  render() {
    return (
      <div className='ibox-content'>

        <div className='col-lg-12 margin-bottom-md'>
          <div className='col-lg-2 no-margin no-padding'>
            <label className='control-label align-left text-underline'>SIAGA</label>
          </div>
          <div className='col-lg-10'>
            <FormFieldWrapper name='gudep_number' fieldValue={this.state.experiences[0].gudep_number} onChange={(fieldName, value) => this._handleFieldValueChange(0, fieldName, value)} className='col-lg-6 no-padding-left'>
              <label className='control-label align-left margin-bottom-xxs'>Nomor Gudep</label>
              <input type='text' className='form-control' />
            </FormFieldWrapper>
            <FormFieldWrapper name='base' fieldValue={this.state.experiences[0].base} onChange={(fieldName, value) => this._handleFieldValueChange(0, fieldName, value)}  className='col-lg-6 no-padding-right'>
              <label className='control-label align-left margin-bottom-xxs'>Nama Pangkalan</label>
              <input type='text' className='form-control' />
            </FormFieldWrapper>
          </div>
        </div>

        <div className='col-lg-10 col-lg-offset-2'>
          <label className='control-label align-left text-underline padding-left-xs'>TANDA KECAKAPAN KHUSUS (TKK)</label>
        </div>

        <div className='col-lg-10 col-lg-offset-2 margin-top-xs'>
          <div className='col-lg-3 no-margin no-padding'>
            <label className='control-label align-left margin-bottom-xxs'></label>
          </div>
          <div className='col-lg-2 align-center'>
            <p className='control-label align-center margin-bottom-xxs'>Mula</p>
          </div>
          <div className='col-lg-2 align-center'>
            <p className='control-label align-center margin-bottom-xxs'>Bantu</p>
          </div>
          <div className='col-lg-2 align-center'>
            <p className='control-label align-center margin-bottom-xxs'>Tata</p>
          </div>
          <div className='col-lg-2 align-center'>
            <p className='control-label align-center margin-bottom-xxs'>Garuda</p>
          </div>
        </div>

        <div className='col-lg-10 col-lg-offset-2 margin-bottom-xxs'>
          <div className='col-lg-3 no-margin padding-left-xs'>
            <p className='control-label align-left margin-bottom-xxs margin-top-xs'>Tahun</p>
          </div>
          <FormFieldWrapper name='year' fieldValue={this.state.experiences[0].achievements[0].year} onChange={(field, value) => this._handleSubFieldValueChange(0, 0, field, value)} className='col-lg-2'>
            <input type='text' className='form-control align-center' />
          </FormFieldWrapper>
          <FormFieldWrapper name='year' fieldValue={this.state.experiences[0].achievements[1].year} onChange={(field, value) => this._handleSubFieldValueChange(0, 1, field, value)}  className='col-lg-2'>
            <input type='text' className='form-control align-center' />
          </FormFieldWrapper>
          <FormFieldWrapper name='year' fieldValue={this.state.experiences[0].achievements[2].year} onChange={(field, value) => this._handleSubFieldValueChange(0, 2, field, value)}  className='col-lg-2'>
            <input type='text' className='form-control align-center' />
          </FormFieldWrapper>
          <FormFieldWrapper name='year' fieldValue={this.state.experiences[0].achievements[3].year} onChange={(field, value) => this._handleSubFieldValueChange(0, 3, field, value)}  className='col-lg-2'>
            <input type='text' className='form-control align-center' />
          </FormFieldWrapper>
        </div>

        <div className='col-lg-10 col-lg-offset-2'>
          <div className='col-lg-3 no-margin padding-left-xs'>
            <p className='control-label align-left margin-bottom-xxs margin-top-xs'>Jumlah TKK</p>
          </div>
          <FormFieldWrapper name='number_of_badges' fieldValue={this.state.experiences[0].achievements[0].number_of_badges} onChange={(field, value) => this._handleSubFieldValueChange(0, 0, field, value)} className='col-lg-2'>
            <input type='text' className='form-control align-center' />
          </FormFieldWrapper>
          <FormFieldWrapper name='number_of_badges' fieldValue={this.state.experiences[0].achievements[1].number_of_badges} onChange={(field, value) => this._handleSubFieldValueChange(0, 1, field, value)}  className='col-lg-2'>
            <input type='text' className='form-control align-center' />
          </FormFieldWrapper>
          <FormFieldWrapper name='number_of_badges' fieldValue={this.state.experiences[0].achievements[2].number_of_badges} onChange={(field, value) => this._handleSubFieldValueChange(0, 2, field, value)}  className='col-lg-2'>
            <input type='text' className='form-control align-center' />
          </FormFieldWrapper>
          <FormFieldWrapper name='number_of_badges' fieldValue={this.state.experiences[0].achievements[3].number_of_badges} onChange={(field, value) => this._handleSubFieldValueChange(0, 3, field, value)}  className='col-lg-2'>
            <input type='text' className='form-control align-center' />
          </FormFieldWrapper>
        </div>

        <div className='col-lg-10 col-lg-offset-2 margin-top-lg'>
          <label className='control-label align-left text-underline padding-left-xs'>Giat Siaga Telah Diikuti </label>
        </div>

        <div className='col-lg-10 col-lg-offset-2 margin-top-xs'>
          <div className='col-lg-1 align-center'>
            <label className='control-label align-left margin-bottom-xxs'>No</label>
          </div>
          <div className='col-lg-8'>
            <label className='control-label align-left margin-bottom-xxs'>Nama Kegiatan</label>
          </div>
          <div className='col-lg-2 align-center'>
            <label className='control-label align-left margin-bottom-xxs'>Tahun</label>
          </div>
          <div className='col-lg-1 align-center'>
            <label className='control-label align-left margin-bottom-xxs'>Aksi</label>
          </div>
        </div>

        {this.props.experiences[0].activities.map((activity, index) => {
          return (
            <div key={index} className='col-lg-10 col-lg-offset-2 margin-top-xs'>
              <div className='col-lg-1 align-center'>
                <p>{index + 1}</p>
              </div>
              <div className='col-lg-8'>
                <p>{activity.name}</p>
              </div>
              <div className='col-lg-2 align-center'>
                <p>{activity.year}</p>
              </div>
              <div className='col-lg-1'>
                <button className='btn btn-danger btn-xs' onClick={() => this._handleRemoveActivityEntry(0, index)}>Hapus</button>
              </div>
            </div>
          );
        })}

        <div className='col-lg-10 col-lg-offset-2 margin-bottom-xxs'>
          <div className='col-lg-1 align-center'>
            <p className='control-label align-center margin-top-xxs'>{this.props.experiences[0].activities.length + 1}.</p>
          </div>
          <FormFieldWrapper name='_firstActivityItem.name' fieldValue={this.state._firstActivityItem.name} onChange={(field, value) => this._handleInternalFieldValueChange(field, value)} className='col-lg-8'>
            <input type='text' className='form-control' name='experiences.activities.name' />
          </FormFieldWrapper>
          <FormFieldWrapper name='_firstActivityItem.year' fieldValue={this.state._firstActivityItem.year} onChange={(field, value) => this._handleInternalFieldValueChange(field, value)} className='col-lg-2'>
            <input type='text' className='form-control align-center' name='experiences.activities.year' />
          </FormFieldWrapper>
          <button className='btn btn-success pull-right' onClick={() => this._handleAddNewActivityEntry(0, '_firstActivityItem')}>Tambah Giat Siaga</button>
        </div>

        <div className='col-lg-12'><div className='hr-line-dashed'></div></div>

        <div className='col-lg-12 margin-bottom-md'>
          <div className='col-lg-2 no-margin no-padding'>
            <label className='control-label align-left text-underline'>PENGGALANG</label>
          </div>
          <div className='col-lg-10'>
            <FormFieldWrapper name='gudep_number' fieldValue={this.state.experiences[1].gudep_number} onChange={(field, value) => this._handleFieldValueChange(1, field, value)} className='col-lg-6 no-padding-left'>
              <label className='control-label align-left margin-bottom-xxs'>Nomor Gudep</label>
              <input type='text' className='form-control' name='experiences.gudep_number'></input>
            </FormFieldWrapper>
            <FormFieldWrapper name='base' fieldValue={this.state.experiences[1].base} onChange={(field, value) => this._handleFieldValueChange(1, field, value)} className='col-lg-6 no-padding-right'>
              <label className='control-label align-left margin-bottom-xxs'>Nama Pangkalan</label>
              <input type='text' className='form-control' name='experiences.base'></input>
            </FormFieldWrapper>
          </div>
        </div>

        <div className='col-lg-10 col-lg-offset-2'>
          <div className='col-lg-3 no-margin no-padding'>
            <label className='control-label align-left margin-bottom-xxs'></label>
          </div>
          <div className='col-lg-2 align-center'>
            <p className='control-label align-center margin-bottom-xxs'>Ramu</p>
          </div>
          <div className='col-lg-2 align-center'>
            <p className='control-label align-center margin-bottom-xxs'>Rakit</p>
          </div>
          <div className='col-lg-2 align-center'>
            <p className='control-label align-center margin-bottom-xxs'>Terap</p>
          </div>
          <div className='col-lg-2 align-center'>
            <p className='control-label align-center margin-bottom-xxs'>Garuda</p>
          </div>
        </div>

        <div className='col-lg-10 col-lg-offset-2 margin-bottom-xxs'>
          <div className='col-lg-3 no-margin padding-left-xs'>
            <label className='control-label align-left margin-bottom-xxs margin-top-xs'>Pencapaian Tingkat</label>
          </div>
          <FormFieldWrapper name='year' fieldValue={this.state.experiences[1].achievements[0].year} onChange={(field, value) => this._handleSubFieldValueChange(1, 0, field, value)} className='col-lg-2'>
            <input type='text' className='form-control align-center' name='experiences.achievement.year' placeholder='Tahun'></input>
          </FormFieldWrapper>
          <FormFieldWrapper name='year' fieldValue={this.state.experiences[1].achievements[1].year} onChange={(field, value) => this._handleSubFieldValueChange(1, 1, field, value)} className='col-lg-2'>
            <input type='text' className='form-control align-center' name='experiences.achievement.year' placeholder='Tahun'></input>
          </FormFieldWrapper>
          <FormFieldWrapper name='year' fieldValue={this.state.experiences[1].achievements[2].year} onChange={(field, value) => this._handleSubFieldValueChange(1, 2, field, value)} className='col-lg-2'>
            <input type='text' className='form-control align-center' name='experiences.achievement.year' placeholder='Tahun'></input>
          </FormFieldWrapper>
          <FormFieldWrapper name='year' fieldValue={this.state.experiences[1].achievements[3].year} onChange={(field, value) => this._handleSubFieldValueChange(1, 3, field, value)} className='col-lg-2'>
            <input type='text' className='form-control align-center' name='experiences.achievement.year' placeholder='Tahun'></input>
          </FormFieldWrapper>
        </div>

        <div className='col-lg-10 col-lg-offset-2 margin-top-md'>
          <label className='control-label align-left text-underline padding-left-xs'>TANDA KECAKAPAN KHUSUS (TKK)</label>
        </div>

        <div className='col-lg-10 col-lg-offset-2 margin-top-md'>
          <div className='col-lg-4'>
            <label>Cari Nama TKK</label>
            <input className='form-control' type='text' value={this.state._badgeTypeName} onChange={(event) => this._handleInternalFieldValueChange('_badgeTypeName', event.target.value)} />
          </div>
          <div className='col-lg-4'>
            <label>Kategori</label>
            <select className='form-control' value={this.state._category} onChange={(event) => this._handleInternalFieldValueChange('_category', event.target.value)}>
              <option value='purwa'>Purwa</option>
              <option value='madya'>Madya</option>
              <option value='utama'>Utama</option>
            </select>
          </div>
          <div className='col-lg-4'>
            <p><strong>Menampilkan 20 hasil teratas. Lihat hasil pencarian di bawah ini, dan Klik [+] untuk menambahkan TKK.</strong></p>
          </div>
        </div>
        <div className='col-lg-10 col-lg-offset-2 margin-top-md'>
          {this.props.badgeTypes.filter((type) => new RegExp(this.state._badgeTypeName, 'i').test(type.name)).slice(0, 20).map((badgeType) => {
            return (
              <span className='badgeTypeEntry' key={badgeType.unique_name}>
                {badgeType.name}
                <a href='#' onClick={(event) => this._handleAddNewBadgeEntry(event, { type: badgeType._id, category: this.state._category })}> [+]</a>
              </span>
            );
          })}
        </div>

        <div className='col-lg-10 col-lg-offset-2 margin-top-lg'>
          <div className='col-lg-12'>
            <div className='col-lg-4 align-center'>
              <label className='control-label margin-bottom-xxs'>PURWA</label>
              {this.props.experiences[1].badges.filter((i) => i.category === 'purwa').map((badge, index) => {
                return (
                  <p key={index} className='badgeEntry'>{this.props.badgeTypeDictionary[badge.type].name || ''} <a href='#' onClick={(event) => this._handleRemoveBadgeEntry(event, badge._id)}>(Hapus)</a></p>
                );
              })}
            </div>
            <div className='col-lg-4 align-center'>
              <label className='control-label margin-bottom-xxs'>MADYA</label>
              {this.props.experiences[1].badges.filter((i) => i.category === 'madya').map((badge, index) => {
                return (
                  <p key={index} className='badgeEntry'>{this.props.badgeTypeDictionary[badge.type].name || ''} <a href='#' onClick={(event) => this._handleRemoveBadgeEntry(event, badge._id)}>(Hapus)</a></p>
                );
              })}
            </div>
            <div className='col-lg-4 align-center'>
              <label className='control-label margin-bottom-xxs'>UTAMA</label>
              {this.props.experiences[1].badges.filter((i) => i.category === 'utama').map((badge, index) => {
                return (
                  <p key={index} className='badgeEntry'>{this.props.badgeTypeDictionary[badge.type].name || ''} <a href='#' onClick={(event) => this._handleRemoveBadgeEntry(event, badge._id)}>(Hapus)</a></p>
                );
              })}
            </div>
          </div>
        </div>

        <div className='col-lg-10 col-lg-offset-2 margin-top-lg'>
          <label className='control-label align-left text-underline padding-left-xs'>Giat Penggalang yang Telah Diikuti </label>
        </div>

        <div className='col-lg-10 col-lg-offset-2 margin-top-xs'>
          <div className='col-lg-1 align-center'>
            <label className='control-label align-left margin-bottom-xxs'>No</label>
          </div>
          <div className='col-lg-8'>
            <label className='control-label align-left margin-bottom-xxs'>Nama Kegiatan</label>
          </div>
          <div className='col-lg-2 align-center'>
            <label className='control-label align-left margin-bottom-xxs'>Tahun</label>
          </div>
          <div className='col-lg-1 align-center'>
            <label className='control-label align-left margin-bottom-xxs'>Aksi</label>
          </div>
        </div>

        {this.props.experiences[1].activities.map((activity, index) => {
          return (
            <div key={index} className='col-lg-10 col-lg-offset-2 margin-top-xs'>
              <div className='col-lg-1 align-center'>
                <p>{index + 1}</p>
              </div>
              <div className='col-lg-8'>
                <p>{activity.name}</p>
              </div>
              <div className='col-lg-2 align-center'>
                <p>{activity.year}</p>
              </div>
              <div className='col-lg-1'>
                <button className='btn btn-danger btn-xs' onClick={() => this._handleRemoveActivityEntry(1, index)}>Hapus</button>
              </div>
            </div>
          );
        })}

        <div className='col-lg-10 col-lg-offset-2 margin-bottom-xxs'>
          <div className='col-lg-1 align-center'>
            <p className='control-label align-center margin-top-xxs'>{this.state.experiences[1].activities.length + 1}.</p>
          </div>
          <FormFieldWrapper name='_secondActivityItem.name' fieldValue={this.state._secondActivityItem.name} onChange={(field, value) => this._handleInternalFieldValueChange(field, value)} className='col-lg-8'>
            <input type='text' className='form-control' />
          </FormFieldWrapper>
          <FormFieldWrapper name='_secondActivityItem.year' fieldValue={this.state._secondActivityItem.year} onChange={(field, value) => this._handleInternalFieldValueChange(field, value)} className='col-lg-2'>
            <input type='text' className='form-control align-center' />
          </FormFieldWrapper>

          <button className='btn btn-success pull-right' onClick={() => this._handleAddNewActivityEntry(1, '_secondActivityItem')}>Tambah Giat Penggalang</button>
        </div>

        <div className='col-lg-12'><div className='hr-line-dashed'></div></div>

        <div style={{ width: '100%', height: '1px', clear: 'both' }}></div>
      </div>
    );
  }
}