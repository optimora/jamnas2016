import React from 'react';

export default class MedicalHistoryFormView extends React.Component {
  static get propTypes() {
    return {
      emergencyContacts: React.PropTypes.array.isRequired,
      emergencyRadios: React.PropTypes.array.isRequired,
      medicalHistory: React.PropTypes.array.isRequired
    };
  }

  render() {
    return(
      <div className='ibox float-e-margins'>
        <div className='ibox-title'>
          <h5>Data Riwayat Kesehatan</h5>
          <div className='ibox-tools'>
            <a className='collapse-link'>
              <i className='fa fa-chevron-up'></i>
            </a>
          </div>
        </div>
        <div className='ibox-content'>

          <div className='col-lg-12'>
            <label className='control-label align-left text-underline padding-left-xs'>Penyakit yang Dimiliki</label>
          </div>

          <div className='col-lg-12 margin-top-xs'>
            <div className='col-lg-1 align-center'>
              <label className='control-label align-left margin-bottom-xxs'>No</label>
            </div>
            <div className='col-lg-3 align-center'>
              <label className='control-label align-left margin-bottom-xxs'>Nama Penyakit</label>
            </div>
            <div className='col-lg-4 align-center'>
              <label className='control-label align-left margin-bottom-xxs'>Obat Pribadi yang Dimiliki</label>
            </div>
            <div className='col-lg-4 align-center'>
              <label className='control-label align-left margin-bottom-xxs'>Penanganan Medis</label>
            </div>
          </div>

          {this.props.medicalHistory.map((item, index) => {
            return (
              <div key={index} className='col-lg-12 margin-top-xs'>
                <div className='col-lg-1 align-center'>
                  <p className='control-label align-center margin-top-xxs'>{index + 1}.</p>
                </div>
                <div className='col-lg-3 align-center'>
                  <p className='control-label align-center margin-top-xxs'>{item.disease}</p>
                </div>
                <div className='col-lg-4 align-center'>
                  <p className='control-label align-center margin-top-xxs'>{item.medicine}</p>
                </div>
                <div className='col-lg-4 align-center'>
                  <p className='control-label align-center margin-top-xxs'>{item.treatment}</p>
                </div>
              </div>
            );
          })}

          <div className='col-lg-12'><div className='hr-line-dashed'></div></div>

          <div className='col-lg-12'>
            <label className='control-label align-left text-underline padding-left-xs'>Nomor darurat yang bisa dihubungi di kampung halaman:</label>
          </div>

          <div className='col-lg-12 margin-top-xs'>
            <div className='col-lg-1 align-center'>
              <label className='control-label align-left margin-bottom-xxs'>No</label>
            </div>
            <div className='col-lg-2 align-center'>
              <label className='control-label align-left margin-bottom-xxs'>Telepon</label>
            </div>
            <div className='col-lg-3 align-center'>
              <label className='control-label align-left margin-bottom-xxs'>Handphone</label>
            </div>
            <div className='col-lg-3 align-center'>
              <label className='control-label align-left margin-bottom-xxs'>Nama Pemilik</label>
            </div>
            <div className='col-lg-3 align-center'>
              <label className='control-label align-center margin-bottom-xxs'>Hubungan dengan Peserta</label>
            </div>
          </div>

          {this.props.emergencyContacts.map((item, index) => {
            return (
              <div className='col-lg-12 margin-top-xxs'>
                <div className='col-lg-1 align-center'>
                  <p className='control-label align-center margin-top-xxs'>{index + 1}.</p>
                </div>
                <div className='col-lg-2 align-center'>
                  <p className='control-label align-center margin-top-xxs'>{item.phone.home}</p>
                </div>
                <div className='col-lg-3 align-center'>
                  <p className='control-label align-center margin-top-xxs'>{item.phone.mobile}</p>
                </div>
                <div className='col-lg-3 align-center'>
                  <p className='control-label align-center margin-top-xxs'>{item.name}</p>
                </div>
                <div className='col-lg-3 align-center'>
                  <p className='control-label align-center margin-top-xxs'>{item.relation}</p>
                </div>
              </div>
            );
          })}

          <div className='col-lg-12'><div className='hr-line-dashed'></div></div>

          <div className='col-lg-12'>
            <label className='control-label align-left text-underline padding-left-xs'>Radio yang bisa dihubungi di kampung halaman:</label>
          </div>

          <div className='col-lg-12 margin-top-xs'>
            <div className='col-lg-1 align-center'>
              <label className='control-label align-left margin-bottom-xxs'>No</label>
            </div>
            <div className='col-lg-3 align-center'>
              <label className='control-label align-left margin-bottom-xxs'>Call Sign</label>
            </div>
            <div className='col-lg-3 align-center'>
              <label className='control-label align-left margin-bottom-xxs'>Nama Pemilik</label>
            </div>
            <div className='col-lg-2 align-center'>
              <label className='control-label align-left margin-bottom-xxs'>Frekuensi Radio Amatir</label>
            </div>
            <div className='col-lg-3 align-center'>
              <label className='control-label align-center margin-bottom-xxs'>Lokal</label>
            </div>
          </div>

          {this.props.emergencyRadios.map((item, index) => {
            return (
              <div className='col-lg-12 margin-top-xs'>
                <div className='col-lg-1 align-center'>
                  <p>{index + 1}</p>
                </div>
                <div className='col-lg-3 align-center'>
                  <p>{item.call_sign}</p>
                </div>
                <div className='col-lg-3 align-center'>
                  <p>{item.name}</p>
                </div>
                <div className='col-lg-2 align-center'>
                  <p>{item.frequency}</p>
                </div>
                <div className='col-lg-3 align-center'>
                  <p>{item.local_area}</p>
                </div>
              </div>
            );
          })}

          <div style={{ width: '100%', height: '1px', clear: 'both' }}></div>
        </div>
      </div>
    );
  }
}
