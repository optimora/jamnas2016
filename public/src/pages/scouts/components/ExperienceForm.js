import React from 'react';

import JuniorExperienceForm from './JuniorExperienceForm';
import SeniorExperienceForm from './SeniorExperienceForm';

const emptySeniorForm = ['siaga', 'penggalang', 'penegak', 'pandega', 'pembina'].map((level) => {
  return {
    level,
    year: '',
    gudep_number: '',
    base: ''
  }
});

const emptyJuniorForm = [
  {
    level: 'siaga',
    achievements: [
      ...['mula', 'bantu', 'tata', 'garuda'].map((subLevel) => ({
        sub_level: subLevel,
        year: '',
        number_of_badges: ''
      }))
    ],
    activities: [],
    badges: []
  },
  {
    level: 'penggalang',
    achievements: [
      ...['ramu', 'rakit', 'terap', 'garuda'].map((subLevel) => ({
        sub_level: subLevel,
        year: '',
        number_of_badges: ''
      }))
    ],
    activities: [],
    badges: []
  }
];

export default class ExperienceForm extends React.Component {
  constructor(props, context) {
    super(props, context);

    this._initialize();
  }
  
  static get propTypes() {
    return {
      type: React.PropTypes.string.isRequired,
      experiences: React.PropTypes.array.isRequired,
      badgeTypes: React.PropTypes.array.isRequired,

      onChange: React.PropTypes.func.isRequired,
      onSave: React.PropTypes.func.isRequired
    };
  }

  _handleBadgeTypeSearch(name) {
    this.props.onBadgeTypeSearch(name);
  }

  _handleFieldValueChange(values) {
    this.setState({
      experiences: values
    });
    this.props.onChange(values);
  }

  _handleSubmit() {
    this.props.onSave();
  }

  _initialize() {
    let initialState = {
      errors: {}
    };

    if (this.props.experiences.length == 0) {
      if (this.props.type === 'participant') {
        initialState.experiences = emptyJuniorForm.slice(0);
      }
      else {
        initialState.experiences = emptySeniorForm.slice(0);
      }
    }
    else {
      initialState.experiences = this.props.experiences;
    }

    this.state = initialState;

    this.badgeTypeDictionary = {};
    this.props.badgeTypes.forEach((badgeType) => {
      this.badgeTypeDictionary[badgeType._id] = badgeType;
    });
  }

  componentWillReceiveProps(props) {
    let modifiedState = { errors: {} };
    modifiedState.experiences = props.experiences;
    if (props.type === 'participant') {
      if (modifiedState.experiences.length > 2) {
        // This means, we've to handle type transition from non-participant to participant by providing
        // needed data.
        modifiedState.experiences.forEach((experience, index) => {
          if (index < 2) {
            if (!experience.achievements || experience.achievements.length < 4) {
              experience.achievements = emptyJuniorForm[index].achievements.slice(0);
            }
          }
        });

        this.setState({
          experiences: modifiedState.experiences
        });
      }
    }
    else {
      if (modifiedState.experiences.length < 5) {
        // For participant to non-participant, we'll just erase old data.
        modifiedState.experiences = emptySeniorForm.slice(0);

        this.setState({
          experiences: modifiedState.experiences
        })
      }
    }
  }

  render() {
    let experienceSubForm;
    if (this.props.type === 'participant') {
      experienceSubForm = <JuniorExperienceForm
        experiences={this.state.experiences.length == 0 ? this.state.experiences.slice(0) : this.state.experiences}
        badgeTypes={this.props.badgeTypes}
        badgeTypeDictionary={this.badgeTypeDictionary}
        onChange={this._handleFieldValueChange.bind(this)} />;
    }
    else {
      experienceSubForm = <SeniorExperienceForm
        experiences={this.state.experiences.length == 0 ? this.state.experiences.slice(0) : this.state.experiences}
        onChange={this._handleFieldValueChange.bind(this)}
        onBadgeTypeSearch={this._handleBadgeTypeSearch.bind(this)} />;
    }

    return(
      <div className='row'>
        <div className='col-lg-12'>
          <div className='ibox float-e-margins'>
            <div className='ibox-title'>
              <h5>KEPRAMUKAAN</h5>
              <div className='ibox-tools'>
                <a className='collapse-link'>
                  <i className='fa fa-chevron-up'></i>
                </a>
              </div>
            </div>
            {experienceSubForm}
            <button className='form-control btn btn-green bold-700 uppercase' type='submit' onClick={this._handleSubmit.bind(this)}>
              <i className='fa fa-save margin-right-xs'></i>Simpan Data Peserta
            </button>
          </div>
        </div>
      </div>

    );
  }
}
