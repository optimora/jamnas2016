import React from 'react';

import DialogHelper from '../../../lib/helpers/DialogHelper';

import FileUploadField from '../../../components/common/forms/FileUploadField';

export default class DocumentForm extends React.Component {
  constructor(...args) {
    super(...args);

    this._initialize();
  }

  static get propTypes() {
    return {
      documents: React.PropTypes.array.isRequired,

      onChange: React.PropTypes.func.isRequired,
      onSave: React.PropTypes.func.isRequired
    };
  }

  _handleFieldValueChange(index, field, value) {
    let documents = this.state.documents.slice(0);
    documents[index][field] = value;
    this.setState(
      {
        documents
      },
      () => {
        this.props.onChange(this.state.documents);
      }
    );
  }

  _handleSave() {
    this.props.onSave();
  }

  _handleFileUploadFailed() {
    DialogHelper.showErrorMessage('Gagal meng-upload data.', 'Periksa kembali file yang Anda upload. Hanya berkas gambar atau dokumen PDF dengan ukuran kurang dari atau sama dengan 1 MB yang diizinkan untuk di-upload.');
  }

  _initialize() {
    this.state = {
      documents: this.props.documents.slice(0)
    };
  }

  render() {
    return (
      <div className='row'>
        <div className='col-lg-12'>
          <div className='ibox float-e-margins'>
            <div className='ibox-title'>
              <h5>KELENGKAPAN DOKUMEN</h5>
              <div className='ibox-tools'>
                <a className='collapse-link'>
                  <i className='fa fa-chevron-up'></i>
                </a>
              </div>
            </div>
            <div className='ibox-content'>

              <div className='col-lg-12 margin-bottom-xxs'></div>

              <div className='col-lg-12 margin-bottom-xs'>
                <div className='col-lg-4 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Nama Dokumen</label>
                </div>
                <div className='col-lg-4 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Aksi</label>
                </div>
                <div className='col-lg-4 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Keterangan</label>
                </div>
              </div>

              {this.props.documents.map((scoutDocument, index) => {
                return (
                  <div key={scoutDocument.type} className='col-lg-12 margin-bottom-xs'>
                    <div className='col-lg-4 align-left'>
                      <p className='margin-top-xs'>{index + 1}. {scoutDocument.description}</p>
                    </div>
                    <div className='col-lg-4 align-center'>
                      <FileUploadField
                        name={scoutDocument.type}
                        topic='scout-documents'
                        label='Unggah Dokumen'
                        fieldValue={this.state.documents[index].stored_file}
                        onSuccess={(data) => this._handleFieldValueChange(index, 'stored_file', data === '' ? data : data.storedFile._id)}
                        onFail={this._handleFileUploadFailed.bind(this)}/>
                    </div>
                    <div className='col-lg-4 align-center'>
                      <input type='text' className='form-control' value={this.state.documents[index].notes} onChange={(event) => this._handleFieldValueChange(index, 'notes', event.target.value)} />
                    </div>
                  </div>
                );
              })}

              <div style={{ width: '100%', height: '1px', clear: 'both' }}></div>
            </div>
            <button className='form-control btn btn-green bold-700 uppercase' type='submit' onClick={this._handleSave.bind(this)}>
              <i className='fa fa-save margin-right-xs'></i>Simpan Data Peserta
            </button>
          </div>
        </div>
      </div>
    );
  }
}