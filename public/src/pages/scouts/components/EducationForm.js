import React from 'react';

import FormFieldWrapper from '../../../components/common/forms/FormFieldWrapper';

export default class EducationForm extends React.Component {
  constructor(props, context) {
    super(props, context);

    this._initialize();
  }
  
  static get propTypes() {
    return {
      data: React.PropTypes.array.isRequired,

      onChange: React.PropTypes.func.isRequired,
      onSave: React.PropTypes.func.isRequired
    };
  }

  _handleFieldValueChange(index, field, value) {
    let updatedRow = Object.assign({}, this.state.educations[index], {
      [field]: value
    });
    let educations = this.state.educations.slice(0);
    educations[index] = updatedRow;
    this.setState(
      {
        educations
      },
      () => {
        this.props.onChange(this.state.educations);
      }
    );
  }

  _initialize() {
    this.state = {
      educations: this.props.data,
      errors: {}
    };
  }

  componentWillReceiveProps(props) {
    this.setState({
      educations: props.data
    });
  }

  render() {
    return(
      <div className='row'>
        <div className='col-lg-12'>
          <div className='ibox float-e-margins'>
            <div className='ibox-title'>
              <h5>PENDIDIKAN FORMAL</h5>
              <div className='ibox-tools'>
                <a className='collapse-link'>
                  <i className='fa fa-chevron-up'></i>
                </a>
              </div>
            </div>
            <div className='ibox-content'>
              <div className='col-lg-12 margin-bottom-xs'>
                <div className='col-lg-1 no-margin no-padding align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Jenjang</label>
                </div>
                <div className='col-lg-3 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Nama Sekolah</label>
                </div>
                <div className='col-lg-3 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Kota</label>
                </div>
                <div className='col-lg-2 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Tahun Lulus</label>
                </div>
                <div className='col-lg-3 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Keterangan</label>
                </div>
              </div>

              {this.props.data.map((education, index) => {
                return (
                  <div key={index} className='col-lg-12 margin-bottom-xs'>
                    <div className='col-lg-1 no-margin no-padding align-center'>
                      <label className='control-label align-left margin-bottom-xxs margin-top-xs'>{education.level}</label>
                    </div>
                    <div className='col-lg-3'>
                      <FormFieldWrapper name='school_name' fieldValue={this.state.educations[index].school_name} onChange={(name, value) => this._handleFieldValueChange(index, name, value)}>
                        <input type='text' className='form-control align-center' />
                      </FormFieldWrapper>
                    </div>
                    <div className='col-lg-3'>
                      <FormFieldWrapper name='city' fieldValue={this.state.educations[index].city} onChange={(name, value) => this._handleFieldValueChange(index, name, value)}>
                        <input type='text' className='form-control align-center' />
                      </FormFieldWrapper>
                    </div>
                    <div className='col-lg-2'>
                      <FormFieldWrapper name='graduate_year' fieldValue={this.state.educations[index].graduate_year} onChange={(name, value) => this._handleFieldValueChange(index, name, value)}>
                        <input type='text' className='form-control align-center' />
                      </FormFieldWrapper>
                    </div>
                    <div className='col-lg-3'>
                      <FormFieldWrapper name='notes' fieldValue={this.state.educations[index].notes} onChange={(name, value) => this._handleFieldValueChange(index, name, value)}>
                        <input type='text' className='form-control align-center' />
                      </FormFieldWrapper>
                    </div>
                  </div>
                );
              })}

              <div style={{ width: '100%', height: '1px', clear: 'both' }}></div>
            </div>
            <button className='form-control btn btn-green bold-700 uppercase' type='submit' onClick={() => { this.props.onSave(); }}>
              <i className='fa fa-save margin-right-xs'></i>Simpan Data Peserta
            </button>
          </div>
        </div>
      </div>
    );
  }
}
