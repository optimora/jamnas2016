import React from 'react';

import FormFieldWrapper from '../../../components/common/forms/FormFieldWrapper';

export default class CourseForm extends React.Component {
  constructor(...args) {
    super(...args);

    this._initialize();
  }

  static get propTypes() {
    return {
      courses: React.PropTypes.array.isRequired,
      externalCourses: React.PropTypes.array.isRequired,

      onChange: React.PropTypes.func.isRequired,
      onSave: React.PropTypes.func.isRequired
    };
  }

  _emitFieldChange(fieldSegment, values) {
    this.props.onChange(fieldSegment, values);
  }

  _handleFieldValueChange(index, field, value) {
    const fieldSegments = field.split('.');
    let courses = this.state.courses.slice(0);
    courses[index][fieldSegments[1]] = value;
    this.setState(
      {
        courses
      },
      () => {
        this._emitFieldChange(fieldSegments[0], this.state.courses);
      }
    );
  }

  _handleInternalFieldValueChange(field, value) {
    console.log(field, value);
    this.setState(
      {
        _externalCourseItem: Object.assign({}, this.state._externalCourseItem, {
          [field]: value
        })
      }
    )
  }

  _handleSave() {
    this.props.onSave();
  }

  _addInternalValueEntry() {
    let value = Object.assign({}, this.state._externalCourseItem);
    this.setState({
      _externalCourseItem: {
        name: '',
        year: '',
        location: '',
        notes: ''
      }
    });
    this.setState(
      {
        external_courses: [
          ...this.state.external_courses,
          value
        ]
      },
      () => {
        this._emitFieldChange('external_courses', this.state.external_courses);
      }
    );
  }

  _removeInternalValueEntry(index) {
    this.setState(
      {
        external_courses: [
          ...this.state.external_courses.slice(0, index),
          ...this.state.external_courses.slice(index + 1)
        ]
      },
      () => {
        this._emitFieldChange('external_courses', this.state.external_courses);
      }
    );
  }

  _initialize() {
    let state = {
      courses: this.props.courses.slice(0),
      external_courses: this.props.externalCourses.slice(0),

      _externalCourseItem: {
        name      : '',
        year      : '',
        location  : '',
        notes     : ''
      }
    };

    this.state = state;
  }

  render() {
    return (
      <div className='row'>
        <div className='col-lg-12'>
          <div className='ibox float-e-margins'>
            <div className='ibox-title'>
              <h5>KURSUS & PELATIHAN</h5>
              <div className='ibox-tools'>
                <a className='collapse-link'>
                  <i className='fa fa-chevron-up'></i>
                </a>
              </div>
            </div>
            <div className='ibox-content'>

              <div className='col-lg-12 margin-bottom-xs'>
                <p>KURSUS/PELATIHAN KEPRAMUKAAN</p>
                <div className='col-lg-1 no-margin no-padding align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Nama</label>
                </div>
                <div className='col-lg-2 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Tahun</label>
                </div>
                <div className='col-lg-4 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Tempat</label>
                </div>
                <div className='col-lg-5 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Keterangan</label>
                </div>
              </div>

              {this.props.courses.map((course, index) => {
                return (
                  <div key={index} className='col-lg-12 margin-bottom-xxs'>
                    <div className='col-lg-1 no-margin no-padding align-center'>
                      <label className='control-label align-left margin-bottom-xxs margin-top-xs'>{course.name}</label>
                    </div>
                    <FormFieldWrapper name='courses.year' fieldValue={this.state.courses[index].year} onChange={(field, value) => this._handleFieldValueChange(index, field, value)} className='col-lg-2'>
                      <input type='text' className='form-control align-center' name='scout.courses.year' />
                    </FormFieldWrapper>
                    <FormFieldWrapper name='courses.location' fieldValue={this.state.courses[index].location} onChange={(field, value) => this._handleFieldValueChange(index, field, value)} className='col-lg-4'>
                      <input type='text' className='form-control align-center' name='scout.courses.location'></input>
                    </FormFieldWrapper>
                    <FormFieldWrapper name='courses.notes' fieldValue={this.state.courses[index].notes} onChange={(field, value) => this._handleFieldValueChange(index, field, value)} className='col-lg-5'>
                      <input type='text' className='form-control align-center' name='scout.courses.notes'></input>
                    </FormFieldWrapper>
                  </div>
                );
              })}

              <div className='col-lg-12'><div className='hr-line-dashed'></div></div>

              <div className='col-lg-12 margin-bottom-xs'>
                <p>KURSUS/PELATIHAN DI LUAR KEPRAMUKAAN</p>
                <div className='col-lg-3 no-margin no-padding align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Nama</label>
                </div>
                <div className='col-lg-2 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Tahun</label>
                </div>
                <div className='col-lg-3 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Tempat</label>
                </div>
                <div className='col-lg-3 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Keterangan</label>
                </div>
                <div className='col-lg-1'>
                  <label className='control-label align-left margin-bottom-xxs'>Aksi</label>
                </div>
              </div>

              {this.props.externalCourses.map((course, index) => {
                return (
                  <div key={index} className='col-lg-12 margin-bottom-xs'>
                    <div className='col-lg-3 no-margin no-padding align-center'>
                      <p>{course.name}</p>
                    </div>
                    <div className='col-lg-2 align-center'>
                      <p>{course.year}</p>
                    </div>
                    <div className='col-lg-3 align-center'>
                      <p>{course.location}</p>
                    </div>
                    <div className='col-lg-3 align-center'>
                      <p>{course.notes}</p>
                    </div>
                    <div className='col-lg-1'>
                      <button className='btn-xs btn-danger' onClick={() => this._removeInternalValueEntry(index)}>Hapus</button>
                    </div>
                  </div>
                );
              })}

              <div className='col-lg-12 margin-bottom-xxs'>
                <FormFieldWrapper name='name' fieldValue={this.state._externalCourseItem.name} onChange={this._handleInternalFieldValueChange.bind(this)} className='col-lg-3 no-margin no-padding align-center'>
                  <input type='text' className='form-control align-center' name='scout.external_courses.name' />
                </FormFieldWrapper>
                <FormFieldWrapper name='year' fieldValue={this.state._externalCourseItem.year} onChange={this._handleInternalFieldValueChange.bind(this)} className='col-lg-2'>
                  <input type='text' className='form-control align-center' name='scout.external_courses.year' />
                </FormFieldWrapper>
                <FormFieldWrapper name='location' fieldValue={this.state._externalCourseItem.location} onChange={this._handleInternalFieldValueChange.bind(this)} className='col-lg-3'>
                  <input type='text' className='form-control align-center' name='scout.external_courses.location' />
                </FormFieldWrapper>
                <FormFieldWrapper name='notes' fieldValue={this.state._externalCourseItem.notes} onChange={this._handleInternalFieldValueChange.bind(this)} className='col-lg-3'>
                  <input type='text' className='form-control align-center' name='scout.external_courses.notes' />
                </FormFieldWrapper>
              </div>

              <div className='col-lg-12'>
                <button className='btn btn-success pull-right' onClick={this._addInternalValueEntry.bind(this)}>Tambahkan Kursus/Pelatihan di Luar Kepramukaan</button>
              </div>

              <div style={{ width: '100%', height: '1px', clear: 'both' }}></div>
            </div>
            <button className='form-control btn btn-green bold-700 uppercase' type='submit' onClick={this._handleSave.bind(this)}>
              <i className='fa fa-save margin-right-xs'></i>Simpan Data Peserta
            </button>
          </div>
        </div>
      </div>
    );
  }
}