import React from 'react';

import FormFieldWrapper from '../../../components/common/forms/FormFieldWrapper';

export default class SeniorExperienceForm extends React.Component {
  constructor(...args) {
    super(...args);

    this._initialize();
  }

  static get propTypes() {
    return {
      experiences: React.PropTypes.array.isRequired,

      onChange: React.PropTypes.func.isRequired
    };
  }

  _handleFieldValueChange(index, fieldName, value) {
    let experiences = this.state.experiences.slice(0);
    experiences[index][fieldName] = value;
    this.setState(
      {
        experiences: experiences
      },
      () => {
        this.props.onChange(this.state.experiences);
      }
    );
  }

  _initialize() {
    this.state = {
      experiences: this.props.experiences.slice(0),
      errors: {}
    };
  }

  _printCaptionByEnum(raw) {
    return raw[0].toUpperCase() + raw.split('').slice(1).join('');
  }

  componentWillReceiveProps(props) {
    this.setState({
      experiences: props.experiences
    });
  }

  render() {
    return (
      <div className='ibox-content'>

        <div className='col-lg-12 margin-bottom-xs'>
          <div className='col-lg-2 no-margin no-padding align-center'>
            <label className='control-label align-left margin-bottom-xxs'>Nama Golongan</label>
          </div>
          <div className='col-lg-2 align-center'>
            <label className='control-label align-left margin-bottom-xxs'>Tahun</label>
          </div>
          <div className='col-lg-3 align-center'>
            <label className='control-label align-left margin-bottom-xxs'>Nomor Gudep</label>
          </div>
          <div className='col-lg-4 align-center'>
            <label className='control-label align-left margin-bottom-xxs'>Pangkalan</label>
          </div>
        </div>

        {this.props.experiences.map((experience, index) => {
          return (
            <div key={index} className='col-lg-12 margin-bottom-xxs'>
              <div className='col-lg-2 no-margin no-padding align-center'>
                <p className='control-label margin-bottom-xxs margin-top-xs align-center'>{this._printCaptionByEnum(experience.level)}</p>
              </div>
              <FormFieldWrapper key={'year'} name='year' fieldValue={this.state.experiences[index].year} onChange={(field, value) => this._handleFieldValueChange(index, field, value)} className='col-lg-2'>
                <input type='text' className='form-control align-center' />
              </FormFieldWrapper>
              <FormFieldWrapper key={'gudep_number'} name='gudep_number' fieldValue={this.state.experiences[index].gudep_number} onChange={(field, value) => this._handleFieldValueChange(index, field, value)} className='col-lg-3'>
                <input type='text' className='form-control align-center' />
              </FormFieldWrapper>
              <FormFieldWrapper key={'base'} name='base' fieldValue={this.state.experiences[index].base} onChange={(field, value) => this._handleFieldValueChange(index, field, value)} className='col-lg-4'>
                <input type='text' className='form-control align-center' />
              </FormFieldWrapper>
            </div>
          );
        })}

        <div style={{ width: '100%', height: '1px', clear: 'both' }}></div>
      </div>
    );
  }
}
