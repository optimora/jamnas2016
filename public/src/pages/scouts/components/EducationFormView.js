import React from 'react';

export default class EducationFormView extends React.Component {
  static get propTypes() {
    return {
      educations: React.PropTypes.array.isRequired
    };
  }
  
  render() {
    var items = [];

    if(this.props.items && this.props.items.length > 0 ) {
      for (var i = 0; i < this.props.items.length; i++) {
        items.push(
          <div className='col-lg-12 margin-top-xxs'>
            <div className='col-lg-1 no-margin no-padding align-center'>
              <label className='control-label align-left margin-bottom-xxs margin-top-xs'>{this.props.items[i].level}</label>
            </div>
            <div className='col-lg-3'>
              <p className="control-label align-center margin-bottom-xxs margin-top-xs">{this.props.items[i].school_name}</p>
            </div>
            <div className='col-lg-3'>
              <p className="control-label align-center margin-bottom-xxs margin-top-xs">{this.props.items[i].province}</p>
            </div>
            <div className='col-lg-2'>
              <p className="control-label align-center margin-bottom-xxs margin-top-xs">{this.props.items[i].graduate_year}</p>
            </div>
            <div className='col-lg-3'>
              <p className="control-label align-center margin-bottom-xxs margin-top-xs">{this.props.items[i].notes}</p>
            </div>
          </div>
        );
      }
    }


    return(
      <div className='ibox float-e-margins'>
        <div className='ibox-title'>
          <h5>Data Pendidikan Formal</h5>
          <div className='ibox-tools'>
            <a className='collapse-link'>
              <i className='fa fa-chevron-up'></i>
            </a>
          </div>
        </div>
        <div className='ibox-content'>

          <div className='col-lg-12 margin-bottom-xs'>
            <div className='col-lg-1 no-margin no-padding align-center'>
              <label className='control-label align-left margin-bottom-xxs'>Jenjang</label>
            </div>
            <div className='col-lg-3 align-center'>
              <label className='control-label align-left margin-bottom-xxs'>Nama Sekolah</label>
            </div>
            <div className='col-lg-3 align-center'>
              <label className='control-label align-left margin-bottom-xxs'>Kota</label>
            </div>
            <div className='col-lg-2 align-center'>
              <label className='control-label align-left margin-bottom-xxs'>Tahun Lulus</label>
            </div>
            <div className='col-lg-3 align-center'>
              <label className='control-label align-left margin-bottom-xxs'>Keterangan</label>
            </div>
          </div>
          
          {this.props.educations.map((education, index) => {
            return <div key={index} className='col-lg-12 margin-top-xxs'>
              <div className='col-lg-1 no-margin no-padding align-center'>
                <label className='control-label align-left margin-bottom-xxs margin-top-xs'>{education.level}</label>
              </div>
              <div className='col-lg-3'>
                <p className="control-label align-center margin-bottom-xxs margin-top-xs">{education.school_name}</p>
              </div>
              <div className='col-lg-3'>
                <p className="control-label align-center margin-bottom-xxs margin-top-xs">{education.city}</p>
              </div>
              <div className='col-lg-2'>
                <p className="control-label align-center margin-bottom-xxs margin-top-xs">{education.graduate_year}</p>
              </div>
              <div className='col-lg-3'>
                <p className="control-label align-center margin-bottom-xxs margin-top-xs">{education.notes}</p>
              </div>
            </div>;
          })}

          <div style={{ width: '100%', height: '1px', clear: 'both' }}></div>
        </div>
      </div>
    );
  }
}
