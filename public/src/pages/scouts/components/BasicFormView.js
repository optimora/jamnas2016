import moment from 'moment';
import React from 'react';

export default class BasicFormView extends React.Component {
  static get propTypes() {
    return {
      scout: React.PropTypes.object.isRequired,
      branch: React.PropTypes.object.isRequired,
      subbranch: React.PropTypes.object.isRequired
    }
  }

  _mapFieldEnumToString(field, value) {
    const fieldEnumToStringMap = {
      sex: {
        male: 'Laki-laki',
        female: 'Perempuan'
      },
      subtype: {
        'leader-head-cmt': 'Ketua Pinkonda',
        'leader-vice-cmt': 'Wakil Pinkonda',
        'leader-secretary-cmt': 'Sekretaris Pinkonda',
        'leader-staff-cmt': 'Anggota Pinkonda',
        'leader-medical-cmt': 'Tim Dokter',
        'leader-exhibition-cmt': 'Tim Pameran',
        'leader-cultural-cmt': 'Tim Kegiatan Budaya',
        'leader-head-dct': 'Ketua Pinkoncab',
        'leader-secretary-dct': 'Wakil Pinkoncab',
        'leader-staff-dct': 'Anggota Pinkoncab'
      },
      participant_role: {
        'leader': 'Pemimpin Regu',
        'co-leader': 'Wakil Pemimpin Regu',
        'member': 'Anggota'
      },
      type: {
        'participant': 'Peserta Jamnas X 2016',
        'leader-cmt': 'Pimpinan Kontingen Daerah',
        'leader-staff': 'Staff Kontingen Daerah',
        'coach': 'Pembina Pendamping',
        'leader-dct': 'Pimpinan Kontingen Cabang',
      }
    };

    return (fieldEnumToStringMap[field] || {})[value] || '-';
  }

  render() {
    return(
      <div className='ibox float-e-margins'>
        <div className='ibox-title'>
          <h5>Data Pribadi</h5>
        </div>
        <div className='ibox-content'>
          <form method='get' className='form-horizontal'>

            <div className='col-lg-12'>
              <div className='col-lg-3'>
                <center className='bg-dark-gray padding-top-bottom-xs'>
                  <img src={this.props.scout.avatar && this.props.scout.avatar != '' ? '/stored_files/' + this.props.scout.avatar : 'http://placehold.it/150x200'} style={{ width: '200px' }} />
                </center>
              </div>
              <div className='col-lg-9'>

                <div className='col-lg-12 no-margin no-padding'>
                  <div className='col-lg-5'>
                    <div className='form-group'>
                      <label className='control-label align-left margin-bottom-xxs'>Kwartir Daerah</label>
                      <p>{this.props.branch.name}</p>
                    </div>
                  </div>
                  <div className='col-lg-6 col-lg-offset-1'>
                    <div className='form-group'>
                      <label className='control-label align-left margin-bottom-xxs'>Kwartir Cabang</label>
                      <p>{this.props.subbranch.name}</p>
                    </div>
                  </div>
                </div>

                <div className='col-lg-12 no-margin no-padding'><div className='hr-line-dashed no-margin'></div></div>

                <div className='col-lg-12 no-margin no-padding'>
                  <div className="col-lg-6">
                    <div className='form-group padding-right-md'>
                      <label className='control-label align-left margin-bottom-xxs'>Keikutsertaan dalam JAMNAS</label>
                      <p>{this._mapFieldEnumToString('type', this.props.scout.type)}</p>
                    </div>
                  </div>
                </div>

                <div className='col-lg-12 no-margin no-padding'>
                  <div className='col-lg-6'>
                    <div className='form-group padding-right-md'>
                      <label className='control-label align-left margin-bottom-xxs'>Jabatan Dalam Kepanitiaan</label>
                      <p>{this._mapFieldEnumToString('subtype', this.props.scout.subtype)}</p>
                    </div>
                  </div>
                </div>
                <div className='col-lg-12 no-margin no-padding'>
                  <div className='col-lg-6'>
                    <div className='form-group padding-right-md'>
                      <label className='control-label align-left margin-bottom-xxs'>Regu</label>
                      <p>{this.props.scout.group ? this.props.scout.group.name : ' - '}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className='col-lg-12 margin-top-md'><div className='hr-line-dashed no-margin'></div></div>

            <div className='col-lg-12'>
              <div className='col-lg-12 margin-top-xs no-padding'>
                <div className='col-lg-3'>
                  <div className='form-group padding-right-md'>
                    <label className='control-label align-left margin-bottom-xxs'>Nama Lengkap</label>
                    <p>{this.props.scout.name}</p>
                  </div>
                </div>
                <div className='col-lg-3'>
                  <div className='form-group padding-right-md'>
                    <label className='control-label align-left margin-bottom-xxs'>NTA</label>
                    <p>{this.props.scout.nta}</p>
                  </div>
                </div>
                <div className='col-lg-3'>
                  <div className='form-group'>
                    <label className='control-label align-left margin-bottom-xxs'>Jenis Kelamin</label>
                    <p>{this._mapFieldEnumToString('sex', this.props.scout.sex)}</p>
                  </div>
                </div>
              </div>

              <div className='col-lg-12 no-padding'>
                <div className='col-lg-3'>
                  <div className='form-group padding-right-md'>
                    <label className='control-label align-left margin-bottom-xxs'>Tempat Lahir</label>
                    <p>{this.props.scout.birthplace}</p>
                  </div>
                </div>
                <div className='col-lg-3'>
                  <div className='form-group padding-right-md' id='data_3'>
                    <label className='control-label align-left margin-bottom-xxs'>Tanggal Lahir</label>
                    <p>{moment(this.props.scout.birthday).format('DD-MM-YYYY')}</p>
                  </div>
                </div>
                <div className='col-lg-3'>
                  <div className='form-group padding-right-md'>
                    <label className='control-label align-left margin-bottom-xxs'>Golongan Darah</label>
                    <p>{this.props.scout.blood_type}</p>
                  </div>
                </div>
                <div className='col-lg-2'>
                  <div className='form-group'>
                    <label className='control-label align-left margin-bottom-xxs'>Agama</label>
                    <p>{this.props.scout.religion}</p>
                  </div>
                </div>
              </div>
            </div>
          </form>
          <div style={{ width: '100%', height: '1px', clear: 'both' }}></div>
        </div>
      </div>
    );
  }
}