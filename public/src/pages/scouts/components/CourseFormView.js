import React from 'react';

export default class CourseFormView extends React.Component {
  static get propTypes() {
    return {
      courses: React.PropTypes.array.isRequired
    };
  }

  render() {
    return (
      <div className='ibox float-e-margins'>
        <div className='ibox-title'>
          <h5>{this.props.title}</h5>
          <div className='ibox-tools'>
            <a className='collapse-link'>
              <i className='fa fa-chevron-up'></i>
            </a>
          </div>
        </div>
        <div className='ibox-content'>
          <div className='col-lg-12 margin-bottom-xs'>
            <div className='col-lg-2 no-margin no-padding align-center'>
              <label className='control-label align-left margin-bottom-xxs'>Nama</label>
            </div>
            <div className='col-lg-2 align-center'>
              <label className='control-label align-left margin-bottom-xxs'>Tahun</label>
            </div>
            <div className='col-lg-4 align-center'>
              <label className='control-label align-left margin-bottom-xxs'>Tempat</label>
            </div>
            <div className='col-lg-4 align-center'>
              <label className='control-label align-left margin-bottom-xxs'>Keterangan</label>
            </div>
          </div>
          {this.props.courses.map((course, index) => {
            return (
              <div key={index} className='col-lg-12 margin-bottom-xxs'>
                <div className='col-lg-2 no-margin no-padding align-center'>
                  <p className="control-label align-center margin-bottom-xxs margin-top-xs"><strong>{course.name}</strong></p>
                </div>
                <div className='col-lg-2'>
                  <p className="control-label align-center margin-bottom-xxs margin-top-xs">{course.year}</p>
                </div>
                <div className='col-lg-4'>
                  <p className="control-label align-center margin-bottom-xxs margin-top-xs">{course.city}</p>
                </div>
                <div className='col-lg-4'>
                  <p className="control-label align-center margin-bottom-xxs margin-top-xs">{course.notes}</p>
                </div>
              </div>
            );
          })}
          <div style={{ width: '100%', height: '1px', clear: 'both' }}></div>
        </div>
      </div>
    );
  }
};
