import ObjectPath from 'object-path-immutable';
import React from 'react';

import FormFieldWrapper from '../../../components/common/forms/FormFieldWrapper';

export default class ContactForm extends React.Component {
  constructor(...args) {
    super(...args);

    this.state = Object.assign({}, this.props.data);
  }

  static get propTypes() {
    return {
      caption: React.PropTypes.string.isRequired,
      data: React.PropTypes.object.isRequired,
      locations: React.PropTypes.array.isRequired,
      subLocations: React.PropTypes.array.isRequired,
      subLocationsFieldId: React.PropTypes.string.isRequired,
      branch: React.PropTypes.object.isRequired,
      subbranch: React.PropTypes.object.isRequired,
      mode: React.PropTypes.oneOf(['simple', 'extended']),

      onChange: React.PropTypes.func.isRequired,
      onProvinceChange: React.PropTypes.func.isRequired,
      onSave: React.PropTypes.func.isRequired
    };
  }

  _handleFieldValueChange(fieldName, value) {
    const fieldSegments = fieldName.split('.');
    let updatedState = Object.assign({}, this.state);
    updatedState = ObjectPath.set(updatedState, fieldSegments, value);
    this.setState(updatedState, () => {
      this.props.onChange(Object.assign({}, this.state));
      if (fieldName === 'province') {
        this.props.onProvinceChange(this.state.province, this.props.subLocationsFieldId);
      }

      console.log(fieldName, value, this.state);
    });
  }

  _handleSave() {
    this.props.onSave();
  }

  render() {
    let nameAndOccupationField = null;
    if (this.props.mode === 'extended') {
      nameAndOccupationField = [
        <div key='nameField' className='col-lg-12 margin-bottom-xs'>
          <div className='col-lg-3 no-margin no-padding'>
            <label className='control-label align-left margin-bottom-xxs'>Nama Orang Tua</label>
          </div>
          <div className='col-lg-6'>
            <FormFieldWrapper name='name' fieldValue={this.state.name} onChange={this._handleFieldValueChange.bind(this)}>
              <input type='text' className='form-control' name='contact.name' />
            </FormFieldWrapper>
          </div>
        </div>,

        <div key='occupationField' className='col-lg-12 margin-bottom-xs'>
          <div className='col-lg-3 no-margin no-padding'>
            <label className='control-label align-left margin-bottom-xxs'>Pekerjaan</label>
          </div>
          <div className='col-lg-6'>
            <FormFieldWrapper name='occupation' fieldValue={this.state.occupation} onChange={this._handleFieldValueChange.bind(this)}>
              <input type='text' className='form-control' name='contact.occupation' />
            </FormFieldWrapper>
          </div>
        </div>
      ];
    }
    else {
      nameAndOccupationField = [];
    }

    return (
      <div className='row'>
        <div className='col-lg-12'>
          <div className='ibox float-e-margins'>
            <div className='ibox-title'>
              <h5>{this.props.caption}</h5>
              <div className='ibox-tools'>
                <a className='collapse-link'>
                  <i className='fa fa-chevron-up'></i>
                </a>
              </div>
            </div>
            <div className='ibox-content'>
              {nameAndOccupationField.map((field) => field)}
              <div className='col-lg-12'>
                <div className='col-lg-3 no-margin no-padding'>
                  <label className='control-label align-left margin-bottom-xxs'>Alamat Rumah</label>
                </div>
                <div className='col-lg-9'>
                  <FormFieldWrapper name='address' fieldValue={this.state.address}  onChange={this._handleFieldValueChange.bind(this)}>
                    <textarea className='form-control margin-bottom-xxs' rows='3'></textarea>
                  </FormFieldWrapper>
                  <FormFieldWrapper name='province' fieldValue={this.state.province} onChange={this._handleFieldValueChange.bind(this)} className='col-lg-5 no-padding-left margin-top-xs'>
                    <label className='control-label align-left margin-bottom-xxs'>Provinsi</label>
                    <select className='form-control m-b no-margin' name='contact.province'>
                      <option values=''>- Pilih -</option>
                      {this.props.locations.map((location) => {
                        return <option key={location._id} value={location._id}>{location.name}</option>;
                      })}
                    </select>
                  </FormFieldWrapper>
                  <FormFieldWrapper name='regency_city' fieldValue={this.state.regency_city} onChange={this._handleFieldValueChange.bind(this)} className='col-lg-5 no-padding-right margin-top-xs'>
                    <label className='control-label align-left margin-bottom-xxs'>Kota / Kabupaten</label>
                    <select className='form-control m-b no-margin' name='contact.regency_city'>
                      <option values=''>- Pilih -</option>
                      {this.props.subLocations.map((location) => {
                        return <option key={location._id} value={location._id}>{location.name}</option>;
                      })}
                    </select>
                  </FormFieldWrapper>
                  <FormFieldWrapper name='zipcode' fieldValue={this.state.zipcode} onChange={this._handleFieldValueChange.bind(this)} className='col-lg-2 no-padding-right margin-top-xs'>
                    <label className='control-label align-left margin-bottom-xxs'>Kodepos</label>
                    <input type='text' className='form-control m-b no-margin' name='contact.zipcode' />
                  </FormFieldWrapper>
                </div>
              </div>

              <div className='col-lg-12'><div className='hr-line-dashed'></div></div>

              <div className='col-lg-12'>
                <div className='col-lg-3 no-margin no-padding'>
                  <label className='control-label align-left margin-bottom-xxs'>Kontak</label>
                </div>
                <div className='col-lg-9'>
                  <div className='col-lg-6 no-padding-left'>
                    <label className='control-label align-left margin-bottom-xxs'>No Telp. Rumah</label>
                    <FormFieldWrapper name='phone.home' fieldValue={this.state.phone.home} onChange={this._handleFieldValueChange.bind(this)} className="input-group m-b">
                      <span className="input-group-addon"><i className='glyphicon glyphicon-phone-alt'></i></span>
                      <input type='text' className='form-control m-b no-margin' />
                    </FormFieldWrapper>
                  </div>
                  <div className='col-lg-6 no-padding-right'>
                    <label className='control-label align-left margin-bottom-xxs'>No. Handphone</label>
                    <FormFieldWrapper name='phone.mobile' fieldValue={this.state.phone.mobile} onChange={this._handleFieldValueChange.bind(this)} className="input-group m-b">
                      <span className="input-group-addon"><i className='glyphicon glyphicon-phone'></i></span>
                      <input type='text' className='form-control m-b no-margin' />
                    </FormFieldWrapper>
                  </div>
                </div>
              </div>

              <div style={{ width: '100%', height: '1px', clear: 'both' }}></div>
            </div>
            <button className='form-control btn btn-green bold-700 uppercase' onClick={() => this._handleSave()}>
              <i className='fa fa-save margin-right-xs'></i>Simpan Data Peserta
            </button>
          </div>
        </div>
      </div>
    );
  }
}