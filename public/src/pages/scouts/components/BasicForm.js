import React from 'react';

import FileUploadField from '../../../components/common/forms/FileUploadField';
import FormFieldWrapper from '../../../components/common/forms/FormFieldWrapper';
import SimpleDatePickerField from '../../../components/common/forms/SimpleDatePickerField';

export default class BasicForm extends React.Component {
  constructor(props, context) {
    super(props, context);

    this._initialize();
  }

  static get propTypes() {
    return {
      groups: React.PropTypes.array.isRequired,
      data: React.PropTypes.object.isRequired,
      errors: React.PropTypes.object,

      onChange: React.PropTypes.func.isRequired,
      onSave: React.PropTypes.func.isRequired,
      onAddNewGroup: React.PropTypes.func.isRequired,

      // Optional Props
      branch: React.PropTypes.object,
      branches: React.PropTypes.array,
      committeeRoles: React.PropTypes.array,
      committeeRoleDetails: React.PropTypes.array,
      committeeRoleExtras: React.PropTypes.array,
      subbranch: React.PropTypes.object,
      subbranches: React.PropTypes.array,
      lockScoutTypeChanges: React.PropTypes.bool
    };
  }

  _generateData() {
    let data = Object.assign({}, this.state);
    Object.keys(data).forEach((key) => {
      if (key[0] === '_') {
        delete data[key];
      }
    });
    return data;
  }

  _handleFieldValueChange(fieldName, value) {
    var updatedValue = {
      [fieldName]: value,
      _isPristine: false
    };
    this.setState(updatedValue, () => {
      this.props.onChange(fieldName, value);
    });
  }

  _handleFileUploadFailed() {
    alert('Gagal meng-upload data. Periksa kembali file yang Anda upload. Hanya berkas gambar dengan ukuran kurang dari atau sama dengan 1 MB yang diizinkan untuk di-upload.')
  }

  _handleAddNewGroup() {
    this.props.onAddNewGroup(this.state._groupName);
  }

  _handleSave() {
    this.props.onSave('basic', this._generateData());
  }

  _initialize() {
    const initialData = {
      name: '',
      document_status: 'draft',
      sex: '',
      birthplace: '',
      birthday: new Date(1900, 0, 1, 0, 0, 0),
      religion: '',
      blood_type: '',
      avatar: '',
      nta: '',
      type: '',
      subtype: '',
      participant_role: '',
      group: '',
      branch: '',
      subbranch: '',
      committee_role: '',
      committee_role_detail: '',
      committee_role_extra: '',

      _groupName: '',
      _isPristine: true
    };

    this.state = Object.assign({}, initialData, this.props.data);
  }

  _isValidValue(fieldName) {
    return this.props.errors && this.props.errors[fieldName] == undefined;
  }

  _getValidationMessage(fieldName) {
    if (this.props.errors) {
      if (this._isValidValue(fieldName)) {
        return null;
      }
      return this.props.errors[fieldName];
    }
    return null;
  }

  componentWillReceiveProps(props) {
    this.setState({
      data: Object.assign({}, props.data)
    });
  }

  render() {
    let scoutSubTypeSelectField;
    let scoutSubTypeSelectFieldClassName = 'col-lg-6 no-margin padding-right-md no-padding-left no-padding-right';
    switch (this.state.type) {
      case 'leader-cmt':
        scoutSubTypeSelectField = <FormFieldWrapper name='subtype' fieldValue={this.state.subtype} className={scoutSubTypeSelectFieldClassName} onChange={this._handleFieldValueChange.bind(this)} isValid={this._isValidValue('subtype')} validationMessage={this._getValidationMessage('subtype')}>
          <label className='control-label align-left margin-bottom-xxs'>Jabatan:</label>
          <select className='form-control m-b no-margin'>
            <option value=''>- Pilih -</option>
            <option value='leader-head-cmt'>Ketua Pinkonda</option>
            <option value='leader-vice-cmt'>Wakil Pinkonda</option>
            <option value='leader-secretary-cmt'>Sekretaris Pinkonda</option>
            <option value='leader-staff-cmt'>Anggota Pinkonda</option>
          </select>
        </FormFieldWrapper>;
        break;
      case 'leader-staff':
        scoutSubTypeSelectField = <FormFieldWrapper name='subtype' fieldValue={this.state.subtype} className={scoutSubTypeSelectFieldClassName} onChange={this._handleFieldValueChange.bind(this)} isValid={this._isValidValue('subtype')} validationMessage={this._getValidationMessage('subtype')}>
          <label className='control-label align-left margin-bottom-xxs'>Jabatan:</label>
          <select className='form-control m-b no-margin'>
            <option value=''>- Pilih -</option>
            <option value='leader-medical-cmt'>Tim Dokter</option>
            <option value='leader-exhibition-cmt'>Tim Pameran</option>
            <option value='leader-cultural-cmt'>Tim Kegiatan Budaya</option>
          </select>
        </FormFieldWrapper>;
        break;
      case 'leader-dct':
        scoutSubTypeSelectField = <FormFieldWrapper name='subtype' fieldValue={this.state.subtype} className={scoutSubTypeSelectFieldClassName} onChange={this._handleFieldValueChange.bind(this)} isValid={this._isValidValue('subtype')} validationMessage={this._getValidationMessage('subtype')}>
          <label className='control-label align-left margin-bottom-xxs'>Jabatan:</label>
          <select className='form-control m-b no-margin'>
            <option value=''>- Pilih -</option>
            <option value='leader-head-dct'>Ketua Pinkoncab</option>
            <option value='leader-secretary-dct'>Wakil Pinkoncab</option>
            <option value='leader-staff-dct'>Anggota Pinkoncab</option>
          </select>
        </FormFieldWrapper>;
        break;
      case 'participant':
        scoutSubTypeSelectField = <FormFieldWrapper name='participant_role' fieldValue={this.state.participant_role} className={scoutSubTypeSelectFieldClassName} onChange={this._handleFieldValueChange.bind(this)} isValid={this._isValidValue('participant_role')} validationMessage={this._getValidationMessage('participant_role')}>
          <label className='control-label align-left margin-bottom-xxs'>Jabatan:</label>
          <select className='form-control m-b no-margin'>
            <option value=''>- Pilih -</option>
            <option value='leader'>Pemimpin Regu</option>
            <option value='co-leader'>Wakil Pemimpin Regu</option>
            <option value='member'>Anggota</option>
          </select>
        </FormFieldWrapper>;
        break;
      default:
        scoutSubTypeSelectField = <FormFieldWrapper name='subtype' fieldValue={this.state.subtype} className={scoutSubTypeSelectFieldClassName} onChange={this._handleFieldValueChange.bind(this)} isValid={this._isValidValue('subtype')} validationMessage={this._getValidationMessage('subtype')}>
          <label className='control-label align-left margin-bottom-xxs'>Jabatan:</label>
          <select className='form-control m-b no-margin'>
            <option value=''>- Pilih -</option>
          </select>
        </FormFieldWrapper>;
    }

    let scoutGroupSelectField;
    if (this.state.type === 'participant') {
      scoutGroupSelectField = <div className='col-lg-12 no-margin no-padding'>
        <div className='col-lg-4'>
          <FormFieldWrapper name='group' fieldValue={this.state.group} onChange={this._handleFieldValueChange.bind(this)} isValid={this._isValidValue('group')} validationMessage={this._getValidationMessage('group')}>
            <label className='control-label align-left margin-bottom-xxs'>Regu</label>
            <select className='form-control m-b no-margin'>
              <option value=''>- Pilih Regu -</option>
              {this.props.groups.map((group) => {
                return <option key={group._id} value={group._id}>{group.name}</option>;
              })}
            </select>
          </FormFieldWrapper>
        </div>

        <div className='col-lg-3 padding-top-mlg'>
          <a data-toggle='modal' className='fg-red' href='#modal-form'>
            <i className='fa fa-plus'></i> Tambah Regu
          </a>
          <div id="modal-form" className="modal fade" aria-hidden="true">
            <div className="modal-dialog">
              <div className="modal-content">
                <div className="modal-body">
                  <div className="row">
                    <div className="col-sm-12 align-center">
                      <h3 className="m-t-none m-b">Tambah Nama Regu</h3>
                      <p>Masukkan nama regu baru yang Anda inginkan</p>
                      <div className='col-lg-6 col-lg-offset-3'>
                        <input type='text' className='form-control align-center' value={this.state._groupName} onChange={(event) => this._handleFieldValueChange('_groupName', event.target.value)} />
                        <button className='btn bg-jamnas-red margin-top-xs' onClick={() => this._handleAddNewGroup()}><i className='fa fa-save'></i> Simpan</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>;
    }
    else {
      scoutGroupSelectField = null;
    }

    let avatarImage;
    if (this.props.data.avatar && this.props.data.avatar !== '') {
      avatarImage = <img src={'/stored_files/' + this.props.data.avatar} width='150' />
    }
    else {
      avatarImage = <img src='http://placehold.it/150x200' />;
    }

    const disableFieldIfNotCommittee = () => this.props.data.type !== 'committee';
    let scoutBranchesSelectField;
    if (this.props.branch && typeof this.props.branch === 'object') {
      scoutBranchesSelectField = <select className='form-control m-b no-margin' value='' name='scout.branch' disabled={this.props.data.type !== 'committee'}>
        <option value={this.props.branch._id}>{this.props.branch.name}</option>
      </select>;
    }
    else {
      scoutBranchesSelectField = <FormFieldWrapper className='m-b no-margin' name='branch' fieldValue={this.state.branch} onChange={(name, value) => this._handleFieldValueChange(name, value)} disableField={disableFieldIfNotCommittee()} branches={this.props.branches} isValid={this._isValidValue('branch')} validationMessage={this._getValidationMessage('branch')}>
        <select className='form-control' disabled={this.props.disableField}>
          <option value=''>- Pilih -</option>
          {this.props.branches.map(branch => <option key={branch._id} value={branch._id}>{branch.name}</option>)}
        </select>
      </FormFieldWrapper>;
    }

    let scoutSubbranchesSelectField;
    if (this.props.subbranch && typeof this.props.subbranch === 'object') {
      scoutSubbranchesSelectField = <select className='form-control m-b no-margin' value='' name='scout.subbranch' disabled={this.props.data.type !== 'committee'}>
        <option value={this.props.subbranch._id}>{this.props.subbranch.name}</option>
      </select>;
    }
    else {
      scoutSubbranchesSelectField = <FormFieldWrapper className='m-b no-margin' name='subbranch' fieldValue={this.state.subbranch} onChange={(name, value) => this._handleFieldValueChange(name, value)} disableField={disableFieldIfNotCommittee()} subbranches={this.props.subbranches} isValid={this._isValidValue('subbranch')} validationMessage={this._getValidationMessage('subbranch')}>
        <select className='form-control' disabled={this.props.disableField}>
          <option value=''>- Pilih -</option>
          {this.props.subbranches.map(branch => <option key={branch._id} value={branch._id}>{branch.name}</option>)}
        </select>
      </FormFieldWrapper>;
    }

    let scoutCommitteeRoleSelectField = <div></div>;
    let scoutCommitteeRoleDetailSelectField = <div></div>;
    let scoutCommitteeRoleDetailExtraSelectField = <div></div>;
    if (this.props.data.type === 'committee') {
      scoutCommitteeRoleSelectField = <FormFieldWrapper className='m-b no-margin' name='committee_role' fieldValue={this.state.committee_role} onChange={(name, value) => this._handleFieldValueChange(name, value)} isValid={this._isValidValue('committee_role')} validationMessage={this._getValidationMessage('committee_role')}>
        <label className='control-label align-left margin-bottom-xxs'>Jabatan Kepanitiaan</label>
        <select className='form-control' disabled={this.props.disableField}>
          <option value=''>- Pilih -</option>
          {this.props.committeeRoles.map(role => <option key={role.value} value={role.value}>{role.name}</option>)}
        </select>
      </FormFieldWrapper>;

      scoutCommitteeRoleDetailSelectField = <FormFieldWrapper className='m-b no-margin' name='committee_role_detail' fieldValue={this.state.committee_role_detail} onChange={(name, value) => this._handleFieldValueChange(name, value)} isValid={this._isValidValue('committee_role_detail')} validationMessage={this._getValidationMessage('committee_role_detail')}>
        <label className='control-label align-left margin-bottom-xxs'>Jabatan Kepanitiaan Rinci</label>
        <select className='form-control' disabled={this.props.disableField}>
          <option value=''>- Pilih -</option>
          {this.props.committeeRoleDetails.map(role => <option key={role.value} value={role.value}>{role.name}</option>)}
        </select>
      </FormFieldWrapper>;

      scoutCommitteeRoleDetailExtraSelectField = <FormFieldWrapper className='m-b no-margin' name='committee_role_extra' fieldValue={this.state.committee_role_extra} onChange={(name, value) => this._handleFieldValueChange(name, value)} isValid={this._isValidValue('committee_role_extra')} validationMessage={this._getValidationMessage('committee_role_extra')}>
        <label className='control-label align-left margin-bottom-xxs'>Detail Kepanitiaan</label>
        <select className='form-control' disabled={this.props.disableField}>
          <option value=''>- Pilih -</option>
          {this.props.committeeRoleExtras.map(role => <option key={role.value} value={role.value}>{role.name}</option>)}
        </select>
      </FormFieldWrapper>;
    }

    // What an ugly way. -_-
    const hideIfTrue = (condition) => ( condition ? { display: 'none' } : { display: 'block' });

    return(
      <div className='row'>
        <div className='col-lg-12'>
          <div className='ibox float-e-margins'>
            <div className='ibox-title'>
              <div className='col-lg-12'>
                <h3 className='align-center'>BIODATA PESERTA JAMBORE NASIONAL X 2016</h3>
              </div>
            </div>
            <div className='ibox-content'>
              <div className='form-horizontal'>
                <div className='col-lg-12'>
                  <div className='col-lg-3 align-center'>
                    {avatarImage}
                    <FileUploadField
                      name='scout-avatar'
                      topic='avatar'
                      label='Unggah Foto'
                      fieldValue={this.state.avatar}
                      onSuccess={(data) => this._handleFieldValueChange('avatar', data === '' ? data : data.storedFile._id)}
                      onFail={this._handleFileUploadFailed.bind(this)}/>
                  </div>
                  <div className='col-lg-9'>
                    <div className='col-lg-12 no-margin no-padding'>
                      <div className='col-lg-5'>
                        <div className='form-group'>
                          <label className='control-label align-left margin-bottom-xxs'>Kwartir Daerah</label>
                          {scoutBranchesSelectField}
                        </div>
                      </div>
                      <div className='col-lg-6 col-lg-offset-1'>
                        <div className='form-group'>
                          <label className='control-label align-left margin-bottom-xxs'>Kwartir Cabang</label>
                          {scoutSubbranchesSelectField}
                        </div>
                      </div>
                    </div>

                    <div className='col-lg-12 no-margin no-padding' style={hideIfTrue(this.props.data.type === 'committee')}>
                      {
                        this.props.lockScoutTypeChanges || false ?
                          <div></div> :
                          <div style={{ color: 'red' }}>Perhatian! Pastikan data keikutsertaan Anda benar. Anda tidak dapat mengubah data keikutsertaan setelah data disimpan.</div>
                      }
                      <div className='hr-line-dashed no-margin'></div>
                    </div>

                    <div className='col-lg-12 no-margin no-padding' style={hideIfTrue(this.props.data.type === 'committee')}>
                      <FormFieldWrapper name='type' fieldValue={this.state.type} className='col-lg-6 no-margin padding-right-md no-padding-left' onChange={this._handleFieldValueChange.bind(this)} isValid={this._isValidValue('type')} validationMessage={this._getValidationMessage('type')}>
                        <label className='control-label align-left margin-bottom-xxs'>Keikutsertaan Jamnas sebagai:</label>
                        <select className='m-b no-margin form-control' disabled={this.props.lockScoutTypeChanges || false}>
                          <option value=''>- Pilih -</option>
                          <option value='participant'>Peserta Jamnas X 2016</option>
                          <option value='leader-cmt'>Pimpinan Kontingen Daerah</option>
                          <option value='leader-staff'>Staff Kontingen Daerah</option>
                          <option value='coach'>Pembina Pendamping</option>
                          <option value='leader-dct'>Pimpinan Kontingen Cabang</option>
                        </select>
                      </FormFieldWrapper>
                      {scoutSubTypeSelectField}
                    </div>
                    <div className='col-lg-12 no-margin no-padding' style={hideIfTrue(this.props.data.type !== 'committee')}>
                      <div className='col-lg-12 no-margin no-padding'>
                        <div>Informasi Kepanitiaan</div>
                        <div className='hr-line-dashed no-margin'></div>
                      </div>
                      {scoutCommitteeRoleSelectField}
                      {scoutCommitteeRoleDetailSelectField}
                      {scoutCommitteeRoleDetailExtraSelectField}
                    </div>
                    {scoutGroupSelectField}
                  </div>
                </div>

                <div className='col-lg-12 margin-top-md'>
                  <div className='hr-line-dashed no-margin'></div>
                </div>

                <div className='col-lg-12'>

                  <div className='col-lg-12 margin-top-xs no-padding'>
                    <div className='col-lg-5'>
                      <FormFieldWrapper name='name' fieldValue={this.state.name} onChange={this._handleFieldValueChange.bind(this)} className='form-group padding-right-md' isValid={this._isValidValue('name')} validationMessage={this._getValidationMessage('name')}>
                        <label className='control-label align-left margin-bottom-xxs'>Nama Lengkap</label>
                        <input type='text' className='form-control' />
                      </FormFieldWrapper>
                    </div>
                    <div className='col-lg-4'>
                      <FormFieldWrapper name='nta' fieldValue={this.state.nta} onChange={this._handleFieldValueChange.bind(this)} className='form-group padding-right-md' isValid={this._isValidValue('nta')} validationMessage={this._getValidationMessage('nta')}>
                        <label className='control-label align-left margin-bottom-xxs'>NTA</label>
                        <input type='text' className='form-control' />
                      </FormFieldWrapper>
                    </div>
                    <div className='col-lg-3'>
                      <FormFieldWrapper name='sex' fieldValue={this.state.sex} onChange={this._handleFieldValueChange.bind(this)} isValid={this._isValidValue('sex')} validationMessage={this._getValidationMessage('sex')} isValid={this._isValidValue('sex')} validationMessage={this._getValidationMessage('sex')}>
                        <label className='control-label align-left margin-bottom-xxs'>Jenis Kelamin</label>
                        <select className='form-control m-b no-margin'>
                          <option value=''>- Pilih -</option>
                          <option value='male'>Laki-Laki</option>
                          <option value='female'>Perempuan</option>
                        </select>
                      </FormFieldWrapper>
                    </div>
                  </div>

                  <div className='col-lg-12 no-padding'>
                    <div className='col-lg-3'>
                      <FormFieldWrapper name='birthplace' fieldValue={this.state.birthplace}  onChange={this._handleFieldValueChange.bind(this)} className='form-group padding-right-md' isValid={this._isValidValue('birthplace')} validationMessage={this._getValidationMessage('birthplace')}>
                        <label className='control-label align-left margin-bottom-xxs'>Tempat Lahir</label>
                        <input type='text' className='form-control' />
                      </FormFieldWrapper>
                    </div>
                    <div className='col-lg-5'>
                      <div className='form-group margin-left-xs' id='data_1'>
                        <label className='control-label align-left margin-bottom-xxs'>Tanggal Lahir</label>
                        <SimpleDatePickerField minYear={1900} value={this.state.birthday} fieldValue={this.state.birthday} onChange={(date) => {this._handleFieldValueChange('birthday', date)}} />
                      </div>
                    </div>
                    <div className='col-lg-2'>
                      <FormFieldWrapper name='blood_type' fieldValue={this.state.blood_type} onChange={this._handleFieldValueChange.bind(this)} className='form-group padding-right-md' isValid={this._isValidValue('blood_type')} validationMessage={this._getValidationMessage('blood_type')}>
                        <label className='control-label align-left margin-bottom-xxs'>Golongan Darah</label>
                        <select className='form-control m-b no-margin'>
                          <option value=''>- Pilih -</option>
                          <option value='A'>A</option>
                          <option value='B'>B</option>
                          <option value='AB'>AB</option>
                          <option value='O'>O</option>
                        </select>
                      </FormFieldWrapper>
                    </div>
                    <div className='col-lg-2'>
                      <FormFieldWrapper name='religion' fieldValue={this.state.religion} onChange={this._handleFieldValueChange.bind(this)} isValid={this._isValidValue('religion')} validationMessage={this._getValidationMessage('religion')}>
                        <label className='control-label align-left margin-bottom-xxs'>Agama</label>
                        <select className='form-control m-b'>
                          <option value=''>- Pilih -</option>
                          <option value='islam'>Islam</option>
                          <option value='kristen_protestan'>Kristen Protestan</option>
                          <option value='kristen_katolik'>Kristen Katolik</option>
                          <option value='hindu'>Hindu</option>
                          <option value='budha'>Budha</option>
                          <option value='konghucu'>Konghucu</option>
                          <option value='others'>Lainnya</option>
                        </select>
                      </FormFieldWrapper>
                    </div>
                  </div>
                </div>
              </div>
              <div style={{ width: '100%', height: '1px', clear: 'both' }}></div>
            </div>
            <button className={'form-control btn btn-green bold-700 uppercase'} onClick={() => this._handleSave()}>
              <i className='fa fa-save margin-right-xs'></i>Simpan Data Peserta
            </button>
          </div>
        </div>
      </div>
    );
  }
}
