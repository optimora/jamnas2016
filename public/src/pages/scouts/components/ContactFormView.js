import React from 'react';

export default class ContactFormView extends React.Component {
  static get propTypes() {
    return {
      contact: React.PropTypes.object.isRequired
    };
  }

  render() {
    if (this.props.contact == null) {
      return <div></div>;
    }

    const isDefNotNullAndNotEmpty = (val) => {
      return val != null && val != undefined && val != '';
    };
    let nameFieldValue = <div></div>;
    if (isDefNotNullAndNotEmpty(this.props.contact.name)) {
      nameFieldValue = <div className='col-lg-12 margin-bottom-xs'>
        <div className='col-lg-3 no-margin no-padding'>
          <label className='control-label align-left margin-bottom-xxs'>Nama Orang Tua</label>
        </div>
        <div className='col-lg-6'>
          <p>{this.props.contact.name}</p>
        </div>
      </div>;
    }
    let occupationFieldValue = <div></div>;
    if (isDefNotNullAndNotEmpty(this.props.contact.occupation)) {
      occupationFieldValue = <div className='col-lg-12 margin-bottom-xs'>
        <div className='col-lg-3 no-margin no-padding'>
          <label className='control-label align-left margin-bottom-xxs'>Pekerjaan</label>
        </div>
        <div className='col-lg-6'>
          <p>{this.props.contact.occupation}</p>
        </div>
      </div>
    }

    const printName = (obj) => {
      if (obj && obj.name && obj.name !== '') {
        return obj.name;
      }
      return '-';
    };

    return(
      <div className='ibox float-e-margins'>
        <div className='ibox-title'>
          <h5>Informasi Kontak {this.props.contact.type}</h5>
          <div className='ibox-tools'>
            <a className='collapse-link'>
              <i className='fa fa-chevron-up'></i>
            </a>
          </div>
        </div>
        <div className='ibox-content'>
          {nameFieldValue}
          {occupationFieldValue}
          <div className='col-lg-12'>
            <div className='col-lg-3 no-margin no-padding'>
              <label className='control-label align-left margin-bottom-xxs'>Alamat Rumah</label>
            </div>
            <div className='col-lg-9'>
              <p>{this.props.contact.address}</p>
              <div className='col-lg-4 no-padding-left margin-top-xs'>
                <label className='control-label align-left margin-bottom-xxs'>Provinsi</label>
                <p>{printName(this.props.contact.province)}</p>
              </div>
              <div className='col-lg-5 no-padding-right margin-top-xs'>
                <label className='control-label align-left margin-bottom-xxs'>Kota / Kabupaten</label>
                <p>{printName(this.props.contact.regency_city)}</p>
              </div>
              <div className='col-lg-2 no-padding-right margin-top-xs'>
                <label className='control-label align-left margin-bottom-xxs'>Kodepos</label>
                <p>{this.props.contact.zipcode}</p>
              </div>
            </div>
          </div>

          <div className='col-lg-12'><div className='hr-line-dashed'></div></div>

          <div className='col-lg-12'>
            <div className='col-lg-3 no-margin no-padding'>
              <label className='control-label align-left margin-bottom-xxs'>Kontak</label>
            </div>
            <div className='col-lg-9'>
              <div className='col-lg-4 no-padding-left'>
                <label className='control-label align-left margin-bottom-xxs'>No Telp. Rumah</label>
                <p>{(this.props.contact.phone || { home: ' - ' }).home}</p>
              </div>
              <div className='col-lg-4 no-padding-right'>
                <label className='control-label align-left margin-bottom-xxs'>No. Handphone</label>
                <p>{(this.props.contact.phone || { mobile: ' - ' }).mobile}</p>
              </div>
            </div>
          </div>

          <div style={{ width: '100%', height: '1px', clear: 'both' }}></div>
        </div>
      </div>
    );
  }
}
