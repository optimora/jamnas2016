import ObjectPath from 'object-path';
import React from 'react';

import FormFieldWrapper from '../../../components/common/forms/FormFieldWrapper';

const fieldSegmentToFieldMap = {
  _medicalHistoryItem: 'medical_history',
  _emergencyContactItem: 'emergency_contacts',
  _emergencyRadioItem: 'emergency_radios'
};

const fieldToFieldSegmentMap = {
  medical_history: '_medicalHistoryItem',
  emergency_contacts: '_emergencyContactItem',
  emergency_radios: '_emergencyRadioItem'
};

export default class MedicalHistoryForm extends React.Component {
  constructor(props, context) {
    super(props, context);

    this._initialize();
  }

  static get propTypes() {
    return {
      medicalHistory: React.PropTypes.array.isRequired,
      emergencyContacts: React.PropTypes.array.isRequired,
      emergencyRadios: React.PropTypes.array.isRequired,

      onChange: React.PropTypes.func.isRequired,
      onSave: React.PropTypes.func.isRequired
    };
  }

  _emitFieldValueChange(fieldName, value) {
    this.props.onChange(fieldName, value);
    this._reset(fieldToFieldSegmentMap[fieldName]);
  }

  _handleFieldValueChange(fieldName, value) {
    const fieldSegment = fieldName.split('.');
    let modifiedState = Object.assign({}, this.state[fieldSegment[0]]);
    ObjectPath.set(modifiedState, fieldSegment.slice(1), value);
    this.setState({
      [fieldSegment[0]]: Object.assign({}, modifiedState)
    }, () => { console.log(fieldName, value, modifiedState); });
  }

  _addEntry(fieldSegment, entry) {
    this.setState(
      {
        [fieldSegment]: [...this.state[fieldSegment], entry]
      },
      () => {
        this._emitFieldValueChange(fieldSegment, this.state[fieldSegment]);
      }
    );
  }

  _removeEntry(fieldSegment, index) {
    this.setState(
      {
        [fieldSegment]: [
          ...this.state[fieldSegment].slice(0, index),
          ...this.state[fieldSegment].slice(index + 1)
        ]
      },
      () => {
        this._emitFieldValueChange(fieldSegment, this.state[fieldSegment]);
      }
    );
  }

  _initialize() {
    this.state = {
      medical_history: [...this.props.medicalHistory],
      emergency_contacts: [...this.props.emergencyContacts],
      emergency_radios: [...this.props.emergencyRadios],

      _errors: {},
      _medicalHistoryItem: {
        disease: '',
        medicine: '',
        treatment: ''
      },
      _emergencyContactItem: {
        phone: {
          home: '',
          mobile: ''
        },
        name: '',
        relation: ''
      },
      _emergencyRadioItem: {
        name        : '',
        call_sign   : '',
        frequency   : '',
        local_area  : ''
      }
    };
  }

  _save(fieldSegment) {
    this._addEntry(fieldSegmentToFieldMap[fieldSegment], Object.assign({}, this.state[fieldSegment]));
  }

  _reset(fieldSegment) {
    const emptyMedicalHistoryItem = {
      disease: '',
      medicine: '',
      treatment: ''
    };

    const emptyEmergencyContactItem = {
      phone: {
        home: '',
        mobile: ''
      },
      name: '',
      relation: ''
    };

    const emptyEmergencyRadioItem = {
      name        : '',
      call_sign   : '',
      frequency   : '',
      local_area  : ''
    };

    let emptyItem;
    switch (fieldSegment) {
      case '_medicalHistoryItem':
        emptyItem = Object.assign({}, emptyMedicalHistoryItem);
        break;
      case '_emergencyContactItem':
        emptyItem = Object.assign({}, emptyEmergencyContactItem);
        break;
      case '_emergencyRadioItem':
        emptyItem = Object.assign({}, emptyEmergencyRadioItem);
        break;
      default:
        emptyItem = {};
    }

    this.setState(
      {
        [fieldSegment]: emptyItem
      },
      () => {
        // TODO: Think better way. -_-
        this.forceUpdate();
      }
    );
  }

  render() {
    return (
      <div className='row'>
        <div className='col-lg-12'>
          <div className='ibox float-e-margins'>
            <div className='ibox-title'>
              <h5>RIWAYAT KESEHATAN</h5>
              <div className='ibox-tools'>
                <a className='collapse-link'>
                  <i className='fa fa-chevron-up'></i>
                </a>
              </div>
            </div>
            <div className='ibox-content'>

              <div className='col-lg-12'>
                <label className='control-label align-left text-underline padding-left-xs'>PENYAKIT YANG DIMILIKI</label>
              </div>

              <div className='col-lg-12 margin-top-xs'>
                <div className='col-lg-1 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>No</label>
                </div>
                <div className='col-lg-3 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Nama Penyakit</label>
                </div>
                <div className='col-lg-3 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Obat Pribadi yang Dimiliki</label>
                </div>
                <div className='col-lg-4 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Penanganan Medis</label>
                </div>
                <div classnamme='col-lg-1 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Aksi</label>
                </div>
              </div>

              {this.props.medicalHistory.map((medicalHistory, index) => {
                return (
                  <div key={index} className='col-lg-12 margin-top-xs'>
                    <div className='col-lg-1 align-center'>
                      <p>{index + 1}</p>
                    </div>
                    <div className='col-lg-3 align-center'>
                      <p>{medicalHistory.disease}</p>
                    </div>
                    <div className='col-lg-3 align-center'>
                      <p>{medicalHistory.medicine}</p>
                    </div>
                    <div className='col-lg-4 align-center'>
                      <p>{medicalHistory.treatment}</p>
                    </div>
                    <div classnamme='col-lg-1 align-center'>
                      <button className='btn btn-danger btn-xs' onClick={() => this._removeEntry('medical_history', index)}>Hapus</button>
                    </div>
                  </div>
                );
              })}

              <div className='col-lg-12 margin-top-xs'>
                <div className='col-lg-1 align-center'>
                  <p className='control-label align-center margin-top-xxs'>{this.state.medical_history.length + 1}.</p>
                </div>
                <div className='col-lg-3 align-center'>
                  <FormFieldWrapper name='_medicalHistoryItem.disease' fieldValue={this.state._medicalHistoryItem.disease} onChange={this._handleFieldValueChange.bind(this)}>
                    <input type='text' className='form-control' />
                  </FormFieldWrapper>
                </div>
                <div className='col-lg-3 align-center'>
                  <FormFieldWrapper name='_medicalHistoryItem.medicine' fieldValue={this.state._medicalHistoryItem.medicine} onChange={this._handleFieldValueChange.bind(this)}>
                    <input type='text' className='form-control' />
                  </FormFieldWrapper>
                </div>
                <div className='col-lg-4 align-center'>
                  <FormFieldWrapper name='_medicalHistoryItem.treatment' fieldValue={this.state._medicalHistoryItem.treatment} onChange={this._handleFieldValueChange.bind(this)}>
                    <input type='text' className='form-control' />
                  </FormFieldWrapper>
                </div>
              </div>
              <div className='col-lg-12 margin-top-xs'>
                <button className='btn btn-success pull-right' onClick={() => this._save('_medicalHistoryItem')}>Tambah Entry</button>
              </div>

              <div className='col-lg-12'><div className='hr-line-dashed'></div></div>

              <div className='col-lg-12'>
                <label className='control-label align-left text-underline padding-left-xs'>Nomor darurat yang bisa dihubungi di kampung halaman:</label>
              </div>

              <div className='col-lg-12 margin-top-xs'>
                <div className='col-lg-1 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>No</label>
                </div>
                <div className='col-lg-2 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Telepon</label>
                </div>
                <div className='col-lg-2 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Handphone</label>
                </div>
                <div className='col-lg-3 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Nama Pemilik</label>
                </div>
                <div className='col-lg-3 align-center'>
                  <label className='control-label align-center margin-bottom-xxs'>Hubungan dengan Peserta</label>
                </div>
                <div className='col-lg-1 align-center'>
                  <label className='control-label align-center margin-bottom-xxs'>Aksi</label>
                </div>
              </div>

              {this.props.emergencyContacts.map((contact, index) => {
                return (
                  <div key={index} className='col-lg-12 margin-top-xs'>
                    <div className='col-lg-1 align-center'>
                      <p>{index + 1}</p>
                    </div>
                    <div className='col-lg-2 align-center'>
                      <p>{contact.phone.home || ''}</p>
                    </div>
                    <div className='col-lg-2 align-center'>
                      <p>{contact.phone.mobile || ''}</p>
                    </div>
                    <div className='col-lg-3 align-center'>
                      <p>{contact.name || ''}</p>
                    </div>
                    <div className='col-lg-3 align-center'>
                      <p>{contact.relation || ''}</p>
                    </div>
                    <div classnamme='col-lg-1 align-center'>
                      <button className='btn btn-danger btn-xs' onClick={() => this._removeEntry('emergency_contacts', index)}>Hapus</button>
                    </div>
                  </div>
                );
              })}

              <div className='col-lg-12 margin-top-xxs'>
                <div className='col-lg-1 align-center'>
                  <p className='control-label align-center margin-top-xxs'>{this.props.emergencyContacts.length + 1}.</p>
                </div>
                <div className='col-lg-2 align-center'>
                  <FormFieldWrapper name='_emergencyContactItem.phone.home' fieldValue={this.state._emergencyContactItem.phone.home} onChange={this._handleFieldValueChange.bind(this)}>
                    <input type='text' className='form-control' />
                  </FormFieldWrapper>
                </div>
                <div className='col-lg-2 align-center'>
                  <FormFieldWrapper name='_emergencyContactItem.phone.mobile' fieldValue={this.state._emergencyContactItem.phone.mobile} onChange={this._handleFieldValueChange.bind(this)}>
                    <input type='text' className='form-control' />
                  </FormFieldWrapper>
                </div>
                <div className='col-lg-3 align-center'>
                  <FormFieldWrapper name='_emergencyContactItem.name' fieldValue={this.state._emergencyContactItem.name} onChange={this._handleFieldValueChange.bind(this)}>
                    <input type='text' className='form-control' />
                  </FormFieldWrapper>
                </div>
                <div className='col-lg-3 align-center'>
                  <FormFieldWrapper name='_emergencyContactItem.relation' fieldValue={this.state._emergencyContactItem.relation} onChange={this._handleFieldValueChange.bind(this)}>
                    <input type='text' className='form-control' />
                  </FormFieldWrapper>
                </div>
              </div>

              <div className='col-lg-12 margin-top-xs'>
                <button className='btn btn-success pull-right' onClick={() => this._save('_emergencyContactItem')}>Tambah Entry</button>
              </div>

              <div className='col-lg-12'><div className='hr-line-dashed'></div></div>

              <div className='col-lg-12'>
                <label className='control-label align-left text-underline padding-left-xs'>Call sign darurat yang bisa dihubungi di kampung halaman:</label>
              </div>

              <div className='col-lg-12 margin-top-xs'>
                <div className='col-lg-1 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>No</label>
                </div>
                <div className='col-lg-2 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Call Sign</label>
                </div>
                <div className='col-lg-3 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Nama Pemilik</label>
                </div>
                <div className='col-lg-2 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Frekuensi Radio Amatir</label>
                </div>
                <div className='col-lg-3 align-center'>
                  <label className='control-label align-center margin-bottom-xxs'>Lokal</label>
                </div>
                <div className='col-lg-1 align-center'>
                  <label className='control-label align-center margin-bottom-xxs'>Aksi</label>
                </div>
              </div>

              {this.props.emergencyRadios.map((emergencyRadio, index) => {
                return (
                  <div key={index} className='col-lg-12 margin-top-xs'>
                    <div className='col-lg-1 align-center'>
                      <p>{index + 1}</p>
                    </div>
                    <div className='col-lg-2 align-center'>
                      <p>{emergencyRadio.call_sign}</p>
                    </div>
                    <div className='col-lg-3 align-center'>
                      <p>{emergencyRadio.name}</p>
                    </div>
                    <div className='col-lg-2 align-center'>
                      <p>{emergencyRadio.frequency}</p>
                    </div>
                    <div className='col-lg-3 align-center'>
                      <p>{emergencyRadio.local_area}</p>
                    </div>
                    <div classnamme='col-lg-1 align-center'>
                      <button className='btn btn-danger btn-xs' onClick={() => this._removeEntry('emergency_radios', index)}>Hapus</button>
                    </div>
                  </div>
                );
              })}

              <div className='col-lg-12 margin-top-xxs'>
                <div className='col-lg-1 align-center'>
                  <p className='control-label align-center margin-top-xxs'>{this.props.emergencyRadios.length + 1}.</p>
                </div>
                <FormFieldWrapper name='_emergencyRadioItem.call_sign' fieldValue={this.state._emergencyRadioItem.call_sign} onChange={this._handleFieldValueChange.bind(this)} className='col-lg-2 align-center'>
                  <input type='text' className='form-control' name='scout.emergency_radios.call_sign' />
                </FormFieldWrapper>
                <FormFieldWrapper name='_emergencyRadioItem.name' fieldValue={this.state._emergencyRadioItem.name} onChange={this._handleFieldValueChange.bind(this)} className='col-lg-3 align-center'>
                  <input type='text' className='form-control' name='scout.emergency_radios.name' />
                </FormFieldWrapper>
                <FormFieldWrapper name='_emergencyRadioItem.frequency' fieldValue={this.state._emergencyRadioItem.frequency} onChange={this._handleFieldValueChange.bind(this)} className='col-lg-2 align-center'>
                  <input type='text' className='form-control' name='scout.emergency_radios.frequency' />
                </FormFieldWrapper>
                <FormFieldWrapper name='_emergencyRadioItem.local_area' fieldValue={this.state._emergencyRadioItem.local_area} onChange={this._handleFieldValueChange.bind(this)} className='col-lg-3 align-center'>
                  <input type='text' className='form-control' name='scout.emergency_radios.local_area' />
                </FormFieldWrapper>
              </div>

              <div className='col-lg-12 margin-top-xs'>
                <button className='btn btn-success pull-right' onClick={() => this._save('_emergencyRadioItem')}>Tambah Entry</button>
              </div>

              <div style={{ width: '100%', height: '1px', clear: 'both' }}></div>
            </div>
            <button className='form-control btn btn-green bold-700 uppercase' type='submit' onClick={() => this.props.onSave()}>
              <i className='fa fa-save margin-right-xs'></i>Simpan Data Peserta
            </button>
          </div>
        </div>
      </div>

    );
  }
}
