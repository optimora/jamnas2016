import React from 'react';

import Page from '../Page';

import DetailPageContainer from './containers/DetailPageContainer';
import ScoutSubbranchFormHeader from '../../components/common/ui/ScoutSubbranchFormHeader';

export default class DetailPage extends Page {
  constructor(...args) {
    super(...args);

    this.branch = window.currentPageData['branch'];
    this.scout = window.currentPageData['scout'];
    this.subbranch = window.currentPageData['subbranch'];
  }

  setup() {
    this.render(
      <div className="row wrapper border-bottom white-bg page-heading padding-bottom-xs">
        <div className="col-lg-12">
          <ScoutSubbranchFormHeader branch={this.branch} subbranch={this.subbranch} />
        </div>
      </div>,
      $(this._selectors.header)[0]
    );
    this.render(
      <DetailPageContainer
        scout={this.scout}
        branch={this.branch}
        subbranch={this.subbranch} />,
      $(this._selectors.main)[0]
    );
  }
}