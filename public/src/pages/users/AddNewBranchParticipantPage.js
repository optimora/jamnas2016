import React from 'react';
import ReactDom from 'react-dom';

import HeaderContent from './components/HeaderContent';
import ParticipantsBasicForm from './components/ParticipantsBasicForm';
import ParticipantsAddressForm from './components/ParticipantsAddressForm';
import ParticipantsParentForm from './components/ParticipantsParentForm';
import ParticipantsEducationForm from './components/ParticipantsEducationForm';
import ParticipantsScoutingForm from './components/ParticipantsScoutingForm';
import ParticipantsMedicalForm from './components/ParticipantsMedicalForm';
import ParticipantsCourseForm from './components/ParticipantsCourseForm';
import ParticipantsDocumentForm from './components/ParticipantsDocumentForm';

export default class AddNewBranchParticipantPage {
  constructor(selectors) {
    this._selectors = selectors;
  }

  setup() {
    ReactDom.render(
      <div>
        <HeaderContent subtitle='Formulir Pendaftaran Peserta' title={user.branch.name} logo={'/public/assets/images/branch_logo/' + user.branch.logo}/>
      </div>,
      $(this._selectors.header)[0]
    );

    ReactDom.render(
      <div>
        <ParticipantsBasicForm />
      </div>,
      $(this._selectors.participantsBasicForm)[0]
    );

    ReactDom.render(
      <div>
        <ParticipantsAddressForm />
      </div>,
      $(this._selectors.participantsAddressForm)[0]
    );

    ReactDom.render(
      <div>
        <ParticipantsParentForm />
      </div>,
      $(this._selectors.participantsParentForm)[0]
    )

    ReactDom.render(
      <div>
        <ParticipantsEducationForm />
      </div>,
      $(this._selectors.participantsEducationForm)[0]
    )

    ReactDom.render(
      <div>
        <ParticipantsScoutingForm />
      </div>,
      $(this._selectors.participantsScoutingForm)[0]
    )

    ReactDom.render(
      <div>
        <ParticipantsCourseForm />
      </div>,
      $(this._selectors.participantsCourseForm)[0]
    )

    ReactDom.render(
      <div>
        <ParticipantsMedicalForm />
      </div>,
      $(this._selectors.participantsMedicalForm)[0]
    )

    ReactDom.render(
      <div>
        <ParticipantsDocumentForm />
      </div>,
      $(this._selectors.participantsDocumentForm)[0]
    )
  }
};
