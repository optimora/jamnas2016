import React from 'react';
import ReactDom from 'react-dom';

import HeaderContent from './components/HeaderContent';
import BasicForm from './components/BasicForm';
import AddressForm from './components/AddressForm';
import ParentForm from './components/ParentForm';
import EducationForm from './components/EducationForm';
import CourseForm from './components/CourseForm';
import ScoutingForm from './components/ScoutingForm';

export default class AddNewCommitteePage {
    constructor(selectors, data) {
        this._selectors = selectors;
        this._data = data;
    }

    setup() {
        ReactDom.render(
            <div>
                <div className="row wrapper border-bottom white-bg page-heading padding-bottom-xs">
                    <div className="col-lg-12">
                        <HeaderContent title="Panitia Penyelenggara" subtitle="Formulir Pendaftaran"/>
                    </div>
                </div>
                <div className="wrapper wrapper-content">
                    <div className="col-lg-10">
                        <div className="row">
                            <div className="col-lg-12">
                                <BasicForm title="BIODATA PANITIA JAMBORE NASIONAL X 2016" isForm="true"/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12">
                                <AddressForm isForm="true"/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12">
                                <ParentForm isForm="true"/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12">
                                <EducationForm isForm="true" items={this._data.scout.educations}/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12">
                                <ScoutingForm isForm="true" items={this._data.scout.experiences}/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12">
                                <CourseForm title="Data Kursus atau Pelatihan Kepramukaan" isFixedName="true" isForm="true" items={ this._data.scout.courses } />
                            </div>
                        </div>
                        <div className="row margin-bottom-xlg">
                            <div className="col-lg-12">
                                <CourseForm title="Data Kursus atau Pelatihan Di Luar Kepramukaan" isForm="true" items={ this._data.scout.external_courses }/>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-2">

                    </div>
                </div>
            </div>,
            $(this._selectors.add_new_committee)[0]
        );
    }
};
/**
 * Created by saras on 5/18/16.
 */
