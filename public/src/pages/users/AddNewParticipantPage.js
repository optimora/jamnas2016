import React from 'react';
import ReactDom from 'react-dom';

import HeaderContent from './components/HeaderContent';
import DataGrid from './components/DataGrid';

export default class AddNewParticipantPage {
  constructor(selectors, data) {
    this._selectors = selectors;
    this._data = data;
  }

  setup() {
    ReactDom.render(
      <div>
        <DataGrid title="Pendaftaran peserta Jambore Nasional X 2016 dikelompokkan berdasarkan Kwartir Cabang" items={this._data.branches} label="Kontingen Kwartir Cabang" unit="peserta"/>
      </div>,
      $(this._selectors.scoutsGrid)[0]
    );

    ReactDom.render(
      <div>
        <HeaderContent subtitle="Peserta Jambore Nasional X 2016" title={ this._data.user.name } logo={'../public/assets/images/branch_logo/' + this._data.user.branch.logo }/>
      </div>,
      $(this._selectors.header)[0]
    );
  }
};
