import { Provider } from 'react-redux';
import { applyMiddleware, combineReducers, createStore } from 'redux';

import React from 'react';
import ReactDom from 'react-dom';
import thunk from 'redux-thunk';

import * as CommonAction from '../../actions/common/actions';

import HeaderContent from './components/HeaderContent';
import KwardaWillingness from './components/BranchWillingnessLetter';
import usersBranchWillingnessLetterReducers from './reducers/usersBranchWillingnessLetterReducers';

export default class BranchWillingnessLetter {
  constructor(selectors, data) {
    this._selectors = selectors;
    this._data = data;
  }

  _createReducers() {
    return combineReducers({
      BranchWillingnessLetter: usersBranchWillingnessLetterReducers
    });
  }

  _createStore() {
    let reducers = this._createReducers();
    return createStore(reducers, applyMiddleware(thunk));
  }

  getRenderedComponent() {
    return this._renderedComponent || null;
  }

  setup() {
    this._store = this._createStore();
    this._store.dispatch(CommonAction.fetchScoutBranches());
    this._renderedComponent = ReactDom.render(
        <Provider store={this._store}>
          <HeaderContent subtitle='Formulir D.01 - Kesediaan Kwartir Daerah' title={window.currentPageData.user.branch.name} logo={'/public/assets/images/branch_logo/' + user.branch.logo}/>
        </Provider>,
        $(this._selectors.header)[0]
    );

    this._renderedComponent = ReactDom.render(
        <Provider store={this._store}>
          <KwardaWillingness/>
        </Provider>,
        $(this._selectors.kwardaWillingness)[0]
    );

  }
};
