import React from 'react';
import ReactDom from 'react-dom';

import ScoutBranchFormHeader from '../../components/common/ui/ScoutBranchFormHeader';
import BranchPayment from './components/BranchPaymentConfirmation';

export default class BranchPaymentConfirmation {
  constructor(selectors) {
    this._selectors = selectors;
  }

  setup() {
    ReactDom.render(
      <div className="row wrapper border-bottom white-bg page-heading padding-bottom-xs">
        <div className="col-lg-12">
          <ScoutBranchFormHeader branch={window.currentPageData['branch']} />
        </div>
      </div>,
      $(this._selectors.header)[0]
    );

    ReactDom.render(
      <div>
        <BranchPayment />
      </div>,
      $(this._selectors.branchPayment)[0]
    );

  }
};
