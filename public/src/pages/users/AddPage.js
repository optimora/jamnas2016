import { Provider } from 'react-redux';
import { applyMiddleware, combineReducers, createStore } from 'redux';

import React from 'react';
import ReactDom from 'react-dom';
import thunk from 'redux-thunk';

import * as CommonAction from '../../actions/common/actions';

import AddPageContainer from './containers/AddPageContainer';
import usersAddPageReducers from './reducers/usersAddPageReducers';

export default class AddPage {
  constructor(selectors) {
    this._selectors = selectors;
  }

  _createReducers() {
    return combineReducers({
      AddPage: usersAddPageReducers
    });
  }

  _createStore() {
    let reducers = this._createReducers();
    return createStore(reducers, applyMiddleware(thunk));
  }

  getRenderedComponent() {
    return this._renderedComponent || null;
  }

  setup() {
    this._store = this._createStore();
    this._store.dispatch(CommonAction.fetchScoutBranches());
    this._renderedComponent = ReactDom.render(
      <Provider store={this._store}>
        <AddPageContainer />
      </Provider>,
      $(this._selectors.main)[0]
    );
  }
};
