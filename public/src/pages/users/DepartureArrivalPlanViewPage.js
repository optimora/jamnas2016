import React from 'react';
import ReactDom from 'react-dom';

import HeaderContent from './components/HeaderContent';
import BranchContactForm from './components/BranchContactForm';
import KontingenDetailForm from './components/KontingenDetailForm';
import DepartArriveReturnForm from './components/DepartArriveReturnForm';

export default class DepartureArrivalPlanViewPage {
    constructor(selectors) {
        this._selectors = selectors;
    }

    setup() {
        ReactDom.render(
            <div>
                <div className="row wrapper border-bottom white-bg page-heading padding-bottom-xs">
                    <div className="col-lg-12">
                      <HeaderContent subtitle='Formulir D.07 - Rencana Kedatangan dan Kepulangan Kontingen' title={user.branch.name} logo={'/public/assets/images/branch_logo/' + user.branch.logo}/>
                    </div>
                </div>
                <div className="wrapper wrapper-content">
                    <div className="col-lg-10">
                        <div className="row">
                            <div className="col-lg-12">
                                <BranchContactForm isForm ="false"/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12">
                                <KontingenDetailForm isForm="false"/>
                            </div>
                        </div>
                        <div className="row margin-bottom-xlg">
                            <div className="col-lg-12">
                                <DepartArriveReturnForm isForm="false"/>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-2">

                    </div>
                </div>
            </div>,
            $(this._selectors.departure_arrival_plan_view)[0]
        );
    }
};
/**
 * Created by saras on 5/18/16.
 */
