import { Provider } from 'react-redux';
import { applyMiddleware, combineReducers, createStore } from 'redux';

import React from 'react';
import ReactDom from 'react-dom';
import thunk from 'redux-thunk';

import * as usersBranchRegistrationPageActions from './actions/usersBranchRegistrationPageActions';

import Page from '../Page';
import usersBranchRegistrationPageReducers from './reducers/usersBranchRegistrationPageReducers';

import BranchRegistrationViewPageContainer from './containers/BranchRegistrationViewPageContainer'
import ScoutBranchFormHeader from '../../components/common/ui/ScoutBranchFormHeader';

export default class BranchRegistrationViewPage extends Page {
    constructor(selectors) {
        super(selectors);
    }

    _createReducers() {
        return combineReducers({
            BranchRegistrationPage: usersBranchRegistrationPageReducers
        });
    }

    _createStore() {
        let reducers = this._createReducers();
        return createStore(reducers, applyMiddleware(thunk));
    }

    _initialize() {
        this._store.dispatch(usersBranchRegistrationPageActions.filter_branch(window.currentPageData.user.branch._id));
    }

    setup() {
        this._store = this._createStore();
        this.render(
            <div>
                <Provider store={this._store}>
                    <BranchRegistrationViewPageContainer />
                </Provider>
            </div>,
            $(this._selectors.branch_registration_view)[0]
        );
        this._initialize();
    }
}