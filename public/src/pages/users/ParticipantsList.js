import React from 'react';
import ReactDom from 'react-dom';

import HeaderContent from './components/HeaderContent';
import ParticipantsTable from './components/ParticipantsTable';

export default class ParticipantsList {
  constructor(selectors) {
    this._selectors = selectors;
  }

  setup() {
    ReactDom.render(
      <div>
        <HeaderContent subtitle='Jambore Nasional X 2016' title="Peserta"/>
      </div>,
      $(this._selectors.header)[0]
    );

    ReactDom.render(
      <div>
        <ParticipantsTable />
      </div>,
      $(this._selectors.participant_list)[0]
    );

  }
};
