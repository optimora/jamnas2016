import { Provider } from 'react-redux';
import { applyMiddleware, combineReducers, createStore } from 'redux';

import React from 'react';
import ReactDom from 'react-dom';
import thunk from 'redux-thunk';

import * as usersDashboardPageActions from './actions/usersDashboardPageActions';

import Page from '../Page';
import usersDashboardPageReducers from './reducers/usersDashboardPageReducers';

import DashboardPageContainer from './containers/DashboardPageContainer'
import ScoutBranchFormHeader from '../../components/common/ui/ScoutBranchFormHeader';

export default class DashboardPage extends Page {
  constructor(selectors) {
    super(selectors);
  }

  _createReducers() {
    return combineReducers({
      DashboardPage: usersDashboardPageReducers
    });
  }

  _createStore() {
    let reducers = this._createReducers();
    return createStore(reducers, applyMiddleware(thunk));
  }

  _initialize() {
    this._store.dispatch(usersDashboardPageActions.initialize(window.currentPageData['branch']['_id']));
  }

  setup() {
    this._store = this._createStore();
    this.render(
      <div className="row wrapper border-bottom white-bg page-heading padding-bottom-xs">
        <div className="col-lg-12">
          <ScoutBranchFormHeader branch={window.currentPageData['branch']} />
        </div>
      </div>,
      $(this._selectors.header)[0]
    );
    this.render(
      <div>
        <Provider store={this._store}>
          <DashboardPageContainer />
        </Provider>
      </div>,
      $(this._selectors.main)[0]
    );
    this._initialize();
  }
}