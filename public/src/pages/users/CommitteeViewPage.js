import React from 'react';
import ReactDom from 'react-dom';

import HeaderContent from './components/HeaderContent';
import BasicForm from './components/BasicForm';
import AddressForm from './components/AddressForm';
import ParentForm from './components/ParentForm';
import EducationForm from './components/EducationForm';
import CourseForm from './components/CourseForm';
import ScoutingForm from './components/ScoutingForm';


export default class CommitteeViewPage {
    constructor(selectors, data) {
        this._selectors = selectors;
        this._data = data;
    }

    setup() {
        ReactDom.render(
            <div>
                <div className="row wrapper border-bottom white-bg page-heading padding-bottom-xs">
                    <div className="col-lg-12">
                        <HeaderContent title="Data Panitia Penyelenggara" subtitle="Jambore Nasional X 2016"/>
                    </div>
                </div>
                <div className="wrapper wrapper-content">
                    <div className="col-lg-10">
                        <div className="row">
                            <div className="col-lg-12">
                                <BasicForm title="BIODATA PANITIA PENYELENGGARA JAMBORE NASIONAL X 2016" isForm="false" data={this._data.scout}/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12">
                                <AddressForm isForm="false" data={this._data.scout.contact[0]}/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12">
                                <ParentForm isForm="false" data={this._data.scout.parent_contacts[0]}/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12">
                                <EducationForm isForm="false" items={this._data.scout.educations}/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12">
                                <ScoutingForm isForm="false" items={this._data.scout.experiences}/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12">
                                <CourseForm title="Data Kursus atau Pelatihan Kepramukaan" isForm="false" isFixedName="true" items={ this._data.scout.courses }/>
                            </div>
                        </div>
                        <div className="row margin-bottom-xlg">
                            <div className="col-lg-12">
                                <CourseForm title="Data Kursus atau Pelatihan Di Luar Kepramukaan" isForm="false" items={ this._data.scout.external_courses }/>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-2">

                    </div>
                </div>
            </div>,
            $(this._selectors.committee_view)[0]
        );
    }
};
/**
 * Created by saras on 5/18/16.
 */
