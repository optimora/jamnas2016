import React from 'react';
import ReactDom from 'react-dom';

import HeaderContent from './components/HeaderContent';
import SubBranchContactForm from './components/SubBranchContactForm';
import SubBranchParticipantForm from './components/SubBranchParticipantForm';
import SubmitButton from './components/SubmitButton';

export default class SubBranchRegistrationPage {
    constructor(selectors) {
        this._selectors = selectors;
    }

    setup() {
        ReactDom.render(
            <div>
                <div className="row wrapper border-bottom white-bg page-heading padding-bottom-xs">
                    <div className="col-lg-12">
                        <HeaderContent subtitle='Formulir C.02 - Pendaftaran Kwartir Cabang' title={user.branch.name} logo={'/public/assets/images/branch_logo/' + user.branch.logo}/>
                    </div>
                </div>
                <div className="wrapper wrapper-content">
                    <div className="col-lg-10">
                        <div className="row">
                            <div className="col-lg-12">
                                <SubBranchContactForm title="FORMULIR REGISTRASI KWARTIR CABANG" isForm ="true"/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12">
                                <SubBranchParticipantForm isForm ="true"/>
                            </div>
                        </div>
                        <div className="row margin-bottom-xlg">
                            <div className="col-lg-12">
                                <SubmitButton attachment="Lampirkan bukti otentik formulir Registrasi Kwartir Cabang C.02" prefix="subbranch_registration" filevalue="kwarcab-registration-letter"/>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-2">

                    </div>
                </div>
            </div>,
            $(this._selectors.subbranch_registration)[0]
        );
    }
};
