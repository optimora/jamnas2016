import React from 'react';
import ReactDom from 'react-dom';

import Page from '../Page';

import FileInputField from '../../components/common/forms/FileInputField';

export default class UploadTestPage extends Page {
  _handleUploadFileSuccess(data) {
    console.log(data);
  }

  _handleUploadFileFail(data) {
    console.log(data);
  }

  setup() {
    this.render(
      <FileInputField
        name='testFileUpload'
        topic='miscellaneous'
        label='Test Upload'
        onSuccess={this._handleUploadFileSuccess.bind(this)}
        onFail={this._handleUploadFileFail.bind(this)}/>,
      $(this._selectors.main)[0]
    );
  }
}