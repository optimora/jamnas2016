import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import React from 'react';

import * as commonActions from '../../../actions/common/actions';
import * as usersDashboardPageActions from '../actions/usersDashboardPageActions';

import DoubleStatBox from '../components/DoubleStatBox';
import ScoutBranchDataGrid from '../../../components/common/ui/ScoutBranchDataGrid';
import StatBox from '../components/StatBox';

class DashboardPageContainer extends React.Component {
  static get propTypes() {
    return {
      DashboardPage: React.PropTypes.object.isRequired,
      actions: React.PropTypes.object.isRequired
    };
  }

  render() {
    let branches = [];
    let totalRegisteredScouts = 0;
    this.props.DashboardPage.branches.forEach((branch) => {
      branches.push({
        id: branch._id,
        label: 'Kontingen Kwartir Cabang',
        name: branch.name,
        count: (branch.stats || {}).numberOfScouts != undefined ? branch.stats.numberOfScouts : '?'
      });
      totalRegisteredScouts += branch.stats && branch.stats.numberOfScouts && typeof branch.stats.numberOfScouts === 'number' ? branch.stats.numberOfScouts : 0;
    });

    let currentBranch = window.currentPageData.branch;
    branches.push({
      id: currentBranch._id,
      label: 'Pimpinan Kontingen',
      name: currentBranch.name,
      count: '?',
      color: '#0000ff',
      extraParams: { leader: true }
    });

    return (
      <div className='wrapper wrapper-content'>
        <div className='row'>
          <div className='col-lg-3'>
            <StatBox unit='Peserta Terdaftar' value={totalRegisteredScouts}/>
          </div>
          <div className='col-lg-6'></div>
          <div className='col-lg-3'></div>
        </div>

        <div className='row'>
          <div className='col-lg-12'>
            <ScoutBranchDataGrid name='Pendaftaran peserta Jambore Nasional X 2016' items={branches} />
          </div>
        </div>

        <div className='row'>
          <div className='col-lg-12'>

          </div>
        </div>
      </div>
    );
  }
}

const actionCreators = Object.assign({}, commonActions, usersDashboardPageActions);

const mapStateToProps = (state) => {
  return state;
};

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(actionCreators, dispatch)
  };
};

let connector = connect(
  mapStateToProps,
  mapDispatchToProps
);

export default connector(DashboardPageContainer);
