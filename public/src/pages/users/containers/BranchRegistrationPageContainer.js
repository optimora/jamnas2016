import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import React from 'react';

import * as commonActions from '../../../actions/common/actions';
import * as usersBranchRegistrationPageActions from '../actions/usersBranchRegistrationPageActions';

import HeaderContent from '../components/HeaderContent';
import BranchContactForm from '../components/BranchContactForm';
import TotalBranchParticipantsForm from '../components/TotalBranchParticipantsForm';
import TotalDisabledParticipantsForm from '../components/TotalDisabledParticipantsForm';
import TotalHeadBranchForm from '../components/TotalHeadBranchForm';
import SubmitButton from '../../../components/common/forms/SubmitButton';

class BranchRegistrationPageContainer extends React.Component {
    static get propTypes() {
        return {
            BranchRegistrationPage: React.PropTypes.object.isRequired,
            actions: React.PropTypes.object.isRequired
        };
    }

    _handleSubmit(event) {
        event.preventDefault();
        this.props.actions.create(this.props.BranchRegistrationPage.data);
    }

    _handleFieldValueChanges(name, value) {
        this.props.actions.formUpdateValue(name, value);
    }

    render() {
        return (
            <div>
                <div className="row wrapper border-bottom white-bg page-heading padding-bottom-xs">
                    <div className="col-lg-12">
                        <HeaderContent title={user.branch.name} subtitle="Formulir D.02 - Pendaftaran Kwartir Daerah"
                                       logo={'/public/assets/images/branch_logo/' + user.branch.logo}/>
                    </div>
                </div>
                <div className="wrapper wrapper-content">
                    <div className="col-lg-10">
                        <div className="row">
                            <div className="col-lg-12">
                                <BranchContactForm title="FORMULIR REGISTRASI KWARTIR DAERAH" isForm="true"
                                                   prefix="branch_registration" />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12">
                                <TotalBranchParticipantsForm isForm="true" data={this.props.BranchRegistrationPage.data}/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12">
                                <TotalDisabledParticipantsForm isForm="true" data={this.props.BranchRegistrationPage.data}/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12">
                                <TotalHeadBranchForm isForm="true" data={this.props.BranchRegistrationPage.data}/>
                            </div>
                        </div>
                        <div className="row margin-bottom-xlg">
                            <div className="col-lg-12">
                                <div className='ibox float-e-margins'>
                                    <div className='ibox-content'>
                                        <div className='col-lg-12 margin-top-lg'>
                                            <div className='col-lg-12'>
                                                <p className='bold-700'>{this.props.attachment}</p>
                                                <input type='file' name={this.props.prefix + '.file.name'}></input>
                                                <input type='hidden' name={this.props.prefix +'.file.type'} value={this.props.filevalue}></input>
                                            </div>
                                        </div>
                                        <div className="col-lg-12 m-md ">
                                            <div className="hr-line-dashed no-margin"></div>
                                        </div>
                                        <div className='col-lg-6 col-lg-offset-3 '>
                                            <SubmitButton
                                                label='Kirim Surat Pendaftaran'
                                                formId='formBranchRegistration'
                                                className='form-control btn btn-green bold-700'
                                                icon='fa-paper-plane'
                                                submitHandler={this._handleSubmit.bind(this)}
                                            />
                                        </div>

                                        <div style={{ width: '100%', height: '1px', clear: 'both' }}></div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                    <div className="col-lg-2">

                    </div>
                </div>
            </div>
        );
    }
}

const actionCreators = Object.assign({}, commonActions, usersBranchRegistrationPageActions);

const mapStateToProps = (state) => {
    return state;
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(actionCreators, dispatch)
    };
};

let connector = connect(
    mapStateToProps,
    mapDispatchToProps
);

export default connector(BranchRegistrationPageContainer);

