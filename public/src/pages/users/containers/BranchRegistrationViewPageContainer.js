import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import React from 'react';
import ReactDom from 'react-dom';

import * as commonActions from '../../../actions/common/actions';
import * as usersBranchRegistrationPageActions from '../actions/usersBranchRegistrationPageActions';

import HeaderContent from '../components/HeaderContent';
import BranchContactForm from '../components/BranchContactForm';
import TotalBranchParticipantsForm from '../components/TotalBranchParticipantsForm';
import TotalDisabledParticipantsForm from '../components/TotalDisabledParticipantsForm';
import TotalHeadBranchForm from '../components/TotalHeadBranchForm';
import SubmitButton from '../components/SubmitButton';

class BranchRegistrationViewPageContainer extends React.Component {
    static get propTypes() {
        return {
            BranchRegistrationPage: React.PropTypes.object.isRequired,
            actions: React.PropTypes.object.isRequired
        };
    }

    render() {
        
        return(
            <div>
                <div className="row wrapper border-bottom white-bg page-heading padding-bottom-xs">
                    <div className="col-lg-12">
                        <HeaderContent title={user.branch.name} subtitle="Formulir D.02 - Pendaftaran Kwartir Daerah" logo={'/public/assets/images/branch_logo/' + user.branch.logo}/>
                    </div>
                </div>
                <div className="wrapper wrapper-content">
                    <div className="col-lg-10">
                        <div className="row">
                            <div className="col-lg-12">
                                <BranchContactForm title="FORMULIR REGISTRASI KWARTIR DAERAH" isForm ="false" prefix="branch_registration" data={this.props.BranchRegistrationPage.data}/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12">
                                <TotalBranchParticipantsForm isForm ="false" data={this.props.BranchRegistrationPage.data}/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12">
                                <TotalDisabledParticipantsForm isForm ="false" data={this.props.BranchRegistrationPage.data}/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12">
                                <TotalHeadBranchForm isForm ="false" data={this.props.BranchRegistrationPage.data}/>
                            </div>
                        </div>
                        <div className="row margin-bottom-xlg">
                            <div className="col-lg-12">
                                <SubmitButton attachment="Lampirkan bukti otentik formulir Registrasi Kwartir Daerah D.02" prefix="branch_registration" filevalue="kwarda-registration-letter"/>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-2">

                    </div>
                </div>
            </div>
        );
    }
}

const actionCreators = Object.assign({}, commonActions, usersBranchRegistrationPageActions);

const mapStateToProps = (state) => {
    return state;
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(actionCreators, dispatch)
    };
};

let connector = connect(
    mapStateToProps,
    mapDispatchToProps
);

export default connector(BranchRegistrationViewPageContainer);

