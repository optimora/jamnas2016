import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import React from 'react';

import * as commonActions from '../../../actions/common/actions';
import * as usersAddPageActions from '../actions/usersAddPageActions';

import AddForm from '../components/AddForm';

class AddPageContainer extends React.Component {
  static get propTypes() {
    return {
      AddPage: React.PropTypes.object,
      actions: React.PropTypes.object
    };
  }

  _handleSubmit() {
    console.log(this.props);
    this.props.actions.addUser(this.props.AddPage.user);
  }

  _handleFormChange(fieldName, value) {
    this.props.actions.formUpdateValue(fieldName, value);
  }

  render() {
    return (
      <div id="addPageComponents">
        <AddForm data={this.props.AddPage} onChange={this._handleFormChange.bind(this)} submitHandler={this._handleSubmit.bind(this)} />
      </div>
    );
  }
}

const actionCreators = Object.assign({}, commonActions, usersAddPageActions);

const mapStateToProps = (state) => {
  return state;
};

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(actionCreators, dispatch)
  };
};

let connector = connect(
  mapStateToProps,
  mapDispatchToProps
);

export default connector(AddPageContainer);
