import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import React from 'react';

import * as commonActions from '../../../actions/common/actions';
import * as usersLoginPageActions from '../actions/usersLoginPageActions';

import LoginForm from '../components/LoginForm';

class LoginPageContainer extends React.Component {
  static get propTypes() {
    return {
      LoginPage: React.PropTypes.object,
      actions: React.PropTypes.object
    }
  }

  _handleSubmit() {
    console.log(this.props);
    this.props.actions.login(this.props.LoginPage.user);
  }

  _handleFieldValueChanges(fieldName, value) {
    this.props.actions.formUpdateValue(fieldName, value);
  }

  render() {
    let data = {
      message: this.props.LoginPage.message,
      messageType: this.props.LoginPage.messageType
    };
    return (
      <div id='loginPageComponents'>
        <LoginForm data={data} submitHandler={this._handleSubmit.bind(this)} onChange={this._handleFieldValueChanges.bind(this)} />
      </div>
    );
  }
}

const actionCreators = Object.assign({}, commonActions, usersLoginPageActions);

const mapStateToProps = (state) => {
  return state;
};

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(actionCreators, dispatch)
  };
};

let connector = connect(
  mapStateToProps,
  mapDispatchToProps
);

export default connector(LoginPageContainer);
