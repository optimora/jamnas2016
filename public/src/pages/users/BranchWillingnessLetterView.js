import { Provider } from 'react-redux';
import { applyMiddleware, combineReducers, createStore } from 'redux';

import React from 'react';
import ReactDom from 'react-dom';
import thunk from 'redux-thunk';

import * as usersBranchWillingnessLetterActions from './actions/usersBranchWillingnessLetterActions';

import Page from '../Page';
import HeaderContent from './components/HeaderContent';
import KwardaWillingness from './components/BranchWillingnessLetterView';
import usersBranchWillingnessLetterReducers from './reducers/usersBranchWillingnessLetterReducers';

export default class BranchWillingnessLetter extends Page {
  constructor(selectors) {
    super(selectors);
  }

  _createReducers() {
    return combineReducers({
      BranchWillingnessLetter: usersBranchWillingnessLetterReducers
    });
  }

  _createStore() {
    let reducers = this._createReducers();
    return createStore(reducers, applyMiddleware(thunk));
  }
  
  _initialize() {
    this._store.dispatch(usersBranchWillingnessLetterActions.filter_branch(window.currentPageData.user.branch._id));
  }

  setup() {
    this._store = this._createStore();
    this.render(
      <Provider store={this._store}>
        <HeaderContent subtitle='Formulir D.01 - Kesediaan Kwartir Cabang' title={window.currentPageData.user.branch.name} logo={'../../public/assets/images/branch_logo/' + user.branch.logo}/>
      </Provider>,
      $(this._selectors.header)[0]
    );

    this.render(
      <Provider store={this._store}>
        <KwardaWillingness />
      </Provider>,
      $(this._selectors.kwardaWillingness)[0]
    );

    this._initialize();
  }
};
