import React from 'react';
import ReactDom from 'react-dom';

import HeaderContent from './components/HeaderContent';
import BranchTransportsTable from './components/BranchTransportsTable';

export default class BranchTransports {
  constructor(selectors) {
    this._selectors = selectors;
  }

  setup() {
    ReactDom.render(
      <div>
        <HeaderContent subtitle='Daftar Data Keberangkatan Kedatangan dan Kepulangan' title="Kwartir Daerah"/>
      </div>,
      $(this._selectors.header)[0]
    );

    ReactDom.render(
      <div>
        <BranchTransportsTable />
      </div>,
      $(this._selectors.branch_transport_list)[0]
    );

  }
};
