import React from 'react';
import ReactDom from 'react-dom';

import HeaderContent from './components/HeaderContent';
import CommitteesTable from './components/CommitteesTable';

export default class CommitteesList {
  constructor(selectors) {
    this._selectors = selectors;
  }

  setup() {
    ReactDom.render(
      <div>
        <HeaderContent subtitle='Jambore Nasional X 2016' title="Panitia"/>
      </div>,
      $(this._selectors.header)[0]
    );

    ReactDom.render(
      <div>
        <CommitteesTable />
      </div>,
      $(this._selectors.committee_list)[0]
    );

  }
};
