import React from 'react';
import ReactDom from 'react-dom';

import HeaderContent from './components/HeaderContent';
import KwarcabWillingness from './components/SubBranchWillingnessLetter';

export default class SubBranchWillingnessLetter {
  constructor(selectors) {
    this._selectors = selectors;
  }

  setup() {
    ReactDom.render(
      <div>
        <HeaderContent subtitle='Formulir C.01 - Kesediaan Kwartir Cabang' title={user.branch.name} logo={'/public/assets/images/branch_logo/' + user.branch.logo}/>
      </div>,
      $(this._selectors.header)[0]
    );

    ReactDom.render(
      <div>
        <KwarcabWillingness />
      </div>,
      $(this._selectors.kwarcabWillingness)[0]
    );

  }
};
