import React from 'react';
import ReactDom from 'react-dom';
import _ from 'underscore';

import HeaderContent from './components/HeaderContent';
import BasicForm from './components/BasicForm';
import AddressForm from './components/AddressForm';
import ParentForm from './components/ParentForm';
import EducationForm from './components/EducationForm';
import CourseForm from './components/CourseForm';
import DetailedScoutingForm from './components/DetailedScoutingForm';
import MedicalForm from './components/MedicalForm';



export default class ParticipantViewPage {
  constructor(selectors, data) {
    this._selectors = selectors;
    this._data = data;
  }

  setup() {
    let experienceLevel =  _.groupBy(this._data.scout.experiences, function(num){ return num.level; });

    ReactDom.render(
        <div>
          <div className="row wrapper border-bottom white-bg page-heading padding-bottom-xs">
            <div className="col-lg-12">
              <HeaderContent subtitle='Data Peserta Jambore Nasional X 2016' title={user.branch.name} logo={'/public/assets/images/branch_logo/' + user.branch.logo}/>
            </div>
          </div>
          <div className="wrapper wrapper-content">
            <div className="col-lg-10">
              <div className="row">
                <div className="col-lg-12">
                  <BasicForm title="BIODATA PESERTA JAMBORE NASIONAL X 2016" isForm="false" data={this._data.scout}/>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-12">
                  <AddressForm isForm="false" data={this._data.scout.contact[0]}/>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-12">
                  <ParentForm isForm="false" data={this._data.scout.parent_contacts[0]}/>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-12">
                  <EducationForm isForm="false" items={this._data.scout.educations}/>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-12">
                  <DetailedScoutingForm data={ _.first(experienceLevel.siaga) }/>

                </div>
              </div>
              <div className="row">
                <div className="col-lg-12">
                  <DetailedScoutingForm data={  _.first(experienceLevel.penggalang) }/>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-12">
                  <CourseForm title="Data Kursus atau Pelatihan Kepramukaan" isForm="false" isFixedName="true" items={ this._data.scout.courses }/>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-12">
                  <CourseForm title="Data Kursus atau Pelatihan Di Luar Kepramukaan" isForm="false" items={ this._data.scout.external_courses }/>
                </div>
              </div>
              <div className="row margin-bottom-xlg">
                <div className="col-lg-12">
                  <MedicalForm />
                </div>
              </div>
            </div>
            <div className="col-lg-2">

            </div>
          </div>
        </div>,
        $(this._selectors.participant_view)[0]
    );
  }
};
/**
 * Created by saras on 5/18/16.
 */
