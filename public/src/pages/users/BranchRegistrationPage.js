import { Provider } from 'react-redux';
import { applyMiddleware, combineReducers, createStore } from 'redux';

import React from 'react';
import ReactDom from 'react-dom';
import thunk from 'redux-thunk';

import * as usersBranchRegistrationPageActions from './actions/usersBranchRegistrationPageActions';

import Page from '../Page';
import usersBranchRegistrationPageReducers from './reducers/usersBranchRegistrationPageReducers';

import BranchRegistrationPageContainer from './containers/BranchRegistrationPageContainer'
import ScoutBranchFormHeader from '../../components/common/ui/ScoutBranchFormHeader';

export default class BranchRegistrationPage extends Page {
    constructor(selectors) {
        super(selectors);
    }

    _createReducers() {
        return combineReducers({
            BranchRegistrationPage: usersBranchRegistrationPageReducers
        });
    }

    _createStore() {
        let reducers = this._createReducers();
        return createStore(reducers, applyMiddleware(thunk));
    }

    _initialize() {
        //this._store.dispatch(usersBranchRegistrationPageActions.initialize(window.currentPageData['branch']['_id']));
    }

    setup() {
        this._store = this._createStore();
        this.render(
            <div>
                <Provider store={this._store}>
                    <BranchRegistrationPageContainer />
                </Provider>
            </div>,
            $(this._selectors.branch_registration)[0]
        );
        this._initialize();
    }
}
