import * as CommonAction from '../../../actions/common/actions';
import * as CommonActionType from '../../../actions/common/actionTypes';
import * as BranchWillingnessLetterActionType from './usersBranchWillingnessLetterActionTypes.js';

import BranchWillingnessLetterService from '../../../services/BranchWillingnessLetterService';

var service = new BranchWillingnessLetterService();

export const updateBranchWillingnessLetter = (data) => {
    return {
        type: BranchWillingnessLetterActionType.UPDATE_BRANCH_WILLINGNESS_LETTER,
        data
    };
};

// Async Actions

export const create = (data) => {
  return (dispatch) => {
    service.create(
      data,
      (data) => {
        dispatch(CommonAction.redirect('/branch/willingness_letter'));
      },
      (data) => {
        dispatch(CommonAction.showMessage('danger', (data.responseJSON || {}).message));
      }
    );
  };
};

export const filter_branch = (id) => {
  return (dispatch) => {
    service.filter_branch(
        id,
        (data) => {
          if(data.result == null) dispatch(CommonAction.redirect('/branch/willingness_letter/add'));
          else dispatch(updateBranchWillingnessLetter(data.result));

        },
        (data) => {
          dispatch(CommonAction.showMessage('danger', (data.responseJSON || {}).message));
        }
    )
  }
};
