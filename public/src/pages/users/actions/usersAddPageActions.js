import * as Action from '../actions/usersAddPageActions';
import * as CommonAction from '../../../actions/common/actions';

import ScoutBranchService from '../../../services/ScoutBranchService';
import UserService from '../../../services/UserService';

var userService = new UserService();

// Async Actions

export const addUser = (user) => {
  return (dispatch) => {
    userService.addUser(
      user,
      (data) => {
        dispatch(CommonAction.showMessage('success', 'Berhasil.'));
      },
      (data) => {
        dispatch(CommonAction.showMessage('danger', (data.responseJSON || {}).message));
      }
    );
  };
};
