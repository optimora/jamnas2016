import * as CommonAction from '../../../actions/common/actions';
import * as CommonActionType from '../../../actions/common/actionTypes';

import UserService from '../../../services/UserService';

var service = new UserService();

// Async Actions

export const login = (data) => {
  return (dispatch) => {
    service.authenticate(
      data,
      (data) => {
        dispatch(CommonAction.redirect('/users/dashboard'));
      },
      (data) => {
        dispatch(CommonAction.showMessage('danger', (data.responseJSON || {}).message));
      }
    );
  };
};
