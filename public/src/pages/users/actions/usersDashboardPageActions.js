import * as CommonAction from '../../../actions/common/actions';
import * as CommonActionType from '../../../actions/common/actionTypes';
import * as DashboardPageActionType from './usersDashboardPageActionTypes.js';

import ScoutBranchService from '../../../services/ScoutBranchService';
import UserService from '../../../services/UserService';

var scoutBranchService = new ScoutBranchService();
var userService = new UserService();

// General Actions

export const updateBranches = (branches) => {
  return {
    type: DashboardPageActionType.UPDATE_BRANCHES,
    branches
  };
};

export const updateStats = (stats) => {
  return {
    type: DashboardPageActionType.UPDATE_STATS,
    stats
  };
};

export const updateUsers = (users) => {
  return {
    type: DashboardPageActionType.UPDATE_USERS,
    users
  };
};

// Async Actions

export const initialize = (id) => {
  return (dispatch) => {
    // TODO: Get from server.
    dispatch(updateStats({
      numberOfScouts: 0,
      numberOfScoutBranches: 0,
      numberOfScoutSubBranches: 0,
      numberOfCommittees: 0
    }));

    scoutBranchService.getChildren(
      id,
      (data) => {
        dispatch(updateBranches(data.results));
      },
      (data) => {
        console.log(data);
      }
    );
  };
};

export const search = (query) => {

};