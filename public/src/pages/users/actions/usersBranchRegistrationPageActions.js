import * as CommonAction from '../../../actions/common/actions';
import * as CommonActionType from '../../../actions/common/actionTypes';
import * as BranchRegistrationPageActionType from './usersBranchRegistrationPageActionTypes';

import BranchRegistrationService from '../../../services/BranchRegistrationService';

var service = new BranchRegistrationService();

// General Actions

export const update = (data) => {
  return {
    type: BranchRegistrationPageActionType.UPDATE,
    data
  };
};

// Async Actions

export const create = (data) => {
  return (dispatch) => {
    service.create(
        data,
        (data) => {
          dispatch(CommonAction.redirect('/branch/registration'));
        },
        (data) => {
          dispatch(CommonAction.showMessage('danger', (data.responseJSON || {}).message));
        }
    );
  };
};

export const filter_branch = (id) => {
  return (dispatch) => {
    service.filter_branch(
        id,
        (data) => {
          if(data.result == null) dispatch(CommonAction.redirect('/branch/registration/add'));
          else dispatch(update(data.result));

        },
        (data) => {
          dispatch(CommonAction.showMessage('danger', (data.responseJSON || {}).message));
        }
    )
  }
};