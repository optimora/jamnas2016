import React from 'react';
import ReactDom from 'react-dom';

import HeaderContent from './components/HeaderContent';
import BranchParticipantsTable from './components/BranchParticipantsTable';

export default class BranchParticipants {
  constructor(selectors) {
    this._selectors = selectors;
  }

  setup() {
    ReactDom.render(
      <div>
        <HeaderContent subtitle='Jambore Nasional X 2016' title="Peserta Berdasarkan Kwartir Daerah"/>
      </div>,
      $(this._selectors.header)[0]
    );

    ReactDom.render(
      <div>
        <BranchParticipantsTable />
      </div>,
      $(this._selectors.branch_participant_list)[0]
    );

  }
};
