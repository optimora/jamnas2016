import React from 'react';
import ReactDom from 'react-dom';

import HeaderContent from './components/HeaderContent';
import BranchPaymentConfirmationsTable from './components/BranchPaymentConfirmationsTable';

export default class BranchPaymentConfirmations {
  constructor(selectors) {
    this._selectors = selectors;
  }

  setup() {
    ReactDom.render(
      <div>
        <HeaderContent subtitle='Daftar Data Konfirmasi Pembayaran' title="Kwartir Daerah"/>
      </div>,
      $(this._selectors.header)[0]
    );

    ReactDom.render(
      <div>
        <BranchPaymentConfirmationsTable />
      </div>,
      $(this._selectors.branch_payment_confirmation_list)[0]
    );

  }
};
