import * as CommonActionType from '../../../actions/common/actionTypes';
import * as BranchWillingnessLetterActionType from '../actions/usersBranchWillingnessLetterActionTypes.js';
import opath from 'object-path';

/**
* State Schemea
 branch              : null,
 subbranch           : null,
 address             : '',
 zipcode             : '',
 phone               : '',
 mobile_phone        : '',

 number_of           : {
      penggalang_pa     : null,
      penggalang_pi     : null,
      pbk_pa            : null,
      pbk_pi            : null,
      jumlah_peserta    : null,
      bindamping_pa     : null,
      bindamping_pi     : null,
      pinkonda          : null,
      pinkoncab         : null,
      jumlah_pimpinan   : null
    },

 sender_name         : null,
 file                : null,

*/
const initialState = {
  data: {
    branch              : undefined,
    subbranch           : undefined,
    address             : '',
    zipcode             : '',
    phone               : '',
    mobile_phone        : '',

    number_of           : {
      penggalang_pa     : 0,
      penggalang_pi     : 0,
      pbk_pa            : 0,
      pbk_pi            : 0,
      jumlah_peserta    : 0,
      bindamping_pa     : 0,
      bindamping_pi     : 0,
      pinkonda          : 0,
      pinkoncab         : 0,
      jumlah_pimpinan   : 0
    },

    sender_name         : '',
    file                : undefined
  },
  message: null,
  messageType: null

};

export default function usersBranchWillingnessLetterReducers(state = initialState, action) {
  switch (action.type) {
    case BranchWillingnessLetterActionType.UPDATE_BRANCH_WILLINGNESS_LETTER:
      return Object.assign({}, state, { data: action.data });

    case CommonActionType.SHOW_MESSAGE:
      return Object.assign({}, state, {
        messageType: action.messageType,
        message: action.message
      });
    case CommonActionType.FORM_UPDATE_VALUE:
        opath.set(state.data, action.field, action.value);

        /*
        if(action.field == 'number_of.penggalang_pa' || action.field == 'number_of.penggalang_pi' || action.field == 'number_of.pbk_pa' || action.field == 'number_of.pbk_pi' ) {
          opath.set(state.data, 'number_of.jumlah_peserta', parseInt(state.data.number_of.penggalang_pa) + parseInt(state.data.number_of.penggalang_pi) + parseInt(state.data.number_of.pbk_pa) + parseInt(state.data.number_of.pbk_pa));

        } else if(action.field == 'bindamping_pa' || action.field == 'bindamping_pi' || action.field == 'pinkonda' || action.field == 'pinkoncab' ) {

        }
        */

      return state;

    case CommonActionType.REDIRECT:
      // Redirect.
      window.location.assign(window.location.origin + action.url);
    default:
      return state;
  }
};
