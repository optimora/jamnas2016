import * as CommonActionType from '../../../actions/common/actionTypes';
import * as BranchRegistrationPageActionType from '../actions/usersBranchRegistrationPageActionTypes.js';
import opath from 'object-path';

const initialState = {
  data: {
    branch              : undefined,
    address             : '',
    province            : '',
    regency_city        : '',
    zipcode             : '',
    phone               : '',
    mobile_phone        : '',

    participants        : [
    {
      sub_branch          : undefined,
      num_male            : 0,
      num_female          : 0,
      num_male_leader     : 0,
      num_female_leader   : 0

    },
    {
      sub_branch          : undefined,
      num_male            : 0,
      num_female          : 0,
      num_male_leader     : 0,
      num_female_leader   : 0

    },
    {
      sub_branch          : undefined,
      num_male            : 0,
      num_female          : 0,
      num_male_leader     : 0,
      num_female_leader   : 0

    },
    {
      sub_branch          : undefined,
      num_male            : 0,
      num_female          : 0,
      num_male_leader     : 0,
      num_female_leader   : 0

    },
    {
      sub_branch          : undefined,
      num_male            : 0,
      num_female          : 0,
      num_male_leader     : 0,
      num_female_leader   : 0

    },
    {
      sub_branch          : undefined,
      num_male            : 0,
      num_female          : 0,
      num_male_leader     : 0,
      num_female_leader   : 0

    },
    {
      sub_branch          : undefined,
      num_male            : 0,
      num_female          : 0,
      num_male_leader     : 0,
      num_female_leader   : 0

    },
    {
      sub_branch          : undefined,
      num_male            : 0,
      num_female          : 0,
      num_male_leader     : 0,
      num_female_leader   : 0

    },
    {
      sub_branch          : undefined,
      num_male            : 0,
      num_female          : 0,
      num_male_leader     : 0,
      num_female_leader   : 0

    },
    {
      sub_branch          : undefined,
      num_male            : 0,
      num_female          : 0,
      num_male_leader     : 0,
      num_female_leader   : 0

    },
    {
      sub_branch          : undefined,
      num_male            : 0,
      num_female          : 0,
      num_male_leader     : 0,
      num_female_leader   : 0

    },
    {
      sub_branch          : undefined,
      num_male            : 0,
      num_female          : 0,
      num_male_leader     : 0,
      num_female_leader   : 0

    },
    {
      sub_branch          : undefined,
      num_male            : 0,
      num_female          : 0,
      num_male_leader     : 0,
      num_female_leader   : 0

    },
    {
      sub_branch          : undefined,
      num_male            : 0,
      num_female          : 0,
      num_male_leader     : 0,
      num_female_leader   : 0

    },
    {
      sub_branch          : undefined,
      num_male            : 0,
      num_female          : 0,
      num_male_leader     : 0,
      num_female_leader   : 0

    },
    {
      sub_branch          : undefined,
      num_male            : 0,
      num_female          : 0,
      num_male_leader     : 0,
      num_female_leader   : 0

    },
    {
      sub_branch          : undefined,
      num_male            : 0,
      num_female          : 0,
      num_male_leader     : 0,
      num_female_leader   : 0

    },
    {
      sub_branch          : undefined,
      num_male            : 0,
      num_female          : 0,
      num_male_leader     : 0,
      num_female_leader   : 0

    },
    {
      sub_branch          : undefined,
      num_male            : 0,
      num_female          : 0,
      num_male_leader     : 0,
      num_female_leader   : 0

    },
    {
      sub_branch          : undefined,
      num_male            : 0,
      num_female          : 0,
      num_male_leader     : 0,
      num_female_leader   : 0

    },
    {
      sub_branch          : undefined,
      num_male            : 0,
      num_female          : 0,
      num_male_leader     : 0,
      num_female_leader   : 0

    },
    {
      sub_branch          : undefined,
      num_male            : 0,
      num_female          : 0,
      num_male_leader     : 0,
      num_female_leader   : 0

    },
    {
      sub_branch          : undefined,
      num_male            : 0,
      num_female          : 0,
      num_male_leader     : 0,
      num_female_leader   : 0

    },
    {
      sub_branch          : undefined,
      num_male            : 0,
      num_female          : 0,
      num_male_leader     : 0,
      num_female_leader   : 0
    },
    {
      sub_branch          : undefined,
      num_male            : 0,
      num_female          : 0,
      num_male_leader     : 0,
      num_female_leader   : 0

    },
    {
      sub_branch          : undefined,
      num_male            : 0,
      num_female          : 0,
      num_male_leader     : 0,
      num_female_leader   : 0
    }],

    special_participants : {
      tunanetra             : {
        num_male              : 0,
        num_female            : 0,
        num_male_leader       : 0,
        num_female_leader   : 0
      },
      tunarungu             : {
        num_male              : 0,
        num_female            : 0,
        num_male_leader       : 0,
        num_female_leader   : 0
      },
      tunagrahita             : {
        num_male              : 0,
        num_female            : 0,
        num_male_leader       : 0,
        num_female_leader     : 0
      },
      tunadaksa             : {
        num_male              : 0,
        num_female            : 0,
        num_male_leader       : 0,
        num_female_leader     : 0
      }
    },

    head_participants: {
      pinkonda: {
        num_male              : 0,
        num_female            : 0
      },
      pinkoncab: {
        num_male              : 0,
        num_female            : 0
      }
    },

    file                : undefined
  },
  message: null,
  messageType: null
};

export default function usersBranchRegistrationPageReducers(state = initialState, action) {
  switch (action.type) {
    
    case BranchRegistrationPageActionType.UPDATE:
      return Object.assign({}, state, { data: action.data });

    case CommonActionType.SHOW_MESSAGE:
      return Object.assign({}, state, {
        messageType: action.messageType,
        message: action.message
      });

    case CommonActionType.FORM_UPDATE_VALUE:
      opath.set(state.data, action.field, action.value);

      return state;

    case CommonActionType.REDIRECT:
      // Redirect.
      window.location.assign(window.location.origin + action.url);

    default:
      return state;
  }
};
