import * as CommonActionType from '../../../actions/common/actionTypes';
import * as ActionType from '../actions/usersLoginPageActionTypes';

/**
* State Schemea
* {
*   user: {
*     email: ?string,
*     password: ?string
*   },
*   alert: null,
*   error: null
* }
*/
const initialState = {
  user: {
    email: '',
    password: ''
  },
  message: null,
  messageType: null
};

export default function usersLoginPageReducers(state = initialState, action) {
  switch (action.type) {
    case CommonActionType.SHOW_MESSAGE:
      return Object.assign({}, state, {
        messageType: action.messageType,
        message: action.message
      });
    case CommonActionType.FORM_UPDATE_VALUE:
      return Object.assign({}, state, { user: Object.assign({}, state.user, {
        [action.field]: action.value
      })});
    case CommonActionType.REDIRECT:
      // Redirect.
      window.location.assign(window.location.origin + action.url);
      return state;
    default:
      return state;
  }
};
