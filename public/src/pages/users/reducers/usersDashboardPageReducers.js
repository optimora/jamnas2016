import * as CommonActionType from '../../../actions/common/actionTypes';
import * as DashboardPageActionType from '../actions/usersDashboardPageActionTypes.js';

const initialState = {
  loading: true,
  stats: {
    numberOfScouts: 0,
    numberOfScoutBranches: 0,
    numberOfScoutSubbranches: 0,
    numberOfCommittees: 0
  },

  branches: [],
  users: []
};

export default function usersDashboardPageReducers(state = initialState, action) {
  switch (action.type) {
    case DashboardPageActionType.UPDATE_BRANCHES:
      console.log(Object.assign({}, state, { branches: action.branches }));
      return Object.assign({}, state, { branches: action.branches });
    case DashboardPageActionType.UPDATE_USERS:
      return Object.assign({}, state, { users: action.users });
    case DashboardPageActionType.UPDATE_STATS:
      return Object.assign({}, state, { stats: Object.assign({}, state.stats, action.stats) });
    default:
      return state;
  }
};
