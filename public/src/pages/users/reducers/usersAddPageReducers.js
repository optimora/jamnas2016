import * as CommonActionType from '../../../actions/common/actionTypes';
import * as ActionType from '../actions/usersAddPageActions';

const initialState = {
  message: null,
  messageType: null,
  user: {
    name: '',
    email: '',
    password: '',
    branch: '',
    role: ''
  },
  branches: null,
  subBranches: null
};

export default function usersAddPageReducers(state = initialState, action) {
  switch (action.type) {
    case CommonActionType.FORM_UPDATE_VALUE:
      let updatedData = {};
      updatedData[action.field] = action.value;
      return Object.assign({}, state, { user: Object.assign({}, state.user, updatedData) });
    case CommonActionType.FORM_RESET:
      let emptyData = {
        name: '',
        email: '',
        password: '',
        role: '',
        branch: ''
      };
      return Object.assign({}, state, { user: Object.assign({}, state.user, emptyData) });
    case CommonActionType.SHOW_MESSAGE:
      return Object.assign({}, state, { message: action.message, messageType: action.messageType });
    case CommonActionType.REDIRECT:
      // Redirect.
      window.location.url = window.location.origin + action.url;
      return;
    case ActionType.RESET_BRANCH:
      return Object.assign({}, state, { branches: null });
    case ActionType.RESET_SUB_BRANCH:
      return Object.assign({}, state, { subBranches: null });
    case CommonActionType.SCOUT_BRANCH_UPDATE:
      return Object.assign({}, state, { branches: action.branches });
    default:
      return state;
  }
};
