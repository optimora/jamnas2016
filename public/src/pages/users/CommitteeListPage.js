import React from 'react';
import ReactDom from 'react-dom';

import HeaderContent from './components/HeaderContent';
import DataTable from './components/DataTable';

export default class CommitteeListPage {
    constructor(selectors) {
        this._selectors = selectors;
    }

    setup() {
        ReactDom.render(
            <div>
                <div className="row wrapper border-bottom white-bg page-heading padding-bottom-xs">
                    <div className="col-lg-12">
                        <HeaderContent title="Daftar Panitia Penyelenggara" subtitle="Jambore Nasional X 2016"/>
                    </div>
                </div>
                <div className="wrapper wrapper-content">
                    <div className="row">
                        <div className="col-lg-12">
                            <DataTable title="Tabel Data Panitia Penyelenggara" jobs="Panitia JAMNAS X 2016" link="/committees/add" item_prefix_link="/committees/123/view"/>
                        </div>
                    </div>
                </div>
            </div>,
            $(this._selectors.committee_list)[0]
        );
    }
};
/**
 * Created by saras on 5/18/16.
 */
