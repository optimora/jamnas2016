import React from 'react';

export default class BranchTransportsTable extends React.Component {
  render() {
    return(
      <div className='row'>
        <div className='col-lg-12'>
          <div className="ibox float-e-margins">
            <div className="ibox-title">
              <h5>Data Keberangkatan Kedatangan dan Kepulangan</h5>
            </div>
            <div className="ibox-content">
              <div className="row margin-bottom-xs">
                <div className="col-lg-4 pull-left">
                  <p className="padding-top-bottom-xs no-margin">Showing 1 to 5 of 5 entries</p>
                </div>
                <div className="col-lg-3 pull-right">
                  <div className="input-group">
                    <input type="text" placeholder="Search" className="input-sm form-control">
                                        <span className="input-group-btn">
                                            <button type="button" className="btn btn-sm btn-primary"> Go!</button>
                                        </span>
                    </input>
                  </div>
                </div>
              </div>
              <div className="table-responsive">
                <table className="table table-striped table-bordered table-hover dataTables-example" >
                  <thead>
                  <tr>
                    <th className="align-center">#</th>
                    <th className="align-center">Kwartir Daerah</th>
                    <th className="align-center">Jumlah Kontingen</th>
                    <th className="align-center">Tanggal Keberangkatan</th>
                    <th className="align-center">Tanggal Kedatangan</th>
                    <th className="align-center">Tanggal Kepulangan</th>
                    <th className="align-center">Detail</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td>1</td>
                    <td>DKI Jakarta</td>
                    <td className="align-center">189</td>
                    <td className="align-center">22-06-16</td>
                    <td className="align-center">23-06-16</td>
                    <td className="align-center">27-06-16</td>
                    <td className="align-center"><a href="#" className="btn btn-success btn-xs no-margin margin-right-xs">Lihat Detail Data</a></td>
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>Aceh</td>
                    <td className="align-center">56</td>
                    <td className="align-center">23-06-16</td>
                    <td className="align-center">23-06-16</td>
                    <td className="align-center">27-06-16</td>
                    <td className="align-center"><a href="#" className="btn btn-success btn-xs no-margin margin-right-xs">Lihat Detail Data</a></td>
                  </tr>
                  <tr>
                    <td>3</td>
                    <td>DI Yogyakarta</td>
                    <td className="align-center">76</td>
                    <td className="align-center">21-06-16</td>
                    <td className="align-center">23-06-16</td>
                    <td className="align-center">27-06-16</td>
                    <td className="align-center"><a href="#" className="btn btn-success btn-xs no-margin margin-right-xs">Lihat Detail Data</a></td>
                  </tr>
                  <tr>
                    <td>4</td>
                    <td>Lampung</td>
                    <td className="align-center">43</td>
                    <td className="align-center">22-06-16</td>
                    <td className="align-center">23-06-16</td>
                    <td className="align-center">27-06-16</td>
                    <td className="align-center"><a href="#" className="btn btn-success btn-xs no-margin margin-right-xs">Lihat Detail Data</a></td>
                  </tr>
                  <tr>
                    <td>5</td>
                    <td>Bali</td>
                    <td className="align-center">31</td>
                    <td className="align-center">22-06-16</td>
                    <td className="align-center">22-06-16</td>
                    <td className="align-center">27-06-16</td>
                    <td className="align-center"><a href="#" className="btn btn-success btn-xs no-margin margin-right-xs">Lihat Detail Data</a></td>
                  </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>

    );
  }
};
