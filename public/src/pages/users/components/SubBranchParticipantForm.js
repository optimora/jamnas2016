import React from 'react';

export default class TotalDisabledParticipantsForm extends React.Component {
    render() {
        return(
            <div className='ibox float-e-margins'>
                <div className='ibox-title'>
                    <h5>Daftar Peserta</h5>
                    <div className='ibox-tools'>
                        <a className='collapse-link'>
                            <i className='fa fa-chevron-up'></i>
                        </a>
                    </div>
                </div>
                <div className='ibox-content'>
                    <div className="col-lg-12">

                        <div className='col-lg-12 margin-bottom-xs'>
                            <div className='col-lg-3 no-margin no-padding align-center'>
                                <label className='control-label align-left margin-bottom-xxs'>Regu</label>
                            </div>
                            <div className='col-lg-5 align-center'>
                                <label className='control-label align-left margin-bottom-xxs'>Nama</label>
                            </div>
                            <div className='col-lg-4 align-center'>
                                <label className='control-label align-left margin-bottom-xxs'>Pencapaian TKU</label>
                            </div>
                        </div>

                        <div className='col-lg-12 margin-bottom-xxs'>
                            <div className='col-lg-3 align-center no-padding'>
                                <p className='control-label align-center margin-top-xxs'>Pimpinan Regu</p>
                            </div>
                            <div className='col-lg-5 no-padding'>
                                {
                                    this.props.isForm == "true"?
                                        <input type='text' className='form-control align-center' name='subbranch_registration.leader_group.name' placeholder=""></input> :
                                        <p className="align-center margin-top-xxs">Sri Wahyuni</p>
                                }
                            </div>
                            <div className='col-lg-4'>
                                <div className="row">
                                    <div className="col-lg-6 margin-top-xxs">
                                        <label className='control-label align-center margin-bottom-xxs pull-right'>Tingkat SKU</label>
                                    </div>
                                    <div className="col-lg-6">
                                        {
                                            this.props.isForm == "true"?
                                                <select className='form-control m-b no-margin' name='subbranch_registration.leader_group.sku_level'>
                                                    <option values=''>Ramu</option>
                                                    <option values=''>Rakit</option>
                                                    <option values=''>Terap</option>
                                                    <option values=''>Garuda</option>
                                                </select> :
                                                <p className="align-center margin-top-xxs">Garuda</p>
                                        }

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className='col-lg-12 margin-bottom-xxs'>
                            <div className='col-lg-3 align-center no-padding'>
                                <p className='control-label align-center margin-top-xxs'>Wakil Pimpinan Regu</p>
                            </div>
                            <div className='col-lg-5 no-padding'>
                                {
                                    this.props.isForm == "true"?
                                        <input type='text' className='form-control align-center' name='subbranch_registration.vice_leader_group.name' placeholder=""></input> :
                                        <p className="align-center margin-top-xxs">Muhammad Fajar</p>
                                }

                            </div>
                            <div className='col-lg-4'>
                                <div className="row">
                                    <div className="col-lg-6 margin-top-xxs">
                                        <label className='control-label align-center margin-bottom-xxs pull-right'>Tingkat SKU</label>
                                    </div>
                                    <div className="col-lg-6">
                                        {
                                            this.props.isForm == "true"?
                                                <select className='form-control m-b no-margin' name='subbranch_registration.vice_leader_group.sku_level'>
                                                    <option values=''>Ramu</option>
                                                    <option values=''>Rakit</option>
                                                    <option values=''>Terap</option>
                                                    <option values=''>Garuda</option>
                                                </select> :
                                                <p className="align-center margin-top-xxs">Terap</p>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className='col-lg-12 margin-bottom-xxs'>
                            <div className='col-lg-3 align-center no-padding'>
                                <p className='control-label align-center margin-top-xxs'>Anggota</p>
                            </div>
                            <div className='col-lg-5 no-padding'>
                                {
                                    this.props.isForm == "true"?
                                        <input type='text' className='form-control align-center' name='subbranch_registration.members.name' placeholder=""></input> :
                                        <p className="align-center margin-top-xxs">Dwi Hapsari</p>
                                }
                            </div>
                            <div className='col-lg-4'>
                                <div className="row">
                                    <div className="col-lg-6 margin-top-xxs">
                                        <label className='control-label align-center margin-bottom-xxs pull-right'>Tingkat SKU</label>
                                    </div>
                                    <div className="col-lg-6">
                                        {
                                            this.props.isForm == "true"?
                                                <select className='form-control m-b no-margin' name='subbranch_registration.members.sku_level'>
                                                    <option values=''>Ramu</option>
                                                    <option values=''>Rakit</option>
                                                    <option values=''>Terap</option>
                                                    <option values=''>Garuda</option>
                                                </select> :
                                                <p className="align-center margin-top-xxs">Ramu</p>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className='col-lg-12 margin-bottom-xxs'>
                            <div className='col-lg-3 align-center no-padding'>
                                <p className='control-label align-center margin-top-xxs'>Anggota</p>
                            </div>
                            <div className='col-lg-5 no-padding'>
                                {
                                    this.props.isForm == "true"?
                                        <input type='text' className='form-control align-center' name='subbranch_registration.members.name' placeholder=""></input> :
                                        <p className="align-center margin-top-xxs">Ajeng Wulandari</p>
                                }
                            </div>
                            <div className='col-lg-4'>
                                <div className="row">
                                    <div className="col-lg-6 margin-top-xxs">
                                        <label className='control-label align-center margin-bottom-xxs pull-right'>Tingkat SKU</label>
                                    </div>
                                    <div className="col-lg-6">
                                        {
                                            this.props.isForm == "true"?
                                                <select className='form-control m-b no-margin' name='subbranch_registration.members.sku_level'>
                                                    <option values=''>Ramu</option>
                                                    <option values=''>Rakit</option>
                                                    <option values=''>Terap</option>
                                                    <option values=''>Garuda</option>
                                                </select> :
                                                <p className="align-center margin-top-xxs">Ramu</p>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className='col-lg-12 margin-bottom-xxs'>
                            <div className='col-lg-3 align-center no-padding'>
                                <p className='control-label align-center margin-top-xxs'>Anggota</p>
                            </div>
                            <div className='col-lg-5 no-padding'>
                                {
                                    this.props.isForm == "true"?
                                        <input type='text' className='form-control align-center' name='subbranch_registration.members.name' placeholder=""></input> :
                                        <p className="align-center margin-top-xxs">Agung Wicaksana</p>
                                }
                            </div>
                            <div className='col-lg-4'>
                                <div className="row">
                                    <div className="col-lg-6 margin-top-xxs">
                                        <label className='control-label align-center margin-bottom-xxs pull-right'>Tingkat SKU</label>
                                    </div>
                                    <div className="col-lg-6">
                                        {
                                            this.props.isForm == "true"?
                                                <select className='form-control m-b no-margin' name='subbranch_registration.members.sku_level'>
                                                    <option values=''>Ramu</option>
                                                    <option values=''>Rakit</option>
                                                    <option values=''>Terap</option>
                                                    <option values=''>Garuda</option>
                                                </select> :
                                                <p className="align-center margin-top-xxs">Rakit</p>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className='col-lg-12 margin-bottom-xxs'>
                            <div className='col-lg-3 align-center no-padding'>
                                <p className='control-label align-center margin-top-xxs'>Anggota</p>
                            </div>
                            <div className='col-lg-5 no-padding'>
                                {
                                    this.props.isForm == "true"?
                                        <input type='text' className='form-control align-center' name='subbranch_registration.members.name' placeholder=""></input> :
                                        <p className="align-center margin-top-xxs">Bambang Tri</p>
                                }
                            </div>
                            <div className='col-lg-4'>
                                <div className="row">
                                    <div className="col-lg-6 margin-top-xxs">
                                        <label className='control-label align-center margin-bottom-xxs pull-right'>Tingkat SKU</label>
                                    </div>
                                    <div className="col-lg-6">
                                        {
                                            this.props.isForm == "true"?
                                                <select className='form-control m-b no-margin' name='subbranch_registration.members.sku_level'>
                                                    <option values=''>Ramu</option>
                                                    <option values=''>Rakit</option>
                                                    <option values=''>Terap</option>
                                                    <option values=''>Garuda</option>
                                                </select> :
                                                <p className="align-center margin-top-xxs">Ramu</p>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className='col-lg-12 margin-bottom-xxs'>
                            <div className='col-lg-3 align-center no-padding'>
                                <p className='control-label align-center margin-top-xxs'>Anggota</p>
                            </div>
                            <div className='col-lg-5 no-padding'>
                                {
                                    this.props.isForm == "true"?
                                        <input type='text' className='form-control align-center' name='subbranch_registration.members.name' placeholder=""></input> :
                                        <p className="align-center margin-top-xxs">Sari Anggita</p>
                                }
                            </div>
                            <div className='col-lg-4'>
                                <div className="row">
                                    <div className="col-lg-6 margin-top-xxs">
                                        <label className='control-label align-center margin-bottom-xxs pull-right'>Tingkat SKU</label>
                                    </div>
                                    <div className="col-lg-6">
                                        {
                                            this.props.isForm == "true"?
                                                <select className='form-control m-b no-margin' name='subbranch_registration.members.sku_level'>
                                                    <option values=''>Ramu</option>
                                                    <option values=''>Rakit</option>
                                                    <option values=''>Terap</option>
                                                    <option values=''>Garuda</option>
                                                </select> :
                                                <p className="align-center margin-top-xxs">Rakit</p>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className='col-lg-12 margin-bottom-xxs'>
                            <div className='col-lg-3 align-center no-padding'>
                                <p className='control-label align-center margin-top-xxs'>Anggota</p>
                            </div>
                            <div className='col-lg-5 no-padding'>
                                {
                                    this.props.isForm == "true"?
                                        <input type='text' className='form-control align-center' name='subbranch_registration.members.name' placeholder=""></input> :
                                        <p className="align-center margin-top-xxs">Hamim</p>
                                }
                            </div>
                            <div className='col-lg-4'>
                                <div className="row">
                                    <div className="col-lg-6 margin-top-xxs">
                                        <label className='control-label align-center margin-bottom-xxs pull-right'>Tingkat SKU</label>
                                    </div>
                                    <div className="col-lg-6">
                                        {
                                            this.props.isForm == "true"?
                                                <select className='form-control m-b no-margin' name='subbranch_registration.members.sku_level'>
                                                    <option values=''>Ramu</option>
                                                    <option values=''>Rakit</option>
                                                    <option values=''>Terap</option>
                                                    <option values=''>Garuda</option>
                                                </select> :
                                                <p className="align-center margin-top-xxs">Terap</p>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div style={{ width: '100%', height: '1px', clear: 'both' }}></div>
                </div>
            </div>
        );
    }
};
