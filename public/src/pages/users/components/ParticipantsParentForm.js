import React from 'react';

export default class ParticipantsParentForm extends React.Component {
  render() {
    return(
      <div className='row'>
        <div className='col-lg-12'>
          <div className='ibox float-e-margins'>
            <div className='ibox-title'>
              <h5>ORANG TUA (selain PESERTA, KOMPONEN INI TIDAK MUNCUL)</h5>
              <div className='ibox-tools'>
                  <a className='collapse-link'>
                      <i className='fa fa-chevron-up'></i>
                  </a>
              </div>
            </div>
            <div className='ibox-content'>

              <div className='col-lg-12 margin-bottom-xs'>
                <div className='col-lg-3 no-margin no-padding'>
                  <label className='control-label align-left margin-bottom-xxs'>Nama Orang Tua</label>
                </div>
                <div className='col-lg-6'>
                  <input type='text' className='form-control' name='contact.name'></input>
                </div>
              </div>

              <div className='col-lg-12 margin-bottom-xs'>
                <div className='col-lg-3 no-margin no-padding'>
                  <label className='control-label align-left margin-bottom-xxs'>Pekerjaan</label>
                </div>
                <div className='col-lg-6'>
                  <input type='text' className='form-control' name='contact.occupation'></input>
                </div>
              </div>

              <div className='col-lg-12'>
                <div className='col-lg-3 no-margin no-padding'>
                  <label className='control-label align-left margin-bottom-xxs'>Alamat Rumah</label>
                </div>
                <div className='col-lg-9'>
                  <textarea className='form-control' name='contact.address' rows='3'></textarea>
                  <div className='col-lg-5 no-padding-left margin-top-xs'>
                    <label className='control-label align-left margin-bottom-xxs'>Provinsi</label>
                    <select className='form-control m-b no-margin' name='contact.province'>
                      <option values=''>Nusa Tenggara Barat</option>
                    </select>
                  </div>
                  <div className='col-lg-5 no-padding-right margin-top-xs'>
                    <label className='control-label align-left margin-bottom-xxs'>Kota / Kabupaten</label>
                    <select className='form-control m-b no-margin' name='contact.regency_city'>
                      <option values=''>Kota Administrasi Jakarta Selatan</option>
                    </select>
                  </div>
                  <div className='col-lg-2 no-padding-right margin-top-xs'>
                    <label className='control-label align-left margin-bottom-xxs'>Kodepos</label>
                    <input type='text' className='form-control m-b no-margin' name='contact.zipcode'></input>
                  </div>
                </div>
              </div>

              <div className='col-lg-12'><div className='hr-line-dashed'></div></div>

              <div className='col-lg-12'>
                <div className='col-lg-3 no-margin no-padding'>
                  <label className='control-label align-left margin-bottom-xxs'>Kontak</label>
                </div>
                <div className='col-lg-9'>
                  <div className='col-lg-6 no-padding-left'>
                    <label className='control-label align-left margin-bottom-xxs'>No Telp. Rumah</label>
                    <div className="input-group m-b">
                      <span className="input-group-addon"><i className='glyphicon glyphicon-phone-alt'></i></span>
                      <input type='text' className='form-control m-b no-margin' name='contact.phone.home'></input>
                    </div>
                  </div>
                  <div className='col-lg-6 no-padding-right'>
                    <label className='control-label align-left margin-bottom-xxs'>No. Handphone</label>
                    <div className="input-group m-b">
                      <span className="input-group-addon"><i className='glyphicon glyphicon-phone'></i></span>
                      <input type='text' className='form-control m-b no-margin' name='contact.phone.mobile'></input>
                    </div>
                  </div>
                </div>
              </div>

              <div style={{ width: '100%', height: '1px', clear: 'both' }}></div>
            </div>
            <button className='form-control btn btn-green bold-700 uppercase' type='submit'><i className='fa fa-save margin-right-xs'></i>Simpan Data Peserta</button>
          </div>
        </div>
      </div>

    );
  }
};
