import React from 'react';

import PasswordField from '../../../components/common/forms/PasswordField';
import SimpleSelectField from '../../../components/common/forms/SimpleSelectField';
import SubmitButton from '../../../components/common/forms/SubmitButton';
import TextField from '../../../components/common/forms/TextField';

export default class AddForm extends React.Component {
  static get propTypes() {
    return {
      data: React.PropTypes.object.isRequired,

      submitHandler: React.PropTypes.func.isRequired,
      onChange: React.PropTypes.func.isRequired
    };
  }

  _handleSubmit(event) {
    event.preventDefault();
    this.props.submitHandler();
  }

  _handleFieldValueChanges(name, value) {
    this.props.onChange(name, value);
  }

  _shouldSelectBranch() {
    return ['admin', 'super-admin', 'committee-admin', '', null, undefined].indexOf(this.props.data.user.role) == -1;
  }

  render() {
    let availableRoles = ['scout-branch-admin', 'overseas-admin', 'embassy-admin', 'committee-admin', 'admin', 'super-admin'];
    let roleOptions = [];

    availableRoles.forEach((role) => {
      roleOptions.push({
        key: role,
        value: role
      })
    });

    let alert = <div></div>;
    if (this.props.data.message && this.props.data.messageType) {
      alert = <div className={'alert alert-' + this.props.data.messageType}>
        {this.props.data.message}
      </div>
    }

    let branchSelector = '';
    if (this.props.data.branches && this.props.data.branches.length > 0 && this._shouldSelectBranch()) {

      let branchOptions = [];
      this.props.data.branches.forEach((branch) => {
        branchOptions.push({
          key: branch._id,
          value: branch.name
        })
      });
      branchSelector = <SimpleSelectField name='branch' label='Kwartir Daerah' placeholder='- Pilih Kwartir Daerah -' onChangeListener={this._handleFieldValueChanges.bind(this)} options={branchOptions} />
    }

    return (
      <div>
        {alert}
        <form id='formAddUser' onSubmit={this._handleSubmit.bind(this)}>
          <TextField name='name' label='Nama' placeholder='Nama Anda.' onChangeListener={this._handleFieldValueChanges.bind(this)} />
          <TextField name='email' label='E-mail' placeholder='Email Anda.' onChangeListener={this._handleFieldValueChanges.bind(this)} />
          <PasswordField name='password' label='Password' placeholder='Password Anda.' onChangeListener={this._handleFieldValueChanges.bind(this)} />
          <SimpleSelectField name='role' label='Role' placeholder='- Pilih Level -' onChangeListener={this._handleFieldValueChanges.bind(this)} options={roleOptions} />
          {branchSelector}
          <SubmitButton clickHandler={this._handleSubmit.bind(this)} label='Tambah' formId='formAddUser' />
        </form>
      </div>
    );
  }
}
