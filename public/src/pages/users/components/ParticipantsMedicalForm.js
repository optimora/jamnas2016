import React from 'react';

export default class ParticipantsMedicalForm extends React.Component {
  render() {
    return(
      <div className='row'>
        <div className='col-lg-12'>
          <div className='ibox float-e-margins'>
            <div className='ibox-title'>
              <h5>RIWAYAT KESEHATAN</h5>
              <div className='ibox-tools'>
                  <a className='collapse-link'>
                      <i className='fa fa-chevron-up'></i>
                  </a>
              </div>
            </div>
            <div className='ibox-content'>

              <div className='col-lg-12'>
                <label className='control-label align-left text-underline padding-left-xs'>PENYAKIT YANG DIMILIKI</label>
              </div>

              <div className='col-lg-12 margin-top-xs'>
                <div className='col-lg-1 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>No</label>
                </div>
                <div className='col-lg-3 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Nama Penyakit</label>
                </div>
                <div className='col-lg-4 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Obat Pribadi yang Dimiliki</label>
                </div>
                <div className='col-lg-4 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Penanganan Medis</label>
                </div>
              </div>

              <div className='col-lg-12 margin-top-xs'>
                <div className='col-lg-1 align-center'>
                  <p className='control-label align-center margin-top-xxs'>1.</p>
                </div>
                <div className='col-lg-3 align-center'>
                  <input type='text' className='form-control' name='scout.medical_history.disease'></input>
                </div>
                <div className='col-lg-4 align-center'>
                  <input type='text' className='form-control' name='scout.medical_history.medicine'></input>
                </div>
                <div className='col-lg-4 align-center'>
                  <input type='text' className='form-control' name='scout.medical_history.treatment'></input>
                </div>
              </div>

              <div className='col-lg-12 margin-top-xs'>
                <div className='col-lg-1 align-center'>
                  <p className='control-label align-center margin-top-xxs'>2.</p>
                </div>
                <div className='col-lg-3 align-center'>
                  <input type='text' className='form-control' name='scout.medical_history.disease'></input>
                </div>
                <div className='col-lg-4 align-center'>
                  <input type='text' className='form-control' name='scout.medical_history.medicine'></input>
                </div>
                <div className='col-lg-4 align-center'>
                  <input type='text' className='form-control' name='scout.medical_history.treatment'></input>
                </div>
              </div>

              <div className='col-lg-12 margin-top-xs'>
                <div className='col-lg-1 align-center'>
                  <p className='control-label align-center margin-top-xxs'>3.</p>
                </div>
                <div className='col-lg-3 align-center'>
                  <input type='text' className='form-control' name='scout.medical_history.disease'></input>
                </div>
                <div className='col-lg-4 align-center'>
                  <input type='text' className='form-control' name='scout.medical_history.medicine'></input>
                </div>
                <div className='col-lg-4 align-center'>
                  <input type='text' className='form-control' name='scout.medical_history.treatment'></input>
                </div>
              </div>

              <div className='col-lg-12 margin-top-xs'>
                <div className='col-lg-1 align-center'>
                  <p className='control-label align-center margin-top-xxs'>4.</p>
                </div>
                <div className='col-lg-3 align-center'>
                  <input type='text' className='form-control' name='scout.medical_history.disease'></input>
                </div>
                <div className='col-lg-4 align-center'>
                  <input type='text' className='form-control' name='scout.medical_history.medicine'></input>
                </div>
                <div className='col-lg-4 align-center'>
                  <input type='text' className='form-control' name='scout.medical_history.treatment'></input>
                </div>
              </div>

              <div className='col-lg-12 margin-top-xs'>
                <div className='col-lg-1 align-center'>
                  <p className='control-label align-center margin-top-xxs'>5.</p>
                </div>
                <div className='col-lg-3 align-center'>
                  <input type='text' className='form-control' name='scout.medical_history.disease'></input>
                </div>
                <div className='col-lg-4 align-center'>
                  <input type='text' className='form-control' name='scout.medical_history.medicine'></input>
                </div>
                <div className='col-lg-4 align-center'>
                  <input type='text' className='form-control' name='scout.medical_history.treatment'></input>
                </div>
              </div>

              <div className='col-lg-12 margin-top-xs'>
                <div className='col-lg-1 align-center'>
                  <p className='control-label align-center margin-top-xxs'>6.</p>
                </div>
                <div className='col-lg-3 align-center'>
                  <input type='text' className='form-control' name='scout.medical_history.disease'></input>
                </div>
                <div className='col-lg-4 align-center'>
                  <input type='text' className='form-control' name='scout.medical_history.medicine'></input>
                </div>
                <div className='col-lg-4 align-center'>
                  <input type='text' className='form-control' name='scout.medical_history.treatment'></input>
                </div>
              </div>

              <div className='col-lg-12 margin-top-xs'>
                <div className='col-lg-1 align-center'>
                  <p className='control-label align-center margin-top-xxs'>7.</p>
                </div>
                <div className='col-lg-3 align-center'>
                  <input type='text' className='form-control' name='scout.medical_history.disease'></input>
                </div>
                <div className='col-lg-4 align-center'>
                  <input type='text' className='form-control' name='scout.medical_history.medicine'></input>
                </div>
                <div className='col-lg-4 align-center'>
                  <input type='text' className='form-control' name='scout.medical_history.treatment'></input>
                </div>
              </div>

              <div className='col-lg-12 margin-top-xs'>
                <div className='col-lg-1 align-center'>
                  <p className='control-label align-center margin-top-xxs'>8.</p>
                </div>
                <div className='col-lg-3 align-center'>
                  <input type='text' className='form-control' name='scout.medical_history.disease'></input>
                </div>
                <div className='col-lg-4 align-center'>
                  <input type='text' className='form-control' name='scout.medical_history.medicine'></input>
                </div>
                <div className='col-lg-4 align-center'>
                  <input type='text' className='form-control' name='scout.medical_history.treatment'></input>
                </div>
              </div>

              <div className='col-lg-12 margin-top-xs'>
                <div className='col-lg-1 align-center'>
                  <p className='control-label align-center margin-top-xxs'>9.</p>
                </div>
                <div className='col-lg-3 align-center'>
                  <input type='text' className='form-control' name='scout.medical_history.disease'></input>
                </div>
                <div className='col-lg-4 align-center'>
                  <input type='text' className='form-control' name='scout.medical_history.medicine'></input>
                </div>
                <div className='col-lg-4 align-center'>
                  <input type='text' className='form-control' name='scout.medical_history.treatment'></input>
                </div>
              </div>

              <div className='col-lg-12 margin-top-xs'>
                <div className='col-lg-1 align-center'>
                  <p className='control-label align-center margin-top-xxs'>10.</p>
                </div>
                <div className='col-lg-3 align-center'>
                  <input type='text' className='form-control' name='scout.medical_history.disease'></input>
                </div>
                <div className='col-lg-4 align-center'>
                  <input type='text' className='form-control' name='scout.medical_history.medicine'></input>
                </div>
                <div className='col-lg-4 align-center'>
                  <input type='text' className='form-control' name='scout.medical_history.treatment'></input>
                </div>
              </div>

              <div className='col-lg-12'><div className='hr-line-dashed'></div></div>

              <div className='col-lg-12'>
                <label className='control-label align-left text-underline padding-left-xs'>Nomor darurat yang bisa dihubungi di kampung halaman:</label>
              </div>

              <div className='col-lg-12 margin-top-xs'>
                <div className='col-lg-1 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>No</label>
                </div>
                <div className='col-lg-2 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Telepon</label>
                </div>
                <div className='col-lg-3 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Handphone</label>
                </div>
                <div className='col-lg-3 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Nama Pemilik</label>
                </div>
                <div className='col-lg-3 align-center'>
                  <label className='control-label align-center margin-bottom-xxs'>Hubungan dengan Peserta</label>
                </div>
              </div>

              <div className='col-lg-12 margin-top-xxs'>
                <div className='col-lg-1 align-center'>
                  <p className='control-label align-center margin-top-xxs'>1.</p>
                </div>
                <div className='col-lg-2 align-center'>
                  <input type='text' className='form-control' name='scout.emergency_contacts.contact.phone.home'></input>
                </div>
                <div className='col-lg-3 align-center'>
                  <input type='text' className='form-control' name='scout.emergency_contacts.contact.phone.mobile'></input>
                </div>
                <div className='col-lg-3 align-center'>
                  <input type='text' className='form-control' name='scout.emergency_contacts.contact.name'></input>
                </div>
                <div className='col-lg-3 align-center'>
                  <input type='text' className='form-control' name='scout.emergency_contacts.relation'></input>
                </div>
              </div>

              <div className='col-lg-12 margin-top-xxs'>
                <div className='col-lg-1 align-center'>
                  <p className='control-label align-center margin-top-xxs'>2.</p>
                </div>
                <div className='col-lg-2 align-center'>
                  <input type='text' className='form-control' name='scout.emergency_contacts.contact.phone.home'></input>
                </div>
                <div className='col-lg-3 align-center'>
                  <input type='text' className='form-control' name='scout.emergency_contacts.contact.phone.mobile'></input>
                </div>
                <div className='col-lg-3 align-center'>
                  <input type='text' className='form-control' name='scout.emergency_contacts.contact.name'></input>
                </div>
                <div className='col-lg-3 align-center'>
                  <input type='text' className='form-control' name='scout.emergency_contacts.relation'></input>
                </div>
              </div>

              <div className='col-lg-12 margin-top-xxs'>
                <div className='col-lg-1 align-center'>
                  <p className='control-label align-center margin-top-xxs'>3.</p>
                </div>
                <div className='col-lg-2 align-center'>
                  <input type='text' className='form-control' name='scout.emergency_contacts.contact.phone.home'></input>
                </div>
                <div className='col-lg-3 align-center'>
                  <input type='text' className='form-control' name='scout.emergency_contacts.contact.phone.mobile'></input>
                </div>
                <div className='col-lg-3 align-center'>
                  <input type='text' className='form-control' name='scout.emergency_contacts.contact.name'></input>
                </div>
                <div className='col-lg-3 align-center'>
                  <input type='text' className='form-control' name='scout.emergency_contacts.relation'></input>
                </div>
              </div>

              <div className='col-lg-12 margin-top-xxs'>
                <div className='col-lg-1 align-center'>
                  <p className='control-label align-center margin-top-xxs'>4.</p>
                </div>
                <div className='col-lg-2 align-center'>
                  <input type='text' className='form-control' name='scout.emergency_contacts.contact.phone.home'></input>
                </div>
                <div className='col-lg-3 align-center'>
                  <input type='text' className='form-control' name='scout.emergency_contacts.contact.phone.mobile'></input>
                </div>
                <div className='col-lg-3 align-center'>
                  <input type='text' className='form-control' name='scout.emergency_contacts.contact.name'></input>
                </div>
                <div className='col-lg-3 align-center'>
                  <input type='text' className='form-control' name='scout.emergency_contacts.relation'></input>
                </div>
              </div>

              <div className='col-lg-12 margin-top-xxs'>
                <div className='col-lg-1 align-center'>
                  <p className='control-label align-center margin-top-xxs'>5.</p>
                </div>
                <div className='col-lg-2 align-center'>
                  <input type='text' className='form-control' name='scout.emergency_contacts.contact.phone.home'></input>
                </div>
                <div className='col-lg-3 align-center'>
                  <input type='text' className='form-control' name='scout.emergency_contacts.contact.phone.mobile'></input>
                </div>
                <div className='col-lg-3 align-center'>
                  <input type='text' className='form-control' name='scout.emergency_contacts.contact.name'></input>
                </div>
                <div className='col-lg-3 align-center'>
                  <input type='text' className='form-control' name='scout.emergency_contacts.relation'></input>
                </div>
              </div>

              <div className='col-lg-12'><div className='hr-line-dashed'></div></div>

              <div className='col-lg-12'>
                <label className='control-label align-left text-underline padding-left-xs'>Call sign darurat yang bisa dihubungi di kampung halaman:</label>
              </div>

              <div className='col-lg-12 margin-top-xs'>
                <div className='col-lg-1 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>No</label>
                </div>
                <div className='col-lg-2 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Call Sign</label>
                </div>
                <div className='col-lg-3 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Nama Pemilik</label>
                </div>
                <div className='col-lg-3 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Frekuensi Radio Amatir</label>
                </div>
                <div className='col-lg-3 align-center'>
                  <label className='control-label align-center margin-bottom-xxs'>Lokal</label>
                </div>
              </div>

              <div className='col-lg-12 margin-top-xxs'>
                <div className='col-lg-1 align-center'>
                  <p className='control-label align-center margin-top-xxs'>1.</p>
                </div>
                <div className='col-lg-2 align-center'>
                  <input type='text' className='form-control' name='scout.emergency_radios.call_sign'></input>
                </div>
                <div className='col-lg-3 align-center'>
                  <input type='text' className='form-control' name='scout.emergency_radios.name'></input>
                </div>
                <div className='col-lg-3 align-center'>
                  <input type='text' className='form-control' name='scout.emergency_radios.frequency'></input>
                </div>
                <div className='col-lg-3 align-center'>
                  <input type='text' className='form-control' name='scout.emergency_radios.local_area'></input>
                </div>
              </div>

              <div className='col-lg-12 margin-top-xxs'>
                <div className='col-lg-1 align-center'>
                  <p className='control-label align-center margin-top-xxs'>2.</p>
                </div>
                <div className='col-lg-2 align-center'>
                  <input type='text' className='form-control' name='scout.emergency_radios.call_sign'></input>
                </div>
                <div className='col-lg-3 align-center'>
                  <input type='text' className='form-control' name='scout.emergency_radios.name'></input>
                </div>
                <div className='col-lg-3 align-center'>
                  <input type='text' className='form-control' name='scout.emergency_radios.frequency'></input>
                </div>
                <div className='col-lg-3 align-center'>
                  <input type='text' className='form-control' name='scout.emergency_radios.local_area'></input>
                </div>
              </div>

              <div className='col-lg-12 margin-top-xxs'>
                <div className='col-lg-1 align-center'>
                  <p className='control-label align-center margin-top-xxs'>3.</p>
                </div>
                <div className='col-lg-2 align-center'>
                  <input type='text' className='form-control' name='scout.emergency_radios.call_sign'></input>
                </div>
                <div className='col-lg-3 align-center'>
                  <input type='text' className='form-control' name='scout.emergency_radios.name'></input>
                </div>
                <div className='col-lg-3 align-center'>
                  <input type='text' className='form-control' name='scout.emergency_radios.frequency'></input>
                </div>
                <div className='col-lg-3 align-center'>
                  <input type='text' className='form-control' name='scout.emergency_radios.local_area'></input>
                </div>
              </div>

              <div className='col-lg-12 margin-top-xxs'>
                <div className='col-lg-1 align-center'>
                  <p className='control-label align-center margin-top-xxs'>4.</p>
                </div>
                <div className='col-lg-2 align-center'>
                  <input type='text' className='form-control' name='scout.emergency_radios.call_sign'></input>
                </div>
                <div className='col-lg-3 align-center'>
                  <input type='text' className='form-control' name='scout.emergency_radios.name'></input>
                </div>
                <div className='col-lg-3 align-center'>
                  <input type='text' className='form-control' name='scout.emergency_radios.frequency'></input>
                </div>
                <div className='col-lg-3 align-center'>
                  <input type='text' className='form-control' name='scout.emergency_radios.local_area'></input>
                </div>
              </div>

              <div className='col-lg-12 margin-top-xxs'>
                <div className='col-lg-1 align-center'>
                  <p className='control-label align-center margin-top-xxs'>5.</p>
                </div>
                <div className='col-lg-2 align-center'>
                  <input type='text' className='form-control' name='scout.emergency_radios.call_sign'></input>
                </div>
                <div className='col-lg-3 align-center'>
                  <input type='text' className='form-control' name='scout.emergency_radios.name'></input>
                </div>
                <div className='col-lg-3 align-center'>
                  <input type='text' className='form-control' name='scout.emergency_radios.frequency'></input>
                </div>
                <div className='col-lg-3 align-center'>
                  <input type='text' className='form-control' name='scout.emergency_radios.local_area'></input>
                </div>
              </div>

              <div style={{ width: '100%', height: '1px', clear: 'both' }}></div>
            </div>
            <button className='form-control btn btn-green bold-700 uppercase' type='submit'><i className='fa fa-save margin-right-xs'></i>Simpan Data Peserta</button>
          </div>
        </div>
      </div>

    );
  }
};
