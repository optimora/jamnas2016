/**
 * Created by saras on 5/26/16.
 */
import React from 'react';

export default class TotalBranchParticipantsForm extends React.Component {
    render() {
        var items = [];

        for(var i=0; i < this.props.data.participants.length; i++) {
            items.push(
                <div className='col-lg-12 margin-bottom-xxs'>
                    <div className='col-lg-1 align-center no-padding'>
                        <p className='control-label align-left margin-top-xxs'>{i+1}.</p>
                    </div>
                    <div className='col-lg-5 no-padding'>
                        {
                            this.props.isForm  == "true"?
                                <input type='text' className='form-control align-center' name='branch_registration.participant_branches.sub_branch'></input> :
                                <p className="margin-top-xxs">{this.props.data.participants[i].sub_branch }</p>
                        }
                    </div>
                    <div className='col-lg-3'>
                        {
                            this.props.isForm == "true"?
                                <div className="row">
                                    <div className="col-lg-6">
                                        <input type='text' className='form-control align-center' name='branch_registration.participant_branches.num_male_participants' placeholder="Jumlah"></input>
                                    </div>
                                    <div className="col-lg-6">
                                        <input type='text' className='form-control align-center' name='branch_registration.participant_branches.num_female_participants' placeholder="Jumlah"></input>
                                    </div>
                                </div> :
                                <div className="row">
                                    <div className="col-lg-6">
                                        <p className="margin-top-xxs align-center">{this.props.data.participants[i].num_male}</p>
                                    </div>
                                    <div className="col-lg-6">
                                        <p className="margin-top-xxs align-center">{this.props.data.participants[i].num_female}</p>
                                    </div>
                                </div>
                        }
                    </div>
                    <div className='col-lg-3'>
                        {
                            this.props.isForm == "true"?
                                <div className="row">
                                    <div className="col-lg-6">
                                        <input type='text' className='form-control align-center' name='branch_registration.participant_branches.num_male_leaders' placeholder="Jumlah"></input>
                                    </div>
                                    <div className="col-lg-6">
                                        <input type='text' className='form-control align-center' name='branch_registration.participant_branches.num_female_leaders' placeholder="Jumlah"></input>
                                    </div>
                                </div> :
                                <div className="row">
                                    <div className="col-lg-6">
                                        <p className="margin-top-xxs align-center">{this.props.data.participants[i].num_male_leader}</p>
                                    </div>
                                    <div className="col-lg-6">
                                        <p className="margin-top-xxs align-center">{this.props.data.participants[i].num_female_leader}</p>
                                    </div>
                                </div>
                        }
                    </div>
                </div>
            );
        }

        return(
            <div className='ibox float-e-margins'>
                <div className='ibox-title'>
                    <h5>Data Peserta dan Pembina</h5>
                    <div className='ibox-tools'>
                        <a className='collapse-link'>
                            <i className='fa fa-chevron-up'></i>
                        </a>
                    </div>
                </div>
                <div className='ibox-content'>
                    <div className="col-lg-12">

                        <div className='col-lg-12 margin-top-xs'>
                            <div className='col-lg-1 align-center padding-top-bottom-xs'>
                                <h4 className='control-label align-left margin-bottom-xxs'>No</h4>
                            </div>
                            <div className='col-lg-5 padding-top-bottom-xs'>
                                <h4 className='control-label align-left margin-bottom-xxs'>Kwartir Cabang</h4>
                            </div>
                            <div className='col-lg-3 align-center'>
                                <h4>Peserta</h4>
                                <div className="row">
                                    <div className="col-lg-6">
                                        <label className='control-label align-left margin-bottom-xxs'>Putera</label>
                                    </div>
                                    <div className="col-lg-6">
                                        <label className='control-label align-left margin-bottom-xxs'>Puteri</label>
                                    </div>
                                </div>
                            </div>
                            <div className='col-lg-3 align-center'>
                                <h4>Pendamping</h4>
                                <div className="row">
                                    <div className="col-lg-6">
                                        <label className='control-label align-left margin-bottom-xxs'>Putera</label>
                                    </div>
                                    <div className="col-lg-6">
                                        <label className='control-label align-left margin-bottom-xxs'>Puteri</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {items}

                    </div>

                    <div style={{ width: '100%', height: '1px', clear: 'both' }}></div>
                </div>
            </div>
        );
    }
};
