import React from 'react';

export default class BranchPaymentConfirmation extends React.Component {
  render() {
    return(
      <div className='row'>
        <div className='col-lg-12'>
          <div className='ibox float-e-margins'>

            <div className='col-lg-12 title-center align-center'>
              <h5>KONFIRMASI PEMBAYARAN BIAYA PARTISIPASI</h5>
              <h5>JAMBORE NASIONAL X 2016</h5>
            </div>

            <div className='ibox-content'>

              <div className='col-lg-12'>
                <div className='col-lg-4'>
                  <p className='margin-top-xs'>Kwartir Daerah</p>
                </div>
                <div className='col-lg-8'>
                  <p className='margin-top-xs'>DKI Jakarta</p>
                </div>
              </div>

              <div className='col-lg-12'>
                <div className='col-lg-4'>
                  <p className='margin-top-xs'>Metode Pembayaran</p>
                </div>
                <div className='col-lg-8'>
                  <p className='margin-top-xs'>Transfer ATM</p>
                </div>
              </div>

              <div className='col-lg-12'>
                <div className='col-lg-4'>
                  <p className='margin-top-xs'>Jumlah yang Dibayarkan</p>
                </div>
                <div className='col-lg-8'>
                  <p className='margin-top-xs'>Rp. 10.000.000</p>
                </div>
              </div>

              <div className='col-lg-12'>
                <div className='col-lg-4'>
                  <p className='margin-top-xs'>Tanggal Pembayaran</p>
                </div>
                <div className='col-lg-8'>
                  <p className='margin-top-xs'>04 Juni 2016</p>
                </div>
              </div>

              <div className='col-lg-12'>
                <div className='col-lg-4'>
                  <p className='margin-top-xs'>Nama Pemegang Rekening Pengirim</p>
                </div>
                <div className='col-lg-8'>
                  <p className='margin-top-xs'>Erlangga Irsyad Sudiro</p>
                </div>
              </div>

              <div className='col-lg-12'>
                <div className='col-lg-4'>
                  <p className='margin-top-xs'>Nomor Rekening Pengirim</p>
                </div>
                <div className='col-lg-8'>
                  <p className='margin-top-xs'>5040192382</p>
                </div>
              </div>

              <div className='col-lg-12'>
                <div className='col-lg-4'>
                  <p className='margin-top-xs'>Bukti Pembayaran</p>
                </div>
                <div className='col-lg-8'>
                  <p className='margin-top-xs'><a href=''><i className='fa fa-download'></i> Bukti Pembayaran</a></p>
                </div>
              </div>

              <div style={{ width: '100%', height: '1px', clear: 'both' }}></div>

            </div>
          </div>

          <div className='ibox float-e-margins'>
            <div className='col-lg-12 title-center'>
              <h5>KONFIRMASI PEMBAYARAN YANG TELAH DIKIRIMKAN</h5>
            </div>

            <div className='ibox-content'>
              <div className='col-lg-12'>
                <div className="table-responsive">
                    <table className="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Tanggal</th>
                            <th>Metode</th>
                            <th>Jumlah</th>
                            <th>Nomor Rekening</th>
                            <th>Nama Pengirim</th>
                            <th>Lampiran</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>23-Mei-2016</td>
                            <td>ATM Transfer</td>
                            <td>Rp. 30.000.000</td>
                            <td>504090948</td>
                            <td>Erlangga Sudiro</td>
                            <td className="align-center">
                                <a href='' className="btn btn-success btn-xs no-margin">Lihat</a>
                            </td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>30-Mei-2016</td>
                            <td>ATM Transfer</td>
                            <td>Rp. 5.000.000</td>
                            <td>504090948</td>
                            <td>Erlangga Sudiro</td>
                            <td className="align-center">
                                <a href='' className="btn btn-success btn-xs no-margin">Lihat</a>
                            </td>
                        </tr>

                        </tbody>
                    </table>
                </div>
              </div>
              <div style={{ width: '100%', height: '1px', clear: 'both' }}></div>
            </div>
          </div>
        </div>
      </div>

    );
  }
};
