import React from 'react';

export default class SubBranchWillingnessLetter extends React.Component {
  render() {
    return(
      <div className='row'>
        <div className='col-lg-12'>
          <div className='ibox float-e-margins'>

            <div className='col-lg-12 title-center align-center'>
              <h5>KESEDIAAN KWARTIR CABANG MENGIKUTI</h5>
              <h5>JAMBORE NASIONAL X 2016</h5>
            </div>

            <div className='ibox-content'>

              <div className='col-lg-12'>
                <div className='col-lg-4'>
                  <p className='margin-top-xs'>Nama Kwartir Cabang</p>
                </div>
                <div className='col-lg-8'>
                  <input type='text' disabled value='Kota Administrasi Jakarta Selatan' name='willingness_letter.subbranch' className='form-control'></input>
                </div>
              </div>

              <div className='col-lg-12'>
                <div className='col-lg-4'>
                  <p className='margin-top-xs'>Nama Kwartir Daerah</p>
                </div>
                <div className='col-lg-8'>
                  <input type='text' disabled value='DKI Jakarta' name='willingness_letter.branch' className='form-control'></input>
                </div>
              </div>

              <div className='col-lg-12'>
                <div className='col-lg-4'>
                  <p className='margin-top-xs'>Alamat</p>
                </div>
                <div className='col-lg-6'>
                  <textarea name='willingness_letter.address' className='form-control margin-bottom-xs' rows='3' required></textarea>
                </div>
                <div className='col-lg-2 no-padding-left'>
                  <input type='text' name='willingness_letter.zipcode' className='form-control margin-top-mlg align-center' placeholder='Kodepos' required></input>
                </div>
              </div>

              <div className='col-lg-12'>
                <div className='col-lg-4'>
                  <p className='margin-top-xs'>Kontak</p>
                </div>
                <div className='col-lg-8'>
                  <div className='col-lg-6 no-padding-left'>
                    <p className='margin-top-xs'>No Telp Kwarda</p>
                    <div className="input-group m-b margin-top-minus-xs">
                      <span className="input-group-addon"><i className='glyphicon glyphicon-phone-alt'></i></span>
                      <input type='text' className='form-control m-b no-margin' name='willingness_letter.phone' required></input>
                    </div>
                  </div>
                  <div className='col-lg-6 no-padding-right'>
                    <p className='margin-top-xs'>No HP Bindamping</p>
                    <div className="input-group m-b margin-top-minus-xs">
                      <span className="input-group-addon"><i className='glyphicon glyphicon-phone'></i></span>
                      <input type='text' className='form-control m-b no-margin' name='willingness_letter.mobile_phone' required></input>
                    </div>
                  </div>
                </div>
              </div>

              <div className='col-lg-12 margin-bottom-md'><div className='hr-line-dashed no-margin'></div></div>

              <div className='col-lg-12'>
                <h3 className='align-center uppercase'>Dengan ini menyatakan siap dan bersedia</h3>
                <h3 className='align-center uppercase'>Mengikutsertakan Pramuka Penggalang</h3>
                <h3 className='align-center uppercase'>Pada Kegiatan Jambore Nasional 2016</h3>
                <h3 className='align-center uppercase'>Dan Siap Mematuhi Segala Ketentuan Yang Berlaku</h3>
              </div>

              <div className='col-lg-12 margin-top-md margin-bottom-md'><div className='hr-line-dashed no-margin'></div></div>

              <div className='col-lg-12 align-center margin-bottom-xs'>
                <h4 className='uppercase bold-700 underline'>Jumlah Peserta</h4>
              </div>

              <div className='col-lg-12'>
                <div className='col-lg-6 no-margin'>
                  <table>
                    <tbody>
                      <tr>
                        <td width='50%'>Penggalang Pa</td>
                        <td>
                          <div className='input-group margin-bottom-xxs'>
                            <input className='form-control right-border-less' type='text' name='willingness_letter.number_of.penggalang_pa' required></input>
                            <div className='input-group-addon left-border-less'>Orang</div>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td width='50%'>Penggalang Pi</td>
                        <td>
                          <div className='input-group margin-bottom-xxs'>
                            <input className='form-control right-border-less' type='text' name='willingness_letter.number_of.penggalang_pi' required></input>
                            <div className='input-group-addon left-border-less'>Orang</div>
                          </div>
                        </td>
                      </tr>
                      <tr className='margin-bottom-lg'></tr>
                      <tr>
                        <td width='50%' className='bold-700'>Jumlah</td>
                        <td>
                          <div className='input-group margin-bottom-xxs'>
                            <input className='form-control right-border-less' type='text' name='willingness_letter.number_of.jumlah_peserta' required></input>
                            <div className='input-group-addon left-border-less'>Orang</div>
                          </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div className='col-lg-6 no-margin'>
                  <table>
                    <tbody>
                      <tr>
                        <td width='50%'>Bindamping Pa</td>
                        <td>
                          <div className='input-group margin-bottom-xxs'>
                            <input className='form-control right-border-less' type='text' name='willingness_letter.number_of.bindamping_pa' required></input>
                            <div className='input-group-addon left-border-less'>Orang</div>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td width='50%'>Bindamping Pi</td>
                        <td>
                          <div className='input-group margin-bottom-xxs'>
                            <input className='form-control right-border-less' type='text' name='willingness_letter.number_of.bindamping_pi' required></input>
                            <div className='input-group-addon left-border-less'>Orang</div>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td width='50%'>Pinkoncab</td>
                        <td>
                          <div className='input-group margin-bottom-xxs'>
                            <input className='form-control right-border-less' type='text' name='willingness_letter.number_of.pinkoncab' required></input>
                            <div className='input-group-addon left-border-less'>Orang</div>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td width='50%' className='bold-700'>Jumlah</td>
                        <td>
                          <div className='input-group margin-bottom-xxs'>
                            <input className='form-control right-border-less' type='text' name='willingness_letter.number_of.jumlah_pimpinan' required></input>
                            <div className='input-group-addon left-border-less'>Orang</div>
                          </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>

              <div className='col-lg-12 margin-top-lg'>
                <div className='col-lg-12'>
                  <p className='bold-700'>Lampirkan bukti otentik formulir kesediaan Kwartir Cabang C.01</p>
                  <input type='file' name='willingness_letter.file.name'></input>
                  <input type='hidden' name='willingness_letter.file.type' value='kwarda-willingness-letter'></input>
                </div>
              </div>

              <div className='col-lg-12 margin-top-lg'>
                <div className='col-lg-6 col-lg-offset-3'>
                  <table>
                    <tbody>
                      <tr>
                        <td><p className='margin-top-xs margin-right-xs'>Formulir ini dikirim oleh:</p></td>
                        <td><input type='text' className='form-control' name='willingness_letter.sender_name' placeholder='masukkan nama Anda' required></input></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div className='col-lg-12 margin-top-md align-center'>
                <div className='col-lg-6 col-lg-offset-3'>
                  <button className='form-control btn btn-green bold-700' type='submit'><i className='fa fa-paper-plane margin-right-xs'></i>Kirim Surat Kesediaan</button>
                </div>
              </div>

              <div style={{ width: '100%', height: '1px', clear: 'both' }}></div>
            </div>
          </div>
        </div>
      </div>

    );
  }
};
