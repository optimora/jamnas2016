import React from 'react';

export default class ParticipantsScoutingForm extends React.Component {
  render() {
    return(
      <div className='row'>
        <div className='col-lg-12'>
          <div className='ibox float-e-margins'>
            <div className='ibox-title'>
              <h5>KEPRAMUKAAN</h5>
              <div className='ibox-tools'>
                  <a className='collapse-link'>
                      <i className='fa fa-chevron-up'></i>
                  </a>
              </div>
            </div>
            <div className='ibox-content'>

              <p> INI UNTUK PESERTA JAMNAS</p>

              <div className='col-lg-12 margin-bottom-md'>
                <div className='col-lg-2 no-margin no-padding'>
                  <label className='control-label align-left text-underline'>SIAGA</label>
                </div>
                <div className='col-lg-10'>
                  <div className='col-lg-6 no-padding-left'>
                    <label className='control-label align-left margin-bottom-xxs'>Nomor Gudep</label>
                    <input type='hidden' name='experiences.level' value='siaga'></input>
                    <input type='text' className='form-control' name='experiences.gudep_number'></input>
                  </div>
                  <div className='col-lg-6 no-padding-right'>
                    <label className='control-label align-left margin-bottom-xxs'>Nama Pangkalan</label>
                    <input type='text' className='form-control' name='experiences.base'></input>
                  </div>
                </div>
              </div>

              <div className='col-lg-10 col-lg-offset-2'>
                <label className='control-label align-left text-underline padding-left-xs'>TANDA KECAKAPAN KHUSUS (TKK)</label>
              </div>

              <div className='col-lg-10 col-lg-offset-2 margin-top-xs'>
                <div className='col-lg-3 no-margin no-padding'>
                  <label className='control-label align-left margin-bottom-xxs'></label>
                </div>
                <div className='col-lg-2 align-center'>
                  <p className='control-label align-center margin-bottom-xxs'>Mula</p>
                </div>
                <div className='col-lg-2 align-center'>
                  <p className='control-label align-center margin-bottom-xxs'>Bantu</p>
                </div>
                <div className='col-lg-2 align-center'>
                  <p className='control-label align-center margin-bottom-xxs'>Tata</p>
                </div>
                <div className='col-lg-2 align-center'>
                  <p className='control-label align-center margin-bottom-xxs'>Garuda</p>
                </div>
              </div>

              <div className='col-lg-10 col-lg-offset-2 margin-bottom-xxs'>
                <div className='col-lg-3 no-margin padding-left-xs'>
                  <p className='control-label align-left margin-bottom-xxs margin-top-xs'>Tahun</p>
                </div>
                <div className='col-lg-2'>
                  <input type='hidden' name='experiences.achievement.sub_level' value='mula'></input>
                  <input type='text' className='form-control align-center' name='experiences.achievement.year'></input>
                </div>
                <div className='col-lg-2'>
                  <input type='hidden' name='experiences.achievement.sub_level' value='bantu'></input>
                  <input type='text' className='form-control align-center' name='experiences.achievement.year'></input>
                </div>
                <div className='col-lg-2'>
                  <input type='hidden' name='experiences.achievement.sub_level' value='tata'></input>
                  <input type='text' className='form-control align-center' name='experiences.achievement.year'></input>
                </div>
                <div className='col-lg-2'>
                  <input type='hidden' name='experiences.achievement.sub_level' value='garuda'></input>
                  <input type='text' className='form-control align-center' name='experiences.achievement.year'></input>
                </div>
              </div>

              <div className='col-lg-10 col-lg-offset-2'>
                <div className='col-lg-3 no-margin padding-left-xs'>
                  <p className='control-label align-left margin-bottom-xxs margin-top-xs'>Jumlah TKK</p>
                </div>
                <div className='col-lg-2'>
                  <input type='text' className='form-control align-center' name='experiences.achievement.number_of_badges'></input>
                </div>
                <div className='col-lg-2'>
                  <input type='text' className='form-control align-center' name='experiences.achievement.number_of_badges'></input>
                </div>
                <div className='col-lg-2'>
                  <input type='text' className='form-control align-center' name='experiences.achievement.number_of_badges'></input>
                </div>
                <div className='col-lg-2'>
                  <input type='text' className='form-control align-center' name='experiences.achievement.number_of_badges'></input>
                </div>
              </div>

              <div className='col-lg-10 col-lg-offset-2 margin-top-lg'>
                <label className='control-label align-left text-underline padding-left-xs'>Giat Siaga Telah Diikuti </label>
              </div>

              <div className='col-lg-10 col-lg-offset-2 margin-top-xs'>
                <div className='col-lg-1 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>No</label>
                </div>
                <div className='col-lg-9'>
                  <label className='control-label align-left margin-bottom-xxs'>Nama Kegiatan</label>
                </div>
                <div className='col-lg-2 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Tahun</label>
                </div>
              </div>

              <div className='col-lg-10 col-lg-offset-2 margin-bottom-xxs'>
                <div className='col-lg-1 align-center'>
                  <p className='control-label align-center margin-top-xxs'>1.</p>
                </div>
                <div className='col-lg-9'>
                  <input type='text' className='form-control' name='experiences.activities.name'></input>
                </div>
                <div className='col-lg-2'>
                  <input type='text' className='form-control align-center' name='experiences.activities.year'></input>
                </div>
              </div>

              <div className='col-lg-10 col-lg-offset-2 margin-bottom-xxs'>
                <div className='col-lg-1 align-center'>
                  <p className='control-label align-center margin-top-xxs'>2.</p>
                </div>
                <div className='col-lg-9'>
                  <input type='text' className='form-control' name='experiences.activities.name'></input>
                </div>
                <div className='col-lg-2'>
                  <input type='text' className='form-control align-center' name='experiences.activities.year'></input>
                </div>
              </div>

              <div className='col-lg-10 col-lg-offset-2 margin-bottom-xxs'>
                <div className='col-lg-1 align-center'>
                  <p className='control-label align-center margin-top-xxs'>3.</p>
                </div>
                <div className='col-lg-9'>
                  <input type='text' className='form-control' name='experiences.activities.name'></input>
                </div>
                <div className='col-lg-2'>
                  <input type='text' className='form-control align-center' name='experiences.activities.year'></input>
                </div>
              </div>

              <div className='col-lg-10 col-lg-offset-2 margin-bottom-xxs'>
                <div className='col-lg-1 align-center'>
                  <p className='control-label align-center margin-top-xxs'>4.</p>
                </div>
                <div className='col-lg-9'>
                  <input type='text' className='form-control' name='experiences.activities.name'></input>
                </div>
                <div className='col-lg-2'>
                  <input type='text' className='form-control align-center' name='experiences.activities.year'></input>
                </div>
              </div>

              <div className='col-lg-10 col-lg-offset-2 margin-bottom-xxs'>
                <div className='col-lg-1 align-center'>
                  <p className='control-label align-center margin-top-xxs'>5.</p>
                </div>
                <div className='col-lg-9'>
                  <input type='text' className='form-control' name='experiences.activities.name'></input>
                </div>
                <div className='col-lg-2'>
                  <input type='text' className='form-control align-center' name='experiences.activities.year'></input>
                </div>
              </div>

              <div className='col-lg-10 col-lg-offset-2 margin-bottom-xxs'>
                <div className='col-lg-1 align-center'>
                  <p className='control-label align-center margin-top-xxs'>6.</p>
                </div>
                <div className='col-lg-9'>
                  <input type='text' className='form-control' name='experiences.activities.name'></input>
                </div>
                <div className='col-lg-2'>
                  <input type='text' className='form-control align-center' name='experiences.activities.year'></input>
                </div>
              </div>

              <div className='col-lg-12'><div className='hr-line-dashed'></div></div>

              <div className='col-lg-12 margin-bottom-md'>
                <div className='col-lg-2 no-margin no-padding'>
                  <label className='control-label align-left text-underline'>PENGGALANG</label>
                </div>
                <div className='col-lg-10'>
                  <div className='col-lg-6 no-padding-left'>
                    <label className='control-label align-left margin-bottom-xxs'>Nomor Gudep</label>
                    <input type='hidden' name='experiences.level' value='penggalang'></input>
                    <input type='text' className='form-control' name='experiences.gudep_number'></input>
                  </div>
                  <div className='col-lg-6 no-padding-right'>
                    <label className='control-label align-left margin-bottom-xxs'>Nama Pangkalan</label>
                    <input type='text' className='form-control' name='experiences.base'></input>
                  </div>
                </div>
              </div>

              <div className='col-lg-10 col-lg-offset-2'>
                <div className='col-lg-3 no-margin no-padding'>
                  <label className='control-label align-left margin-bottom-xxs'></label>
                </div>
                <div className='col-lg-2 align-center'>
                  <p className='control-label align-center margin-bottom-xxs'>Ramu</p>
                </div>
                <div className='col-lg-2 align-center'>
                  <p className='control-label align-center margin-bottom-xxs'>Rakit</p>
                </div>
                <div className='col-lg-2 align-center'>
                  <p className='control-label align-center margin-bottom-xxs'>Terap</p>
                </div>
                <div className='col-lg-2 align-center'>
                  <p className='control-label align-center margin-bottom-xxs'>Garuda</p>
                </div>
              </div>

              <div className='col-lg-10 col-lg-offset-2 margin-bottom-xxs'>
                <div className='col-lg-3 no-margin padding-left-xs'>
                  <label className='control-label align-left margin-bottom-xxs margin-top-xs'>Pencapaian Tingkat</label>
                </div>
                <div className='col-lg-2'>
                  <input type='hidden' name='experiences.achievement.sub_level' value='ramu'></input>
                  <input type='text' className='form-control align-center' name='experiences.achievement.year' placeholder='Tahun'></input>
                </div>
                <div className='col-lg-2'>
                  <input type='hidden' name='experiences.achievement.sub_level' value='rakit'></input>
                  <input type='text' className='form-control align-center' name='experiences.achievement.year' placeholder='Tahun'></input>
                </div>
                <div className='col-lg-2'>
                  <input type='hidden' name='experiences.achievement.sub_level' value='terap'></input>
                  <input type='text' className='form-control align-center' name='experiences.achievement.year' placeholder='Tahun'></input>
                </div>
                <div className='col-lg-2'>
                  <input type='hidden' name='experiences.achievement.sub_level' value='garuda'></input>
                  <input type='text' className='form-control align-center' name='experiences.achievement.year' placeholder='Tahun'></input>
                </div>
              </div>

              <div className='col-lg-10 col-lg-offset-2 margin-top-md'>
                <label className='control-label align-left text-underline padding-left-xs'>TANDA KECAKAPAN KHUSUS (TKK)</label>
              </div>

              <div className='col-lg-10 col-lg-offset-2 margin-top-xs'>
                <div className='col-lg-12'>
                  <div className='col-lg-4 align-center'>
                    <label className='control-label margin-bottom-xxs'>PURWA</label>
                  </div>
                  <div className='col-lg-4 align-center'>
                    <label className='control-label margin-bottom-xxs'>MADYA</label>
                  </div>
                  <div className='col-lg-4 align-center'>
                    <label className='control-label margin-bottom-xxs'>UTAMA</label>
                  </div>
                </div>
              </div>

              <div className='col-lg-10 col-lg-offset-2'>
                <table>
                  <tbody>
                    <tr>
                      <td width='1%'>
                        <p className='margin-top-xs'>1.</p>
                      </td>
                      <td>
                        <div className='col-lg-12 no-padding'>
                          <div className='col-lg-4 padding-xxs'>
                            <div class="form-group">
                              <div class="input-group">
                                <input type='hidden' name='badges.categoty' value='purwa'></input>
                                <select data-placeholder="Choose a Country..." className="chosen-select form-control" styles="width:350px;" tabIndex="2" name='experiences.badges'>
                                  <option value="">Pilih TKK</option>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div className='col-lg-4 padding-xxs'>
                            <div class="form-group">
                              <div class="input-group">
                                <input type='hidden' name='badges.categoty' value='madya'></input>
                                <select data-placeholder="Choose a Country..." className="chosen-select form-control" styles="width:350px;" tabIndex="2" name='experiences.badges'>
                                  <option value="">Pilih TKK</option>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div className='col-lg-4 padding-xxs'>
                            <div class="form-group">
                              <div class="input-group">
                                <input type='hidden' name='badges.categoty' value='utama'></input>
                                <select data-placeholder="Choose a Country..." className="chosen-select form-control" styles="width:350px;" tabIndex="2" name='experiences.badges'>
                                  <option value="">Pilih TKK</option>
                                </select>
                              </div>
                            </div>
                          </div>
                        </div>

                      </td>
                    </tr>
                    <tr>
                      <td width='1%'>
                        <p className='margin-top-xs'>2.</p>
                      </td>
                      <td>
                        <div className='col-lg-12 no-padding'>
                          <div className='col-lg-4 padding-xxs'>
                            <div class="form-group">
                              <div class="input-group">
                                <input type='hidden' name='badges.categoty' value='purwa'></input>
                                <select data-placeholder="Choose a Country..." className="chosen-select form-control" styles="width:350px;" tabIndex="2" name='experiences.badges'>
                                  <option value="">Pilih TKK</option>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div className='col-lg-4 padding-xxs'>
                            <div class="form-group">
                              <div class="input-group">
                                <input type='hidden' name='badges.categoty' value='madya'></input>
                                <select data-placeholder="Choose a Country..." className="chosen-select form-control" styles="width:350px;" tabIndex="2" name='experiences.badges'>
                                  <option value="">Pilih TKK</option>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div className='col-lg-4 padding-xxs'>
                            <div class="form-group">
                              <div class="input-group">
                                <input type='hidden' name='badges.categoty' value='utama'></input>
                                <select data-placeholder="Choose a Country..." className="chosen-select form-control" styles="width:350px;" tabIndex="2" name='experiences.badges'>
                                  <option value="">Pilih TKK</option>
                                </select>
                              </div>
                            </div>
                          </div>
                        </div>

                      </td>
                    </tr>
                    <tr>
                      <td width='1%'>
                        <p className='margin-top-xs'>3.</p>
                      </td>
                      <td>
                        <div className='col-lg-12 no-padding'>
                          <div className='col-lg-4 padding-xxs'>
                            <div class="form-group">
                              <div class="input-group">
                                <input type='hidden' name='badges.categoty' value='purwa'></input>
                                <select data-placeholder="Choose a Country..." className="chosen-select form-control" styles="width:350px;" tabIndex="2" name='experiences.badges'>
                                  <option value="">Pilih TKK</option>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div className='col-lg-4 padding-xxs'>
                            <div class="form-group">
                              <div class="input-group">
                                <input type='hidden' name='badges.categoty' value='madya'></input>
                                <select data-placeholder="Choose a Country..." className="chosen-select form-control" styles="width:350px;" tabIndex="2" name='experiences.badges'>
                                  <option value="">Pilih TKK</option>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div className='col-lg-4 padding-xxs'>
                            <div class="form-group">
                              <div class="input-group">
                                <input type='hidden' name='badges.categoty' value='utama'></input>
                                <select data-placeholder="Choose a Country..." className="chosen-select form-control" styles="width:350px;" tabIndex="2" name='experiences.badges'>
                                  <option value="">Pilih TKK</option>
                                </select>
                              </div>
                            </div>
                          </div>
                        </div>

                      </td>
                    </tr>
                    <tr>
                      <td width='1%'>
                        <p className='margin-top-xs'>4.</p>
                      </td>
                      <td>
                        <div className='col-lg-12 no-padding'>
                          <div className='col-lg-4 padding-xxs'>
                            <div class="form-group">
                              <div class="input-group">
                                <input type='hidden' name='badges.categoty' value='purwa'></input>
                                <select data-placeholder="Choose a Country..." className="chosen-select form-control" styles="width:350px;" tabIndex="2" name='experiences.badges'>
                                  <option value="">Pilih TKK</option>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div className='col-lg-4 padding-xxs'>
                            <div class="form-group">
                              <div class="input-group">
                                <input type='hidden' name='badges.categoty' value='madya'></input>
                                <select data-placeholder="Choose a Country..." className="chosen-select form-control" styles="width:350px;" tabIndex="2" name='experiences.badges'>
                                  <option value="">Pilih TKK</option>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div className='col-lg-4 padding-xxs'>
                            <div class="form-group">
                              <div class="input-group">
                                <input type='hidden' name='badges.categoty' value='utama'></input>
                                <select data-placeholder="Choose a Country..." className="chosen-select form-control" styles="width:350px;" tabIndex="2" name='experiences.badges'>
                                  <option value="">Pilih TKK</option>
                                </select>
                              </div>
                            </div>
                          </div>
                        </div>

                      </td>
                    </tr>
                    <tr>
                      <td width='1%'>
                        <p className='margin-top-xs'>5.</p>
                      </td>
                      <td>
                        <div className='col-lg-12 no-padding'>
                          <div className='col-lg-4 padding-xxs'>
                            <div class="form-group">
                              <div class="input-group">
                                <input type='hidden' name='badges.categoty' value='purwa'></input>
                                <select data-placeholder="Choose a Country..." className="chosen-select form-control" styles="width:350px;" tabIndex="2" name='experiences.badges'>
                                  <option value="">Pilih TKK</option>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div className='col-lg-4 padding-xxs'>
                            <div class="form-group">
                              <div class="input-group">
                                <input type='hidden' name='badges.categoty' value='madya'></input>
                                <select data-placeholder="Choose a Country..." className="chosen-select form-control" styles="width:350px;" tabIndex="2" name='experiences.badges'>
                                  <option value="">Pilih TKK</option>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div className='col-lg-4 padding-xxs'>
                            <div class="form-group">
                              <div class="input-group">
                                <input type='hidden' name='badges.categoty' value='utama'></input>
                                <select data-placeholder="Choose a Country..." className="chosen-select form-control" styles="width:350px;" tabIndex="2" name='experiences.badges'>
                                  <option value="">Pilih TKK</option>
                                </select>
                              </div>
                            </div>
                          </div>
                        </div>

                      </td>
                    </tr>
                    <tr>
                      <td width='1%'>
                        <p className='margin-top-xs'>6.</p>
                      </td>
                      <td>
                        <div className='col-lg-12 no-padding'>
                          <div className='col-lg-4 padding-xxs'>
                            <div class="form-group">
                              <div class="input-group">
                                <input type='hidden' name='badges.categoty' value='purwa'></input>
                                <select data-placeholder="Choose a Country..." className="chosen-select form-control" styles="width:350px;" tabIndex="2" name='experiences.badges'>
                                  <option value="">Pilih TKK</option>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div className='col-lg-4 padding-xxs'>
                            <div class="form-group">
                              <div class="input-group">
                                <input type='hidden' name='badges.categoty' value='madya'></input>
                                <select data-placeholder="Choose a Country..." className="chosen-select form-control" styles="width:350px;" tabIndex="2" name='experiences.badges'>
                                  <option value="">Pilih TKK</option>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div className='col-lg-4 padding-xxs'>
                            <div class="form-group">
                              <div class="input-group">
                                <input type='hidden' name='badges.categoty' value='utama'></input>
                                <select data-placeholder="Choose a Country..." className="chosen-select form-control" styles="width:350px;" tabIndex="2" name='experiences.badges'>
                                  <option value="">Pilih TKK</option>
                                </select>
                              </div>
                            </div>
                          </div>
                        </div>

                      </td>
                    </tr>
                    <tr>
                      <td width='1%'>
                        <p className='margin-top-xs'>7.</p>
                      </td>
                      <td>
                        <div className='col-lg-12 no-padding'>
                          <div className='col-lg-4 padding-xxs'>
                            <div class="form-group">
                              <div class="input-group">
                                <input type='hidden' name='badges.categoty' value='purwa'></input>
                                <select data-placeholder="Choose a Country..." className="chosen-select form-control" styles="width:350px;" tabIndex="2" name='experiences.badges'>
                                  <option value="">Pilih TKK</option>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div className='col-lg-4 padding-xxs'>
                            <div class="form-group">
                              <div class="input-group">
                                <input type='hidden' name='badges.categoty' value='madya'></input>
                                <select data-placeholder="Choose a Country..." className="chosen-select form-control" styles="width:350px;" tabIndex="2" name='experiences.badges'>
                                  <option value="">Pilih TKK</option>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div className='col-lg-4 padding-xxs'>
                            <div class="form-group">
                              <div class="input-group">
                                <input type='hidden' name='badges.categoty' value='utama'></input>
                                <select data-placeholder="Choose a Country..." className="chosen-select form-control" styles="width:350px;" tabIndex="2" name='experiences.badges'>
                                  <option value="">Pilih TKK</option>
                                </select>
                              </div>
                            </div>
                          </div>
                        </div>

                      </td>
                    </tr>
                    <tr>
                      <td width='1%'>
                        <p className='margin-top-xs'>8.</p>
                      </td>
                      <td>
                        <div className='col-lg-12 no-padding'>
                          <div className='col-lg-4 padding-xxs'>
                            <div class="form-group">
                              <div class="input-group">
                                <input type='hidden' name='badges.categoty' value='purwa'></input>
                                <select data-placeholder="Choose a Country..." className="chosen-select form-control" styles="width:350px;" tabIndex="2" name='experiences.badges'>
                                  <option value="">Pilih TKK</option>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div className='col-lg-4 padding-xxs'>
                            <div class="form-group">
                              <div class="input-group">
                                <input type='hidden' name='badges.categoty' value='madya'></input>
                                <select data-placeholder="Choose a Country..." className="chosen-select form-control" styles="width:350px;" tabIndex="2" name='experiences.badges'>
                                  <option value="">Pilih TKK</option>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div className='col-lg-4 padding-xxs'>
                            <div class="form-group">
                              <div class="input-group">
                                <input type='hidden' name='badges.categoty' value='utama'></input>
                                <select data-placeholder="Choose a Country..." className="chosen-select form-control" styles="width:350px;" tabIndex="2" name='experiences.badges'>
                                  <option value="">Pilih TKK</option>
                                </select>
                              </div>
                            </div>
                          </div>
                        </div>

                      </td>
                    </tr>
                    <tr>
                      <td width='1%'>
                        <p className='margin-top-xs'>9.</p>
                      </td>
                      <td>
                        <div className='col-lg-12 no-padding'>
                          <div className='col-lg-4 padding-xxs'>
                            <div class="form-group">
                              <div class="input-group">
                                <input type='hidden' name='badges.categoty' value='purwa'></input>
                                <select data-placeholder="Choose a Country..." className="chosen-select form-control" styles="width:350px;" tabIndex="2" name='experiences.badges'>
                                  <option value="">Pilih TKK</option>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div className='col-lg-4 padding-xxs'>
                            <div class="form-group">
                              <div class="input-group">
                                <input type='hidden' name='badges.categoty' value='madya'></input>
                                <select data-placeholder="Choose a Country..." className="chosen-select form-control" styles="width:350px;" tabIndex="2" name='experiences.badges'>
                                  <option value="">Pilih TKK</option>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div className='col-lg-4 padding-xxs'>
                            <div class="form-group">
                              <div class="input-group">
                                <input type='hidden' name='badges.categoty' value='utama'></input>
                                <select data-placeholder="Choose a Country..." className="chosen-select form-control" styles="width:350px;" tabIndex="2" name='experiences.badges'>
                                  <option value="">Pilih TKK</option>
                                </select>
                              </div>
                            </div>
                          </div>
                        </div>

                      </td>
                    </tr>
                    <tr>
                      <td width='1%'>
                        <p className='margin-top-xs'>10.</p>
                      </td>
                      <td>
                        <div className='col-lg-12 no-padding'>
                          <div className='col-lg-4 padding-xxs'>
                            <div class="form-group">
                              <div class="input-group">
                                <input type='hidden' name='badges.categoty' value='purwa'></input>
                                <select data-placeholder="Choose a Country..." className="chosen-select form-control" styles="width:350px;" tabIndex="2" name='experiences.badges'>
                                  <option value="">Pilih TKK</option>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div className='col-lg-4 padding-xxs'>
                            <div class="form-group">
                              <div class="input-group">
                                <input type='hidden' name='badges.categoty' value='madya'></input>
                                <select data-placeholder="Choose a Country..." className="chosen-select form-control" styles="width:350px;" tabIndex="2" name='experiences.badges'>
                                  <option value="">Pilih TKK</option>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div className='col-lg-4 padding-xxs'>
                            <div class="form-group">
                              <div class="input-group">
                                <input type='hidden' name='badges.categoty' value='utama'></input>
                                <select data-placeholder="Choose a Country..." className="chosen-select form-control" styles="width:350px;" tabIndex="2" name='experiences.badges'>
                                  <option value="">Pilih TKK</option>
                                </select>
                              </div>
                            </div>
                          </div>
                        </div>

                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>


              <div className='col-lg-10 col-lg-offset-2 margin-top-lg'>
                <label className='control-label align-left text-underline padding-left-xs'>Giat Penggalang yang Telah Diikuti </label>
              </div>

              <div className='col-lg-10 col-lg-offset-2 margin-top-xs'>
                <div className='col-lg-1 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>No</label>
                </div>
                <div className='col-lg-9'>
                  <label className='control-label align-left margin-bottom-xxs'>Nama Kegiatan</label>
                </div>
                <div className='col-lg-2 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Tahun</label>
                </div>
              </div>

              <div className='col-lg-10 col-lg-offset-2 margin-bottom-xxs'>
                <div className='col-lg-1 align-center'>
                  <p className='control-label align-center margin-top-xxs'>1.</p>
                </div>
                <div className='col-lg-9'>
                  <input type='text' className='form-control' name='experiences.activities.name'></input>
                </div>
                <div className='col-lg-2'>
                  <input type='text' className='form-control align-center' name='experiences.activities.year'></input>
                </div>
              </div>

              <div className='col-lg-10 col-lg-offset-2 margin-bottom-xxs'>
                <div className='col-lg-1 align-center'>
                  <p className='control-label align-center margin-top-xxs'>2.</p>
                </div>
                <div className='col-lg-9'>
                  <input type='text' className='form-control' name='experiences.activities.name'></input>
                </div>
                <div className='col-lg-2'>
                  <input type='text' className='form-control align-center' name='experiences.activities.year'></input>
                </div>
              </div>

              <div className='col-lg-10 col-lg-offset-2 margin-bottom-xxs'>
                <div className='col-lg-1 align-center'>
                  <p className='control-label align-center margin-top-xxs'>3.</p>
                </div>
                <div className='col-lg-9'>
                  <input type='text' className='form-control' name='experiences.activities.name'></input>
                </div>
                <div className='col-lg-2'>
                  <input type='text' className='form-control align-center' name='experiences.activities.year'></input>
                </div>
              </div>

              <div className='col-lg-10 col-lg-offset-2 margin-bottom-xxs'>
                <div className='col-lg-1 align-center'>
                  <p className='control-label align-center margin-top-xxs'>4.</p>
                </div>
                <div className='col-lg-9'>
                  <input type='text' className='form-control' name='experiences.activities.name'></input>
                </div>
                <div className='col-lg-2'>
                  <input type='text' className='form-control align-center' name='experiences.activities.year'></input>
                </div>
              </div>

              <div className='col-lg-10 col-lg-offset-2 margin-bottom-xxs'>
                <div className='col-lg-1 align-center'>
                  <p className='control-label align-center margin-top-xxs'>5.</p>
                </div>
                <div className='col-lg-9'>
                  <input type='text' className='form-control' name='experiences.activities.name'></input>
                </div>
                <div className='col-lg-2'>
                  <input type='text' className='form-control align-center' name='experiences.activities.year'></input>
                </div>
              </div>

              <div className='col-lg-10 col-lg-offset-2 margin-bottom-xxs'>
                <div className='col-lg-1 align-center'>
                  <p className='control-label align-center margin-top-xxs'>6.</p>
                </div>
                <div className='col-lg-9'>
                  <input type='text' className='form-control' name='experiences.activities.name'></input>
                </div>
                <div className='col-lg-2'>
                  <input type='text' className='form-control align-center' name='experiences.activities.year'></input>
                </div>
              </div>

              <div className='col-lg-10 col-lg-offset-2 margin-bottom-xxs'>
                <div className='col-lg-1 align-center'>
                  <p className='control-label align-center margin-top-xxs'>7.</p>
                </div>
                <div className='col-lg-9'>
                  <input type='text' className='form-control' name='experiences.activities.name'></input>
                </div>
                <div className='col-lg-2'>
                  <input type='text' className='form-control align-center' name='experiences.activities.year'></input>
                </div>
              </div>


              <div className='col-lg-12'><div className='hr-line-dashed'></div></div>

              <p>INI UNTUK PINKONDA/PEMBINA/PINKONCAB</p>

              <div className='col-lg-12 margin-bottom-xs'>
                <div className='col-lg-2 no-margin no-padding align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Nama Golongan</label>
                </div>
                <div className='col-lg-2 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Tahun</label>
                </div>
                <div className='col-lg-3 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Nomor Gudep</label>
                </div>
                <div className='col-lg-4 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Pangkalan</label>
                </div>
              </div>


              <div className='col-lg-12 margin-bottom-xxs'>
                <div className='col-lg-2 no-margin no-padding align-center'>
                  <p className='control-label margin-bottom-xxs margin-top-xs align-center'>Siaga</p>
                </div>
                <div className='col-lg-2'>
                  <input type='hidden' name='scout.educations.level'></input>
                  <input type='text' className='form-control align-center' name='scout.educations.school_name'></input>
                </div>
                <div className='col-lg-3'>
                  <input type='text' className='form-control align-center' name='scout.educations.regency_city'></input>
                </div>
                <div className='col-lg-4'>
                  <input type='text' className='form-control align-center' name='scout.educations.graduate_year'></input>
                </div>
              </div>

              <div className='col-lg-12 margin-bottom-xxs'>
                <div className='col-lg-2 no-margin no-padding align-center'>
                  <p className='control-label margin-bottom-xxs margin-top-xs align-center'>Penggalang</p>
                </div>
                <div className='col-lg-2'>
                  <input type='hidden' name='scout.educations.level'></input>
                  <input type='text' className='form-control align-center' name='scout.educations.school_name'></input>
                </div>
                <div className='col-lg-3'>
                  <input type='text' className='form-control align-center' name='scout.educations.regency_city'></input>
                </div>
                <div className='col-lg-4'>
                  <input type='text' className='form-control align-center' name='scout.educations.graduate_year'></input>
                </div>
              </div>

              <div className='col-lg-12 margin-bottom-xxs'>
                <div className='col-lg-2 no-margin no-padding align-center'>
                  <p className='control-label margin-bottom-xxs margin-top-xs align-center'>Penegak</p>
                </div>
                <div className='col-lg-2'>
                  <input type='hidden' name='scout.educations.level'></input>
                  <input type='text' className='form-control align-center' name='scout.educations.school_name'></input>
                </div>
                <div className='col-lg-3'>
                  <input type='text' className='form-control align-center' name='scout.educations.regency_city'></input>
                </div>
                <div className='col-lg-4'>
                  <input type='text' className='form-control align-center' name='scout.educations.graduate_year'></input>
                </div>
              </div>

              <div className='col-lg-12 margin-bottom-xxs'>
                <div className='col-lg-2 no-margin no-padding align-center'>
                  <p className='control-label margin-bottom-xxs margin-top-xs align-center'>Pandega</p>
                </div>
                <div className='col-lg-2'>
                  <input type='hidden' name='scout.educations.level'></input>
                  <input type='text' className='form-control align-center' name='scout.educations.school_name'></input>
                </div>
                <div className='col-lg-3'>
                  <input type='text' className='form-control align-center' name='scout.educations.regency_city'></input>
                </div>
                <div className='col-lg-4'>
                  <input type='text' className='form-control align-center' name='scout.educations.graduate_year'></input>
                </div>
              </div>

              <div className='col-lg-12 margin-bottom-xs'>
                <div className='col-lg-2 no-margin no-padding align-center'>
                  <p className='control-label margin-bottom-xxs margin-top-xs align-center'>Pembina</p>
                </div>
                <div className='col-lg-2'>
                  <input type='hidden' name='scout.educations.level'></input>
                  <input type='text' className='form-control align-center' name='scout.educations.school_name'></input>
                </div>
                <div className='col-lg-3'>
                  <input type='text' className='form-control align-center' name='scout.educations.regency_city'></input>
                </div>
                <div className='col-lg-4'>
                  <input type='text' className='form-control align-center' name='scout.educations.graduate_year'></input>
                </div>
              </div>


              <div style={{ width: '100%', height: '1px', clear: 'both' }}></div>
            </div>
            <button className='form-control btn btn-green bold-700 uppercase' type='submit'><i className='fa fa-save margin-right-xs'></i>Simpan Data Peserta</button>
          </div>
        </div>
      </div>

    );
  }
};
