/**
 * Created by saras on 5/15/16.
 */
import React from 'react';

export default class DoubleStatBox extends React.Component {
  static get propTypes() {
    return {
      stat1: React.PropTypes.string.isRequired,
      statLabel1: React.PropTypes.string.isRequired,
      stat2: React.PropTypes.string.isRequired,
      statLabel2: React.PropTypes.string.isRequired
    };
  }

  render() {
    return(
      <div>
        <div className="ibox float-e-margins">
          <div className="ibox-content">
            <div className="row">
              <div className="col-lg-5">
                <h1 className="no-margins">{this.props.stat1}</h1>
                <small>{this.props.statLabel1}</small>
              </div>
              <div className="col-lg-5">
                <h1 className="no-margins">{this.props.stat2}</h1>
                <small>{this.props.statLabel2}</small>
              </div>
              <div className="col-lg-2"><h1><i className="fa fa-lg fa-sitemap pull-right m-t-sm"></i></h1>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
};
