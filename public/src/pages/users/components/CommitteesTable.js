import React from 'react';

export default class CommitteesTable extends React.Component {
  render() {
    return(
      <div className='row'>
        <div className='col-lg-12'>
          <div className="ibox float-e-margins">
            <div className="ibox-title">
              <h5>Data Panitia JAMNAS X 2016</h5>
            </div>
            <div className="ibox-content">
              <div className="row margin-bottom-xs">
                <div className="col-lg-4 pull-left">
                  <p className="padding-top-bottom-xs no-margin">Showing 1 to 5 of 5 entries</p>
                </div>
                <div className="col-lg-3 pull-right">
                  <div className="input-group">
                    <input type="text" placeholder="Search" className="input-sm form-control">
                                        <span className="input-group-btn">
                                            <button type="button" className="btn btn-sm btn-primary"> Go!</button>
                                        </span>
                    </input>
                  </div>
                </div>
              </div>
              <div className="table-responsive">
                <table className="table table-striped table-bordered table-hover dataTables-example" >
                  <thead>
                  <tr>
                    <th className="align-center">#</th>
                    <th className="align-center">Nama</th>
                    <th className="align-center">Jabatan</th>
                    <th className="align-center">Profil Panitia</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td>1</td>
                    <td>Sri Andhini Saraswati</td>
                    <td className="align-center">Panitia Pendukung</td>
                    <td className="align-center"><a href="#" className="btn btn-success btn-xs no-margin margin-right-xs">Lihat Profil Panitia</a></td>
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>Bambang</td>
                    <td className="align-center">Panitia Pendukung</td>
                    <td className="align-center"><a href="#" className="btn btn-success btn-xs no-margin margin-right-xs">Lihat Profil Panitia</a></td>
                  </tr>
                  <tr>
                    <td>3</td>
                    <td>Ani</td>
                    <td className="align-center">Panitia Pendukung</td>
                    <td className="align-center"><a href="#" className="btn btn-success btn-xs no-margin margin-right-xs">Lihat Profil Panitia</a></td>
                  </tr>
                  <tr>
                    <td>4</td>
                    <td>Budi</td>
                    <td className="align-center">Panitia Pendukung</td>
                    <td className="align-center"><a href="#" className="btn btn-success btn-xs no-margin margin-right-xs">Lihat Profil Panitia</a></td>
                  </tr>
                  <tr>
                    <td>5</td>
                    <td>Joko Widodo</td>
                    <td className="align-center">Panitia Pendukung</td>
                    <td className="align-center"><a href="#" className="btn btn-success btn-xs no-margin margin-right-xs">Lihat Profil Panitia</a></td>
                  </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>

    );
  }
};
