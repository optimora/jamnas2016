import React from 'react';

export default class ParentForm extends React.Component {
    render() {
        return(
            <div className='ibox float-e-margins'>
                <div className='ibox-title'>
                    <h5>Data Orang Tua</h5>
                    <div className='ibox-tools'>
                        <a className='collapse-link'>
                            <i className='fa fa-chevron-up'></i>
                        </a>
                    </div>
                </div>
                <div className='ibox-content'>

                    <div className='col-lg-12 margin-bottom-xs'>
                        <div className='col-lg-3 no-margin no-padding'>
                            <label className='control-label align-left margin-bottom-xxs'>Nama Orang Tua</label>
                        </div>
                        <div className='col-lg-6'>
                            {
                                this.props.isForm == "true" ?
                                    <input type='text' className='form-control' name='contact.name'></input> :
                                    <p>{this.props.data.name}</p>
                            }
                        </div>
                    </div>

                    <div className='col-lg-12 margin-bottom-xs'>
                        <div className='col-lg-3 no-margin no-padding'>
                            <label className='control-label align-left margin-bottom-xxs'>Pekerjaan</label>
                        </div>
                        <div className='col-lg-6'>
                            {
                                this.props.isForm == "true" ?
                                    <input type='text' className='form-control' name='contact.occupation'></input>:
                                    <p>{this.props.data.occupation}</p>
                            }
                        </div>
                    </div>

                    <div className='col-lg-12'>
                        <div className='col-lg-3 no-margin no-padding'>
                            <label className='control-label align-left margin-bottom-xxs'>Alamat Rumah</label>
                        </div>
                        <div className='col-lg-9'>
                            {
                                this.props.isForm == "true" ?
                                    <textarea className='form-control' name='contact.address' rows='3'></textarea>:
                                    <p>{this.props.data.address}</p>
                            }
                            {
                                this.props.isForm == "true" ?
                                    <div className='col-lg-5 no-padding-left margin-top-xs'>
                                        <label className='control-label align-left margin-bottom-xxs'>Provinsi</label>
                                        <select className='form-control m-b no-margin' name='contact.province'>
                                            <option values=''>Nusa Tenggara Barat</option>
                                        </select>
                                    </div> :
                                    <div className='col-lg-4 no-padding-left margin-top-xs'>
                                        <label className='control-label align-left margin-bottom-xxs'>Provinsi</label>
                                        <p>{this.props.data.province}</p>
                                    </div>
                            }
                            {
                                this.props.isForm == "true" ?
                                    <div className='col-lg-5 no-padding-right margin-top-xs'>
                                        <label className='control-label align-left margin-bottom-xxs'>Kota / Kabupaten</label>
                                        <select className='form-control m-b no-margin' name='contact.regency_city'>
                                            <option values=''>Kota Administrasi Jakarta Selatan</option>
                                        </select>
                                    </div> :
                                    <div className='col-lg-5 no-padding-right margin-top-xs'>
                                        <label className='control-label align-left margin-bottom-xxs'>Kota / Kabupaten</label>
                                        <p>{this.props.data.regency_city}</p>
                                    </div>
                            }
                            {
                                this.props.isForm == "true" ?
                                    <div className='col-lg-2 no-padding-right margin-top-xs'>
                                        <label className='control-label align-left margin-bottom-xxs'>Kodepos</label>
                                        <input type='text' className='form-control m-b no-margin' name='contact.zipcode'></input>
                                    </div> :
                                    <div className='col-lg-2 no-padding-right margin-top-xs'>
                                        <label className='control-label align-left margin-bottom-xxs'>Kodepos</label>
                                        <p>{this.props.data.zipcode}</p>
                                    </div>
                            }
                        </div>
                    </div>

                    <div className='col-lg-12'><div className='hr-line-dashed'></div></div>

                    <div className='col-lg-12'>
                        <div className='col-lg-3 no-margin no-padding'>
                            <label className='control-label align-left margin-bottom-xxs'>Kontak</label>
                        </div>
                        <div className='col-lg-9'>
                            {
                                this.props.isForm == "true" ?
                                    <div className='col-lg-6 no-padding-left'>
                                        <label className='control-label align-left margin-bottom-xxs'>No Telp. Rumah</label>
                                        <div className="input-group m-b">
                                            <span className="input-group-addon"><i className='glyphicon glyphicon-phone-alt'></i></span>
                                            <input type='text' className='form-control m-b no-margin' name='contact.phone.home'></input>
                                        </div>
                                    </div> :
                                    <div className='col-lg-4 no-padding-left'>
                                        <label className='control-label align-left margin-bottom-xxs'>No Telp. Rumah</label>
                                        <p>0217256229</p>
                                    </div>
                            }
                            {
                                this.props.isForm == "true" ?
                                    <div className='col-lg-6 no-padding-right'>
                                        <label className='control-label align-left margin-bottom-xxs'>No. Handphone</label>
                                        <div className="input-group m-b">
                                            <span className="input-group-addon"><i className='glyphicon glyphicon-phone'></i></span>
                                            <input type='text' className='form-control m-b no-margin' name='contact.phone.mobile'></input>
                                        </div>
                                    </div> :
                                    <div className='col-lg-4 no-padding-right'>
                                        <label className='control-label align-left margin-bottom-xxs'>No. Handphone</label>
                                        <p>0811801234</p>
                                    </div>
                            }
                        </div>
                    </div>

                    <div style={{ width: '100%', height: '1px', clear: 'both' }}></div>
                </div>
            </div>
        );
    }
};
