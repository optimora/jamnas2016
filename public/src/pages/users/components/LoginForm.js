import { reduxForm } from 'redux-form';

import React from 'react';

import PasswordField from '../../../components/common/forms/PasswordField';
import SubmitButton from '../../../components/common/forms/SubmitButton';
import TextField from '../../../components/common/forms/TextField';

class LoginForm extends React.Component {
  static get propTypes() {
    return {
      data: React.PropTypes.object.isRequired,

      submitHandler: React.PropTypes.func,
      onChange: React.PropTypes.func
    };
  }

  _handleSubmit(event) {
    event.preventDefault();
    this.props.submitHandler();
  }

  _handleFieldValueChanges(name, value) {
    this.props.onChange(name, value);
  }

  render() {
    let message = <div></div>;
    if (this.props.data.message) {
      message = <div className={'alert alert-' + this.props.data.messageType}>
        {this.props.data.message}
      </div>;
    }

    return (
      <div>
        {message}
        <form id='formLogin' onSubmit={this._handleSubmit.bind(this)}>
          <TextField name='email' label='E-mail' placeholder='Email Anda.' onChangeListener={this._handleFieldValueChanges.bind(this)} />
          <PasswordField name='password' label='Password' placeholder='Password Anda.' onChangeListener={this._handleFieldValueChanges.bind(this)} />
          <SubmitButton clickHandler={this._handleSubmit.bind(this)} label='Masuk' formId='formLogin' className='align-right btn btn-success' />
        </form>
      </div>
    );
  }
};

export default LoginForm;
