import React from 'react';

export default class SubmitButton extends React.Component {
    render() {
        return(
            <div className='ibox float-e-margins'>
                <div className='ibox-content'>
                    <div className='col-lg-12 margin-top-lg'>
                        <div className='col-lg-12'>
                            <p className='bold-700'>{this.props.attachment}</p>
                            <input type='file' name={this.props.prefix + '.file.name'}></input>
                            <input type='hidden' name={this.props.prefix +'.file.type'} value={this.props.filevalue}></input>
                        </div>
                    </div>
                    <div className='col-lg-12 margin-top-xlg align-center'>
                        <div className='col-lg-6 col-lg-offset-3'>
                            <button className='form-control btn btn-green bold-700' type='submit'><i className='fa fa-paper-plane margin-right-xs'></i>Kirim Surat Kesediaan</button>
                        </div>
                    </div>

                    <div style={{ width: '100%', height: '1px', clear: 'both' }}></div>
                </div>
            </div>
        );
    }
};
