import React from 'react';

export default class MedicalForm extends React.Component {
    render() {
        return(
            <div className='ibox float-e-margins'>
                <div className='ibox-title'>
                    <h5>Data Riwayat Kesehatan</h5>
                    <div className='ibox-tools'>
                        <a className='collapse-link'>
                            <i className='fa fa-chevron-up'></i>
                        </a>
                    </div>
                </div>
                <div className='ibox-content'>

                    <div className='col-lg-12'>
                        <label className='control-label align-left text-underline padding-left-xs'>PENYAKIT YANG DIMILIKI</label>
                    </div>

                    <div className='col-lg-12 margin-top-xs'>
                        <div className='col-lg-1 align-center'>
                            <label className='control-label align-left margin-bottom-xxs'>No</label>
                        </div>
                        <div className='col-lg-3 align-center'>
                            <label className='control-label align-left margin-bottom-xxs'>Nama Penyakit</label>
                        </div>
                        <div className='col-lg-4 align-center'>
                            <label className='control-label align-left margin-bottom-xxs'>Obat Pribadi yang Dimiliki</label>
                        </div>
                        <div className='col-lg-4 align-center'>
                            <label className='control-label align-left margin-bottom-xxs'>Penanganan Medis</label>
                        </div>
                    </div>

                    <div className='col-lg-12 margin-top-xs'>
                        <div className='col-lg-1 align-center'>
                            <p className='control-label align-center margin-top-xxs'>1.</p>
                        </div>
                        <div className='col-lg-3 align-center'>
                            <p className='control-label align-center margin-top-xxs'>Maag</p>
                        </div>
                        <div className='col-lg-4 align-center'>
                            <p className='control-label align-center margin-top-xxs'>Promag</p>
                        </div>
                        <div className='col-lg-4 align-center'>
                            <p className='control-label align-center margin-top-xxs'>-</p>
                        </div>
                    </div>

                    <div className='col-lg-12 margin-top-xs'>
                        <div className='col-lg-1 align-center'>
                            <p className='control-label align-center margin-top-xxs'>2.</p>
                        </div>
                        <div className='col-lg-3 align-center'>
                            <p className='control-label align-center margin-top-xxs'>Diare</p>
                        </div>
                        <div className='col-lg-4 align-center'>
                            <p className='control-label align-center margin-top-xxs'>Diapet</p>
                        </div>
                        <div className='col-lg-4 align-center'>
                            <p className='control-label align-center margin-top-xxs'>-</p>
                        </div>
                    </div>


                    <div className='col-lg-12'><div className='hr-line-dashed'></div></div>

                    <div className='col-lg-12'>
                        <label className='control-label align-left text-underline padding-left-xs'>Nomor darurat yang bisa dihubungi di kampung halaman:</label>
                    </div>

                    <div className='col-lg-12 margin-top-xs'>
                        <div className='col-lg-1 align-center'>
                            <label className='control-label align-left margin-bottom-xxs'>No</label>
                        </div>
                        <div className='col-lg-2 align-center'>
                            <label className='control-label align-left margin-bottom-xxs'>Telepon</label>
                        </div>
                        <div className='col-lg-3 align-center'>
                            <label className='control-label align-left margin-bottom-xxs'>Handphone</label>
                        </div>
                        <div className='col-lg-3 align-center'>
                            <label className='control-label align-left margin-bottom-xxs'>Nama Pemilik</label>
                        </div>
                        <div className='col-lg-3 align-center'>
                            <label className='control-label align-center margin-bottom-xxs'>Hubungan dengan Peserta</label>
                        </div>
                    </div>

                    <div className='col-lg-12 margin-top-xxs'>
                        <div className='col-lg-1 align-center'>
                            <p className='control-label align-center margin-top-xxs'>1.</p>
                        </div>
                        <div className='col-lg-2 align-center'>
                            <p className='control-label align-center margin-top-xxs'>0217371707</p>
                        </div>
                        <div className='col-lg-3 align-center'>
                            <p className='control-label align-center margin-top-xxs'>08138982876</p>
                        </div>
                        <div className='col-lg-3 align-center'>
                            <p className='control-label align-center margin-top-xxs'>Joko Santoso</p>
                        </div>
                        <div className='col-lg-3 align-center'>
                            <p className='control-label align-center margin-top-xxs'>Ayah</p>
                        </div>
                    </div>

                    <div className='col-lg-12 margin-top-xxs'>
                        <div className='col-lg-1 align-center'>
                            <p className='control-label align-center margin-top-xxs'>2.</p>
                        </div>
                        <div className='col-lg-2 align-center'>
                            <p className='control-label align-center margin-top-xxs'>0217371707</p>
                        </div>
                        <div className='col-lg-3 align-center'>
                            <p className='control-label align-center margin-top-xxs'>08167676539</p>
                        </div>
                        <div className='col-lg-3 align-center'>
                            <p className='control-label align-center margin-top-xxs'>Nani Hayati</p>
                        </div>
                        <div className='col-lg-3 align-center'>
                            <p className='control-label align-center margin-top-xxs'>Ibu</p>
                        </div>
                    </div>


                    <div style={{ width: '100%', height: '1px', clear: 'both' }}></div>
                </div>
            </div>
        );
    }
};
