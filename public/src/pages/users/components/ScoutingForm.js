import React from 'react';

export default class ScoutingForm extends React.Component {
    render() {
        var items = [];

        if(this.props.items && this.props.items.length > 0 ) {
            for (var i = 0; i < this.props.items.length; i++) {
                items.push(
                    <div className='col-lg-12 margin-top-xxs'>
                        <div className='col-lg-3 no-margin no-padding align-center'>
                            <label className='control-label align-left margin-bottom-xxs margin-top-xs'>{this.props.items[i].level}</label>
                        </div>
                        <div className='col-lg-2'>
                            {
                                this.props.isForm == "true" ?
                                    <input type='text' className='form-control align-center' name='experiences.year'></input>:
                                    <p className="control-label align-center margin-bottom-xxs margin-top-xs">{this.props.items[i].year}</p>
                            }
                        </div>
                        <div className='col-lg-3'>
                            {
                                this.props.isForm == "true" ?
                                    <input type='text' className='form-control align-center' name='experiences.gudep_number'></input>:
                                    <p className="control-label align-center margin-bottom-xxs margin-top-xs">{this.props.items[i].gudep_number}</p>
                            }
                        </div>
                        <div className='col-lg-4'>
                            {
                                this.props.isForm == "true"?
                                    <input type='text' className='form-control align-center' name='experiences.base'></input>:
                                    <p className="control-label align-center margin-bottom-xxs margin-top-xs">{this.props.items[i].base}</p>
                            }
                        </div>
                    </div>
                );
            }
        }

        return(
            <div className='ibox float-e-margins'>
                <div className='ibox-title'>
                    <h5>Data Kepramukaan</h5>
                    <div className='ibox-tools'>
                        <a className='collapse-link'>
                            <i className='fa fa-chevron-up'></i>
                        </a>
                    </div>
                </div>
                <div className='ibox-content'>

                    <div className='col-lg-12 margin-bottom-xs'>
                        <div className='col-lg-3 no-margin no-padding align-center'>
                            <label className='control-label align-left margin-bottom-xxs'>Nama Golongan</label>
                        </div>
                        <div className='col-lg-2 align-center'>
                            <label className='control-label align-left margin-bottom-xxs'>Tahun</label>
                        </div>
                        <div className='col-lg-3 align-center'>
                            <label className='control-label align-left margin-bottom-xxs'>Nomor Gudep</label>
                        </div>
                        <div className='col-lg-4 align-center'>
                            <label className='control-label align-left margin-bottom-xxs'>Pangkalan</label>
                        </div>
                    </div>

                    {items}

                    <div style={{ width: '100%', height: '1px', clear: 'both' }}></div>
                </div>
            </div>
        );
    }
};
