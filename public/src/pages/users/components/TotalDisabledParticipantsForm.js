import React from 'react';

export default class TotalDisabledParticipantsForm extends React.Component {
    render() {
        return(
            <div className='ibox float-e-margins'>
                <div className='ibox-title'>
                    <h5>Data Peserta dan Pembina PBK</h5>
                    <div className='ibox-tools'>
                        <a className='collapse-link'>
                            <i className='fa fa-chevron-up'></i>
                        </a>
                    </div>
                </div>
                <div className='ibox-content'>
                    <div className="col-lg-12">

                        <div className='col-lg-12 margin-top-xs'>
                            <div className='col-lg-1 align-center padding-top-bottom-xs'>
                                <h4 className='control-label align-left margin-bottom-xxs'>No</h4>
                            </div>
                            <div className='col-lg-5 padding-top-bottom-xs'>
                                <h4 className='control-label align-left margin-bottom-xxs'>PBK</h4>
                            </div>
                            <div className='col-lg-3 align-center'>
                                <h4>Peserta</h4>
                                <div className="row">
                                    <div className="col-lg-6">
                                        <label className='control-label align-left margin-bottom-xxs'>Putera</label>
                                    </div>
                                    <div className="col-lg-6">
                                        <label className='control-label align-left margin-bottom-xxs'>Puteri</label>
                                    </div>
                                </div>
                            </div>
                            <div className='col-lg-3 align-center'>
                                <h4>Pendamping</h4>
                                <div className="row">
                                    <div className="col-lg-6">
                                        <label className='control-label align-left margin-bottom-xxs'>Putera</label>
                                    </div>
                                    <div className="col-lg-6">
                                        <label className='control-label align-left margin-bottom-xxs'>Puteri</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className='col-lg-12 margin-bottom-xxs'>
                            <div className='col-lg-1 align-center no-padding'>
                                <p className='control-label align-left margin-top-xxs'>1.</p>
                            </div>
                            <div className='col-lg-5 no-padding'>
                                <p className='control-label align-left margin-top-xxs'>Tunanetra</p>
                            </div>
                            <div className='col-lg-3'>
                                {
                                    this.props.isForm == "true"?
                                        <div className="row">
                                            <div className="col-lg-6">
                                                <input type='text' className='form-control align-center' name='branch_registration.participants_specials.tuna_netra.num_male_participants' placeholder="Jumlah"></input>
                                            </div>
                                            <div className="col-lg-6">
                                                <input type='text' className='form-control align-center' name='branch_registration.participants_specials.tuna_netra.num_female_participants' placeholder="Jumlah"></input>
                                            </div>
                                        </div> :
                                        <div className="row">
                                            <div className="col-lg-6">
                                                <p className="align-center margin-top-xxs">10</p>
                                            </div>
                                            <div className="col-lg-6">
                                                <p className="align-center margin-top-xxs">15</p>
                                            </div>
                                        </div>
                                }
                            </div>
                            <div className='col-lg-3'>
                                {
                                    this.props.isForm == "true"?
                                        <div className="row">
                                            <div className="col-lg-6">
                                                <input type='text' className='form-control align-center' name='branch_registration.participants_specials.tuna_netra.num_male_leaders' placeholder="Jumlah"></input>
                                            </div>
                                            <div className="col-lg-6">
                                                <input type='text' className='form-control align-center' name='branch_registration.participants_specials.tuna_netra.num_female_leaders' placeholder="Jumlah"></input>
                                            </div>
                                        </div> :
                                        <div className="row">
                                            <div className="col-lg-6">
                                                <p className="align-center margin-top-xxs">5</p>
                                            </div>
                                            <div className="col-lg-6">
                                                <p className="align-center margin-top-xxs">3</p>
                                            </div>
                                        </div>
                                }
                            </div>
                        </div>

                        <div className='col-lg-12 margin-bottom-xxs'>
                            <div className='col-lg-1 align-center no-padding'>
                                <p className='control-label align-left margin-top-xxs'>2.</p>
                            </div>
                            <div className='col-lg-5 no-padding'>
                                <p className='control-label align-left margin-top-xxs'>Tunarungu</p>
                            </div>
                            <div className='col-lg-3'>
                                {
                                    this.props.isForm == "true"?
                                        <div className="row">
                                            <div className="col-lg-6">
                                                <input type='text' className='form-control align-center' name='branch_registration.participants_specials.tuna_rungu.num_male_participants' placeholder="Jumlah"></input>
                                            </div>
                                            <div className="col-lg-6">
                                                <input type='text' className='form-control align-center' name='branch_registration.participants_specials.tuna_rungu.num_female_participants' placeholder="Jumlah"></input>
                                            </div>
                                        </div> :
                                        <div className="row">
                                            <div className="col-lg-6">
                                                <p className="align-center margin-top-xxs">10</p>
                                            </div>
                                            <div className="col-lg-6">
                                                <p className="align-center margin-top-xxs">15</p>
                                            </div>
                                        </div>
                                }
                            </div>
                            <div className='col-lg-3'>
                                {
                                    this.props.isForm == "true"?
                                        <div className="row">
                                            <div className="col-lg-6">
                                                <input type='text' className='form-control align-center' name='branch_registration.participants_specials.tuna_rungu.num_male_leaders' placeholder="Jumlah"></input>
                                            </div>
                                            <div className="col-lg-6">
                                                <input type='text' className='form-control align-center' name='branch_registration.participants_specials.tuna_rungu.num_female_leaders' placeholder="Jumlah"></input>
                                            </div>
                                        </div> :
                                        <div className="row">
                                            <div className="col-lg-6">
                                                <p className="align-center margin-top-xxs">5</p>
                                            </div>
                                            <div className="col-lg-6">
                                                <p className="align-center margin-top-xxs">3</p>
                                            </div>
                                        </div>
                                }
                            </div>
                        </div>

                        <div className='col-lg-12 margin-bottom-xxs'>
                            <div className='col-lg-1 align-center no-padding'>
                                <p className='control-label align-left margin-top-xxs'>3.</p>
                            </div>
                            <div className='col-lg-5 no-padding'>
                                <p className='control-label align-left margin-top-xxs'>Tunagrahita</p>
                            </div>
                            <div className='col-lg-3'>
                                {
                                    this.props.isForm == "true"?
                                        <div className="row">
                                            <div className="col-lg-6">
                                                <input type='text' className='form-control align-center' name='branch_registration.participants_specials.tuna_grahita.num_male_participants' placeholder="Jumlah"></input>
                                            </div>
                                            <div className="col-lg-6">
                                                <input type='text' className='form-control align-center' name='branch_registration.participants_specials.tuna_grahita.num_female_participants' placeholder="Jumlah"></input>
                                            </div>
                                        </div> :
                                        <div className="row">
                                            <div className="col-lg-6">
                                                <p className="align-center margin-top-xxs">10</p>
                                            </div>
                                            <div className="col-lg-6">
                                                <p className="align-center margin-top-xxs">15</p>
                                            </div>
                                        </div>
                                }
                            </div>
                            <div className='col-lg-3'>
                                {
                                    this.props.isForm == "true"?
                                        <div className="row">
                                            <div className="col-lg-6">
                                                <input type='text' className='form-control align-center' name='branch_registration.participants_specials.tuna_grahita.num_male_leaders' placeholder="Jumlah"></input>
                                            </div>
                                            <div className="col-lg-6">
                                                <input type='text' className='form-control align-center' name='branch_registration.participants_specials.tuna_grahita.num_female_leaders' placeholder="Jumlah"></input>
                                            </div>
                                        </div> :
                                        <div className="row">
                                            <div className="col-lg-6">
                                                <p className="align-center margin-top-xxs">5</p>
                                            </div>
                                            <div className="col-lg-6">
                                                <p className="align-center margin-top-xxs">3</p>
                                            </div>
                                        </div>
                                }
                            </div>
                        </div>

                        <div className='col-lg-12 margin-bottom-xxs'>
                            <div className='col-lg-1 align-center no-padding'>
                                <p className='control-label align-left margin-top-xxs'>4.</p>
                            </div>
                            <div className='col-lg-5 no-padding'>
                                <p className='control-label align-left margin-top-xxs'>Tunadaksa</p>
                            </div>
                            <div className='col-lg-3'>
                                {
                                    this.props.isForm == "true"?
                                        <div className="row">
                                            <div className="col-lg-6">
                                                <input type='text' className='form-control align-center' name='branch_registration.participants_specials.tuna_daksa.num_male_participants' placeholder="Jumlah"></input>
                                            </div>
                                            <div className="col-lg-6">
                                                <input type='text' className='form-control align-center' name='branch_registration.participants_specials.tuna_daksa.num_female_participants' placeholder="Jumlah"></input>
                                            </div>
                                        </div> :
                                        <div className="row">
                                            <div className="col-lg-6">
                                                <p className="align-center margin-top-xxs">10</p>
                                            </div>
                                            <div className="col-lg-6">
                                                <p className="align-center margin-top-xxs">15</p>
                                            </div>
                                        </div>
                                }
                            </div>
                            <div className='col-lg-3'>
                                {
                                    this.props.isForm == "true"?
                                        <div className="row">
                                            <div className="col-lg-6">
                                                <input type='text' className='form-control align-center' name='branch_registration.participants_specials.tuna_daksa.num_male_leaders' placeholder="Jumlah"></input>
                                            </div>
                                            <div className="col-lg-6">
                                                <input type='text' className='form-control align-center' name='branch_registration.participants_specials.tuna_daksa.num_female_leaders' placeholder="Jumlah"></input>
                                            </div>
                                        </div> :
                                        <div className="row">
                                            <div className="col-lg-6">
                                                <p className="align-center margin-top-xxs">5</p>
                                            </div>
                                            <div className="col-lg-6">
                                                <p className="align-center margin-top-xxs">3</p>
                                            </div>
                                        </div>
                                }
                            </div>
                        </div>
                    </div>

                    <div style={{ width: '100%', height: '1px', clear: 'both' }}></div>
                </div>
            </div>
        );
    }
};
