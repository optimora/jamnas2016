import React from 'react';

export default class TotalDisabledParticipantsForm extends React.Component {
    render() {
        return(
            <div className='ibox float-e-margins'>
                <div className='ibox-title'>
                    <h5>Data PINKONDA dan PINKONCAB</h5>
                    <div className='ibox-tools'>
                        <a className='collapse-link'>
                            <i className='fa fa-chevron-up'></i>
                        </a>
                    </div>
                </div>
                <div className='ibox-content'>
                    <div className="col-lg-12">

                        <div className='col-lg-12 margin-bottom-xs'>
                            <div className='col-lg-1 no-margin no-padding align-center'>
                                <label className='control-label align-left margin-bottom-xxs'>No</label>
                            </div>
                            <div className='col-lg-4 align-center'>
                                <label className='control-label align-left margin-bottom-xxs'>PINKONDA / PINKONCAB</label>
                            </div>
                            <div className='col-lg-2 align-center'>
                                <label className='control-label align-left margin-bottom-xxs'>Putera</label>
                            </div>
                            <div className='col-lg-2 align-center'>
                                <label className='control-label align-left margin-bottom-xxs'>Puteri</label>
                            </div>
                        </div>

                        <div className='col-lg-12 margin-bottom-xxs'>
                            <div className='col-lg-1 align-center'>
                                <p className='control-label align-center margin-top-xxs'>1.</p>
                            </div>
                            <div className='col-lg-4'>
                                <p className='control-label align-center margin-top-xxs'>Pimpinan Kontingen Daerah</p>
                            </div>
                            <div className='col-lg-2'>
                                {
                                    this.props.isForm == "true"?
                                        <input type='text' className='form-control align-center' name='branch_registration.pinkonda.male' placeholder="Jumlah"></input> :
                                        <p className="align-center margin-top-xxs">9</p>
                                }
                            </div>
                            <div className='col-lg-2'>
                                <div className="row">
                                    {
                                        this.props.isForm == "true"?
                                            <input type='text' className='form-control align-center' name='branch_registration.pinkonda.female' placeholder="Jumlah"></input> :
                                            <p className="align-center margin-top-xxs">8</p>
                                    }
                                </div>
                            </div>
                        </div>

                        <div className='col-lg-12 margin-bottom-xxs'>
                            <div className='col-lg-1 align-center'>
                                <p className='control-label align-center margin-top-xxs'>2.</p>
                            </div>
                            <div className='col-lg-4'>
                                <p className='control-label align-center margin-top-xxs'>Pimpinan Kontingen Cabang</p>
                            </div>
                            <div className='col-lg-2'>
                                {
                                    this.props.isForm == "true"?
                                        <input type='text' className='form-control align-center' name='branch_registration.pinkoncab.male' placeholder="Jumlah"></input> :
                                        <p className="align-center margin-top-xxs">7</p>
                                }
                            </div>
                            <div className='col-lg-2'>
                                <div className="row">
                                    {
                                        this.props.isForm == "true"?
                                            <input type='text' className='form-control align-center' name='branch_registration.pinkoncab.female' placeholder="Jumlah"></input> :
                                            <p className="align-center margin-top-xxs">9</p>
                                    }
                                </div>
                            </div>
                        </div>

                    </div>

                    <div style={{ width: '100%', height: '1px', clear: 'both' }}></div>
                </div>
            </div>
        );
    }
};
