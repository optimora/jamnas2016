import React from 'react';
import moment from 'moment';

export default class BasicForm extends React.Component {
    render() {
        return(
            <div className='ibox float-e-margins'>
                <div className='ibox-title'>
                    <div className='col-lg-12'>
                        <h3 className='align-center'>{this.props.title}</h3>
                    </div>
                </div>
                <div className='ibox-content'>
                    <form method='get' className='form-horizontal'>

                        <div className='col-lg-12'>
                            <div className='col-lg-3'>
                                <center className='bg-dark-gray padding-top-bottom-xs'>
                                    <img src='http://placehold.it/150x200'></img>
                                </center>
                                <input type='file' className='margin-top-xs'></input>
                            </div>
                            <div className='col-lg-9'>

                                <div className='col-lg-12 no-margin no-padding'>
                                    <div className='col-lg-5'>
                                        <div className='form-group'>
                                            <label className='control-label align-left margin-bottom-xxs'>Kwartir Daerah</label>
                                            {
                                                this.props.isForm == "true" ?
                                                    <select className='form-control m-b no-margin' name='scout.branch' disabled>
                                                        <option values=''>DKI Jakarta</option>
                                                    </select> :
                                                    <p>{this.props.data.branch.name}</p>
                                            }
                                        </div>
                                    </div>
                                    <div className='col-lg-6 col-lg-offset-1'>
                                        <div className='form-group'>
                                            <label className='control-label align-left margin-bottom-xxs'>Kwartir Cabang</label>
                                            {
                                                this.props.isForm == "true" ?
                                                    <select className='form-control m-b no-margin' name='scout.subbranch' disabled>
                                                        <option value=''>Kota Jakarta Selatan</option>
                                                    </select> :
                                                    <p>{this.props.data.subbranch.name}</p>
                                            }
                                        </div>
                                    </div>
                                </div>

                                <div className='col-lg-12 no-margin no-padding'><div className='hr-line-dashed no-margin'></div></div>

                                <div className='col-lg-12 no-margin no-padding'>
                                    <div className="col-lg-6">
                                        <div className='form-group padding-right-md'>
                                            <label className='control-label align-left margin-bottom-xxs'>Keikutsertaan dalam JAMNAS</label>
                                            {
                                                this.props.isForm == "true" ?
                                                    <select className='form-control m-b no-margin' name='scout.type' disabled>
                                                        <option values='participant'>Panitia Penyelenggara Jamnas X 2016</option>
                                                    </select> :
                                                    <p>
                                                        {
                                                            this.props.data.type == "committee"?
                                                                "Panitia Jamnas X 2016":
                                                                "Peserta Jamnas X 2016"
                                                        }
                                                    </p>
                                            }
                                        </div>
                                    </div>
                                </div>

                                <div className='col-lg-12 no-margin no-padding'>
                                    <div className='col-lg-6'>
                                        <div className='form-group padding-right-md'>
                                            <label className='control-label align-left margin-bottom-xxs'>Jabatan Dalam Kepanitiaan</label>
                                            {
                                                this.props.isForm == "true" ?
                                                    <select className='form-control m-b no-margin' name='user.subtype'>
                                                        <option value=''>Panitia Pendukung</option>
                                                        <option value=''>Penyelenggara</option>
                                                        <option value=''>Penasehat</option>
                                                    </select> :
                                                    <p>
                                                        {
                                                            this.props.data.subtype == "committee-support"?
                                                                "Panitia Pendukung":
                                                                "Anggota"
                                                        }
                                                    </p>
                                            }
                                        </div>
                                    </div>
                                </div>
                                {
                                    this.props.isForm == "true" ?
                                        <div></div> :
                                        <div>
                                            {
                                                this.props.data.type == "participant"?
                                                    <div className='col-lg-12 no-margin no-padding'>
                                                        <div className='col-lg-6'>
                                                            <div className='form-group padding-right-md'>
                                                                <label className='control-label align-left margin-bottom-xxs'>Regu</label>
                                                                <p>{this.props.data.group.name}</p>
                                                            </div>
                                                        </div>
                                                    </div> :
                                                    <div></div>
                                            }
                                        </div>
                                }



                            </div>
                        </div>

                        <div className='col-lg-12 margin-top-md'><div className='hr-line-dashed no-margin'></div></div>

                        <div className='col-lg-12'>

                            <div className='col-lg-12 margin-top-xs no-padding'>
                                {
                                    this.props.isForm == "true"?
                                        <div className='col-lg-5'>
                                            <div className='form-group padding-right-md'>
                                                <label className='control-label align-left margin-bottom-xxs'>Nama Lengkap</label>
                                                <input type='text' className='form-control' name='user.name'></input>
                                            </div>
                                        </div>:
                                        <div className='col-lg-3'>
                                            <div className='form-group padding-right-md'>
                                                <label className='control-label align-left margin-bottom-xxs'>Nama Lengkap</label>
                                                <p>{this.props.data.user.name}</p>
                                            </div>
                                        </div>
                                }
                                {
                                    this.props.isForm == "true"?
                                        <div className='col-lg-4'>
                                            <div className='form-group padding-right-md'>
                                                <label className='control-label align-left margin-bottom-xxs'>NTA</label>
                                                <input type='text' className='form-control' name='scout.nta'></input>
                                            </div>
                                        </div> :
                                        <div className='col-lg-3'>
                                            <div className='form-group padding-right-md'>
                                                <label className='control-label align-left margin-bottom-xxs'>NTA</label>
                                                <p>{this.props.data.nta}</p>
                                            </div>
                                        </div>
                                }

                                {
                                    this.props.isForm == "true"?
                                        <div className='col-lg-3'>
                                            <div className='form-group'>
                                                <label className='control-label align-left margin-bottom-xxs'>Jenis Kelamin</label>
                                                <select className='form-control m-b no-margin' name='scout.gender'>
                                                    <option value='male'>Laki-Laki</option>
                                                    <option value='female'>Perempuan</option>
                                                </select>
                                            </div>
                                        </div> :
                                        <div className='col-lg-3'>
                                            <div className='form-group'>
                                                <label className='control-label align-left margin-bottom-xxs'>Jenis Kelamin</label>
                                                <p>{this.props.data.gender}</p>
                                            </div>
                                        </div>
                                }

                            </div>

                            <div className='col-lg-12 no-padding'>
                                {
                                    this.props.isForm == "true"?
                                        <div className='col-lg-4'>
                                            <div className='form-group padding-right-md'>
                                                <label className='control-label align-left margin-bottom-xxs'>Tempat Lahir</label>
                                                <input type='text' className='form-control' name='scout.birthplace'></input>
                                            </div>
                                        </div> :
                                        <div className='col-lg-3'>
                                            <div className='form-group padding-right-md'>
                                                <label className='control-label align-left margin-bottom-xxs'>Tempat Lahir</label>
                                                <p>{this.props.data.birthplace}</p>
                                            </div>
                                        </div>
                                }
                                {
                                    this.props.isForm == "true"?
                                        <div className='col-lg-3'>
                                            <div className='form-group padding-right-md' id='data_3'>
                                                <label className='control-label align-left margin-bottom-xxs'>Tanggal Lahir</label>
                                                <div className='input-group date'>
                                                    <span className='input-group-addon'><i className='fa fa-calendar'></i></span><input type='text' className='form-control' value='10/11/2013' name='scout.birthday'></input>
                                                </div>
                                            </div>
                                        </div> :
                                        <div className='col-lg-3'>
                                            <div className='form-group padding-right-md' id='data_3'>
                                                <label className='control-label align-left margin-bottom-xxs'>Tanggal Lahir</label>
                                                <p>{moment(this.props.data.birthday).format('DD-MM-YYYY')}</p>
                                            </div>
                                        </div>
                                }
                                {
                                    this.props.isForm == "true"?
                                        <div className='col-lg-3'>
                                            <div className='form-group padding-right-md'>
                                                <label className='control-label align-left margin-bottom-xxs'>Golongan Darah</label>
                                                <select className='form-control m-b no-margin' name='scout.blood_type'>
                                                    <option value='A'>A</option>
                                                    <option value='B'>B</option>
                                                    <option value='AB'>AB</option>
                                                    <option value='O'>O</option>
                                                </select>
                                            </div>
                                        </div> :
                                        <div className='col-lg-3'>
                                            <div className='form-group padding-right-md'>
                                                <label className='control-label align-left margin-bottom-xxs'>Golongan Darah</label>
                                                <p>{this.props.data.blood_type}</p>
                                            </div>
                                        </div>
                                }
                                {
                                    this.props.isForm == "true"?
                                        <div className='col-lg-2'>
                                            <div className='form-group'>
                                                <label className='control-label align-left margin-bottom-xxs'>Agama</label>
                                                <select className='form-control m-b' name='scout.religion'>
                                                    <option value='islam'>Islam</option>
                                                    <option value='kristen_protestan'>Kristen Protestan</option>
                                                    <option value='kristen_katolik'>Kristen Katolik</option>
                                                    <option value='hindu'>Hindu</option>
                                                    <option value='budha'>Budha</option>
                                                    <option value='konghucu'>Konghucu</option>
                                                    <option value='others'>Lainnya</option>
                                                </select>
                                            </div>
                                        </div> :
                                        <div className='col-lg-2'>
                                            <div className='form-group'>
                                                <label className='control-label align-left margin-bottom-xxs'>Agama</label>
                                                <p>{this.props.data.religion}</p>
                                            </div>
                                        </div>
                                }
                            </div>
                        </div>
                    </form>
                    <div style={{ width: '100%', height: '1px', clear: 'both' }}></div>
                </div>
            </div>
        );
    }
};


/*
    CONTEKAN (NANTI DI HAPUS)
 <div className='col-lg-3'>
 <div className='form-group'>
 <label className='control-label align-left margin-bottom-xxs'>Regu</label>
 <select className='form-control m-b no-margin' name='participant_group'>
 <option value=''>Rajawali</option>
 <option value=''>Elang</option>
 <option value=''>Tulip</option>
 <option value=''>Mawar</option>
 </select>
 </div>
 </div>

 <div className='col-lg-3 padding-top-mlg'>
 <a data-toggle='modal' className='fg-red' href='#modal-form'>
 <i className='fa fa-plus'></i> Tambah Regu
 </a>
 <div id="modal-form" className="modal fade" aria-hidden="true">
 <div className="modal-dialog">
 <div className="modal-content">
 <div className="modal-body">
 <div className="row">
 <div className="col-sm-12 align-center">
 <h3 className="m-t-none m-b">Tambah Nama Regu</h3>
 <p>Masukkan nama regu baru yang Anda inginkan</p>
 <div className='col-lg-6 col-lg-offset-3'>
 <input type='text' className='form-control align-center' name='new_group'></input>
 <button className='btn bg-jamnas-red margin-top-xs'><i className='fa fa-save'></i> Simpan</button>
 </div>

 </div>
 </div>
 </div>
 </div>
 </div>
 </div>
 </div>
* */
