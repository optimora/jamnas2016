import React from 'react';

export default class ParticipantsDocumentForm extends React.Component {
  render() {
    return(
      <div className='row'>
        <div className='col-lg-12'>
          <div className='ibox float-e-margins'>
            <div className='ibox-title'>
              <h5>KELENGKAPAN DOKUMEN</h5>
              <div className='ibox-tools'>
                  <a className='collapse-link'>
                      <i className='fa fa-chevron-up'></i>
                  </a>
              </div>
            </div>
            <div className='ibox-content'>

              <div className='col-lg-12 margin-bottom-xxs'>

              </div>

              <div className='col-lg-12 margin-bottom-xs'>
                <div className='col-lg-4 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Nama Dokumen</label>
                </div>
                <div className='col-lg-4 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Unggah Dokumen</label>
                </div>
                <div className='col-lg-4 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Keterangan</label>
                </div>
              </div>

              <div className='col-lg-12 margin-bottom-xs'>
                <div className='col-lg-4 align-left'>
                  <p className='margin-top-xs'>1. KTA Gerakan Pramuka</p>
                </div>
                <div className='col-lg-4 align-center'>
                  <inpu type='hidden' name='files.type' value='kta'></inpu>
                  <input type='file' className='margin-top-xs' name='files.name'></input>
                </div>
                <div className='col-lg-4 align-center'>
                  <input type='text' className='form-control' name='files.notes'></input>
                </div>
              </div>

              <div className='col-lg-12 margin-bottom-xs'>
                <div className='col-lg-4 align-left'>
                  <p className='margin-top-xs'>2. Surat Keterangan Sehat dari Dokter</p>
                </div>
                <div className='col-lg-4 align-center'>
                  <input type='hidden' name='files.type' value='medical-letter'></input>
                  <input type='file' className='margin-top-xs' name='files.name'></input>
                </div>
                <div className='col-lg-4 align-center'>
                  <input type='text' className='form-control' name='files.notes'></input>
                </div>
              </div>

              <div className='col-lg-12 margin-bottom-xs'>
                <div className='col-lg-4 align-left'>
                  <p className='margin-top-xs'>3. Surat Keterangan dari Orang Tua</p>
                </div>
                <div className='col-lg-4 align-center'>
                  <inpu type='hidden' name='files.type' value='parent-approval'></inpu>
                  <input type='file' className='margin-top-xs' name='files.name'></input>
                </div>
                <div className='col-lg-4 align-center'>
                  <input type='text' className='form-control' name='files.notes'></input>
                </div>
              </div>

              <div className='col-lg-12 margin-bottom-xs'>
                <div className='col-lg-4 align-left'>
                  <p className='margin-top-xs'>4. Surat Keterangan dari Sekolah</p>
                </div>
                <div className='col-lg-4 align-center'>
                  <input type='hidden' name='files.type' value='parent-approval'></input>
                  <input type='file' className='margin-top-xs' name='files.name'></input>
                </div>
                <div className='col-lg-4 align-center'>
                  <input type='text' className='form-control' name='files.notes'></input>
                </div>
              </div>

              <div className='col-lg-12 margin-bottom-xs'>
                <div className='col-lg-4 align-left'>
                  <p className='margin-top-xs'>5. Asuransi</p>
                </div>
                <div className='col-lg-4 align-center'>
                  <input type='hidden' name='files.type' value='parent-approval'></input>
                  <input type='file' className='margin-top-xs' name='files.name'></input>
                </div>
                <div className='col-lg-4 align-center'>
                  <input type='text' className='form-control' name='files.notes'></input>
                </div>
              </div>

              <div style={{ width: '100%', height: '1px', clear: 'both' }}></div>
            </div>
            <button className='form-control btn btn-green bold-700 uppercase' type='submit'><i className='fa fa-save margin-right-xs'></i>Simpan Data Peserta</button>
          </div>
        </div>
      </div>

    );
  }
};
