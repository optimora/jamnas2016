import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import React from 'react';
import moment from 'moment';

import * as commonActions from '../../../actions/common/actions';
import * as usersBranchWillingnessLetterActions from '../actions/usersBranchWillingnessLetterActions';


class BranchWillingnessLetterView extends React.Component {
  static get propTypes() {
    return {
      BranchWillingnessLetter: React.PropTypes.object,
      actions: React.PropTypes.object
    }
  }

  render() {
    return(
      <div className='row'>
        <div className='col-lg-12'>
          <div className='ibox float-e-margins'>

            <div className='col-lg-12 title-center align-center'>
              <h5>KESEDIAAN KWARTIR DAERAH MENGIKUTI</h5>
              <h5>JAMBORE NASIONAL X 2016</h5>
            </div>

            <div className='ibox-content'>

              <div className='col-lg-12'>
                <div className='col-lg-4'>
                  <p>Nama Kwartir Daerah</p>
                </div>
                <div className='col-lg-8'>
                  <p>: {window.currentPageData.user.branch.name}</p>
                </div>
              </div>

              <div className='col-lg-12'>
                <div className='col-lg-4'>
                  <p>Alamat</p>
                </div>
                <div className='col-lg-6'>
                  <p>: {this.props.BranchWillingnessLetter.data.address}</p>
                </div>
              </div>

              <div className='col-lg-12'>
                <div className='col-lg-4'>
                  <p>No Telp Kwarcab</p>
                </div>
                <div className='col-lg-6'>
                  <p>: {this.props.BranchWillingnessLetter.data.phone}</p>
                </div>
              </div>

              <div className='col-lg-12'>
                <div className='col-lg-4'>
                  <p>No HP Bindamping</p>
                </div>
                <div className='col-lg-6'>
                  <p>: {this.props.BranchWillingnessLetter.data.mobile_phone}</p>
                </div>
              </div>


              <div className='col-lg-12 margin-bottom-md'><div className='hr-line-dashed no-margin'></div></div>

              <div className='col-lg-12'>
                <h3 className='align-center uppercase'>Dengan ini menyatakan siap dan bersedia</h3>
                <h3 className='align-center uppercase'>Mengikutsertakan Pramuka Penggalang</h3>
                <h3 className='align-center uppercase'>Pada Kegiatan Jambore Nasional 2016</h3>
                <h3 className='align-center uppercase'>Dan Siap Mematuhi Segala Ketentuan Yang Berlaku</h3>
              </div>

              <div className='col-lg-12 margin-top-md margin-bottom-md'><div className='hr-line-dashed no-margin'></div></div>

              <div className='col-lg-12 align-center margin-bottom-xs'>
                <h4 className='uppercase bold-700 underline'>Jumlah Peserta</h4>
              </div>

              <div className='col-lg-12'>
                <div className='col-lg-4 col-lg-offset-2'>
                  <table>
                    <tbody>
                      <tr>
                        <td width='50%'>Penggalang Pa</td>
                        <td className='align-right'>{this.props.BranchWillingnessLetter.data.number_of.penggalang_pa} Orang</td>
                      </tr>
                      <tr>
                        <td width='50%'>Penggalang Pi</td>
                        <td className='align-right'>{this.props.BranchWillingnessLetter.data.number_of.penggalang_pi} Orang</td>
                      </tr>
                      <tr>
                        <td width='50%'>PBK Pa</td>
                        <td className='align-right'>{this.props.BranchWillingnessLetter.data.number_of.pbk_pa} Orang</td>
                      </tr>
                      <tr>
                        <td width='50%'>PBK Pi</td>
                        <td className='align-right'>{this.props.BranchWillingnessLetter.data.number_of.pbk_pi} Orang</td>
                      </tr>
                      <tr className='margin-bottom-lg'></tr>
                      <tr>
                        <td width='50%' className='bold-700'>Jumlah Peserta</td>
                        <td className='bold-700 align-right'>{this.props.BranchWillingnessLetter.data.number_of.jumlah_peserta} Orang</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div className='col-lg-4 no-margin'>
                  <table>
                    <tbody>
                      <tr>
                        <td width='50%' className='margin-top-xs'>Bindamping Putra</td>
                        <td className='align-right'>{this.props.BranchWillingnessLetter.data.number_of.bindamping_pa} Orang</td>
                      </tr>
                      <tr>
                        <td width='50%'>Bindamping Pi</td>
                        <td className='align-right margin-top-lg'>{this.props.BranchWillingnessLetter.data.number_of.bindamping_pi} Orang</td>
                      </tr>
                      <tr>
                        <td width='50%'>Pinkonda</td>
                        <td className='align-right'>{this.props.BranchWillingnessLetter.data.number_of.pinkonda} Orang</td>
                      </tr>
                      <tr>
                        <td width='50%'>Pinkoncab</td>
                        <td className='align-right'>{this.props.BranchWillingnessLetter.data.number_of.pinkoncab} Orang</td>
                      </tr>
                      <tr>
                        <td width='50%' className='bold-700'>Jumlah</td>
                        <td className='align-right bold-700'>{this.props.BranchWillingnessLetter.data.number_of.jumlah_pimpinan} Orang</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>

              <div className='col-lg-12 margin-top-lg'>
                <div className='col-lg-8 col-lg-offset-2'>
                    <a href={'/stored_files/' + this.props.BranchWillingnessLetter.data.file} target='_blank' className='form-control btn btn-green bold-700' type='button'><i className='fa fa-download margin-right-xs'></i> Lampiran Form D.01 - {window.currentPageData.user.branch.name}</a>
                    <p className='fg-red align-center'>Jika Anda tidak dapat melihat Lampiran Form D.01, maka Kwartir Daerah tersebut tidak melampirkan bukti otentik.</p>
                </div>
              </div>

              <div className='col-lg-12 margin-top-lg'>
                <div className='col-lg-8 col-lg-offset-2 align-center'>
                  <p className='margin-top-xs margin-right-xs'>Formulir telah dikirimkan pada <span className='bold-700 fg-red'>{moment(new Date(this.props.BranchWillingnessLetter.data.created)).format("DD/MM/YYYY HH:mm:ss")}</span></p>
                </div>
              </div>

              <div style={{ width: '100%', height: '1px', clear: 'both' }}></div>
            </div>
          </div>
        </div>
      </div>

    );
  }
};

const actionCreators = Object.assign({}, commonActions, usersBranchWillingnessLetterActions);
const mapStateToProps = (state) => {
  return state;
};

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(actionCreators, dispatch)
  };
};

let connector = connect(
    mapStateToProps,
    mapDispatchToProps
);

export default connector(BranchWillingnessLetterView);
