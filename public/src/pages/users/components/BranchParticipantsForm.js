import React from 'react';

export default class BranchParticipantsForm extends React.Component {
  render() {
    return(
      <div className='row'>
        <div className='col-lg-12'>
          <div className='ibox float-e-margins'>
            <div className='ibox-title'>
              <div className='col-lg-12'>
                <h3 className='align-center'>BIODATA PESERTA JAMBORE NASIONAL X 2016</h3>
              </div>
            </div>
            <div className='ibox-content'>
              <form method='get' className='form-horizontal'>

                <div className='col-lg-12'>
                  <div className='col-lg-3'>
                    <center className='bg-dark-gray padding-top-bottom-xs'>
                        <img src='http://placehold.it/150x200'></img>
                    </center>
                    <input type='file' className='margin-top-xs'></input>
                  </div>
                  <div className='col-lg-9'>

                    <div className='col-lg-12 no-margin no-padding'>
                      <div className='col-lg-5'>
                        <div className='form-group'>
                          <label className='control-label align-left margin-bottom-xxs'>Kwartir Daerah</label>
                          <select className='form-control m-b no-margin' name='participant_branch_name' disabled>
                            <option values=''>DKI Jakarta</option>
                          </select>
                        </div>
                      </div>
                      <div className='col-lg-6 col-lg-offset-1'>
                        <div className='form-group'>
                          <label className='control-label align-left margin-bottom-xxs'>Kwartir Cabang</label>
                          <select className='form-control m-b no-margin' name='participant_sub_branch_name' disabled>
                            <option value=''>Kota Jakarta Selatan</option>
                          </select>
                        </div>
                      </div>
                    </div>

                    <div className='col-lg-12 no-margin no-padding'><div className='hr-line-dashed no-margin'></div>                  </div>

                    <div className='col-lg-12 margin-top-xs no-padding'>
                      <div className='col-lg-7'>
                        <div className='form-group padding-right-md'>
                          <label className='control-label align-left margin-bottom-xxs'>Nama Lengkap</label>
                          <input type='text' className='form-control' name='participant_name'></input>
                        </div>
                      </div>
                      <div className='col-lg-5'>
                        <div className='form-group'>
                          <label className='control-label align-left margin-bottom-xxs'>NTA</label>
                          <input type='text' className='form-control' name='participant_nta'></input>
                        </div>
                      </div>
                    </div>

                    <div className='col-lg-12 no-padding'>
                      <div className='col-lg-5'>
                        <div className='form-group padding-right-md'>
                          <label className='control-label align-left margin-bottom-xxs'>Tempat Lahir</label>
                          <input type='text' className='form-control' name='participant_birth_place'></input>
                        </div>
                      </div>
                      <div className='col-lg-4'>
                        <div className='form-group padding-right-md' id='data_3'>
                          <label className='control-label align-left margin-bottom-xxs'>Tanggal Lahir</label>
                          <div className='input-group date'>
                              <span className='input-group-addon'><i className='fa fa-calendar'></i></span><input type='text' className='form-control' value='10/11/2013'></input>
                          </div>
                        </div>
                      </div>
                      <div className='col-lg-3'>
                        <div className='form-group'>
                          <label className='control-label align-left margin-bottom-xxs'>Jenis Kelamin</label>
                          <select className='form-control m-b no-margin' name='participant_gender'>
                            <option value='male'>Laki-Laki</option>
                            <option value='female'>Perempuan</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
              <div style={{ width: '100%', height: '1px', clear: 'both' }}></div>
            </div>
          </div>
        </div>
      </div>

    );
  }
};
