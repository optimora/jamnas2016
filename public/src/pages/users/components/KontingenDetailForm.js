/**
 * Created by saras on 5/26/16.
 */
import React from 'react';

export default class KontingenDetailForm extends React.Component {
    render() {
        return(
            <div className='ibox float-e-margins'>
                <div className='ibox-title'>
                    <h5>Detail Kontingen</h5>
                    <div className='ibox-tools'>
                        <a className='collapse-link'>
                            <i className='fa fa-chevron-up'></i>
                        </a>
                    </div>
                </div>
                <div className='ibox-content'>
                    <div className="col-lg-12">

                        <div className='col-lg-12 margin-top-xs'>
                            <div className='col-lg-1 align-center'>
                                <label className='control-label align-left margin-bottom-xxs'>No</label>
                            </div>
                            <div className='col-lg-7'>
                                <label className='control-label align-left margin-bottom-xxs'>Klasifikasi</label>
                            </div>
                            <div className='col-lg-2 align-center'>
                                <label className='control-label align-left margin-bottom-xxs'>Putera</label>
                            </div>
                            <div className='col-lg-2 align-center'>
                                <label className='control-label align-left margin-bottom-xxs'>Puteri</label>
                            </div>
                        </div>

                        {
                            this.props.isForm == "true"?
                                <div className='col-lg-12 margin-bottom-xxs'>
                                    <div className='col-lg-1 align-center'>
                                        <p className='control-label align-center margin-top-xxs'>1.</p>
                                    </div>
                                    <div className='col-lg-7'>
                                        <p className='control-label align-left margin-top-xxs'>Pimpinan Kontingen Daerah</p>
                                    </div>
                                    <div className='col-lg-2'>
                                        <input type='text' className='form-control align-center' name='transports.contingen.contingen_branch_leader.male' placeholder="Jumlah"></input>
                                    </div>
                                    <div className='col-lg-2'>
                                        <input type='text' className='form-control align-center' name='transports.contingen.contingen_branch_leader.female' placeholder="Jumlah"></input>
                                    </div>
                                </div> :
                                <div className='col-lg-12 margin-bottom-xxs'>
                                    <div className='col-lg-1 align-center'>
                                        <p className='control-label align-center margin-top-xxs'>1.</p>
                                    </div>
                                    <div className='col-lg-7'>
                                        <p className='control-label align-left margin-top-xxs'>Pimpinan Kontingen Daerah</p>
                                    </div>
                                    <div className='col-lg-2'>
                                        <p className='control-label align-center margin-top-xxs'>3</p>
                                    </div>
                                    <div className='col-lg-2'>
                                        <p className='control-label align-center margin-top-xxs'>7</p>
                                    </div>
                                </div>
                        }

                        {
                            this.props.isForm == "true"?
                                <div className='col-lg-12 margin-bottom-xxs'>
                                    <div className='col-lg-1 align-center'>
                                        <p className='control-label align-center margin-top-xxs'>2.</p>
                                    </div>
                                    <div className='col-lg-7'>
                                        <p className='control-label align-left margin-top-xxs'>Staf Kontingen Daerah</p>
                                    </div>
                                    <div className='col-lg-2'>
                                        <input type='text' className='form-control align-center' name='transports.contingen.contingen_branch_staff.male' placeholder="Jumlah"></input>
                                    </div>
                                    <div className='col-lg-2'>
                                        <input type='text' className='form-control align-center' name='transports.contingen.contingen_branch_staff.female' placeholder="Jumlah"></input>
                                    </div>
                                </div> :
                                <div className='col-lg-12 margin-bottom-xxs'>
                                    <div className='col-lg-1 align-center'>
                                        <p className='control-label align-center margin-top-xxs'>2.</p>
                                    </div>
                                    <div className='col-lg-7'>
                                        <p className='control-label align-left margin-top-xxs'>Staf Kontingen Daerah</p>
                                    </div>
                                    <div className='col-lg-2'>
                                        <p className='control-label align-center margin-top-xxs'>10</p>
                                    </div>
                                    <div className='col-lg-2'>
                                        <p className='control-label align-center margin-top-xxs'>5</p>
                                    </div>
                                </div>
                        }


                        {
                            this.props.isForm == "true"?
                                <div className='col-lg-12 margin-bottom-xxs'>
                                    <div className='col-lg-1 align-center'>
                                        <p className='control-label align-center margin-top-xxs'>3.</p>
                                    </div>
                                    <div className='col-lg-7'>
                                        <p className='control-label align-left margin-top-xxs'>Pimpinan Kontingen Cabang</p>
                                    </div>
                                    <div className='col-lg-2'>
                                        <input type='text' className='form-control align-center' name='transports.contingen.contingen_subbranch_leader.male' placeholder="Jumlah"></input>
                                    </div>
                                    <div className='col-lg-2'>
                                        <input type='text' className='form-control align-center' name='transports.contingen.contingen_subbranch_leader.female' placeholder="Jumlah"></input>
                                    </div>
                                </div> :
                                <div className='col-lg-12 margin-bottom-xxs'>
                                    <div className='col-lg-1 align-center'>
                                        <p className='control-label align-center margin-top-xxs'>3.</p>
                                    </div>
                                    <div className='col-lg-7'>
                                        <p className='control-label align-left margin-top-xxs'>Pimpinan Kontingen Cabang</p>
                                    </div>
                                    <div className='col-lg-2'>
                                        <p className='control-label align-center margin-top-xxs'>7</p>
                                    </div>
                                    <div className='col-lg-2'>
                                        <p className='control-label align-center margin-top-xxs'>3</p>
                                    </div>
                                </div>
                        }

                        {
                            this.props.isForm == "true"?
                                <div className='col-lg-12 margin-bottom-xxs'>
                                    <div className='col-lg-1 align-center'>
                                        <p className='control-label align-center margin-top-xxs'>4.</p>
                                    </div>
                                    <div className='col-lg-7'>
                                        <p className='control-label align-left margin-top-xxs'>Pembina Pendamping</p>
                                    </div>
                                    <div className='col-lg-2'>
                                        <input type='text' className='form-control align-center' name='transports.contingen.leader.male' placeholder="Jumlah"></input>
                                    </div>
                                    <div className='col-lg-2'>
                                        <input type='text' className='form-control align-center' name='transports.contingen.leader.female' placeholder="Jumlah"></input>
                                    </div>
                                </div> :
                                <div className='col-lg-12 margin-bottom-xxs'>
                                    <div className='col-lg-1 align-center'>
                                        <p className='control-label align-center margin-top-xxs'>4.</p>
                                    </div>
                                    <div className='col-lg-7'>
                                        <p className='control-label align-left margin-top-xxs'>Pembina Pendamping</p>
                                    </div>
                                    <div className='col-lg-2'>
                                        <p className='control-label align-center margin-top-xxs'>6</p>
                                    </div>
                                    <div className='col-lg-2'>
                                        <p className='control-label align-center margin-top-xxs'>5</p>
                                    </div>
                                </div>
                        }

                        {
                            this.props.isForm == "true"?
                                <div className='col-lg-12 margin-bottom-xxs'>
                                    <div className='col-lg-1 align-center'>
                                        <p className='control-label align-center margin-top-xxs'>5.</p>
                                    </div>
                                    <div className='col-lg-7'>
                                        <p className='control-label align-left margin-top-xxs'>Pramuka Penggalang</p>
                                    </div>
                                    <div className='col-lg-2'>
                                        <input type='text' className='form-control align-center' name='transports.contingen.scout_penggalang.male' placeholder="Jumlah"></input>
                                    </div>
                                    <div className='col-lg-2'>
                                        <input type='text' className='form-control align-center' name='transports.contingen.scout_penggalang.female' placeholder="Jumlah"></input>
                                    </div>
                                </div> :
                                <div className='col-lg-12 margin-bottom-xxs'>
                                    <div className='col-lg-1 align-center'>
                                        <p className='control-label align-center margin-top-xxs'>5.</p>
                                    </div>
                                    <div className='col-lg-7'>
                                        <p className='control-label align-left margin-top-xxs'>Pramuka Penggalang</p>
                                    </div>
                                    <div className='col-lg-2'>
                                        <p className='control-label align-center margin-top-xxs'>58</p>
                                    </div>
                                    <div className='col-lg-2'>
                                        <p className='control-label align-center margin-top-xxs'>57</p>
                                    </div>
                                </div>
                        }

                        {
                            this.props.isForm == "true"?
                                <div className='col-lg-12 margin-bottom-xxs'>
                                    <div className='col-lg-1 align-center'>
                                        <p className='control-label align-center margin-top-xxs'>6.</p>
                                    </div>
                                    <div className='col-lg-7'>
                                        <p className='control-label align-left margin-top-xxs'>Pramuka Berkebutuhan Khusus (PBK)</p>
                                    </div>
                                    <div className='col-lg-2'>
                                        <input type='text' className='form-control align-center' name='transports.contingen.scout_pbk.male' placeholder="Jumlah"></input>
                                    </div>
                                    <div className='col-lg-2'>
                                        <input type='text' className='form-control align-center' name='transports.contingen.scout_pbk.female' placeholder="Jumlah"></input>
                                    </div>
                                </div> :
                                <div className='col-lg-12 margin-bottom-xxs'>
                                    <div className='col-lg-1 align-center'>
                                        <p className='control-label align-center margin-top-xxs'>6.</p>
                                    </div>
                                    <div className='col-lg-7'>
                                        <p className='control-label align-left margin-top-xxs'>Pramuka Berkebutuhan Khusus (PBK)</p>
                                    </div>
                                    <div className='col-lg-2'>
                                        <p className='control-label align-center margin-top-xxs'>23</p>
                                    </div>
                                    <div className='col-lg-2'>
                                        <p className='control-label align-center margin-top-xxs'>30</p>
                                    </div>
                                </div>
                        }

                        {
                            this.props.isForm == "true"?
                                <div className='col-lg-12 margin-bottom-xxs'>
                                    <div className='col-lg-1 align-center'>
                                        <p className='control-label align-center margin-top-xxs'></p>
                                    </div>
                                    <div className='col-lg-7'>
                                        <label className='control-label align-center margin-top-xxs'>Total</label>
                                    </div>
                                    <div className='col-lg-2'>
                                        <input type='text' className='form-control align-center' name='transports.total.male' placeholder="Jumlah" disabled></input>
                                    </div>
                                    <div className='col-lg-2'>
                                        <input type='text' className='form-control align-center' name='transports.total.female' placeholder="Jumlah" disabled></input>
                                    </div>
                                </div> :
                                <div className='col-lg-12 margin-bottom-xxs'>
                                    <div className='col-lg-1 align-center'>
                                        <p className='control-label align-center margin-top-xxs'></p>
                                    </div>
                                    <div className='col-lg-7'>
                                        <label className='control-label align-center margin-top-xxs'>Total</label>
                                    </div>
                                    <div className='col-lg-2'>
                                        <p className='control-label align-center margin-top-xxs'><strong>107</strong></p>
                                    </div>
                                    <div className='col-lg-2'>
                                        <p className='control-label align-center margin-top-xxs'><strong>107</strong></p>
                                    </div>
                                </div>
                        }

                    </div>

                    <div style={{ width: '100%', height: '1px', clear: 'both' }}></div>
                </div>
            </div>
        );
    }
};
