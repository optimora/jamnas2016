import React from 'react';

export default class ParticipantsBasicForm extends React.Component {
  render() {
    return(
      <div className='row'>
        <div className='col-lg-12'>
          <div className='ibox float-e-margins'>
            <div className='ibox-title'>
              <div className='col-lg-12'>
                <h3 className='align-center'>BIODATA PESERTA JAMBORE NASIONAL X 2016</h3>
              </div>
            </div>
            <div className='ibox-content'>
              <form method='get' className='form-horizontal'>

                <div className='col-lg-12'>
                  <div className='col-lg-3'>
                    <center className='bg-dark-gray padding-top-bottom-xs'>
                        <img src='http://placehold.it/150x200' name='scout.avatar'></img>
                    </center>
                    <input type='file' className='margin-top-xs'></input>
                  </div>
                  <div className='col-lg-9'>

                    <div className='col-lg-12 no-margin no-padding'>
                      <div className='col-lg-5'>
                        <div className='form-group'>
                          <label className='control-label align-left margin-bottom-xxs'>Kwartir Daerah</label>
                          <select className='form-control m-b no-margin' name='scout.branch' disabled>
                            <option values=''>DKI Jakarta</option>
                          </select>
                        </div>
                      </div>
                      <div className='col-lg-6 col-lg-offset-1'>
                        <div className='form-group'>
                          <label className='control-label align-left margin-bottom-xxs'>Kwartir Cabang</label>
                          <select className='form-control m-b no-margin' name='scout.subbranch' disabled>
                            <option value=''>Kota Jakarta Selatan</option>
                          </select>
                        </div>
                      </div>
                    </div>

                    <div className='col-lg-12 no-margin no-padding'><div className='hr-line-dashed no-margin'></div></div>

                    <div className='col-lg-12'>
                      <div className='form-group'>
                        <div className='col-lg-6 no-margin padding-right-md no-padding-left'>
                          <label className='control-label align-left margin-bottom-xxs'>Keikutsertaan Jamnas sebagai:</label>
                          <select className='form-control m-b no-margin' name='scout.type'>
                            <option value='participant'>Peserta Jamnas X 2016</option>
                            <option value='leader-cmt'>Pimpinan Kontingen Daerah</option>
                            <option value='leader-staff'>Staff Kontingen Daerah</option>
                            <option value='coach'>Pembina Pendamping</option>
                            <option value='leader-dct'>Pimpinan Kontingen Cabang</option>
                          </select>
                        </div>
                        <div className='col-lg-6 no-margin padding-right-md no-padding-left'>
                          <label className='control-label align-left margin-bottom-xxs'>Jabatan:</label>
                          <select className='form-control m-b no-margin' name='scout.subtype'>
                            <option value=''>-Jika PINKONDA-</option>
                            <option value='leader-head-cmt'>Ketua Pinkonda</option>
                            <option value='leader-vice-cmt'>Wakil Pinkonda</option>
                            <option value='leader-secretary-cmt'>Sekretaris Pinkonda</option>
                            <option value='leader-staff-cmt'>Anggota Pinkonda</option>
                          </select>
                          <select className='form-control m-b no-margin' name='scout.subtype'>
                            <option value=''>-Jika Staff KONDA-</option>
                            <option value='leader-medical-cmt'>Tim Dokter</option>
                            <option value='leader-exhibition-cmt'>Tim Pameran</option>
                            <option value='leader-cultural-cmt'>Tim Kegiatan Budaya</option>
                          </select>
                          <select className='form-control m-b no-margin' name='scout.subtype'>
                            <option value=''>-Jika PINKONCAB-</option>
                            <option value='leader-head-dct'>Ketua Pinkoncab</option>
                            <option value='leader-secretary-dct'>Wakil Pinkoncab</option>
                            <option value='leader-staff-dct'>Anggota Pinkoncab</option>
                          </select>
                          <select className='form-control m-b no-margin' name='scout.participantRole'>
                            <option value=''>- Jika Peserta Jamnas 2016-</option>
                            <option value='leader'>Pemimpin Regu</option>
                            <option value='co-leader'>Wakil Pemimpin Regu</option>
                            <option value='member'>Anggota</option>
                          </select>
                        </div>
                      </div>
                    </div>

                    <div className='col-lg-12 no-margin no-padding'>
                      <div className='col-lg-4'>
                        <div className='form-group'>
                          <label className='control-label align-left margin-bottom-xxs'>Regu</label>
                          <select className='form-control m-b no-margin' name='group.name'>
                            <option value=''>Rajawali</option>
                            <option value=''>Elang</option>
                            <option value=''>Tulip</option>
                            <option value=''>Mawar</option>
                          </select>
                        </div>
                      </div>

                      <div className='col-lg-3 padding-top-mlg'>
                        <a data-toggle='modal' className='fg-red' href='#modal-form'>
                          <i className='fa fa-plus'></i> Tambah Regu
                        </a>
                        <div id="modal-form" className="modal fade" aria-hidden="true">
                          <div className="modal-dialog">
                            <div className="modal-content">
                              <div className="modal-body">
                                <div className="row">
                                  <div className="col-sm-12 align-center">
                                    <h3 className="m-t-none m-b">Tambah Nama Regu</h3>
                                    <p>Masukkan nama regu baru yang Anda inginkan</p>
                                    <div className='col-lg-6 col-lg-offset-3'>
                                        <input type='text' className='form-control align-center' name='group.name'></input>
                                        <button className='btn bg-jamnas-red margin-top-xs'><i className='fa fa-save'></i> Simpan</button>
                                    </div>

                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>

                <div className='col-lg-12 margin-top-md'><div className='hr-line-dashed no-margin'></div></div>

                <div className='col-lg-12'>

                  <div className='col-lg-12 margin-top-xs no-padding'>
                    <div className='col-lg-5'>
                      <div className='form-group padding-right-md has-error'>
                        <label className='control-label align-left margin-bottom-xxs'>Nama Lengkap</label>
                        <input type='text' className='form-control margin-bottom-xxs' name='user.name'></input>
                        <mark className='register'>Nama tidak dikenal</mark>
                      </div>
                    </div>
                    <div className='col-lg-4'>
                      <div className='form-group padding-right-md'>
                        <label className='control-label align-left margin-bottom-xxs'>NTA</label>
                        <input type='text' className='form-control' name='scout.nta'></input>
                      </div>
                    </div>
                    <div className='col-lg-3'>
                      <div className='form-group has-error'>
                        <label className='control-label align-left margin-bottom-xxs'>Jenis Kelamin</label>
                        <select className='form-control m-b margin-bottom-xxs' name='scout.gender'>
                          <option value='male'>Laki-Laki</option>
                          <option value='female'>Perempuan</option>
                        </select>
                        <mark className='register'>Pilih salah satu</mark>
                      </div>
                    </div>
                  </div>

                  <div className='col-lg-12 no-padding'>
                    <div className='col-lg-3'>
                      <div className='form-group padding-right-md'>
                        <label className='control-label align-left margin-bottom-xxs'>Tempat Lahir</label>
                        <input type='text' className='form-control' name='scout.birthplace'></input>
                      </div>
                    </div>
                    <div className='col-lg-5'>
                      <div className='form-group margin-left-xs' id='data_1'>
                        <label className='control-label align-left margin-bottom-xxs'>Tanggal Lahir</label>
                        <div className="form-inline">
                          <div className="form-group">
                            <select className="form-control margin-right-xs" id="sel1">
                              <option value="1">1</option>
                              <option value="2">2</option>
                              <option value="3">3</option>
                              <option value="4">4</option>
                              <option value="5">5</option>
                              <option value="6">6</option>
                              <option value="7">7</option>
                              <option value="8">8</option>
                              <option value="9">9</option>
                              <option value="10">10</option>
                              <option value="11">11</option>
                              <option value="12">12</option>
                              <option value="13">13</option>
                              <option value="14">14</option>
                              <option value="15">15</option>
                              <option value="16">16</option>
                              <option value="17">17</option>
                              <option value="18">18</option>
                              <option value="19">19</option>
                              <option value="20">20</option>
                              <option value="21">21</option>
                              <option value="22">22</option>
                              <option value="23">23</option>
                              <option value="24">24</option>
                              <option value="25">25</option>
                              <option value="26">26</option>
                              <option value="27">27</option>
                              <option value="28">28</option>
                              <option value="29">29</option>
                              <option value="30">30</option>
                              <option value="31">31</option>
                            </select>
                            <select className="form-control margin-right-xs" id="sel2">
                              <option value="1">Januari</option>
                              <option value="2">Februari</option>
                              <option value="3">Maret</option>
                              <option value="4">April</option>
                              <option value="5">Mei</option>
                              <option value="6">Juni</option>
                              <option value="7">Juli</option>
                              <option value="8">Agustus</option>
                              <option value="9">September</option>
                              <option value="10">Oktober</option>
                              <option value="11">November</option>
                              <option value="12">Desember</option>
                            </select>
                            <select className="form-control margin-right-xs" id="sel3">
                              <option value="196X">196X</option>
                              <option value='2010'>2010</option>
                            </select>
                          </div>
                        </div>

                      </div>
                    </div>
                    <div className='col-lg-2'>
                      <div className='form-group padding-right-md'>
                        <label className='control-label align-left margin-bottom-xxs'>Golongan Darah</label>
                        <select className='form-control m-b no-margin' name='scout.blood_type'>
                          <option value='A'>A</option>
                          <option value='B'>B</option>
                          <option value='AB'>AB</option>
                          <option value='O'>O</option>
                        </select>
                      </div>
                    </div>
                    <div className='col-lg-2'>
                      <div className='form-group'>
                        <label className='control-label align-left margin-bottom-xxs'>Agama</label>
                        <select className='form-control m-b' name='scout.religion'>
                          <option value='islam'>Islam</option>
                          <option value='kristen_protestan'>Kristen Protestan</option>
                          <option value='kristen_katolik'>Kristen Katolik</option>
                          <option value='hindu'>Hindu</option>
                          <option value='budha'>Budha</option>
                          <option value='konghucu'>Konghucu</option>
                          <option value='others'>Lainnya</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
              <div style={{ width: '100%', height: '1px', clear: 'both' }}></div>

            </div>
            <button className='form-control btn btn-green bold-700 uppercase' type='submit'><i className='fa fa-save margin-right-xs'></i>Simpan Data Peserta</button>

          </div>
        </div>
      </div>

    );
  }
};
