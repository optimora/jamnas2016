import React from 'react';

export default class BranchPaymentConfirmation extends React.Component {
  render() {
    return(
      <div className='row'>
        <div className='col-lg-12'>
          <div className='ibox float-e-margins'>

            <div className='col-lg-12 title-center align-center'>
              <h5>KONFIRMASI PEMBAYARAN BIAYA PARTISIPASI</h5>
              <h5>JAMBORE NASIONAL X 2016</h5>
            </div>

            <div className='ibox-content'>

              <div className='col-lg-12'>
                <div className='col-lg-4'>
                  <p className='margin-top-xs'>Kwartir Daerah</p>
                </div>
                <div className='col-lg-8'>
                  <input type='text' disabled value='DKI Jakarta' name='payment.branch' className='form-control'></input>
                </div>
              </div>

              <div className='col-lg-12'>
                <div className='col-lg-4'>
                  <p className='margin-top-md'>Metode Pembayaran</p>
                </div>
                <div className='col-lg-8'>
                  <select name='payment.method' className='form-control margin-top-xs'>
                    <option value='atm'>Transfer melalui ATM</option>
                    <option value='setor-tunai'>Setor Tunai</option>
                    <option value='internet-banking'>Transfer melalui Internet Banking</option>
                    <option value='sms-banking'>Transfer melalui SMS Banking</option>
                  </select>
                </div>
              </div>

              <div className='col-lg-12'>
                <div className='col-lg-4'>
                  <p className='margin-top-md'>Jumlah yang Dibayarkan</p>
                </div>
                <div className='col-lg-8'>
                  <div className="input-group m-b margin-top-xs">
                    <span className="input-group-addon">Rp.</span>
                    <input type='text' className='form-control m-b no-margin' name='payment.amount'></input>
                  </div>
                </div>
              </div>

              <div className='col-lg-12'>
                <div className='col-lg-4'>
                  <p className='margin-top-xs'>Tanggal Pembayaran</p>
                </div>
                <div className='col-lg-8'>
                  <div className="form-inline">
                    <div className="form-group">
                      <select className="form-control margin-right-xs" id="sel1">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option>
                        <option value="13">13</option>
                        <option value="14">14</option>
                        <option value="15">15</option>
                        <option value="16">16</option>
                        <option value="17">17</option>
                        <option value="18">18</option>
                        <option value="19">19</option>
                        <option value="20">20</option>
                        <option value="21">21</option>
                        <option value="22">22</option>
                        <option value="23">23</option>
                        <option value="24">24</option>
                        <option value="25">25</option>
                        <option value="26">26</option>
                        <option value="27">27</option>
                        <option value="28">28</option>
                        <option value="29">29</option>
                        <option value="30">30</option>
                        <option value="31">31</option>
                      </select>
                      <select className="form-control margin-right-xs" id="sel2">
                        <option value="1">Januari</option>
                        <option value="2">Februari</option>
                        <option value="3">Maret</option>
                        <option value="4">April</option>
                        <option value="5">Mei</option>
                        <option value="6">Juni</option>
                        <option value="7">Juli</option>
                        <option value="8">Agustus</option>
                        <option value="9">September</option>
                        <option value="10">Oktober</option>
                        <option value="11">November</option>
                        <option value="12">Desember</option>
                      </select>
                      <select className="form-control margin-right-xs" id="sel3">
                        <option value="2016">2016</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>

              <div className='col-lg-12'>
                <div className='col-lg-4'>
                  <p className='margin-top-md'>Nama Pemegang Rekening Pengirim</p>
                </div>
                <div className='col-lg-8'>
                  <input type='text' name='payment.account_name' className='form-control margin-top-xs'></input>
                </div>
              </div>

              <div className='col-lg-12'>
                <div className='col-lg-4'>
                  <p className='margin-top-md'>Nomor Rekening Pengirim</p>
                </div>
                <div className='col-lg-8'>
                  <input type='text' name='payment.account_number' className='form-control margin-top-xs'></input>
                </div>
              </div>

              <div className='col-lg-12'>
                <div className='col-lg-4'>
                  <p className='margin-top-md'>Bukti Pembayaran</p>
                </div>
                <div className='col-lg-8'>
                  <input type='file' name='payment.receipt.url' className='form-control margin-top-xs'></input>
                </div>
              </div>

              <div className='col-lg-6 col-lg-offset-3 margin-top-lg'>
                <button className='form-control btn btn-green bold-700 uppercase' type='submit'><i className='fa fa-paper-plane margin-right-xs'></i>Kirim Bukti Pembayaran</button>
              </div>

              <div style={{ width: '100%', height: '1px', clear: 'both' }}></div>

            </div>
          </div>

          <div className='ibox float-e-margins'>
            <div className='col-lg-12 title-center'>
              <h5>KONFIRMASI PEMBAYARAN YANG TELAH DIKIRIMKAN</h5>
            </div>

            <div className='ibox-content'>
              <div className='col-lg-12'>
                <div className="table-responsive">
                    <table className="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Tanggal</th>
                            <th>Metode</th>
                            <th>Jumlah</th>
                            <th>Nomor Rekening</th>
                            <th>Nama Pengirim</th>
                            <th>Lampiran</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>23-Mei-2016</td>
                            <td>ATM Transfer</td>
                            <td>Rp. 30.000.000</td>
                            <td>504090948</td>
                            <td>Erlangga Sudiro</td>
                            <td className="align-center">
                                <a href='' className="btn btn-success btn-xs no-margin">Lihat</a>
                            </td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>30-Mei-2016</td>
                            <td>ATM Transfer</td>
                            <td>Rp. 5.000.000</td>
                            <td>504090948</td>
                            <td>Erlangga Sudiro</td>
                            <td className="align-center">
                                <a href='' className="btn btn-success btn-xs no-margin">Lihat</a>
                            </td>
                        </tr>

                        </tbody>
                    </table>
                </div>
              </div>
              <div style={{ width: '100%', height: '1px', clear: 'both' }}></div>
            </div>
          </div>
        </div>
      </div>

    );
  }
};
