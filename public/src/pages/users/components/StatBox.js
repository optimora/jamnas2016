/**
 * Created by saras on 5/15/16.
 */
import React from 'react';

export default class StatBox extends React.Component {
    render() {
        return(
            <div>
                <div className="ibox float-e-margins">
                    <div className="ibox-content">
                        <h1 className="no-margins">
                            { this.props.value }
                            <i className="fa fa-lg fa-user pull-right m-t-sm"></i>
                        </h1>
                        <small>{ this.props.unit }</small>
                    </div>
                </div>
            </div>
        );
    }
};
