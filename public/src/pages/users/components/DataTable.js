import React from 'react';

export default class DataTable extends React.Component {
    render() {
        return(
            <div>
                <div className="ibox float-e-margins">
                    <div className="ibox-title">
                        <h5>{this.props.title}</h5>
                    </div>
                    <div className="ibox-content">
                        <div className="row">
                            <div className="col-lg-2 pull-left">
                                <a href={this.props.link} className="btn btn-sm btn-primary">
                                    <i className="fa fa-plus"></i> Input Data
                                </a>
                            </div>
                            <div className="col-lg-3 pull-right">
                                <div className="input-group">
                                    <input type="text" placeholder="Search" className="input-sm form-control">
                                        <span className="input-group-btn">
                                            <button type="button" className="btn btn-sm btn-primary"> Go!</button>
                                        </span>
                                    </input>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-4">
                                <p className="padding-top-bottom-xs no-margin">Showing 1 to 2 of 2 entries</p>
                            </div>
                        </div>
                        <div className="table-responsive">
                            <table className="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nama</th>
                                    <th>Kwartir Daerah</th>
                                    <th>Kwartir Cabang</th>
                                    <th>Keikutsertaan</th>
                                    <th>Tanggal Daftar</th>
                                    <th>Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Arkka Dhiratara</td>
                                    <td>DKI Jakarta</td>
                                    <td>Jakarta Selatan</td>
                                    <td>{this.props.jobs}</td>
                                    <td>15-Mar-16</td>
                                    <td className="align-center">
                                        <a href={this.props.item_prefix_link} className="btn btn-success btn-xs no-margin margin-right-xs">Lihat</a>
                                        <a href="#" className="btn btn-info btn-xs no-margin margin-right-xs">Ubah</a>
                                        <a href="#" className="btn btn-danger btn-xs no-margin">Hapus</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Sri Andhini Saraswati</td>
                                    <td>DKI Jakarta</td>
                                    <td>Jakarta Selatan</td>
                                    <td>{this.props.jobs}</td>
                                    <td>16-Mar-16</td>
                                    <td className="align-center">
                                        <a href={this.props.item_prefix_link} className="btn btn-success btn-xs no-margin margin-right-xs">Lihat</a>
                                        <a href="#" className="btn btn-info btn-xs no-margin margin-right-xs">Ubah</a>
                                        <a href="#" className="btn btn-danger btn-xs no-margin">Hapus</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Emanuel Agung Cahyono</td>
                                    <td>DKI Jakarta</td>
                                    <td>Jakarta Pusat</td>
                                    <td>{this.props.jobs}</td>
                                    <td>17-Mar-16</td>
                                    <td className="align-center">
                                        <a href={this.props.item_prefix_link} className="btn btn-success btn-xs no-margin margin-right-xs">Lihat</a>
                                        <a href="#" className="btn btn-info btn-xs no-margin margin-right-xs">Ubah</a>
                                        <a href="#" className="btn btn-danger btn-xs no-margin">Hapus</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>Imam Hidayat</td>
                                    <td>DKI Jakarta</td>
                                    <td>Jakarta Barat</td>
                                    <td>{this.props.jobs}</td>
                                    <td>18-Mar-16</td>
                                    <td className="align-center">
                                        <a href={this.props.item_prefix_link} className="btn btn-success btn-xs no-margin margin-right-xs">Lihat</a>
                                        <a href="#" className="btn btn-info btn-xs no-margin margin-right-xs">Ubah</a>
                                        <a href="#" className="btn btn-danger btn-xs no-margin">Hapus</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>5</td>
                                    <td>Ardya Armaji</td>
                                    <td>DKI Jakarta</td>
                                    <td>Jakarta Timur</td>
                                    <td>{this.props.jobs}</td>
                                    <td>19-Mar-16</td>
                                    <td className="align-center">
                                        <a href={this.props.item_prefix_link} className="btn btn-success btn-xs no-margin margin-right-xs">Lihat</a>
                                        <a href="#" className="btn btn-info btn-xs no-margin margin-right-xs">Ubah</a>
                                        <a href="#" className="btn btn-danger btn-xs no-margin">Hapus</a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
};
