import React from 'react';

export default class ScoutCourseForm extends React.Component {

    render() {
        var items = [];

        if(this.props.items && this.props.items.length > 0 ) {
            for(var i=0; i < this.props.items.length; i++) {
                items.push(
                    <div className='col-lg-12 margin-bottom-xxs'>
                        <div className='col-lg-2 no-margin no-padding align-center'>
                            { this.props.isFixedName != "true" && this.props.isForm == "true"  ?
                                <input type='text' className='form-control align-center' name='course_name' defaultValue={this.props.items[i].name}></input>:
                                <p className="control-label align-center margin-bottom-xxs margin-top-xs"><strong>{this.props.items[i].name}</strong></p>
                            }
                        </div>
                        <div className='col-lg-2'>
                            { this.props.isForm == "true" ?
                                <input type='text' className='form-control align-center' name='course_year' defaultValue={this.props.items[i].year}></input>:
                                <p className="control-label align-center margin-bottom-xxs margin-top-xs">{this.props.items[i].year}</p>
                            }

                        </div>
                        <div className='col-lg-4'>
                            { this.props.isForm == "true" ?
                                <input type='text' className='form-control align-center' name='course_location' defaultValue={this.props.items[i].province}></input>:
                                <p className="control-label align-center margin-bottom-xxs margin-top-xs">{this.props.items[i].province}</p>
                            }
                        </div>
                        <div className='col-lg-4'>
                            { this.props.isForm == "true" ?
                                <input type='text' className='form-control align-center' name='course_note' defaultValue={this.props.items[i].notes}></input> :
                                <p className="control-label align-center margin-bottom-xxs margin-top-xs">{this.props.items[i].notes}</p>
                            }
                        </div>
                    </div>

                );
            }
        } else {
            for(var i=0; i < 4; i++) {
                items.push(
                    <div className='col-lg-12 margin-bottom-xxs'>
                        <div className='col-lg-2 no-margin no-padding align-center'>
                            { this.props.isView == "true" ?
                                <label className="control-label align-left margin-bottom-xxs margin-top-xs">{this.props.items[i].name}</label> :
                                <input type='text' className='form-control align-center' name='course_name'></input>
                            }
                        </div>
                        <div className='col-lg-2'>
                            <input type='text' className='form-control align-center' name='course_year'></input>
                        </div>
                        <div className='col-lg-4'>
                            <input type='text' className='form-control align-center' name='course_location'></input>
                        </div>
                        <div className='col-lg-4'>
                            <input type='text' className='form-control align-center' name='course_note'></input>
                        </div>
                    </div>

                );
            }
        }

        
        return(
            <div className='ibox float-e-margins'>
                <div className='ibox-title'>
                    <h5>{this.props.title}</h5>
                    <div className='ibox-tools'>
                        <a className='collapse-link'>
                            <i className='fa fa-chevron-up'></i>
                        </a>
                    </div>
                </div>
                <div className='ibox-content'>

                    <div className='col-lg-12 margin-bottom-xs'>
                        <div className='col-lg-2 no-margin no-padding align-center'>
                            <label className='control-label align-left margin-bottom-xxs'>Nama</label>
                        </div>
                        <div className='col-lg-2 align-center'>
                            <label className='control-label align-left margin-bottom-xxs'>Tahun</label>
                        </div>
                        <div className='col-lg-4 align-center'>
                            <label className='control-label align-left margin-bottom-xxs'>Tempat</label>
                        </div>
                        <div className='col-lg-4 align-center'>
                            <label className='control-label align-left margin-bottom-xxs'>Keterangan</label>
                        </div>
                    </div>

                    {items}

                    <div style={{ width: '100%', height: '1px', clear: 'both' }}></div>
                </div>
            </div>
        );
    }
};
