import React from 'react';

export default class SubBranchWillingnessLetterView extends React.Component {
  render() {
    return(
      <div className='row'>
        <div className='col-lg-12'>
          <div className='ibox float-e-margins'>

            <div className='col-lg-12 title-center align-center'>
              <h5>KESEDIAAN KWARTIR CABANG MENGIKUTI</h5>
              <h5>JAMBORE NASIONAL X 2016</h5>
            </div>

            <div className='ibox-content'>

              <div className='col-lg-12'>
                <div className='col-lg-4'>
                  <p className='margin-top-xs'>Nama Kwartir Cabang</p>
                </div>
                <div className='col-lg-8'>
                  <p className='margin-top-xs'>: Jakarta Selatan</p>
                </div>
              </div>

              <div className='col-lg-12'>
                <div className='col-lg-4'>
                  <p>Nama Kwartir Daerah</p>
                </div>
                <div className='col-lg-8'>
                  <p>: DKI Jakarta</p>
                </div>
              </div>

              <div className='col-lg-12'>
                <div className='col-lg-4'>
                  <p>Alamat</p>
                </div>
                <div className='col-lg-6'>
                  <p>: Jalan Medan Merdeka Timur No 6 Jakarta Pusat 10110</p>
                </div>
              </div>

              <div className='col-lg-12'>
                <div className='col-lg-4'>
                  <p>No Telp Kwarcab</p>
                </div>
                <div className='col-lg-6'>
                  <p>: +62 21 3508161</p>
                </div>
              </div>

              <div className='col-lg-12'>
                <div className='col-lg-4'>
                  <p>No HP Bindamping</p>
                </div>
                <div className='col-lg-6'>
                  <p>: +62 811 0800 999</p>
                </div>
              </div>


              <div className='col-lg-12 margin-bottom-md'><div className='hr-line-dashed no-margin'></div></div>

              <div className='col-lg-12'>
                <h3 className='align-center uppercase'>Dengan ini menyatakan siap dan bersedia</h3>
                <h3 className='align-center uppercase'>Mengikutsertakan Pramuka Penggalang</h3>
                <h3 className='align-center uppercase'>Pada Kegiatan Jambore Nasional 2016</h3>
                <h3 className='align-center uppercase'>Dan Siap Mematuhi Segala Ketentuan Yang Berlaku</h3>
              </div>

              <div className='col-lg-12 margin-top-md margin-bottom-md'><div className='hr-line-dashed no-margin'></div></div>

              <div className='col-lg-12 align-center margin-bottom-xs'>
                <h4 className='uppercase bold-700 underline'>Jumlah Peserta</h4>
              </div>

              <div className='col-lg-12'>
                <div className='col-lg-4 col-lg-offset-2'>
                  <table>
                    <tbody>
                      <tr>
                        <td width='50%'>Penggalang Pa</td>
                        <td className='align-right'>20 Orang</td>
                      </tr>
                      <tr>
                        <td width='50%'>Penggalang Pi</td>
                        <td className='align-right'>20 Orang</td>
                      </tr>
                      <tr className='margin-bottom-lg'></tr>
                      <tr>
                        <td width='50%' className='bold-700'>Jumlah</td>
                        <td className='bold-700 align-right'>20 Orang</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div className='col-lg-4 no-margin'>
                  <table>
                    <tbody>
                      <tr>
                        <td width='50%' className='margin-top-xs'>Bindamping Pa</td>
                        <td className='align-right'>20 Orang</td>
                      </tr>
                      <tr>
                        <td width='50%'>Bindamping Pi</td>
                        <td className='align-right margin-top-lg'>20 Orang</td>
                      </tr>
                      <tr>
                        <td width='50%'>Pinkoncab</td>
                        <td className='align-right'>20 Orang</td>
                      </tr>
                      <tr>
                        <td width='50%' className='bold-700'>Jumlah</td>
                        <td className='align-right bold-700'>20 Orang</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>

              <div className='col-lg-12 margin-top-lg'>
                <div className='col-lg-12'>
                  <p className='bold-700'>Lampiran: <a href='' name='Klik to download willingness letter'><i className='fa fa-download'></i>  Form C.01 Kwarcab Jakarta Selatan</a></p>
                </div>
              </div>

              <div className='col-lg-12 margin-top-lg'>
                <div className='col-lg-8 col-lg-offset-2 align-center'>
                  <p className='margin-top-xs margin-right-xs'>Formulir ini dikirim oleh <span className='bold-700 fg-red'>NAMA PENGIRIM</span> pada <span className='bold-700 fg-red'>TANGGAL DIKIRIM</span></p>
                </div>
              </div>

              <div style={{ width: '100%', height: '1px', clear: 'both' }}></div>
            </div>
          </div>
        </div>
      </div>

    );
  }
};
