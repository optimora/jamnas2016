/**
 * Created by saras on 5/26/16.
 */
import React from 'react';

export default class DepartArriveReturnForm extends React.Component {
    render() {
        return(
            <div className='ibox float-e-margins'>
                <div className='ibox-title'>
                    <h5>Keberangkatan, Kedatangan dan Kepulangan</h5>
                    <div className='ibox-tools'>
                        <a className='collapse-link'>
                            <i className='fa fa-chevron-up'></i>
                        </a>
                    </div>
                </div>
                <div className='ibox-content'>
                    <div className='col-lg-12'>
                        <div className='col-lg-3 no-margin no-padding'>
                            <label className='control-label align-left margin-bottom-xxs'>Keberangkatan</label>
                        </div>
                        <div className='col-lg-9'>
                            <div className="col-lg-12 no-padding">

                                <div className='col-lg-4'>
                                    <div className='form-group padding-right-sm' id='data_3'>
                                        <label className='control-label align-left margin-bottom-xxs'>Tanggal</label>
                                        {
                                            this.props.isForm == "true"?
                                                <div className='input-group date'>
                                                    <span className='input-group-addon'><i className='fa fa-calendar'></i></span><input type='text' className='form-control' value='10/11/2013' name='transports.departure.date'></input>
                                                </div> :
                                                <p>10/11/2016</p>
                                        }
                                    </div>
                                </div>

                                <div className='col-lg-2'>
                                    <div className='form-group padding-right-sm'>
                                        <label className='control-label align-left margin-bottom-xxs'>Waktu</label>
                                        {
                                            this.props.isForm == "true"?
                                                <input type='text' className='form-control' name='transports.departure.date' placeholder="09:00"></input> :
                                                <p>09:00</p>
                                        }
                                    </div>
                                </div>

                                <div className='col-lg-6'>
                                    <div className='form-group'>
                                        <label className='control-label align-left margin-bottom-xxs'>Angkutan</label>
                                        {
                                            this.props.isForm == "true"?
                                                <input type='text' className='form-control' name='transports.departure.transportation_type' placeholder="Bus, Mobil, Motor dst."></input> :
                                                <p>Bus</p>
                                        }
                                    </div>
                                </div>

                            </div>

                            <div className="col-lg-12 no-padding">
                                <div className="col-lg-3">
                                    <label className='control-label align-left margin-bottom-xxs'>Tempat Keberangkatan</label>
                                </div>
                                <div className="col-lg-9">
                                    {
                                        this.props.isForm == "true"?
                                            <input type='text' className='form-control' name='transports.departure.location'></input> :
                                            <p className="margin-top-xs">Terminal Lebak Bulus</p>
                                    }
                                </div>
                            </div>

                        </div>
                    </div>

                    <div className='col-lg-12 padding-top-bottom-xs'><div className='hr-line-dashed'></div></div>

                    <div className='col-lg-12'>
                        <div className='col-lg-3 no-margin no-padding'>
                            <label className='control-label align-left margin-bottom-xxs'>Kedatangan</label>
                        </div>
                        <div className='col-lg-9'>
                            <div className="col-lg-12 no-padding">

                                <div className='col-lg-4'>
                                    <div className='form-group padding-right-sm' id='data_3'>
                                        <label className='control-label align-left margin-bottom-xxs'>Tanggal</label>
                                        {
                                            this.props.isForm == "true"?
                                                <div className='input-group date'>
                                                    <span className='input-group-addon'><i className='fa fa-calendar'></i></span><input type='text' className='form-control' value='10/11/2013' name='transports.arrival.date'></input>
                                                </div> :
                                                <p>10/11/16</p>
                                        }
                                    </div>
                                </div>

                                <div className='col-lg-2'>
                                    <div className='form-group padding-right-sm'>
                                        <label className='control-label align-left margin-bottom-xxs'>Waktu</label>
                                        {
                                            this.props.isForm == "true"?
                                                <input type='text' className='form-control' name='transports.arrival.date' placeholder="09:00"></input> :
                                                <p>11:00</p>
                                        }
                                    </div>
                                </div>

                                <div className='col-lg-6'>
                                    <div className='form-group'>
                                        <label className='control-label align-left margin-bottom-xxs'>Angkutan</label>
                                        {
                                            this.props.isForm == "true"?
                                                <input type='text' className='form-control' name='transports.arrival.transportation_type' placeholder="Bus, Mobil, Motor dst."></input> :
                                                <p>Bus</p>
                                        }
                                    </div>
                                </div>

                            </div>

                            <div className="col-lg-12 no-padding">
                                <div className="col-lg-3">
                                    <label className='control-label align-left margin-top-xxs'>Tempat Kedatangan</label>
                                </div>
                                <div className="col-lg-9">
                                    {
                                        this.props.isForm == "true"?
                                            <input type='text' className='form-control' name='transports.arrival.location'></input> :
                                            <p className="margin-top-xxs">Cibubur</p>
                                    }
                                </div>
                            </div>

                        </div>
                    </div>

                    <div className='col-lg-12 padding-top-bottom-xs'><div className='hr-line-dashed'></div></div>

                    <div className='col-lg-12'>
                        <div className='col-lg-3 no-margin no-padding'>
                            <label className='control-label align-left margin-bottom-xxs'>Kepulangan</label>
                        </div>
                        <div className='col-lg-9'>

                            <div className="col-lg-12 no-padding margin-bottom-xs">
                                <div className="col-lg-3">
                                    <label className='control-label align-left margin-top-xxs'>Dari Perkemahan</label>
                                </div>
                                <div className="col-lg-9">
                                    {
                                        this.props.isForm == "true"?
                                            <input type='text' className='form-control' name='transports.return.location'></input> :
                                            <p className="margin-top-xxs">Cibubur</p>
                                    }
                                </div>
                            </div>

                            <div className="col-lg-12 no-padding">

                                <div className='col-lg-4'>
                                    <div className='form-group padding-right-sm' id='data_3'>
                                        <label className='control-label align-left margin-bottom-xxs'>Tanggal</label>
                                        {
                                            this.props.isForm == "true"?
                                                <div className='input-group date'>
                                                    <span className='input-group-addon'><i className='fa fa-calendar'></i></span><input type='text' className='form-control' value='10/11/2013' name='transports.return.date'></input>
                                                </div> :
                                                <p>11/11/16</p>
                                        }
                                    </div>
                                </div>

                                <div className='col-lg-2'>
                                    <div className='form-group padding-right-sm'>
                                        <label className='control-label align-left margin-bottom-xxs'>Waktu</label>
                                        {
                                            this.props.isForm == "true"?
                                                <input type='text' className='form-control' name='transports.return.date' placeholder="09:00"></input> :
                                                <p>10:00</p>
                                        }
                                    </div>
                                </div>

                                <div className='col-lg-6'>
                                    <div className='form-group'>
                                        <label className='control-label align-left margin-bottom-xxs'>Angkutan</label>
                                        {
                                            this.props.isForm == "true"?
                                                <input type='text' className='form-control' name='transports.return.transportation_type' placeholder="Bus, Mobil, Motor dst."></input> :
                                                <p>Bus</p>
                                        }
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>

                    <div style={{ width: '100%', height: '1px', clear: 'both' }}></div>
                </div>
            </div>
        );
    }
};
