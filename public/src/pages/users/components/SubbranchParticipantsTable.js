import React from 'react';

export default class SubBranchParticipantsTable extends React.Component {
  render() {
    return(
      <div className='row'>
        <div className='col-lg-12'>
          <div className="ibox float-e-margins">
            <div className="ibox-title">
              <h5>Data Peserta JAMNAS X 2016</h5>
            </div>
            <div className="ibox-content">
              <div className="row margin-bottom-xs">
                <div className="col-lg-4 pull-left">
                  <p className="padding-top-bottom-xs no-margin">Showing 1 to 5 of 5 entries</p>
                </div>
                <div className="col-lg-3 pull-right">
                  <div className="input-group">
                    <input type="text" placeholder="Search" className="input-sm form-control">
                                        <span className="input-group-btn">
                                            <button type="button" className="btn btn-sm btn-primary"> Go!</button>
                                        </span>
                    </input>
                  </div>
                </div>
              </div>
              <div className="table-responsive">
                <table className="table table-striped table-bordered table-hover dataTables-example" >
                  <thead>
                  <tr>
                    <th className="align-center">#</th>
                    <th className="align-center">Kwartir Cabang</th>
                    <th className="align-center">Peserta</th>
                    <th className="align-center">Pinkonda</th>
                    <th className="align-center">Pinkoncab</th>
                    <th className="align-center">Bindamping</th>
                    <th className="align-center">Staf Konda</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td>1</td>
                    <th>Kwarcab Jakarta Pusat</th>
                    <td className="align-center">243</td>
                    <td className="align-center">0</td>
                    <td className="align-center">27</td>
                    <td className="align-center">30</td>
                    <td className="align-center">0</td>
                  </tr>
                  <tr>
                    <td>2</td>
                    <th>Kwarcab Jakarta Selatan</th>
                    <td className="align-center">718</td>
                    <td className="align-center">0</td>
                    <td className="align-center">64</td>
                    <td className="align-center">70</td>
                    <td className="align-center">0</td>
                  </tr>
                  <tr>
                    <td>3</td>
                    <th>Kwarcab Jakarta Utara</th>
                    <td className="align-center">520</td>
                    <td className="align-center">5</td>
                    <td className="align-center">47</td>
                    <td className="align-center">30</td>
                    <td className="align-center">1</td>
                  </tr>
                  <tr>
                    <td>4</td>
                    <th>Kwarcab Jakarta Timur</th>
                    <td className="align-center">0</td>
                    <td className="align-center">0</td>
                    <td className="align-center">9</td>
                    <td className="align-center">8</td>
                    <td className="align-center">0</td>
                  </tr>
                  <tr>
                    <td>5</td>
                    <th>Kwarcab Jakarta Barat</th>
                    <td className="align-center">256</td>
                    <td className="align-center">1</td>
                    <td className="align-center">17</td>
                    <td className="align-center">27</td>
                    <td className="align-center">1</td>
                  </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>

    );
  }
};
