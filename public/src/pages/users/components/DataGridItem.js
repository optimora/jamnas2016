import React from 'react';

export default class DataGridItem extends React.Component {
  render() {
      return(
        <div className="col-md-3 margin-bottom-md">
          <center className="bg-jamnas-red scout-branch-grid">
              <p>{this.props.label}</p>
              <h3 className="bold-700 margin-top-minus-xs height-40">{this.props.name}</h3>
              <h1 className="font-40">{this.props.scouts_num}</h1>
              <h3 className="margin-top-minus-xs"><i className="fa fa-users"></i> {this.props.unit}</h3>
              <a href="/participants" target="_self" className="btn fg-red margin-top-md bg-gray"><i className="fa fa-list"></i> Selengkapnya</a>
          </center>
        </div>
    );
  }
};
