import React from 'react';
import DataGridItem from './DataGridItem';

export default class DataGrid extends React.Component {
  render() {
      var items = [];
      for(var i=0; i < this.props.items.length; i++) {
        items.push(<DataGridItem id={this.props.items[i].id} name={this.props.items[i].name} scouts_num={this.props.items[i].scouts_num} label={this.props.label} unit={this.props.unit}/>);
      }

      return(
      <div className="ibox float-e-margins">
        <div className="ibox-title">
            <h4>{ this.props.title }</h4>
        </div>
        <div className="ibox-content padding-bottom-lg">
          <div className="row">
              {items}
          </div>
        </div>
      </div>
    );
  }
};
