import React from 'react';

export default class BranchContactForm extends React.Component {

    render() {
        return(
            <div className='ibox float-e-margins'>
                <div className='ibox-title'>
                    <div className='col-lg-12'>
                        <h3 className='align-center'>{this.props.title}</h3>
                    </div>
                </div>
                <div className='ibox-content'>
                    <div className="col-lg-12">
                        <div className="col-lg-3 no-margin no-padding">
                            <label className='control-label align-left margin-bottom-xxs'>Kwartir Daerah</label>
                        </div>
                        <div className="col-lg-9">
                            <select className='form-control m-b no-margin' name='branch_registration.branch' disabled>
                                <option values=''>DKI Jakarta</option>
                            </select>
                        </div>
                    </div>

                    <div className='col-lg-12'><div className='hr-line-dashed'></div></div>

                    <div className='col-lg-12'>
                        <div className='col-lg-3 no-margin no-padding'>
                            <label className='control-label align-left margin-bottom-xxs'>Alamat Rumah</label>
                        </div>
                        <div className='col-lg-9'>
                            {
                                this.props.isForm == "true" ?
                                    <textarea className='form-control' name={this.props.prefix + '.address'} rows='3'></textarea> :
                                    <p>{this.props.data.address}</p>
                            }
                            {
                                this.props.isForm == "true" ?
                                    <div className='col-lg-5 no-padding-left margin-top-xs'>
                                        <label className='control-label align-left margin-bottom-xxs'>Provinsi</label>
                                        <select className='form-control m-b no-margin' name={this.props.prefix + '.province'}>
                                            <option values=''>Nusa Tenggara Barat</option>
                                        </select>
                                    </div> :
                                    <div className='col-lg-4 no-padding-left margin-top-xs'>
                                        <label className='control-label align-left margin-bottom-xxs'>Provinsi</label>
                                        <p>{this.props.data.province}</p>
                                    </div>
                            }
                            {
                                this.props.isForm == "true" ?
                                    <div className='col-lg-5 no-padding-right margin-top-xs'>
                                        <label className='control-label align-left margin-bottom-xxs'>Kota / Kabupaten</label>
                                        <select className='form-control m-b no-margin' name={this.props.prefix + '.regency_city'}>
                                            <option values=''>Kota Administrasi Jakarta Selatan</option>
                                        </select>
                                    </div> :
                                    <div className='col-lg-5 no-padding-right margin-top-xs'>
                                        <label className='control-label align-left margin-bottom-xxs'>Kota / Kabupaten</label>
                                        <p>{this.props.data.regency_city}</p>
                                    </div>
                            }
                            {
                                this.props.isForm == "true" ?
                                    <div className='col-lg-2 no-padding-right margin-top-xs'>
                                        <label className='control-label align-left margin-bottom-xxs'>Kodepos</label>
                                        <input type='text' className='form-control m-b no-margin' name={this.props.prefix + '.zipcode'}></input>
                                    </div> :
                                    <div className='col-lg-2 no-padding-right margin-top-xs'>
                                        <label className='control-label align-left margin-bottom-xxs'>Kodepos</label>
                                        <p>{this.props.data.zipcode}</p>
                                    </div>
                            }

                        </div>
                    </div>

                    <div className='col-lg-12'><div className='hr-line-dashed'></div></div>

                    <div className='col-lg-12'>
                        <div className='col-lg-3 no-margin no-padding'>
                            <label className='control-label align-left margin-bottom-xxs'>Kontak</label>
                        </div>
                        <div className='col-lg-9'>
                            {
                                this.props.isForm == "true" ?
                                    <div className='col-lg-6 no-padding-left'>
                                        <label className='control-label align-left margin-bottom-xxs'>No Telp. Rumah</label>
                                        <div className="input-group m-b">
                                            <span className="input-group-addon"><i className='glyphicon glyphicon-phone-alt'></i></span>
                                            <input type='text' className='form-control m-b no-margin' name={this.props.prefix + '.phone'}></input>
                                        </div>
                                    </div> :

                                    <div className='col-lg-4 no-padding-left'>
                                        <label className='control-label align-left margin-bottom-xxs'>No Telp. Rumah</label>
                                        <p>{this.props.data.phone}</p>
                                    </div>
                            }
                            {
                                this.props.isForm == "true" ?
                                    <div className='col-lg-6 no-padding-right'>
                                        <label className='control-label align-left margin-bottom-xxs'>No. Handphone Pinkonda</label>
                                        <div className="input-group m-b">
                                            <span className="input-group-addon"><i className='glyphicon glyphicon-phone'></i></span>
                                            <input type='text' className='form-control m-b no-margin' name={this.props.prefix + '.mobile_phone'}></input>
                                        </div>
                                    </div> :

                                    <div className='col-lg-4 no-padding-right'>
                                        <label className='control-label align-left margin-bottom-xxs'>No. Handphone Pinkonda</label>
                                        <p>{this.props.data.mobile_phone}</p>
                                    </div>
                            }
                        </div>
                    </div>

                    <div style={{ width: '100%', height: '1px', clear: 'both' }}></div>
                </div>
            </div>
        );
    }
};
