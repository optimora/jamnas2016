import React from 'react';

export default class BranchPaymentConfirmationsTable extends React.Component {
  render() {
    return(
      <div className='row'>
        <div className='col-lg-12'>
          <div className="ibox float-e-margins">
            <div className="ibox-title">
              <h5>Data Konfirmasi Pembayaran</h5>
            </div>
            <div className="ibox-content">
              <div className="row margin-bottom-xs">
                <div className="col-lg-4 pull-left">
                  <p className="padding-top-bottom-xs no-margin">Showing 1 to 5 of 5 entries</p>
                </div>
                <div className="col-lg-3 pull-right">
                  <div className="input-group">
                    <input type="text" placeholder="Search" className="input-sm form-control">
                                        <span className="input-group-btn">
                                            <button type="button" className="btn btn-sm btn-primary"> Go!</button>
                                        </span>
                    </input>
                  </div>
                </div>
              </div>
              <div className="table-responsive">
                <table className="table table-striped table-bordered table-hover dataTables-example" >
                  <thead>
                  <tr>
                    <th className="align-center">#</th>
                    <th className="align-center">Tanggal</th>
                    <th className="align-center">Kwartir Daerah</th>
                    <th className="align-center">Nominal</th>
                    <th className="align-center">Bukti Transfer</th>
                    <th className="align-center">Keterangan</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td>1</td>
                    <td className="align-center">20-06-16</td>
                    <td className="align-center">DKI Jakarta</td>
                    <td className="align-center">Rp. 200.000,00</td>
                    <td className="align-center"><a href="#" className="btn btn-success btn-xs no-margin margin-right-xs">Lihat Bukti Transfer</a></td>
                    <td className="align-center">-</td>
                  </tr>
                  <tr>
                    <td>2</td>
                    <td className="align-center">20-06-16</td>
                    <td className="align-center">Aceh</td>
                    <td className="align-center">Rp. 200.000,00</td>
                    <td className="align-center"><a href="#" className="btn btn-success btn-xs no-margin margin-right-xs">Lihat Bukti Transfer</a></td>
                    <td className="align-center">-</td>
                  </tr>
                  <tr>
                    <td>3</td>
                    <td className="align-center">19-06-16</td>
                    <td className="align-center">Banten</td>
                    <td className="align-center">Rp. 200.000,00</td>
                    <td className="align-center"><a href="#" className="btn btn-success btn-xs no-margin margin-right-xs">Lihat Bukti Transfer</a></td>
                    <td className="align-center">-</td>
                  </tr>
                  <tr>
                    <td>4</td>
                    <td className="align-center">17-06-16</td>
                    <td className="align-center">Jawa Barat</td>
                    <td className="align-center">Rp. 200.000,00</td>
                    <td className="align-center"><a href="#" className="btn btn-success btn-xs no-margin margin-right-xs">Lihat Bukti Transfer</a></td>
                    <td className="align-center">-</td>
                  </tr>
                  <tr>
                    <td>5</td>
                    <td className="align-center">15-06-16</td>
                    <td className="align-center">Kalimantan Timur</td>
                    <td className="align-center">Rp. 200.000,00</td>
                    <td className="align-center"><a href="#" className="btn btn-success btn-xs no-margin margin-right-xs">Lihat Bukti Transfer</a></td>
                    <td className="align-center">-</td>
                  </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>

    );
  }
};
