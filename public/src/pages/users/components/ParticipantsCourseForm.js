import React from 'react';

export default class ParticipantsCourseForm extends React.Component {
  render() {
    return(
      <div className='row'>
        <div className='col-lg-12'>
          <div className='ibox float-e-margins'>
            <div className='ibox-title'>
              <h5>KURSUS & PELATIHAN</h5>
              <div className='ibox-tools'>
                  <a className='collapse-link'>
                      <i className='fa fa-chevron-up'></i>
                  </a>
              </div>
            </div>
            <div className='ibox-content'>

              <div className='col-lg-12 margin-bottom-xs'>
                <p>KURSUS/PELATIHAN KEPRAMUKAAN</p>
                <div className='col-lg-1 no-margin no-padding align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Nama</label>
                </div>
                <div className='col-lg-2 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Tahun</label>
                </div>
                <div className='col-lg-4 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Tempat</label>
                </div>
                <div className='col-lg-5 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Keterangan</label>
                </div>
              </div>

              <div className='col-lg-12 margin-bottom-xxs'>
                <div className='col-lg-1 no-margin no-padding align-center'>
                  <label className='control-label align-left margin-bottom-xxs margin-top-xs'>KMD</label>
                </div>
                <div className='col-lg-2'>
                  <input type='hidden' name='scout.courses.name' value='KMD'></input>
                  <input type='text' className='form-control align-center' name='scout.courses.year'></input>
                </div>
                <div className='col-lg-4'>
                  <input type='text' className='form-control align-center' name='scout.courses.location'></input>
                </div>
                <div className='col-lg-5'>
                  <input type='text' className='form-control align-center' name='scout.courses.notes'></input>
                </div>
              </div>

              <div className='col-lg-12 margin-bottom-xxs'>
                <div className='col-lg-1 no-margin no-padding align-center'>
                  <label className='control-label align-left margin-bottom-xxs margin-top-xs'>KML</label>
                </div>
                <div className='col-lg-2'>
                  <input type='hidden' name='scout.courses.name' value='KMD'></input>
                  <input type='text' className='form-control align-center' name='scout.courses.year'></input>
                </div>
                <div className='col-lg-4'>
                  <input type='text' className='form-control align-center' name='scout.courses.location'></input>
                </div>
                <div className='col-lg-5'>
                  <input type='text' className='form-control align-center' name='scout.courses.notes'></input>
                </div>
              </div>

              <div className='col-lg-12 margin-bottom-xxs'>
                <div className='col-lg-1 no-margin no-padding align-center'>
                  <label className='control-label align-left margin-bottom-xxs margin-top-xs'>KPD</label>
                </div>
                <div className='col-lg-2'>
                  <input type='hidden' name='scout.courses.name' value='KMD'></input>
                  <input type='text' className='form-control align-center' name='scout.courses.year'></input>
                </div>
                <div className='col-lg-4'>
                  <input type='text' className='form-control align-center' name='scout.courses.location'></input>
                </div>
                <div className='col-lg-5'>
                  <input type='text' className='form-control align-center' name='scout.courses.notes'></input>
                </div>
              </div>

              <div className='col-lg-12 margin-bottom-xs'>
                <div className='col-lg-1 no-margin no-padding align-center'>
                  <label className='control-label align-left margin-bottom-xxs margin-top-xs'>KPL</label>
                </div>
                <div className='col-lg-2'>
                  <input type='hidden' name='scout.courses.name' value='KMD'></input>
                  <input type='text' className='form-control align-center' name='scout.courses.year'></input>
                </div>
                <div className='col-lg-4'>
                  <input type='text' className='form-control align-center' name='scout.courses.location'></input>
                </div>
                <div className='col-lg-5'>
                  <input type='text' className='form-control align-center' name='scout.courses.notes'></input>
                </div>
              </div>

              <div className='col-lg-12'><div className='hr-line-dashed'></div></div>

              <div className='col-lg-12 margin-bottom-xs'>
                <p>KURSUS/PELATIHAN DI LUAR KEPRAMUKAAN</p>
                <div className='col-lg-4 no-margin no-padding align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Nama</label>
                </div>
                <div className='col-lg-2 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Tahun</label>
                </div>
                <div className='col-lg-3 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Tempat</label>
                </div>
                <div className='col-lg-3 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Keterangan</label>
                </div>
              </div>

              <div className='col-lg-12 margin-bottom-xxs'>
                <div className='col-lg-4 no-margin no-padding align-center'>
                  <input type='text' className='form-control align-center' name='scout.external_courses.name'></input>
                </div>
                <div className='col-lg-2'>
                  <input type='text' className='form-control align-center' name='scout.external_courses.year'></input>
                </div>
                <div className='col-lg-3'>
                  <input type='text' className='form-control align-center' name='scout.external_courses.location'></input>
                </div>
                <div className='col-lg-3'>
                  <input type='text' className='form-control align-center' name='scout.external_courses.notes'></input>
                </div>
              </div>

              <div className='col-lg-12 margin-bottom-xxs'>
                <div className='col-lg-4 no-margin no-padding align-center'>
                  <input type='text' className='form-control align-center' name='scout.external_courses.name'></input>
                </div>
                <div className='col-lg-2'>
                  <input type='text' className='form-control align-center' name='scout.external_courses.year'></input>
                </div>
                <div className='col-lg-3'>
                  <input type='text' className='form-control align-center' name='scout.external_courses.location'></input>
                </div>
                <div className='col-lg-3'>
                  <input type='text' className='form-control align-center' name='scout.external_courses.notes'></input>
                </div>
              </div>

              <div className='col-lg-12 margin-bottom-xxs'>
                <div className='col-lg-4 no-margin no-padding align-center'>
                  <input type='text' className='form-control align-center' name='scout.external_courses.name'></input>
                </div>
                <div className='col-lg-2'>
                  <input type='text' className='form-control align-center' name='scout.external_courses.year'></input>
                </div>
                <div className='col-lg-3'>
                  <input type='text' className='form-control align-center' name='scout.external_courses.location'></input>
                </div>
                <div className='col-lg-3'>
                  <input type='text' className='form-control align-center' name='scout.external_courses.notes'></input>
                </div>
              </div>

              <div className='col-lg-12 margin-bottom-xxs'>
                <div className='col-lg-4 no-margin no-padding align-center'>
                  <input type='text' className='form-control align-center' name='scout.external_courses.name'></input>
                </div>
                <div className='col-lg-2'>
                  <input type='text' className='form-control align-center' name='scout.external_courses.year'></input>
                </div>
                <div className='col-lg-3'>
                  <input type='text' className='form-control align-center' name='scout.external_courses.location'></input>
                </div>
                <div className='col-lg-3'>
                  <input type='text' className='form-control align-center' name='scout.external_courses.notes'></input>
                </div>
              </div>


              <div style={{ width: '100%', height: '1px', clear: 'both' }}></div>
            </div>
            <button className='form-control btn btn-green bold-700 uppercase' type='submit'><i className='fa fa-save margin-right-xs'></i>Simpan Data Peserta</button>
          </div>
        </div>
      </div>

    );
  }
};
