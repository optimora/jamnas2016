import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import React from 'react';

import * as commonActions from '../../../actions/common/actions';
import * as usersBranchWillingnessLetterActions from '../actions/usersBranchWillingnessLetterActions';
import TextField from '../../../components/common/forms/TextField';
import TextAreaField from '../../../components/common/forms/TextAreaField';
import SubmitButton from '../../../components/common/forms/SubmitButton';

class BranchWillingnessLetter extends React.Component {
  static get propTypes() {
    return {
      BranchWillingnessLetter: React.PropTypes.object,
      actions: React.PropTypes.object
    }
  }

  _handleSubmit(event) {
    event.preventDefault();
    this.props.actions.create(this.props.BranchWillingnessLetter.data);
  }

  _handleFieldValueChanges(name, value) {
    this.props.actions.formUpdateValue(name, value);
  }

  render() {
    let data = {
      message: this.props.message,
      messageType: this.props.messageType
    };

    return(
      <div className='row'>
        <div className='col-lg-12'>
          <div className='ibox float-e-margins'>

            <div className='col-lg-12 title-center align-center'>
              <h5>KESEDIAAN KWARTIR DAERAH MENGIKUTI</h5>
              <h5>JAMBORE NASIONAL X 2016</h5>
            </div>

            <div className='ibox-content'>
              <form id='formBranchWillingnessLetter' onSubmit={this._handleSubmit.bind(this)}>

                <div className='col-lg-12'>
                  <div className='col-lg-4'>
                    <p className='margin-top-xs'>Nama Kwartir Daerah</p>
                  </div>
                  <div className='col-lg-8'>
                    <p className='margin-top-xs'>{window.currentPageData.user.branch.name}</p>
                  </div>
                </div>

                <div className='col-lg-12'>
                  <div className='col-lg-4'>
                    <p className='margin-top-xs'>Alamat</p>
                  </div>
                  <div className='col-lg-8'>
                    <TextAreaField name='address' placeholder='' rows='3' onChangeListener={this._handleFieldValueChanges.bind(this)} />
                  </div>
                </div>

                <div className='col-lg-12'>
                  <div className='col-lg-4'>
                    <p className='margin-top-xs'>Kodepos</p>
                  </div>
                  <div className='col-lg-2'>
                    <TextField name='zipcode' placeholder='' onChangeListener={this._handleFieldValueChanges.bind(this)} />
                  </div>
                </div>

                <div className='col-lg-12'>
                  <div className='col-lg-4'>
                    <p className='margin-top-xs'>Kontak</p>
                  </div>
                  <div className='col-lg-8'>
                    <div className='col-lg-6 no-padding-left'>
                      <p className='margin-top-xs'>No Telp Kwarda</p>
                      <div className="input-group m-b margin-top-minus-xs">
                        <span className="input-group-addon"><i className='glyphicon glyphicon-phone-alt'></i></span>
                        <TextField name='phone' placeholder='' onChangeListener={this._handleFieldValueChanges.bind(this)} />
                      </div>
                    </div>
                    <div className='col-lg-6 no-padding-right'>
                      <p className='margin-top-xs'>No HP Pinkonda</p>
                      <div className="input-group m-b margin-top-minus-xs">
                        <span className="input-group-addon"><i className='glyphicon glyphicon-phone'></i></span>
                        <TextField name='mobile_phone' placeholder='' onChangeListener={this._handleFieldValueChanges.bind(this)} />
                      </div>
                    </div>
                  </div>
                </div>

                <div className='col-lg-12 margin-bottom-md'><div className='hr-line-dashed no-margin'></div></div>

                <div className='col-lg-12'>
                  <h3 className='align-center uppercase'>Dengan ini menyatakan siap dan bersedia</h3>
                  <h3 className='align-center uppercase'>Mengikutsertakan Pramuka Penggalang</h3>
                  <h3 className='align-center uppercase'>Pada Kegiatan Jambore Nasional 2016</h3>
                  <h3 className='align-center uppercase'>Dan Siap Mematuhi Segala Ketentuan Yang Berlaku</h3>
                </div>

                <div className='col-lg-12 margin-top-md margin-bottom-md'><div className='hr-line-dashed no-margin'></div></div>

                <div className='col-lg-12 align-center margin-bottom-xs'>
                  <h4 className='uppercase bold-700 underline'>Jumlah Peserta</h4>
                </div>

                <div className='col-lg-12'>
                  <div className='col-lg-6 no-margin'>
                    <table>
                      <tbody>
                        <tr>
                          <td width='50%'>Penggalang Putra</td>
                          <td>
                            <div className='input-group margin-bottom-xxs'>
                              <TextField name='number_of.penggalang_pa' placeholder='' onChangeListener={this._handleFieldValueChanges.bind(this)} />
                              <div className='input-group-addon left-border-less'>Orang</div>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td width='50%'>Penggalang Putri</td>
                          <td>
                            <div className='input-group margin-bottom-xxs'>
                              <TextField name='number_of.penggalang_pi' placeholder='' onChangeListener={this._handleFieldValueChanges.bind(this)} />
                              <div className='input-group-addon left-border-less'>Orang</div>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td width='50%'>PBK Putra</td>
                          <td>
                            <div className='input-group margin-bottom-xxs'>
                              <TextField name='number_of.pbk_pa' placeholder='' onChangeListener={this._handleFieldValueChanges.bind(this)} />
                              <div className='input-group-addon left-border-less'>Orang</div>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td width='50%'>PBK Putri</td>
                          <td>
                            <div className='input-group margin-bottom-xxs'>
                              <TextField name='number_of.pbk_pi' placeholder='' onChangeListener={this._handleFieldValueChanges.bind(this)} />
                              <div className='input-group-addon left-border-less'>Orang</div>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td width='50%' className='bold-700'>Jumlah</td>
                          <td>
                            <div className='input-group margin-bottom-xxs'>
                              <TextField name='number_of.jumlah_peserta' disabled placeholder='' onChangeListener={this._handleFieldValueChanges.bind(this)} />
                              <div className='input-group-addon left-border-less'>Orang</div>
                            </div>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div className='col-lg-6 no-margin'>
                    <table>
                      <tbody>
                        <tr>
                          <td width='50%'>Bindamping Putra</td>
                          <td>
                            <div className='input-group margin-bottom-xxs'>
                              <TextField name='number_of.bindamping_pa' placeholder='' onChangeListener={this._handleFieldValueChanges.bind(this)} />
                              <div className='input-group-addon left-border-less'>Orang</div>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td width='50%'>Bindamping Putri</td>
                          <td>
                            <div className='input-group margin-bottom-xxs'>
                              <TextField name='number_of.bindamping_pi' placeholder='' onChangeListener={this._handleFieldValueChanges.bind(this)} />
                              <div className='input-group-addon left-border-less'>Orang</div>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td width='50%'>Pinkonda</td>
                          <td>
                            <div className='input-group margin-bottom-xxs'>
                              <TextField name='number_of.pinkonda' placeholder='' onChangeListener={this._handleFieldValueChanges.bind(this)} />
                              <div className='input-group-addon left-border-less'>Orang</div>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td width='50%'>Pinkoncab</td>
                          <td>
                            <div className='input-group margin-bottom-xxs'>
                              <TextField name='number_of.pinkoncab' placeholder='' onChangeListener={this._handleFieldValueChanges.bind(this)} />
                              <div className='input-group-addon left-border-less'>Orang</div>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td width='50%' className='bold-700'>Jumlah</td>
                          <td>
                            <div className='input-group margin-bottom-xxs'>
                              <TextField name='number_of.jumlah_pimpinan' disabled placeholder='' onChangeListener={this._handleFieldValueChanges.bind(this)} />
                              <div className='input-group-addon left-border-less'>Orang</div>
                            </div>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>

                <div className='col-lg-12 m-md'><div className='hr-line-dashed no-margin'></div></div>

                <div className='col-lg-12'>
                  <div className='col-lg-12'>
                    <p className='margin-top-xs bold-700'>Lampirkan bukti otentik Formulir Kesediaan Kwartir Daerah D.01 <span className='fg-red bold-700'>(wajib)</span></p>
                  </div>
                  <div className='col-lg-12'>
                      <input type='file' name='willingness_letter.file.name'></input>
                      <input type='hidden' name='willingness_letter.file.type' value='kwarda-willingness-letter'></input>
                  </div>
                </div>



                <div className='col-lg-12 m-md'><div className='hr-line-dashed no-margin'></div></div>

                <div className='col-lg-12 align-center'>
                  <div className='col-lg-6 col-lg-offset-3'>
                    <p className='fg-red'>Pastikan Anda sudah mengisi seluruh form dengan benar dan melampirkan bukti otentik</p>
                    <SubmitButton submitHandler={this._handleSubmit.bind(this)} label='Kirim Surat Kesediaan' formId='formWillingnessLetter' className='form-control btn btn-green bold-700' icon="fa-paper-plane"/>
                  </div>
                </div>

                <div style={{ width: '100%', height: '1px', clear: 'both' }}></div>
              </form>
            </div>
          </div>
        </div>
      </div>

    );
  }
};


const actionCreators = Object.assign({}, commonActions, usersBranchWillingnessLetterActions);

const mapStateToProps = (state) => {
  return state;
};

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(actionCreators, dispatch)
  };
};

let connector = connect(
    mapStateToProps,
    mapDispatchToProps
);

export default connector(BranchWillingnessLetter);

/*
 <div className='col-lg-12 m-md'><div className='hr-line-dashed no-margin'></div></div>

 <div className='col-lg-12'>
 <div className='col-lg-4'>
 <p className='margin-top-xs'>Formulir ini dikirim oleh:</p>
 </div>
 <div className='col-lg-8'>
 <TextField name='sender_name' placeholder='Nama Anda' onChangeListener={this._handleFieldValueChanges.bind(this)} />
 </div>
 </div>
 */
