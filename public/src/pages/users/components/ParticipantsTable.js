import React from 'react';

export default class ParticipantsTable extends React.Component {
  render() {
    return(
      <div className='row'>
        <div className='col-lg-12'>
          <div className="ibox float-e-margins">
            <div className="ibox-title">
              <h5>Data Peserta JAMNAS X 2016</h5>
            </div>
            <div className="ibox-content">
              <div className="row margin-bottom-xs">
                <div className="col-lg-4 pull-left">
                  <p className="padding-top-bottom-xs no-margin">Showing 1 to 5 of 5 entries</p>
                </div>
                <div className="col-lg-3 pull-right">
                  <div className="input-group">
                    <input type="text" placeholder="Search" className="input-sm form-control">
                                        <span className="input-group-btn">
                                            <button type="button" className="btn btn-sm btn-primary"> Go!</button>
                                        </span>
                    </input>
                  </div>
                </div>
              </div>
              <div className="table-responsive">
                <table className="table table-striped table-bordered table-hover dataTables-example" >
                  <thead>
                  <tr>
                    <th className="align-center">#</th>
                    <th className="align-center">Nama</th>
                    <th className="align-center">Kwartir Daerah</th>
                    <th className="align-center">Kwartir Cabang</th>
                    <th className="align-center">Type</th>
                    <th className="align-center">Subtype</th>
                    <th className="align-center">Profil Peserta</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td>1</td>
                    <td>Sri Andhini Saraswati</td>
                    <td className="align-center">DKI Jakarta</td>
                    <td className="align-center">Jakarta Selatan</td>
                    <td className="align-center">Peserta</td>
                    <td className="align-center">Peserta</td>
                    <td className="align-center"><a href="#" className="btn btn-success btn-xs no-margin margin-right-xs">Lihat Profil Peserta</a></td>
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>Bambang</td>
                    <td className="align-center">DKI Jakarta</td>
                    <td className="align-center">Jakarta Selatan</td>
                    <td className="align-center">Peserta</td>
                    <td className="align-center">Peserta</td>
                    <td className="align-center"><a href="#" className="btn btn-success btn-xs no-margin margin-right-xs">Lihat Profil Peserta</a></td>
                  </tr>
                  <tr>
                    <td>3</td>
                    <td>Adi</td>
                    <td className="align-center">Jawa Barat</td>
                    <td className="align-center">Bandung</td>
                    <td className="align-center">Peserta</td>
                    <td className="align-center">Peserta</td>
                    <td className="align-center"><a href="#" className="btn btn-success btn-xs no-margin margin-right-xs">Lihat Profil Peserta</a></td>
                  </tr>
                  <tr>
                    <td>4</td>
                    <td>Ajeng</td>
                    <td className="align-center">Jawa Barat</td>
                    <td className="align-center">Tasikmalaya</td>
                    <td className="align-center">Peserta</td>
                    <td className="align-center">Peserta</td>
                    <td className="align-center"><a href="#" className="btn btn-success btn-xs no-margin margin-right-xs">Lihat Profil Peserta</a></td>
                  </tr>
                  <tr>
                    <td>5</td>
                    <td>Andi</td>
                    <td className="align-center">Jawa Tengah</td>
                    <td className="align-center">Magelang</td>
                    <td className="align-center">Peserta</td>
                    <td className="align-center">Peserta</td>
                    <td className="align-center"><a href="#" className="btn btn-success btn-xs no-margin margin-right-xs">Lihat Profil Peserta</a></td>
                  </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>

    );
  }
};
