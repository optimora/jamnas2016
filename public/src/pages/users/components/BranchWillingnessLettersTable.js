import React from 'react';

export default class BranchWillingnessLettersTable extends React.Component {
  render() {
    return(
      <div className='row'>
        <div className='col-lg-12'>
          <div className="ibox float-e-margins">
            <div className="ibox-title">
              <h5>Surat Kesediaan dan Surat Pendaftaran</h5>
            </div>
            <div className="ibox-content">
              <div className="row margin-bottom-xs">
                <div className="col-lg-4 pull-left">
                  <p className="padding-top-bottom-xs no-margin">Showing 1 to 5 of 5 entries</p>
                </div>
                <div className="col-lg-3 pull-right">
                  <div className="input-group">
                    <input type="text" placeholder="Search" className="input-sm form-control">
                                        <span className="input-group-btn">
                                            <button type="button" className="btn btn-sm btn-primary"> Go!</button>
                                        </span>
                    </input>
                  </div>
                </div>
              </div>
              <div className="table-responsive">
                <table className="table table-striped table-bordered table-hover dataTables-example" >
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>Nama Kwarda</th>
                    <th>Surat Kesediaan</th>
                    <th>Surat Pendaftaran</th>
                    <th>Daftar Kwarcab</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td>1</td>
                    <td>DKI Jakarta</td>
                    <td className="align-center"><a href="#" className="btn btn-warning btn-xs no-margin margin-right-xs">Lihat Surat Kesediaan</a></td>
                    <td className="align-center"><a href="#" className="btn btn-success btn-xs no-margin margin-right-xs">Lihat Surat Pendaftaran</a></td>
                    <td className="align-center"><a href="/admin/subbranch/willingness_letters" className="btn btn-primary btn-xs no-margin margin-right-xs">Lihat</a></td>
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>Banten</td>
                    <td className="align-center"><a href="#" className="btn btn-warning btn-xs no-margin margin-right-xs">Lihat Surat Kesediaan</a></td>
                    <td className="align-center"><a href="#" className="btn btn-success btn-xs no-margin margin-right-xs">Lihat Surat Pendaftaran</a></td>
                    <td className="align-center"><a href="#" className="btn btn-primary btn-xs no-margin margin-right-xs">Lihat</a></td>
                  </tr>
                  <tr>
                    <td>3</td>
                    <td>Aceh</td>
                    <td className="align-center"><a href="#" className="btn btn-warning btn-xs no-margin margin-right-xs">Lihat Surat Kesediaan</a></td>
                    <td className="align-center"><a href="#" className="btn btn-success btn-xs no-margin margin-right-xs">Lihat Surat Pendaftaran</a></td>
                    <td className="align-center"><a href="#" className="btn btn-primary btn-xs no-margin margin-right-xs">Lihat</a></td>
                  </tr>
                  <tr>
                    <td>4</td>
                    <td>Sumatera Barat</td>
                    <td className="align-center"><a href="#" className="btn btn-warning btn-xs no-margin margin-right-xs">Lihat Surat Kesediaan</a></td>
                    <td className="align-center"><a href="#" className="btn btn-success btn-xs no-margin margin-right-xs">Lihat Surat Pendaftaran</a></td>
                    <td className="align-center"><a href="#" className="btn btn-primary btn-xs no-margin margin-right-xs">Lihat</a></td>
                  </tr>
                  <tr>
                    <td>5</td>
                    <td>Sumatera Utara</td>
                    <td className="align-center"><a href="#" className="btn btn-warning btn-xs no-margin margin-right-xs">Lihat Surat Kesediaan</a></td>
                    <td className="align-center"><a href="#" className="btn btn-success btn-xs no-margin margin-right-xs">Lihat Surat Pendaftaran</a></td>
                    <td className="align-center"><a href="#" className="btn btn-primary btn-xs no-margin margin-right-xs">Lihat</a></td>
                  </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>

    );
  }
};
