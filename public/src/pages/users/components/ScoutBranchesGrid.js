import React from 'react';

export default class ScoutBranchesGrid extends React.Component {
  render() {
    var tests = [{}, {}, {}, {}];
    return(
      <div>
        {tests.map((participant, i) => {
          return <div key={i} className='col-md-3 margin-bottom-md'>
            <center className="bg-jamnas-red scout-branch-grid">
              <p>Kontingen Kwartir Cabang</p>
              <h3 className="bold-700 margin-top-minus-xs height-40">Kota Jakarta Pusat</h3>
              <h1 className="font-40">999</h1>
              <h3 className="margin-top-minus-xs"><i className="fa fa-users"></i> peserta</h3>
              <a href="/participants/branch/_BRANCHID_/add" target="_self" className="btn btn-block fg-red margin-top-md bg-gray"><i className="fa fa-plus"></i> Tambah Peserta</a>
              <a href="/participants" target="_self" className="btn btn-block fg-red bg-gray"><i className="fa fa-list"></i> Daftar Peserta</a>
            </center>
          </div>
        })}
      </div>
    );
  }
};
