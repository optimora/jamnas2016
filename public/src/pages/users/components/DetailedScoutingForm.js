import React from 'react';
import _ from 'underscore';

export default class DetailedScoutingForm extends React.Component {
    render() {
        let activities = [];
        for (var i=0; i < this.props.data.activities.length; i++) {
            activities.push(<div className='row'>
                <div className='col-lg-10 col-lg-offset-2 margin-bottom-xxs'>
                    <div className='col-lg-1 align-center'>
                        <p className='control-label align-center margin-top-xxs'>{i+1}.</p>
                    </div>
                    <div className='col-lg-9'>
                        <p>{this.props.data.activities[i].name}</p>
                    </div>
                    <div className='col-lg-2'>
                        <p>{this.props.data.activities[i].year}</p>
                    </div>
                </div>
            </div>);
        }


        let achievementSubLevel =  _.groupBy(this.props.data.achievement, function(num){ return num.sub_level; });
        let achievement;

        switch(this.props.data.level) {
            case 'siaga':
                achievement =
                    <div>
                        <div className='col-lg-10 col-lg-offset-2'>
                            <label className='control-label align-left text-underline padding-left-xs'>TANDA KECAKAPAN KHUSUS (TKK)</label>
                        </div>

                        <div className='col-lg-10 col-lg-offset-2 margin-top-xs'>
                            <div className='col-lg-3 no-margin no-padding'>
                                <label className='control-label align-left margin-bottom-xxs'></label>
                            </div>
                            <div className='col-lg-2 align-center'>
                                <p className='control-label align-center margin-bottom-xxs'>Mula</p>
                            </div>
                            <div className='col-lg-2 align-center'>
                                <p className='control-label align-center margin-bottom-xxs'>Bantu</p>
                            </div>
                            <div className='col-lg-2 align-center'>
                                <p className='control-label align-center margin-bottom-xxs'>Tata</p>
                            </div>
                            <div className='col-lg-2 align-center'>
                                <p className='control-label align-center margin-bottom-xxs'>Garuda</p>
                            </div>
                        </div>

                        <div className='col-lg-10 col-lg-offset-2 margin-bottom-xxs'>
                            <div className='col-lg-3 no-margin padding-left-xs'>
                                <p className='control-label align-left margin-bottom-xxs margin-top-xs'>Tahun</p>
                            </div>
                            <div className='col-lg-2'>
                                <p className='control-label align-center margin-bottom-xxs margin-top-xs'>{ achievementSubLevel.mula ? achievementSubLevel.mula[0].year : '-' }</p>
                            </div>
                            <div className='col-lg-2'>
                                <p className='control-label align-center margin-bottom-xxs margin-top-xs'>{ achievementSubLevel.bantu ? achievementSubLevel.bantu[0].year : '-' }</p>
                            </div>
                            <div className='col-lg-2'>
                                <p className='control-label align-center margin-bottom-xxs margin-top-xs'>{ achievementSubLevel.tata ? achievementSubLevel.tata[0].year : '-' }</p>
                            </div>
                            <div className='col-lg-2'>
                                <p className='control-label align-center margin-bottom-xxs margin-top-xs'>{ achievementSubLevel.garuda ? achievementSubLevel.garuda[0].year : '-' }</p>
                            </div>
                        </div>

                        <div className='col-lg-10 col-lg-offset-2'>
                            <div className='col-lg-3 no-margin padding-left-xs'>
                                <p className='control-label align-left margin-bottom-xxs margin-top-xs'>Jumlah TKK</p>
                            </div>
                            <div className='col-lg-2'>
                                <p className='control-label align-center margin-bottom-xxs margin-top-xs'>{ achievementSubLevel.mula ? achievementSubLevel.mula[0].number_of_badges : '-' }</p>
                            </div>
                            <div className='col-lg-2'>
                                <p className='control-label align-center margin-bottom-xxs margin-top-xs'>{ achievementSubLevel.bantu ? achievementSubLevel.bantu[0].number_of_badges : '-' }</p>
                            </div>
                            <div className='col-lg-2'>
                                <p className='control-label align-center margin-bottom-xxs margin-top-xs'>{ achievementSubLevel.tata ? achievementSubLevel.tata[0].number_of_badges : '-' }</p>
                            </div>
                            <div className='col-lg-2'>
                                <p className='control-label align-center margin-bottom-xxs margin-top-xs'>{ achievementSubLevel.garuda ? achievementSubLevel.garuda[0].number_of_badges : '-' }</p>
                            </div>
                        </div>

                    </div>
                break;

            case 'penggalang':
                let achievementBadges =  _.groupBy(this.props.data.badges, function(num){ return num.category; });

                let purwaBadges = [];
                let madyaBadges = [];
                let utamaBadges = [];

                if(achievementBadges.purwa) {
                    for (var i=0; i < achievementBadges.purwa.length; i++) {
                        purwaBadges.push(<div className="input-group m-md">
                            <span className="input-group-addon">{i+1}</span>
                            <input type='text' className='form-control m-b no-margin' name='badges.name' value={achievementBadges.purwa[i].type.name}></input>
                        </div>);
                    }
                }

                if(achievementBadges.madya) {
                    for (var i=0; i < achievementBadges.madya.length; i++) {
                        madyaBadges.push(<div className="input-group m-md">
                            <span className="input-group-addon">{i+1}</span>
                            <input type='text' className='form-control m-b no-margin' name='badges.name' value={achievementBadges.madya[i].type.name}></input>
                        </div>);
                    }

                }

                if(achievementBadges.utama) {
                    for (var i=0; i < achievementBadges.utama.length; i++) {
                        utamaBadges.push(<div className="input-group m-md">
                            <span className="input-group-addon">{i+1}</span>
                            <input type='text' className='form-control m-b no-margin' name='badges.name' value={achievementBadges.utama[i].type.name}></input>
                        </div>);
                    }
                }


                achievement =
                    <div>
                        <div className='col-lg-10 col-lg-offset-2 margin-top-xs'>
                            <div className='col-lg-3 no-margin no-padding'>
                                <label className='control-label align-left margin-bottom-xxs'></label>
                            </div>
                            <div className='col-lg-2 align-center'>
                                <p className='control-label align-center margin-bottom-xxs'>Ramu</p>
                            </div>
                            <div className='col-lg-2 align-center'>
                                <p className='control-label align-center margin-bottom-xxs'>Rakit</p>
                            </div>
                            <div className='col-lg-2 align-center'>
                                <p className='control-label align-center margin-bottom-xxs'>Terap</p>
                            </div>
                            <div className='col-lg-2 align-center'>
                                <p className='control-label align-center margin-bottom-xxs'>Garuda</p>
                            </div>
                        </div>

                        <div className='col-lg-10 col-lg-offset-2 margin-bottom-xxs'>
                            <div className='col-lg-3 no-margin padding-left-xs'>
                                <p className='control-label align-left margin-bottom-xxs margin-top-xs'>Pencapaian Tingkat</p>
                            </div>
                            <div className='col-lg-2'>
                                <p className='control-label align-center margin-bottom-xxs margin-top-xs'>{ achievementSubLevel.ramu ? achievementSubLevel.ramu[0].year : '-' }</p>
                            </div>
                            <div className='col-lg-2'>
                                <p className='control-label align-center margin-bottom-xxs margin-top-xs'>{ achievementSubLevel.rakit ? achievementSubLevel.rakit[0].year : '-' }</p>
                            </div>
                            <div className='col-lg-2'>
                                <p className='control-label align-center margin-bottom-xxs margin-top-xs'>{ achievementSubLevel.terap ? achievementSubLevel.terap[0].year : '-' }</p>
                            </div>
                            <div className='col-lg-2'>
                                <p className='control-label align-center margin-bottom-xxs margin-top-xs'>{ achievementSubLevel.garuda ? achievementSubLevel.garuda[0].year : '-' }</p>
                            </div>
                        </div>
                        <div className='col-lg-10 col-lg-offset-2'>
                            <label className='control-label align-left text-underline padding-left-xs'>TANDA KECAKAPAN KHUSUS (TKK)</label>
                        </div>



                        <div className="row">
                            <div className='col-lg-10 col-lg-offset-2 margin-top-xs'>
                                <div className='row'>
                                    <div className='col-lg-4 align-center'>
                                        <div className="row">
                                            <div classname="col-lg-12">
                                                <label className='control-label margin-bottom-xxs'>PURWA</label>
                                                { purwaBadges }
                                             </div>
                                        </div>
                                    </div>
                                    <div className='col-lg-4 align-center'>
                                        <div className="row">
                                            <div classname="col-lg-12">
                                                <label className='control-label margin-bottom-xxs'>MADYA</label>
                                                { madyaBadges }
                                            </div>
                                        </div>

                                    </div>
                                    <div className='col-lg-4 align-center'>
                                        <div className="row">
                                            <div classname="col-lg-12">
                                                <label className='control-label margin-bottom-xxs'>UTAMA</label>
                                                { utamaBadges }
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                break;
        }


        return(
            <div className='ibox float-e-margins'>
                <div className='ibox-title'>
                    <h5>Data Kepramukaan</h5>
                    <div className='ibox-tools'>
                        <a className='collapse-link'>
                            <i className='fa fa-chevron-up'></i>
                        </a>
                    </div>
                </div>
                <div className='ibox-content'>

                    <div className='col-lg-12 margin-bottom-md'>
                        <div className='col-lg-2 no-margin no-padding'>
                            <label className='control-label align-left text-underline'>{this.props.data.level}</label>
                        </div>
                        <div className='col-lg-10'>
                            <div className='col-lg-6 no-padding-left'>
                                <label className='control-label align-left margin-bottom-xxs'>Nomor Gudep</label>
                                <p>{this.props.data.gudep_number}</p>
                            </div>
                            <div className='col-lg-6 no-padding-right'>
                                <label className='control-label align-left margin-bottom-xxs'>Nama Pangkalan</label>
                                <p>{this.props.data.base}</p>
                            </div>
                        </div>
                    </div>

                    { achievement }

                    <div className='col-lg-10 col-lg-offset-2 margin-top-lg'>
                        <label className='control-label align-left text-underline padding-left-xs'>Giat Siaga Telah Diikuti </label>
                    </div>

                    { activities }

                    <div style={{ width: '100%', height: '1px', clear: 'both' }}></div>
                </div>
            </div>
        );
    }
};
