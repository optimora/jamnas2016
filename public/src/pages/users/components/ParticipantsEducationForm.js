import React from 'react';

export default class ParticipantsEducationForm extends React.Component {
  render() {
    return(
      <div className='row'>
        <div className='col-lg-12'>
          <div className='ibox float-e-margins'>
            <div className='ibox-title'>
              <h5>PENDIDIKAN FORMAL</h5>
              <div className='ibox-tools'>
                  <a className='collapse-link'>
                      <i className='fa fa-chevron-up'></i>
                  </a>
              </div>
            </div>
            <div className='ibox-content'>

              <div className='col-lg-12 margin-bottom-xs'>
                <div className='col-lg-1 no-margin no-padding align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Jenjang</label>
                </div>
                <div className='col-lg-3 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Nama Sekolah</label>
                </div>
                <div className='col-lg-3 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Kota</label>
                </div>
                <div className='col-lg-2 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Tahun Lulus</label>
                </div>
                <div className='col-lg-3 align-center'>
                  <label className='control-label align-left margin-bottom-xxs'>Keterangan</label>
                </div>
              </div>

              <div className='col-lg-12 margin-bottom-xs'>
                <div className='col-lg-1 no-margin no-padding align-center'>
                  <label className='control-label align-left margin-bottom-xxs margin-top-xs'>SD</label>
                </div>
                <div className='col-lg-3'>
                  <input type='hidden' name='scout.educations.level'></input>
                  <input type='text' className='form-control align-center' name='scout.educations.school_name'></input>
                </div>
                <div className='col-lg-3'>
                  <input type='text' className='form-control align-center' name='scout.educations.regency_city'></input>
                </div>
                <div className='col-lg-2'>
                  <input type='text' className='form-control align-center' name='scout.educations.graduate_year'></input>
                </div>
                <div className='col-lg-3'>
                  <input type='text' className='form-control align-center' name='scout.educations.notes'></input>
                </div>
              </div>

              <div className='col-lg-12 margin-top-xxs'>
                <div className='col-lg-1 no-margin no-padding align-center'>
                  <label className='control-label align-left margin-bottom-xxs margin-top-xs'>SMP</label>
                </div>
                <div className='col-lg-3'>
                  <input type='hidden' name='scout.educations.level'></input>
                  <input type='text' className='form-control align-center' name='scout.educations.school_name'></input>
                </div>
                <div className='col-lg-3'>
                  <input type='text' className='form-control align-center' name='scout.educations.regency_city'></input>
                </div>
                <div className='col-lg-2'>
                  <input type='text' className='form-control align-center' name='scout.educations.graduate_year'></input>
                </div>
                <div className='col-lg-3'>
                  <input type='text' className='form-control align-center' name='scout.educations.notes'></input>
                </div>
              </div>

              <div className='col-lg-12 margin-top-lg'>
                <p>Jika PINKONDA/STAFF KONDA/PEMBINA/PINKONCAB ditambahkan beriku</p>
              </div>

              <div className='col-lg-12 margin-top-xxs'>
                <div className='col-lg-1 no-margin no-padding align-center'>
                  <label className='control-label align-left margin-bottom-xxs margin-top-xs'>SMA</label>
                </div>
                <div className='col-lg-3'>
                  <input type='hidden' name='scout.educations.level'></input>
                  <input type='text' className='form-control align-center' name='scout.educations.school_name'></input>
                </div>
                <div className='col-lg-3'>
                  <input type='text' className='form-control align-center' name='scout.educations.regency_city'></input>
                </div>
                <div className='col-lg-2'>
                  <input type='text' className='form-control align-center' name='scout.educations.graduate_year'></input>
                </div>
                <div className='col-lg-3'>
                  <input type='text' className='form-control align-center' name='scout.educations.notes'></input>
                </div>
              </div>

              <div className='col-lg-12 margin-top-xxs'>
                <div className='col-lg-1 no-margin no-padding align-center'>
                  <label className='control-label align-left margin-bottom-xxs margin-top-xs'>S1</label>
                </div>
                <div className='col-lg-3'>
                  <input type='hidden' name='scout.educations.level'></input>
                  <input type='text' className='form-control align-center' name='scout.educations.school_name'></input>
                </div>
                <div className='col-lg-3'>
                  <input type='text' className='form-control align-center' name='scout.educations.regency_city'></input>
                </div>
                <div className='col-lg-2'>
                  <input type='text' className='form-control align-center' name='scout.educations.graduate_year'></input>
                </div>
                <div className='col-lg-3'>
                  <input type='text' className='form-control align-center' name='scout.educations.notes'></input>
                </div>
              </div>

              <div className='col-lg-12 margin-top-xxs'>
                <div className='col-lg-1 no-margin no-padding align-center'>
                  <label className='control-label align-left margin-bottom-xxs margin-top-xs'>S2</label>
                </div>
                <div className='col-lg-3'>
                  <input type='hidden' name='scout.educations.level'></input>
                  <input type='text' className='form-control align-center' name='scout.educations.school_name'></input>
                </div>
                <div className='col-lg-3'>
                  <input type='text' className='form-control align-center' name='scout.educations.regency_city'></input>
                </div>
                <div className='col-lg-2'>
                  <input type='text' className='form-control align-center' name='scout.educations.graduate_year'></input>
                </div>
                <div className='col-lg-3'>
                  <input type='text' className='form-control align-center' name='scout.educations.notes'></input>
                </div>
              </div>

              <div className='col-lg-12 margin-top-xxs'>
                <div className='col-lg-1 no-margin no-padding align-center'>
                  <label className='control-label align-left margin-bottom-xxs margin-top-xs'>S3</label>
                </div>
                <div className='col-lg-3'>
                  <input type='hidden' name='scout.educations.level'></input>
                  <input type='text' className='form-control align-center' name='scout.educations.school_name'></input>
                </div>
                <div className='col-lg-3'>
                  <input type='text' className='form-control align-center' name='scout.educations.regency_city'></input>
                </div>
                <div className='col-lg-2'>
                  <input type='text' className='form-control align-center' name='scout.educations.graduate_year'></input>
                </div>
                <div className='col-lg-3'>
                  <input type='text' className='form-control align-center' name='scout.educations.notes'></input>
                </div>
              </div>

              <div style={{ width: '100%', height: '1px', clear: 'both' }}></div>
            </div>
            <button className='form-control btn btn-green bold-700 uppercase' type='submit'><i className='fa fa-save margin-right-xs'></i>Simpan Data Peserta</button>
          </div>
        </div>
      </div>

    );
  }
};
