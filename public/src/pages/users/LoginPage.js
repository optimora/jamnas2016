import { Provider } from 'react-redux';
import { applyMiddleware, combineReducers, createStore } from 'redux';

import React from 'react';
import ReactDom from 'react-dom';
import thunk from 'redux-thunk';

import Page from '../Page';

import LoginPageContainer from './containers/LoginPageContainer';
import usersLoginPageReducers from './reducers/usersLoginPageReducers';

export default class LoginPage {
  constructor(selectors) {
    this._selectors = selectors;
  }

  _createReducers() {
    return combineReducers({
      LoginPage: usersLoginPageReducers
    });
  }

  _createStore() {
    let reducers = this._createReducers();
    return createStore(reducers, applyMiddleware(thunk));
  }

  setup() {
    this._store = this._createStore();
    this._renderedComponent = ReactDom.render(
      <Provider store={this._store}>
        <LoginPageContainer />
      </Provider>,
      $(this._selectors.main)[0]
    );
  }
}