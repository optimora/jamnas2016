import React from 'react';
import ReactDom from 'react-dom';

import HeaderContent from './components/HeaderContent';
import DataTable from './components/DataTable';

export default class ParticipantListPage {
  constructor(selectors) {
    this._selectors = selectors;
  }

  setup() {
    ReactDom.render(
        <div>
          <div className="row wrapper border-bottom white-bg page-heading padding-bottom-xs">
            <div className="col-lg-12">
              <HeaderContent title="Kwartir Cabang Kota Administrasi Jakarta Selatan" subtitle="Daftar Peserta Jambore Nasional X 2016 - Kwartir Daerah DKI Jakarta"/>
            </div>
          </div>

          <div className="wrapper wrapper-content no-padding-bottom">
            <div className="ibox-title">
              <div className="col-lg-12 no-padding">
                <h5 className='uppercase'>Formulir Kesediaan Kwartir Cabang</h5>
              </div>
              <div className='col-lg-6 margin-top-xs no-padding margin-bottom-xs'>
                <a className='form-control btn btn-green bold-700 margin-bottom-xs' href='/subbranch/:id/willingness_letter'><i className='fa fa-paper-plane margin-right-xs'></i>Klik untuk mengisi Formulir Kesediaan Kwartir Cabang (C.01)</a>
                <a className='form-control btn btn-green bold-700' href='/subbranch/:id/registration'><i className='fa fa-paper-plane margin-right-xs'></i>Klik untuk mengisi Formulir Pendaftaran Kwartir Cabang (C.02)</a>
              </div>
              <div style={{ width: '100%', height: '1px', clear: 'both' }}></div>
            </div>
          </div>

          <div className="wrapper wrapper-content">
            <div className="row">
              <div className="col-lg-12">
                <DataTable title="TABEL PESERTA JAMBORE NASIONAL X 2016" jobs="Peserta JAMNAS X 2016" link="/participants/branch/_BRANCHID_/add" item_prefix_link="/participants/123/view"/>
              </div>
            </div>
          </div>
        </div>,
        $(this._selectors.participant_list)[0]
    );
  }
};
/**
 * Created by saras on 5/18/16.
 */
