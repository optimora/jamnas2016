import React from 'react';
import ReactDom from 'react-dom';

import HeaderContent from './components/HeaderContent';
import BranchWillingnessLettersTable from './components/BranchWillingnessLettersTable';

export default class BranchWillingnessLetters {
  constructor(selectors) {
    this._selectors = selectors;
  }

  setup() {
    ReactDom.render(
      <div>
        <HeaderContent subtitle='Daftar Surat Kesediaan dan Surat Pendaftaran' title="Kwartir Daerah"/>
      </div>,
      $(this._selectors.header)[0]
    );

    ReactDom.render(
      <div>
        <BranchWillingnessLettersTable />
      </div>,
      $(this._selectors.branch_willingness_letter_list)[0]
    );

  }
};
