import React from 'react';
import ReactDom from 'react-dom';

import HeaderContent from './components/HeaderContent';
import SubBranchParticipantsTable from './components/SubbranchParticipantsTable';

export default class SubBranchParticipants {
  constructor(selectors) {
    this._selectors = selectors;
  }

  setup() {
    ReactDom.render(
      <div>
        <HeaderContent subtitle='Peserta Berdasarkan Kwartir Cabang' title="Kwartir Daerah XXX"/>
      </div>,
      $(this._selectors.header)[0]
    );

    ReactDom.render(
      <div>
        <SubBranchParticipantsTable />
      </div>,
      $(this._selectors.subbranch_participant_list)[0]
    );

  }
};
