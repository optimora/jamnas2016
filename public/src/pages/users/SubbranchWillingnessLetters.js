import React from 'react';
import ReactDom from 'react-dom';

import HeaderContent from './components/HeaderContent';
import SubbranchWillingnessLettersTable from './components/SubbranchWillingnessLettersTable';

export default class SubbranchWillingnessLetters {
  constructor(selectors) {
    this._selectors = selectors;
  }

  setup() {
    ReactDom.render(
      <div>
        <HeaderContent subtitle='Daftar Surat Kesediaan dan Surat Pendaftaran' title="Kwartir Daerah"/>
      </div>,
      $(this._selectors.header)[0]
    );

    ReactDom.render(
      <div>
        <SubbranchWillingnessLettersTable />
      </div>,
      $(this._selectors.subbranch_willingness_letter_list)[0]
    );

  }
};
