import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import React from 'react';

import * as commonActions from '../../../actions/common/actions';
import * as scoutBranchesScoutIndexPageActions from '../actions/scoutBranchesScoutIndexPageActions';

import DialogHelper from '../../../lib/helpers/DialogHelper';

import ScoutDataTable from '../../../components/common/ui/ScoutDataTable';

class ScoutIndexPageContainer extends React.Component {
  constructor(...args) {
    super(...args)

    this.branch = window.currentPageData['branch'];
    this.subbranch = window.currentPageData['subbranch'];
  }

  static get propTypes() {
    return {
      ScoutIndexPage: React.PropTypes.object.isRequired,
      actions: React.PropTypes.object.isRequired
    };
  }

  _initiateDeleteScoutAction(scout) {
    DialogHelper.showConfirmationMessage(
      'Apakah Anda yakin akan menghapus data milik ' + scout.name + '?',
      'Seluruh data kepramukaan dan kontak yang terkait dengan data tersebut di atas akan dihilangkan. <strong>Aksi ini tidak dapat dibatalkan</strong>.',
      () => {
        this._deleteScout(scout._id);
      },
      () => {
        DialogHelper.showSuccessMessage('Aksi Dibatalkan', 'Aksi penghapusan dibatalkan.')
      }
    );
  }

  _generateScoutViewEntries() {
    let entries = this.props.ScoutIndexPage.scouts.slice(0);
    let results = [];
    let counter = 0;
    entries.forEach((entry) => {
      counter++;
      results.push({
        no: counter,
        name: entry.name,
        branchName: window.currentPageData['branch'].name,
        subbranchName: window.currentPageData['subbranch'].name,
        role: entry.type,
        created: entry.created,
        actions: <div>
          <a href={'/scouts/' + entry._id} className='btn btn-success btn-xs no-margin margin-right-xs'>Lihat</a>
          <a href={'/scouts/' + entry._id + '/edit'} className='btn btn-info btn-xs no-margin margin-right-xs'>Ubah</a>
          <a href='#' className='btn btn-danger btn-xs no-margin' onClick={() => this._initiateDeleteScoutAction(entry)}>Hapus</a>
        </div>
      });
    });

    return results;
  }

  _deleteScout(id) {
    this.props.actions.deleteScout(
      id,
      this.subbranch._id,
      () => {
        DialogHelper.showWarningMessage('Berhasil', 'Data telah dihapus.');
      },
      () => {
        DialogHelper.showErrorMessage('Gagal', 'Terjadi kesalahan saat melakukan penghapusan data.')
      }
    );
  }

  _searchScoutByName(name) {
    this.props.actions.fetchScouts(
      this.subbranch._id,
      { name }
    );
  }

  render() {
    const branch = window.currentPageData.branch;
    const subbranch = window.currentPageData.subbranch;
    const roleDictionary = window.currentPageData.rawEnumData;

    console.log(roleDictionary);

    return <div>
      <div className="wrapper wrapper-content no-padding-bottom">
        <div className="ibox-title">
          <div className="col-lg-12 no-padding">
            <h5 className='uppercase'>Formulir C.01 dan C.02</h5>
          </div>
          <div className='col-lg-6 margin-top-xs no-padding margin-bottom-xs'>
            <a className='form-control btn btn-green bold-700 margin-bottom-xs' href={'/subbranch/' + subbranch._id +'/willingness_letter/add'}><i className='fa fa-paper-plane margin-right-xs'></i>Klik untuk mengisi Formulir Kesediaan Kwartir Cabang (C.01)</a>
            <a className='form-control btn btn-green bold-700' href={'/subbranch/' + subbranch._id + '/registration'}><i className='fa fa-paper-plane margin-right-xs'></i>Klik untuk mengisi Formulir Pendaftaran Kwartir Cabang (C.02)</a>
          </div>
          <div style={{ width: '100%', height: '1px', clear: 'both' }}></div>
        </div>
      </div>

      <div className="wrapper wrapper-content">
        <div className="row">
          <div className="col-lg-12">
            <ScoutDataTable
              disableInput={true}
              rows={this._generateScoutViewEntries()}
              title={'Data peserta Jamnas X 2016 dari ' + subbranch.name} inputDataUrl={'/scout_branches/' + subbranch._id + '/scouts/add'}
              onSearchButtonClicked={this._searchScoutByName.bind(this)}
              roleDictionary={roleDictionary} />
          </div>
        </div>
      </div>
    </div>;
  }
}

const actionCreators = Object.assign({}, commonActions, scoutBranchesScoutIndexPageActions);

const mapStateToProps = (state) => {
  return state;
};

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(actionCreators, dispatch)
  };
};

let connector = connect(
  mapStateToProps,
  mapDispatchToProps
);

export default connector(ScoutIndexPageContainer);
