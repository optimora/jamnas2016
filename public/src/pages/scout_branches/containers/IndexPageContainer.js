import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import React from 'react';

import * as commonActions from '../../../actions/common/actions';
import * as scoutBranchesIndexPageActions from '../actions/scoutBranchesIndexPageActions';

import ScoutBranchDataGrid from '../../../components/common/ui/ScoutBranchDataGrid';

class IndexPageContainer extends React.Component {
  static get propTypes() {
    return {
      IndexPage: React.PropTypes.object.isRequired,
      actions: React.PropTypes.object.isRequired
    };
  }

  render() {
    let branches = [];
    let totalRegisteredScouts = 0;
    this.props.IndexPage.branches.forEach((branch) => {
      branches.push({
        id: branch._id,
        label: 'Kontingen Kwartir Cabang',
        name: branch.name,
        count: (branch.stats || {}).numberOfScouts != undefined ? branch.stats.numberOfScouts : '?',
        detailUrl: '/scout_branches'
      });
      totalRegisteredScouts += branch.stats && branch.stats.numberOfScouts && typeof branch.stats.numberOfScouts === 'number' ? branch.stats.numberOfScouts : 0;
    });

    return (
      <div className='wrapper wrapper-content'>
        <div className='row'>
          <div className='col-lg-12'>
            <ScoutBranchDataGrid name='Pendaftaran peserta Jambore Nasional X 2016' items={branches} />
          </div>
        </div>

        <div className='row'>
          <div className='col-lg-12'>

          </div>
        </div>
      </div>
    );
  }
}

const actionCreators = Object.assign({}, commonActions, scoutBranchesIndexPageActions);

const mapStateToProps = (state) => {
  return state;
};

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(actionCreators, dispatch)
  };
};

let connector = connect(
  mapStateToProps,
  mapDispatchToProps
);

export default connector(IndexPageContainer);
