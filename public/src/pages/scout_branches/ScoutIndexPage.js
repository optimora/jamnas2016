import { Provider } from 'react-redux';
import { applyMiddleware, combineReducers, createStore } from 'redux';

import React from 'react';
import ReactDom from 'react-dom';
import thunk from 'redux-thunk';

import * as Action from './actions/scoutBranchesScoutIndexPageActions';

import scoutBranchesScoutIndexPageReducers from './reducers/scoutBranchesScoutIndexPageReducers';

import Page from '../Page';

import ScoutSubbranchFormHeader from '../../components/common/ui/ScoutSubbranchFormHeader';
import ScoutIndexPageContainer from './containers/ScoutIndexPageContainer';

export default class ScoutIndexPage extends Page {
  constructor(selectors) {
    super(selectors);
  }

  _createReducers() {
    return combineReducers({
      ScoutIndexPage: scoutBranchesScoutIndexPageReducers
    });
  }

  _createStore() {
    let reducers = this._createReducers();
    return createStore(reducers, applyMiddleware(thunk));
  }

  _initialize() {
    this._store.dispatch(Action.fetchScouts(window.currentPageData['subbranch']._id));
  }

  setup() {
    this._store = this._createStore();
    this.render(
      <div className="row wrapper border-bottom white-bg page-heading padding-bottom-xs">
        <div className="col-lg-12">
          <ScoutSubbranchFormHeader branch={window.currentPageData['branch']} subbranch={window.currentPageData['subbranch']} />
        </div>
      </div>,
      $(this._selectors.header)[0]
    );
    this.render(
      <Provider store={this._store}>
        <ScoutIndexPageContainer />
      </Provider>,
      $(this._selectors.main)[0]
    );
    this._initialize();
  }
}