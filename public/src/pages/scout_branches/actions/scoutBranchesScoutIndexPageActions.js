import * as ActionType from './scoutBranchesScoutIndexPageActionTypes';

import ScoutService from '../../../services/ScoutService';

var scoutService = new ScoutService();

export const notifyFailed = (message) => ({ type: ActionType.NOTIFY_FAILED, message });
export const notifyDeleted = (message) => ({ type: ActionType.NOTIFY_DELETED, message });
export const updateScoutList = (scouts) => ({ type: ActionType.UPDATE_SCOUT_LIST, scouts });

// Async Actions

export const fetchScouts = (branchId, query = {}) => (dispatch) => {
  let params = {};
  if (window.location.search && window.location.search.indexOf('leader=true') !== -1) {
    params = {
      branch: branchId,
      type: ['leader-cmt', 'leader-staff'],
      _populate: true
    };
  }
  else {
    params = {
      subbranch: branchId,
      type: ['participant', 'leader-dct', 'coach', 'committee', 'guest', 'others'],
      _populate: true
    };
  }
  scoutService.getAll(
    Object.assign(params, query),
    (data) => {
      dispatch(updateScoutList(data.results));
    },
    (data) => {
      dispatch(notifyFailed('Gagal mengambil data kepramukaan dari server. Silakan coba beberapa saat lagi.'))
    }
  );
};

export const deleteScout = (id, branchId, successCallback, errorCallback) => {
  return (dispatch) => {
    scoutService.deleteById(
      id,
      (data) => {
        if (successCallback && typeof successCallback === 'function') {
          successCallback(data.responseJSON);
        }
        dispatch(fetchScouts(branchId, ''))
      },
      (data) => {
        if (errorCallback && typeof errorCallback === 'function') {
          errorCallback(data.responseJSON);
        }
      }
    );
  };
};