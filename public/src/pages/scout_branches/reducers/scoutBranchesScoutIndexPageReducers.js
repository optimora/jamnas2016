import * as ActionType from '../actions/scoutBranchesScoutIndexPageActionTypes';

const initialState = {
  notificationMessage: '',
  scouts: []
};

export default function scoutBranchesScoutIndexPageReducers(state = initialState, action) {
  switch (action.type) {
    case ActionType.NOTIFY_FAILED:
      return Object.assign({}, state, {
        notificationMessage: action.message
      });
    case ActionType.UPDATE_SCOUT_LIST:
      return Object.assign({}, state, {
        scouts: action.scouts
      });
    default:
      return state;
  }
}