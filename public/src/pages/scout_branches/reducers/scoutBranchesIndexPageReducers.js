import * as CommonActionType from '../../../actions/common/actionTypes';
import * as IndexPageActionType from '../actions/scoutBranchesIndexPageActionTypes';

const initialState = {
  loading: true,
  stats: {
    numberOfScouts: 0,
    numberOfScoutBranches: 0,
    numberOfScoutSubbranches: 0,
    numberOfCommittees: 0
  },

  branches: [],
  users: []
};

export default function usersDashboardPageReducers(state = initialState, action) {
  switch (action.type) {
    case IndexPageActionType.UPDATE_BRANCHES:
      return Object.assign({}, state, { branches: action.branches });
    case IndexPageActionType.UPDATE_USERS:
      return Object.assign({}, state, { users: action.users });
    case IndexPageActionType.UPDATE_STATS:
      return Object.assign({}, state, { stats: Object.assign({}, state.stats, action.stats) });
    default:
      return state;
  }
};
