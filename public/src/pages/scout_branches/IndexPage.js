import { Provider } from 'react-redux';
import { applyMiddleware, combineReducers, createStore } from 'redux';

import React from 'react';
import ReactDom from 'react-dom';
import thunk from 'redux-thunk';

import * as scoutBranchesIndexPageActions from './actions/scoutBranchesIndexPageActions';

import Page from '../Page';
import scoutBranchesIndexPageReducers from './reducers/scoutBranchesIndexPageReducers';

import IndexPageContainer from './containers/IndexPageContainer'
import HeaderContent from '../../components/common/ui/HeaderContent';
import ScoutBranchFormHeader from '../../components/common/ui/ScoutBranchFormHeader';

export default class IndexPage extends Page {
  constructor(selectors) {
    super(selectors);
  }

  _createReducers() {
    return combineReducers({
      IndexPage: scoutBranchesIndexPageReducers
    });
  }

  _createStore() {
    let reducers = this._createReducers();
    return createStore(reducers, applyMiddleware(thunk));
  }

  _initialize() {
    this._store.dispatch(scoutBranchesIndexPageActions.initialize());
  }

  setup() {
    this._store = this._createStore();
    this.render(
      <div className="row wrapper border-bottom white-bg page-heading padding-bottom-xs">
        <div className="col-lg-12">
          <HeaderContent title='Daftar Peserta Jambore Nasional X 2016' subtitle='Rekap Data Se-Indonesia' />
        </div>
      </div>,
      $(this._selectors.header)[0]
    );
    this.render(
      <div>
        <Provider store={this._store}>
          <IndexPageContainer />
        </Provider>
      </div>,
      $(this._selectors.main)[0]
    );
    this._initialize();
  }
}