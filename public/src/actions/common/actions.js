import * as ActionType from './actionTypes';

import LocationService from '../../services/LocationService';
import ScoutBranchService from '../../services/ScoutBranchService';
import UserService from '../../services/UserService';

// Service Instances

var locationService = new LocationService();
var scoutBranchService = new ScoutBranchService();
var userService = new UserService();

// General Actions

export const showAlert = (alertType, message) => {
  return {
    type: ActionType.ALERT,
    alertType,
    message
  };
};

export const showLoadingIndicator = () => {
  return {
    type: ActionType.SHOW_LOADING_INDICATOR
  };
};

export const showMessage = (messageType, message) => {
  return {
    type: ActionType.SHOW_MESSAGE,
    messageType,
    message
  };
};

export const showModal = (modalType, message) => {
  return {
    type: ActionType.SHOW_MODAL,
    message
  };
};

export const formUpdateValue = (field, value) => {
  return {
    type: ActionType.FORM_UPDATE_VALUE,
    field,
    value
  };
};

export const formValidationError = (field, message) => {
  return {
    type: ActionType.FORM_VALIDATION_ERROR,
    field,
    message
  };
};

export const formReset = () => {
  return {
    type: ActionType.FORM_RESET
  };
};

export const hideMessage = () => {
  return {
    type: ActionType.HIDE_MESSAGE
  };
};

export const redirect = (url) => {
  return {
    type: ActionType.REDIRECT,
    url
  };
};

export const scoutBranchUpdate = (branches) => {
  return {
    type: ActionType.SCOUT_BRANCH_UPDATE,
    branches
  };
};

export const scoutBranchChildrenUpdate = (branches) => {
  return {
    type: ActionType.SCOUT_BRANCH_CHILDREN_UPDATE,
    branches
  };
};

export const locationUpdate = (locations) => {
  return {
    type: ActionType.LOCATION_UPDATE,
    locations
  };
};

export const locationChildrenUpdate = (locations, fieldId) => {
  return {
    type: ActionType.LOCATION_CHILDREN_UPDATE,
    locations,
    fieldId
  };
};

export const saveCurrentUserData = (user) => {
  return {
    type: ActionType.USER_SAVE_DATA,
    user
  }
};

// Async Actions

export const fetchScoutBranches = () => {
  return (dispatch) => {
    scoutBranchService.getAll(
      {},
      (data) => {
        dispatch(scoutBranchUpdate(data.results));
      },
      (data) => {

      }
    );
  };
};

export const fetchScoutBranchChildren = (id) => {
  return (dispatch) => {
    scoutBranchService.getChildren(
      id,
      (data) => {
        dispatch(scoutBranchChildrenUpdate(data.results));
      },
      (data) => {

      }
    );
  }
};

export const fetchCurrentUserData = () => {
  return (dispatch) => {
    userService.getCurrentUser(
      (data) => {
        dispatch(saveCurrentUserData(data.result));
      },
      (data) => {

      }
    );
  }
};

export const fetchLocations = () => {
  return (dispatch) => {
    locationService.getAll(
      {},
      (data) => {
        dispatch(locationUpdate(data.results))
      },
      (data) => {

      }
    );
  };
};

export const fetchLocationChildren = (id, fieldId) => {
  return (dispatch) => {
    if (id && id != '') {

      locationService.getChildren(
        id,
        (data) => {
          dispatch(locationChildrenUpdate(data.results, fieldId))
        },
        (data) => {

        }
      );
    }
    else {
      dispatch(locationChildrenUpdate([], fieldId));
    }
  }
};