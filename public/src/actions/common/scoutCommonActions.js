import * as ActionType from './scoutCommonActionTypes';
import * as CommonAction from './actions';
import * as CommonActionType from './/actionTypes';

import ScoutService from '../../services/ScoutService';
import ScoutBranchService from '../../services/ScoutBranchService';

var scoutService = new ScoutService();
var scoutBranchService = new ScoutBranchService();

const generateOptionItem = (id, mapping) => ({ value: id, name: mapping[id] });

export const updateCommitteeRoles = (committeeRoles) => ({ type: ActionType.UPDATE_COMMITTEE_ROLES, committeeRoles });
export const updateCommitteeRoleDetails = (committeeRoleDetails) => ({ type: ActionType.UPDATE_COMMITTEE_ROLE_DETAILS, committeeRoleDetails });
export const updateCommitteeRoleExtras = (committeeRoleExtras) => ({ type: ActionType.UPDATE_COMMITTEE_ROLE_EXTRAS, committeeRoleExtras });
export const updateBranches = (branches) => ({ type: ActionType.UPDATE_BRANCHES, branches });
export const updateSubbranches = (subbranches) => ({ type: ActionType.UPDATE_SUBBRANCHES, subbranches });

export const classifyAndUpdateCommitteeRoleDetails = (committeeRole, mapping) => {
  let committeeRoleDetails = [];
  switch (committeeRole) {
    case 'committee-role-activity-division':
      committeeRoleDetails = [
        'committee-role-president',
        'committee-role-vice-president1',
        'committee-role-secretary',
        'committee-section-scout-skills-and-interest',
        'committee-section-water',
        'committee-section-gdv',
        'committee-section-technology-and-cultural-art',
        'committee-section-community-service-and-tour',
        'committee-section-knowledge',
        'committee-section-adventure',
        'committee-section-special',
        'committee-section-adult-member',
        'committee-section-protocol-and-ceremony'
      ];
      break;
    case 'committee-role-logistic-division':
      committeeRoleDetails = [
        'committee-role-president',
        'committee-role-vice-president1',
        'committee-role-vice-president2',
        'committee-role-secretary',
        'committee-section-facilities',
        'committee-section-accommodation',
        'committee-section-consumption',
        'committee-section-equipment'
      ];
      break;
    case 'committee-role-public-service-division':
      committeeRoleDetails = [
        'committee-role-president',
        'committee-role-vice-president1',
        'committee-role-vice-president2',
        'committee-role-secretary',
        'committee-section-health',
        'committee-section-transportation',
        'committee-section-cleanliness'
      ];
      break;
    case 'committee-role-security-division':
      committeeRoleDetails = [
        'committee-role-president',
        'committee-role-vice-president1',
        'committee-role-secretary',
        'committee-role-member'
      ];
      break;
    case 'committee-role-pr-and-oversea-division':
      committeeRoleDetails = [
        'committee-role-president',
        'committee-role-vice-president1',
        'committee-role-secretary',
        'committee-section-publication',
        'committee-section-media-center',
        'committee-section-documentation',
        'committee-section-international-relations'
      ];
      break;
    case 'committee-role-fundraising-division':
      committeeRoleDetails = [
        'committee-role-president',
        'committee-role-vice-president1',
        'committee-role-vice-president2',
        'committee-role-secretary',
        'committee-role-vice-secretary',
        'committee-role-member',
        'committee-section-market',
        'committee-section-store',
        'committee-section-sponsorship'
      ];
      break;
    case 'committee-role-waslitev-division':
      committeeRoleDetails = [
        'committee-role-president',
        'committee-role-vice-president1',
        'committee-role-member'
      ];
      break;
    case 'committee-role-risk-management-division':
      committeeRoleDetails = [
        'committee-role-president',
        'committee-role-vice-president1',
        'committee-role-secretary',
        'committee-role-member'
      ];
      break;
    case 'committee-role-campsite-division':
      committeeRoleDetails = [
        'committee-section-central-campsite',
        'committee-section-men-campsite',
        'committee-section-women-campsite'
      ];
      break;
    default:
      break;
  }

  return (dispatch) => {
    dispatch(updateCommitteeRoleDetails(committeeRoleDetails.map(id => generateOptionItem(id, mapping))));
    dispatch(updateCommitteeRoleExtras([]));
  };
};

export const classifyAndUpdateCommitteeRoleExtras = (committeeRoleDetail, mapping) => {
  let committeeRoleExtras = [];
  switch (committeeRoleDetail) {
    case 'committee-section-central-campsite':
      committeeRoleExtras = [
        'committee-section-campsite-master',
        'committee-section-campsite-vice-master',
        'committee-section-campsite-division-activity',
        'committee-section-campsite-public-activity',
        'committee-section-campsite-sponsorship',
        'committee-section-campsite-staff'
      ];
      break;
    case 'committee-section-men-campsite':
      committeeRoleExtras = [
        'committee-section-campsite-master',
        'committee-section-campsite-vice-master',
        'committee-section-campsite-staff-men',
        'committee-section-campsite-master-tjilik-riwut',
        'committee-section-campsite-master-imam-bonjol',
        'committee-section-campsite-master-hasanudin',
        'committee-section-campsite-master-p-diponegoro',
      ];
      break;
    case 'committee-section-women-campsite':
      committeeRoleExtras = [
        'committee-section-campsite-master',
        'committee-section-campsite-vice-master',
        'committee-section-campsite-staff-women',
        'committee-section-campsite-master-tjut-nya-dien',
        'committee-section-campsite-master-ra-kartini',
        'committee-section-campsite-master-kristina-m-tiahahu',
        'committee-section-campsite-master-maria-w-maramis',
      ];
      break;
    default:
      break;
  }

  return (dispatch) => {
    dispatch(updateCommitteeRoleExtras(committeeRoleExtras.map(id => generateOptionItem(id, mapping))));
  };
};

export const fetchSubbranches = (id) => {
  return (dispatch) => {
    scoutBranchService.getChildren(
      id,
      (data) => {
        dispatch(updateSubbranches(data.results));
      },
      (data) => {
        console.log('Error while fetching subbranches data.');
        console.log(data.responseJSON);
      }
    );
  };
};
