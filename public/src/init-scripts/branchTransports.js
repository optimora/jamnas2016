import BranchTransports from '../pages/users/BranchTransports';

const selectors = {
  // Yang dideklarasikan di jade ya
  header                    : '#header',
  branch_transport_list        : '#branch_transport_list'
};

let pageInstance = new BranchTransports(selectors);
pageInstance.setup();
