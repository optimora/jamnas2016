import DetailPage from '../pages/scouts/DetailPage';

const selectors = {
  header: '#header',
  main: '#scoutDetail'
};

let pageInstance = new DetailPage(selectors);
pageInstance.setup();