import CommitteesList from '../pages/users/CommitteesList';

const selectors = {
  // Yang dideklarasikan di jade ya
  header                    : '#header',
  committee_list           : '#committee_list'
};

let pageInstance = new CommitteesList(selectors);
pageInstance.setup();
