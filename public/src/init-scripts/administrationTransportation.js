import TransportationPage from '../pages/administration/TransportationPage';

const selectors = {
  header                 : '#header',
  branch_transport_list  : '#branch_transport_list'
};

let pageInstance = new TransportationPage(selectors);
pageInstance.setup();