import SubbranchWillingnessLetters from '../pages/users/SubbranchWillingnessLetters';

const selectors = {
  // Yang dideklarasikan di jade ya
  header                            : '#header',
  subbranch_willingness_letter_list    : '#subbranch_willingness_letter_list'
};

let pageInstance = new SubbranchWillingnessLetters(selectors);
pageInstance.setup();
