import DepartureArrivalPlanViewPage from '../pages/users/DepartureArrivalPlanViewPage';

const selectors = {
  // Yang dideklarasikan di jade ya
  departure_arrival_plan_view                    : '#departure_arrival_plan_view'
};

let pageInstance = new DepartureArrivalPlanViewPage(selectors);
pageInstance.setup();
