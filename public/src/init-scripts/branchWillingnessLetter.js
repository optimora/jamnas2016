import BranchWillingnessLetter from '../pages/users/BranchWillingnessLetter';

const selectors = {
  // Yang dideklarasikan di jade ya
  header                    : '#headerAddParticipantsKwarda',
  kwardaWillingness         : '#kwardaWillingness'
};

const data = {
  user: user
};

let pageInstance = new BranchWillingnessLetter(selectors, data);
pageInstance.setup();
