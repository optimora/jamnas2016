import AddNewParticipantPage from '../pages/users/AddNewParticipantPage';

let branches = [
  {
    name: 'Kota Jakarta Pusat',
    scouts_num: 97,
    id: 1314455452
  },
  {
    name: 'Kota Jakarta Selatan',
    scouts_num: 32,
    id: 235223522
  },
  {
    name: 'Kota Jakarta Barat',
    scouts_num: 64,
    id: 8235723928
  },
  {
    name: 'Kota Jakarta Utara',
    scouts_num: 34,
    id: 8235723929
  },
  {
    name: 'Kota Jakarta Timur',
    scouts_num: 84,
    id: 8235723930
  },
  {
    name: 'Kabupaten Kepulauan Seribu',
    scouts_num: 24,
    id: 8235723925
  }
];

const selectors = {
  // Yang dideklarasikan di jade ya
  scoutsGrid   : '#scoutsGrid',
  header      : '#HeaderAddParticipantsKwarda'
};

const data = {
  user: user,
  branches: branches
}

let pageInstance = new AddNewParticipantPage(selectors, data);
pageInstance.setup();
