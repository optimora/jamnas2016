import DashboardPage from '../pages/users/DashboardPage';

const selectors = {
  header: '#header',
  main: '#dashboard'
};

let pageInstance = new DashboardPage(selectors);
pageInstance.setup();
