import IndexPage from '../pages/scout_committee/IndexPage';

const selectors = {
  header: '#header',
  main: '#scoutForm'
};

let pageInstance = new IndexPage(selectors);
pageInstance.setup();