import SubBranchRegistrationPage from '../pages/users/SubBranchRegistrationPage';

const selectors = {
    // Yang dideklarasikan di jade ya
    subbranch_registration                    : '#subbranch_registration'
};

let pageInstance = new SubBranchRegistrationPage(selectors);
pageInstance.setup();
