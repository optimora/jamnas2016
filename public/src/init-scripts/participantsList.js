import ParticipantsList from '../pages/users/ParticipantsList';

const selectors = {
  // Yang dideklarasikan di jade ya
  header                    : '#header',
  participant_list          : '#participant_list'
};

let pageInstance = new ParticipantsList(selectors);
pageInstance.setup();
