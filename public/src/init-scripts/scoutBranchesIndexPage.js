import IndexPage from '../pages/scout_branches/IndexPage';

const selectors = {
  header: '#header',
  main: '#dashboard'
};

let pageInstance = new IndexPage(selectors);
pageInstance.setup();
