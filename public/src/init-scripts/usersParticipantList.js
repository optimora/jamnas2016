import ParticipantListPage from '../pages/users/ParticipantListPage';

const selectors = {
  // Yang dideklarasikan di jade ya
  participant_list                    : '#participant_list'
};

let pageInstance = new ParticipantListPage(selectors);
pageInstance.setup();
