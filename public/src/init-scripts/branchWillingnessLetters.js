import BranchWillingnessLetters from '../pages/users/BranchWillingnessLetters';

const selectors = {
  // Yang dideklarasikan di jade ya
  header                    : '#header',
  branch_willingness_letter_list        : '#branch_willingness_letter_list'
};

let pageInstance = new BranchWillingnessLetters(selectors);
pageInstance.setup();
