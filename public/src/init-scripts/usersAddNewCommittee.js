import AddNewCommitteePage from '../pages/users/AddNewCommitteePage';

const selectors = {
    // Yang dideklarasikan di jade ya
    add_new_committee                    : '#add_new_committee'
};

let scout  = {
    user: {
        name: "Joko Santoso"
    },
    birthplace: "Jakarta",
    educations: [{
        level: 'SD'
    },
    {
        level: 'SMP'
    },
    {
        level: 'SMA'
    },
    {
        level: 'S1'
    },
    {
        level: 'S2'
    }],
    experiences: [{
        level: 'Siaga'
    },
    {
        level: 'Penggalang'
    },
    {
        level: 'Penegak'
    },
    {
        level: 'Pandega'
    },
    {
        level: 'Pembina'
    }],
    courses: [{
        name: 'KMD',

    },
    {
        name: 'KML',

    },
    {
        name: 'KPD',

    },
    {
        name: 'KPL',

    }],

    external_courses: [{
        name: '',

    },
    {
        name: '',

    },
    {
        name: '',

    },
    {
        name: '',

    }]
}

const data = {
    scout : scout,
};

let pageInstance = new AddNewCommitteePage(selectors, data);
pageInstance.setup();
