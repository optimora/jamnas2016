import AddPage from '../pages/scout_committee/AddPage';

const selectors = {
  header: '#header',
  main: '#scoutForm'
};

let pageInstance = new AddPage(selectors);
pageInstance.setup();