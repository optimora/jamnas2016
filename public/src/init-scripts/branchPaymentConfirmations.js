import BranchPaymentConfirmations from '../pages/users/BranchPaymentConfirmations';

const selectors = {
  // Yang dideklarasikan di jade ya
  header                    : '#header',
  branch_payment_confirmation_list        : '#branch_payment_confirmation_list'
};

let pageInstance = new BranchPaymentConfirmations(selectors);
pageInstance.setup();
