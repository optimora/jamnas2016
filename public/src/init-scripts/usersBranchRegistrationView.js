import BranchRegistrationViewPage from '../pages/users/BranchRegistrationViewPage';

const selectors = {
    // Yang dideklarasikan di jade ya
    branch_registration_view                  : '#branch_registration_view'
};

let pageInstance = new BranchRegistrationViewPage(selectors);
pageInstance.setup();
