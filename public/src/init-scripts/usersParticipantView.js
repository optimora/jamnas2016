import ParticipantViewPage from '../pages/users/ParticipantViewPage';

const selectors = {
  // Yang dideklarasikan di jade ya
  participant_view                    : '#participant_view'
};

let scout  = {
  user: {
    name: "Sri Andhini Saraswati"
  },
  gender: 'Female',
  birthplace: "Jakarta",
  birthday: "1992-11-22T01:00:00+01:00",
  religion: 'Islam',
  blood_type: 'A',
  nta: '210000261',
  branch: {
    name: 'DKI Jakarta'
  },
  subbranch: { name:'Kota Jakarta Selatan'},
  type: 'participant',
  subtype: 'participant-regular',
  group: {
    name: 'Rajawali'
  },
  contact:[{
    name: 'Sri Andhini Saraswati',
    occupation: 'Pelajar',
    address: 'Jalan Komplek Hankam No. 21 RT 05 RW 11 Kebayoran Lama Grogol Selatan',
    province: 'DKI Jakarta',
    regency_city: 'Kota Madya Jakarta Selatan',
    zipcode: 12220,
    phone: {
      home: '0217256229',
      mobile: '085693636691'
    }
  }],
  parent_contacts: [{
    name: 'Joko Santoso',
    occupation: 'Pensiun',
    address: 'Jalan Komplek Hankam No. 21 RT 05 RW 11 Kebayoran Lama Grogol Selatan',
    province: 'DKI Jakarta',
    regency_city: 'Kota Madya Jakarta Selatan',
    zipcode: 12220,
    phone: {
      home: '0217256229',
      mobile: '0811801234'
    }
  }],
  educations: [
    {
    level: 'SD',
    school_name: 'Al-Azhar Kemandoran',
    province: 'DKI Jakarta',
    graduate_year: 2004,
    notes: '-'
    },
    {
      level: 'SMP',
      school_name: 'Al-Azhar Kemandoran',
      province: 'DKI Jakarta',
      graduate_year: 2007,
      notes: '-'
    },
    {
      level: 'sma',
      school_name: 'SMAN 82',
      province: 'DKI Jakarta',
      graduate_year: 2010,
      notes: '-'
    }
  ],
  experiences: [{
    level:'siaga',
    gudep_number: '99999',
    base: 'Lorem Ipsum',
    year: '2010',
    achievement: [{
      sub_level: 'mula',
      year: 2000,
      number_of_badges:30
    },
    {
      sub_level: 'bantu',
      year: 2002,
      number_of_badges:10
    }],
    activities: [{
      name: 'Leadership',
      year: 2009
    },
    {
      name: 'Ethics',
      year: 2008
    }]
  },
  {
    level:'penggalang',
    gudep_number: '99999',
    base: 'Lorem Ipsum',
    year: '2011',
    achievement: [{
      sub_level: 'ramu',
      year: 2009,
      number_of_badges: 10,
    },
    {
      sub_level: 'rakit',
      year: 2011,
      number_of_badges: 12,
    }],
    activities: [{
      name: 'Leadership 2',
      year: 2010
    },
    {
      name: 'Ethics 2',
      year: 2011
    }],
    badges: [{
      type:{
        name: 'Masak'
      },
      category: 'purwa'
    },
    {
      type:{
        name: 'Pemetaan'
      },
      category: 'madya'
    },
      {
        type:{
          name: 'Mencuci'
        },
        category: 'utama'
      }]
  }],
  courses: [{
    name: 'KMD',
    year: 2010,
    province: 'DKI Jakarta',
    notes: '-'
  },
    {
      name: 'KML',
      year: 2011,
      province: 'Yogyakarta',
      notes: '-'
    },
    {
      name: 'KPD',
      year: 2010,
      province: 'Cibubur',
      notes: '-'
    },
    {
      name: 'KPL',
      year: 2012,
      province: 'DKI Jakarta',
      notes: '-'
    }],
  external_courses: [{
    name: 'Leadership',
    year: 2013,
    province: 'Sumatera Barat',
    notes: '-'
  }]
}

const data = {
  scout : scout,
};

let pageInstance = new ParticipantViewPage(selectors, data);
pageInstance.setup();
