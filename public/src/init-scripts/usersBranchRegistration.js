import BranchRegistrationPage from '../pages/users/BranchRegistrationPage';

const selectors = {
    // Yang dideklarasikan di jade ya
    branch_registration                   : '#branch_registration'
};

let pageInstance = new BranchRegistrationPage(selectors);
pageInstance.setup();
