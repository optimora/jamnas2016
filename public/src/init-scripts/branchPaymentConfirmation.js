import BranchPaymentConfirmation from '../pages/users/BranchPaymentConfirmation';

const selectors = {
  // Yang dideklarasikan di jade ya
  header                    : '#headerAddParticipantsKwarda',
  branchPayment             : '#branchPayment'
};

let pageInstance = new BranchPaymentConfirmation(selectors);
pageInstance.setup();
