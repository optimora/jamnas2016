import EditPage from '../pages/scouts/EditPage';

const selectors = {
  header: '#header',
  main: '#scoutForm'
};

let pageInstance = new EditPage(selectors);
pageInstance.setup();