import AddNewBranchParticipantPage from '../pages/users/AddNewBranchParticipantPage';

const selectors = {
  // Yang dideklarasikan di jade ya
  header                    : '#HeaderAddParticipantsKwarda',
  participantsBasicForm     : '#participantsBasicForm',
  participantsAddressForm   : '#participantsAddressForm',
  participantsParentForm    : '#participantsParentForm',
  participantsEducationForm : '#participantsEducationForm',
  participantsScoutingForm  : '#participantsScoutingForm',
  participantsMedicalForm   : '#participantsMedicalForm',
  participantsCourseForm    : '#participantsCourseForm',
  participantsDocumentForm  : '#participantsDocumentForm'
};

let pageInstance = new AddNewBranchParticipantPage(selectors);
pageInstance.setup();
