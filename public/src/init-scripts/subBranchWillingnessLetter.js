import SubBranchWillingnessLetter from '../pages/users/SubBranchWillingnessLetter';

const selectors = {
  // Yang dideklarasikan di jade ya
  header                    : '#headerAddParticipantsKwarda',
  kwarcabWillingness        : '#kwarcabWillingness'
};

let pageInstance = new SubBranchWillingnessLetter(selectors);
pageInstance.setup();
