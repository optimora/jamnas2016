import SubBranchParticipants from '../pages/users/SubBranchParticipants';

const selectors = {
  // Yang dideklarasikan di jade ya
  header                        : '#header',
  subbranch_participant_list    : '#subbranch_participant_list'
};

let pageInstance = new SubBranchParticipants(selectors);
pageInstance.setup();
