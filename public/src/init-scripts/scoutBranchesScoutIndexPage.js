import ScoutIndexPage from '../pages/scout_branches/ScoutIndexPage';

const selectors = {
  header: '#header',
  main: '#scoutsIndex'
};

let pageInstance = new ScoutIndexPage(selectors);
pageInstance.setup();