import LoginPage from '../pages/users/LoginPage';

const selectors = {
  main: '#loginFormContainer'
};

let loginPageInstance = new LoginPage(selectors);
loginPageInstance.setup();
