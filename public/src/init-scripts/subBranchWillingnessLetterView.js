import SubBranchWillingnessLetterView from '../pages/users/SubBranchWillingnessLetterView';

const selectors = {
  // Yang dideklarasikan di jade ya
  header                    : '#headerAddParticipantsKwarda',
  kwarcabWillingness        : '#kwarcabWillingness'
};

let pageInstance = new SubBranchWillingnessLetterView(selectors);
pageInstance.setup();
