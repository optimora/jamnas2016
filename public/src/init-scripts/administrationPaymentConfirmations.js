import PaymentConfirmationPage from '../pages/administration/PaymentConfirmationPage';

const selectors = {
  header: '#header',
  main: '#main'
};

let pageInstance = new PaymentConfirmationPage(selectors);
pageInstance.setup();