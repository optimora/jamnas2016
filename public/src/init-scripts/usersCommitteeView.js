import CommitteeViewPage from '../pages/users/CommitteeViewPage';

const selectors = {
    // Yang dideklarasikan di jade ya
    committee_view: '#committee_view'
};

let scout = {
    user: {
        name: "Sri Andhini Saraswati"
    },
    gender: 'Female',
    birthplace: "Jakarta",
    birthday: "1992-11-22T01:00:00+01:00",
    religion: 'Islam',
    blood_type: 'A',
    nta: '210000261',
    branch: {
        name: 'DKI Jakarta'
    },
    subbranch: {name: 'Kota Jakarta Selatan'},
    type: 'committee',
    subtype: 'committee-support',
    contact: [{
        name: 'Sri Andhini Saraswati',
        occupation: 'Pelajar',
        address: 'Jalan Komplek Hankam No. 21 RT 05 RW 11 Kebayoran Lama Grogol Selatan',
        province: 'DKI Jakarta',
        regency_city: 'Kota Madya Jakarta Selatan',
        zipcode: 12220,
        phone: {
            home: '0217256229',
            mobile: '085693636691'
        }
    }],
    parent_contacts: [{
        name: 'Joko Santoso',
        occupation: 'Pensiun',
        address: 'Jalan Komplek Hankam No. 21 RT 05 RW 11 Kebayoran Lama Grogol Selatan',
        province: 'DKI Jakarta',
        regency_city: 'Kota Madya Jakarta Selatan',
        zipcode: 12220,
        phone: {
            home: '0217256229',
            mobile: '0811801234'
        }
    }],
    educations: [{
        level: 'SD',
        school_name: 'Al-Azhar Kemandoran',
        province: 'DKI Jakarta',
        graduate_year: 2004,
        notes: '-'
    },
        {
            level: 'SMP',
            school_name: 'Al-Azhar Kemandoran',
            province: 'DKI Jakarta',
            graduate_year: 2007,
            notes: '-'
        },
        {
            level: 'SMA',
            school_name: 'SMAN 82',
            province: 'DKI Jakarta',
            graduate_year: 2010,
            notes: '-'
        }],
    experiences: [{
        level: 'Siaga',
        gudep_number: '99999',
        base: 'Lorem Ipsum',
        year: '2010'
    },
        {
            level: 'Penggalang',
            gudep_number: '99999',
            base: 'Lorem Ipsum',
            year: '2011'
        }],
    courses: [{
        name: 'KMD',
        year: 2010,
        province: 'DKI Jakarta',
        notes: '-'
    },
        {
            name: 'KML',
            year: 2011,
            province: 'Yogyakarta',
            notes: '-'
        },
        {
            name: 'KPD',
            year: 2010,
            province: 'Cibubur',
            notes: '-'
        },
        {
            name: 'KPL',
            year: 2012,
            province: 'DKI Jakarta',
            notes: '-'
        }],
    external_courses: [{
        name: 'Leadership',
        year: 2013,
        province: 'Sumatera Barat',
        notes: '-'
    }]
}

const data = {
    scout: scout,
};

let pageInstance = new CommitteeViewPage(selectors, data);
pageInstance.setup();
