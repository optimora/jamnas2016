import UploadTestPage from '../pages/users/UploadTestPage';

const selectors = {
  main: '#container'
};

let pageInstance = new UploadTestPage(selectors);
pageInstance.setup();
