import BranchParticipants from '../pages/users/BranchParticipants';

const selectors = {
  // Yang dideklarasikan di jade ya
  header                    : '#header',
  branch_participant_list   : '#branch_participant_list'
};

let pageInstance = new BranchParticipants(selectors);
pageInstance.setup();
