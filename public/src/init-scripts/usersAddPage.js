import AddPage from '../pages/users/AddPage';

let selectors = {
  main: '#usersAddPageContainer'
};

let addPageInstance = new AddPage(selectors);
addPageInstance.setup();
