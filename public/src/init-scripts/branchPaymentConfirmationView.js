import BranchPaymentConfirmationView from '../pages/users/BranchPaymentConfirmationView';

const selectors = {
  // Yang dideklarasikan di jade ya
  header                    : '#headerAddParticipantsKwarda',
  branchPayment             : '#branchPayment'
};

let pageInstance = new BranchPaymentConfirmationView(selectors);
pageInstance.setup();
