import CommitteeListPage from '../pages/users/CommitteeListPage';

const selectors = {
  // Yang dideklarasikan di jade ya
  committee_list                    : '#committee_list'
};

let pageInstance = new CommitteeListPage(selectors);
pageInstance.setup();
