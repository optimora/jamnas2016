import SubBranchRegistrationViewPage from '../pages/users/SubBranchRegistrationViewPage';

const selectors = {
    // Yang dideklarasikan di jade ya
    subbranch_registration_view                    : '#subbranch_registration_view'
};

let pageInstance = new SubBranchRegistrationViewPage(selectors);
pageInstance.setup();
