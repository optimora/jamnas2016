import AddPage from '../pages/scouts/AddPage';

const selectors = {
  header: '#header',
  main: '#scoutForm'
};

let pageInstance = new AddPage(selectors);
pageInstance.setup();
