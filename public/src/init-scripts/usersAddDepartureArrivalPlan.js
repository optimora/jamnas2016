import AddDepartureArrivalPlanPage from '../pages/users/AddDepartureArrivalPlanPage';

const selectors = {
    // Yang dideklarasikan di jade ya
    add_departure_arrival_plan                    : '#add_departure_arrival_plan'
};

let pageInstance = new AddDepartureArrivalPlanPage(selectors);
pageInstance.setup();
