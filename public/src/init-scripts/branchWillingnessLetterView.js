import BranchWillingnessLetterView from '../pages/users/BranchWillingnessLetterView';

const selectors = {
  // Yang dideklarasikan di jade ya
  header                    : '#headerAddParticipantsKwarda',
  kwardaWillingness         : '#kwardaWillingness'
};

let pageInstance = new BranchWillingnessLetterView(selectors);
pageInstance.setup();
